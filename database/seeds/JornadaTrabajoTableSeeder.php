<?php

use App\JornadaTrabajo;
use Illuminate\Database\Seeder;

class JornadaTrabajoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $jornadas = ['Diurna (Mañana y Tarde)','Diurna (Mañana)','Diurna (Tarde)','Diurna y/o Nocturna','Nocturna'];

        foreach($jornadas as $jornada){
            JornadaTrabajo::create([
                'jorn_trab_descripcion' => strtoupper($jornada),
                'jorn_trab_estado' => true
            ]);
        }
    }
}
