<?php

use App\systemAdmin;
use Illuminate\Database\Seeder;

class SystemAdminTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        systemAdmin::create([
            'adm_identificacion'=>'1234',
            'adm_nombre'=>'ADMIN AVATAR',
            'adm_email'=>'admin@avatar.com',
            'adm_estado'=>true
        ]);
    }
}
