<?php

use App\menu;
use Illuminate\Database\Seeder;

class MenuTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        menu::truncate();

        $menus = [
                    [
                        'descripcion' => 'Datos Maestros',
                        'icon' => 'icon-file-text',
                        'rol_id' => 1
                    ],
                    [
                        'descripcion' => 'Datos Maestros',
                        'icon' => 'icon-file-text',
                        'rol_id' => 2
                    ],
                    [
                        'descripcion' => 'Datos Maestros',
                        'icon' => 'icon-file-text',
                        'rol_id' => 3
                    ],
                    [
                        'descripcion' => 'Mi Información',
                        'icon' => 'icon-info',
                        'rol_id' => 3
                    ],
                    [
                        'descripcion' => 'Estudiantes',
                        'icon' => 'icon-group',
                        'rol_id' => 3
                    ],
                    [
                        'descripcion' => 'Gestion de Practicas',
                        'icon' => 'icon-bars',
                        'url' => 'gestion-practicas.index',
                        'rol_id' => 3
                    ]
                ];

        foreach($menus as $menu){
            menu::create([
                'descripcion_menu' => $menu['descripcion'],
                'icon_menu' => $menu['icon'],
                'url_menu' => $menu['url'] ?? null,
                'rol_id' => $menu['rol_id']
            ]);
        }
    }
}
