<?php

Use App\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::truncate();

        $users = [
            [
                'identificacion' => '1234',
                'ciudad_exp' => 1,
                'password' => bcrypt('1234'),
                'nombre' => 'Administrador',
                'tercero' => 1,
                'rol_id' => 1,
                'cargo' => 1,
                'email' => 'admin@avatar.co',
                'telefono' => '3002900390',
                'cumpleanos' => '1980-07-31'
            ],
            [
                'identificacion' => '5678',
                'ciudad_exp' => 1,
                'password' => bcrypt('1234'),
                'nombre' => 'Coordinador Inca',
                'tercero' => 1,
                'rol_id' => 2,
                'cargo' => 3,
                'email' => 'coordinador@mail.co',
                'telefono' => '3002900390',
                'cumpleanos' => '1980-07-31'
            ],
            [
                'identificacion' => '1212',
                'ciudad_exp' => 1,
                'password' => bcrypt('1234'),
                'nombre' => 'Juan Perez',
                'tercero' => '1',
                'rol_id' => 3,
                'cargo' => 4,
                'email' => 'recursoshumanos@mail.co',
                'telefono' => '3002900390',
                'cumpleanos' => '1980-07-31'
            ],
            [
                'identificacion' => '5567',
                'ciudad_exp' => 1,
                'password' => bcrypt('1234'),
                'nombre' => 'Pedro Perez',
                'tercero' =>2,
                'rol_id' => 2,
                'cargo' => 4,
                'email' => 'recursoshumanos@mail.co',
                'telefono' => '3002900390',
                'cumpleanos' => '1980-07-31'
            ]
        ];

        foreach($users as $user){
            User::create([
                'identificacion' => $user['identificacion'],
                'ciudad_id_expedicion' => $user['ciudad_exp'],
                'password' => $user['password'],
                'user_nombre' => strtoupper($user['nombre']),
                'tercero_id'=>$user['tercero'],
                'rol_id' => $user['rol_id'],
                'cargo_id' => strtoupper($user['cargo']),
                'user_email' => $user['email'],
                'user_telefono' => $user['telefono'],
                'user_cumpleanos' => $user['cumpleanos']
            ]);
        }
    }
}
