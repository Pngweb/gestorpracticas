<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(RolesTableSeeder::class);
        $this->call(SystemAdminTableSeeder::class);
        $this->call(MenuTableSeeder::class);
        $this->call(submenuTableSeeder::class);
        $this->call(OrganizacionJuridicaTableSeeder::class);
        $this->call(SectorProductivoTableSeeder::class);
        $this->call(JornadaTrabajoTableSeeder::class);
        $this->call(DepartamentosTableSeeder::class);
        $this->call(CiudadTableSeeder::class);
        $this->call(InstitucionTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(GruposSanguineosTableSeeder::class);
        $this->call(EmpresaTableSeeder::class);
        $this->call(CargosTableSeeder::class);
        $this->call(SistemasMensajeriaTableSeeder::class);
        $this->call(DocumentosLegalesTableSeeder::class);
        $this->call(AreasTableSeeder::class);
        $this->call(ProgramasTableSeeder::class);
    }
}
