<?php

use Illuminate\Database\Seeder;
use App\Empresa;

class EmpresaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $empresas = [
        	[
        		'nombre' => 'EMPRESA 1',
        		'web' => 'empresa1.com',
        		'org_jur' => 1,
        		'sec_prod' => 4,
        		'jorn_trab' => 3,
        		'obj_social' => 'EMPRESA 1 PRUEBA',
        		'no_empleados' => 20,
                'conglomerado' => false
        	],
        	[
        		'nombre' => 'EMPRESA 2',
        		'web' => 'empresa2.com',
        		'org_jur' => 5,
        		'sec_prod' => 3,
        		'jorn_trab' => 1,
        		'obj_social' => 'EMPRESA 2 PRUEBA',
        		'no_empleados' => 30,
                'conglomerado' => true
        	],
        	[
        		'nombre' => 'EMPRESA 3',
        		'web' => 'empresa3.com',
        		'org_jur' => 1,
        		'sec_prod' => 6,
        		'jorn_trab' => 2,
        		'obj_social' => 'EMPRESA 3 PRUEBA',
        		'no_empleados' => 10,
                'conglomerado' => false
        	],
        	[
        		'nombre' => 'EMPRESA 4',
        		'web' => 'empresa4.com',
        		'org_jur' => 1,
        		'sec_prod' => 7,
        		'jorn_trab' => 4,
        		'obj_social' => 'EMPRESA 4 PRUEBA',
        		'no_empleados' => 20,
                'conglomerado' => true
        	],
        	[
        		'nombre' => 'EMPRESA 5',
        		'web' => 'empresa5.com',
        		'org_jur' => 1,
        		'sec_prod' => 1,
        		'jorn_trab' => 1,
        		'obj_social' => 'EMPRESA 5 PRUEBA',
        		'no_empleados' => 40,
                'conglomerado' => true
        	]
        ];

        foreach ($empresas as $empresa) {
        	Empresa::create([
        		'empresa_nombre' => $empresa['nombre'],
        		'empresa_nit' => rand(10000000,99999999),
        		'empresa_web' => $empresa['web'],
        		'empresa_mensajeria' => true,
        		'org_jur_id' => $empresa['org_jur'],
        		'sec_prod_id' => $empresa['sec_prod'],
        		'jorn_trab_id' => $empresa['jorn_trab'],
        		'empresa_objeto_social' => $empresa['obj_social'],
        		'empresa_no_empleados' => $empresa['no_empleados'],
                'empresa_conglomerado' => $empresa['conglomerado'],
        		'created_user' => 1
        	]);
        }
    }
}
