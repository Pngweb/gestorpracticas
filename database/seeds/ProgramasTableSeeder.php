<?php

use Illuminate\Database\Seeder;
use App\Programas;

class ProgramasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $programas = [
        	[
        		'descripcion' => 'TECNICO EN DESARROLLO DE SOFTWARE',
        		'area' => 1,
        		'institucion' => 1,
        	],
        	[
        		'descripcion' => 'TECNICO EN REPARACION DE COMPUTADORES',
        		'area' => 1,
        		'institucion' => 1,
        	],
        	[
        		'descripcion' => 'AUXILIAR DE ENFERMERIA',
        		'area' => 2,
        		'institucion' => 1,
        	],
        	[
        		'descripcion' => 'AUXILIAR DE FARMACIA',
        		'area' => 2,
        		'institucion' => 1,
        	],
        	[
        		'descripcion' => 'SECRETARIADO COMERCIAL',
        		'area' => 3,
        		'institucion' => 1,
        	],
            [
                'descripcion' => 'TECNICO EN PROGRAMACION',
                'area' => 1,
                'institucion' => 3,
            ]
        ];

        DB::table('programas')->truncate();

        foreach ($programas as $programa) {
        	Programas::create([
        		'programa_descripcion' => $programa['descripcion'],
        		'area_id' => $programa['area'],
        		'institucion_id' => $programa['institucion'],
        		'programa_estado' => true
        	]);
        }
    }
}
