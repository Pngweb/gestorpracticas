<?php

use Illuminate\Database\Seeder;
use App\DocumentosLegales;

class DocumentosLegalesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $documentos = ['CAMARA DE COMERCIO', 'REGISTRO DE VINCULACION SENA', 'CONVENIO INTER-INSTITUCIONAL'];

        foreach ($documentos as $documento) {
        	DocumentosLegales::create([
        		'documento_descripcion' => $documento,
        		'documento_estado' => true
        	]);
        }
    }
}
