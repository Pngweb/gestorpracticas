<?php

use Illuminate\Database\Seeder;
use App\Ciudad;

class CiudadTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $ciudades = [
            [
                'nombre' => 'BARRANQUILLA',
                'depto' => 1
            ],
            [
                'nombre' => 'SOLEDAD',
                'depto' => 1
            ],
            [
                'nombre' => 'CARTAGENA',
                'depto' => 2
            ],
            [
                'nombre' => 'BOGOTA',
                'depto' => 5
            ],
            [
                'nombre' => 'MEDELLIN',
                'depto' => 3
            ],
            [
                'nombre' => 'BUCARAMANGA',
                'depto' => 4
            ],
            [
                'nombre' => 'CALI',
                'depto' => 6
            ]
        ];

        foreach($ciudades as $ciudad){
            Ciudad::create([
                'ciudad_descripcion' => $ciudad['nombre'],
                'depto_id' => $ciudad['depto'],
                'ciudad_estado' => true
            ]);
        }
    }
}
