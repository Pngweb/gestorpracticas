<?php

use App\submenu;
use Illuminate\Database\Seeder;

class submenuTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $smenus = [
            [
                'descripcion' => 'Intituciones',
                'url' => 'institucion.index',
                'menu' => 1
            ],
            [
                'descripcion' => 'Empresas',
                'url' => 'empresas.index',
                'menu' => 1
            ],
            [
                'descripcion' => 'Estudiantes',
                'url' => 'estudiantes.index',
                'menu' => 1
            ],
            [
                'descripcion' => 'Estudiantes',
                'url' => 'estudiantes.index',
                'menu' => 2
            ],
            [
                'descripcion' => 'Empresas',
                'url' => 'empresas.index',
                'menu' => 2
            ],
            [
                'descripcion' => 'Convenios',
                'url' => 'convenios.index',
                'menu' => 2
            ],
            [
                'descripcion' => 'Convenios',
                'url' => 'convenios.index',
                'menu' => 3
            ],
            [
                'descripcion' => 'Mi Perfil',
                'url' => 'miPerfil.index',
                'menu' => 4
            ],
            [
                'descripcion' => 'Mensajeria',
                'url' => 'mensajeria.index',
                'menu' => 4
            ],
            [
                'descripcion' => 'Dependencias',
                'url' => 'dependencias.index',
                'menu' => 4
            ],
            [
                'descripcion' => 'Sucursales',
                'url' => 'sucursales.index',
                'menu' => 4
            ],
            [
                'descripcion' => 'Cupos de Practicas',
                'url' => 'cupos-practicas.index',
                'menu' => 4
            ],
            [
                'descripcion' => 'Funcionarios',
                'url' => 'funcionarios.index',
                'menu' => 4
            ],
            [
                'descripcion' => 'Documentos Legales',
                'url' => 'documentos.index',
                'menu' => 4
            ],
            [
                'descripcion' => 'Disponibles',
                'url' => 'estudiantes-disponibles.index',
                'menu' => 5
            ],
            [
                'descripcion' => 'Preseleccionados',
                'url' => 'estudiantes-preseleccionados.index',
                'menu' => 5
            ],
            [
                'descripcion' => 'Seleccionados',
                'url' => 'estudiantes-seleccionados.index',
                'menu' => 5
            ]
        ];

DB::table('submenus')->truncate();

foreach($smenus as $smenu){
    submenu::create([
        'descripcion_smenu' => $smenu['descripcion'],
        'url_smenu' => $smenu['url'],
        'id_menu' => $smenu['menu']
    ]);
}
    }
}
