<?php

use Illuminate\Database\Seeder;
use App\GruposSanguineos;

class GruposSanguineosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $grupos = ['A+','A-','B+','B-','AB+','AB-','O+','O-'];

        foreach($grupos as $grupo){
            GruposSanguineos::create([
                'grupo_sanguineo_descripcion' => $grupo
            ]);
        }
    }
}
