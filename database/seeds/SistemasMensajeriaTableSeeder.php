<?php

use Illuminate\Database\Seeder;
use App\SistemasMensajeria;

class SistemasMensajeriaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $mensajeria = ['SKYPE FOR BUSSINES', 'MICROSOFT TEAMS', 'GOOGLE HANGOUTS', 'ZOOM', 'WHATSAPP', 'OTRO'];

        foreach ($mensajeria as $men) {
	        SistemasMensajeria::create([
	        	'mensajeria_descripcion' => $men,
	        	'mensajeria_estado' => true
	        ]);
        }
    }
}
