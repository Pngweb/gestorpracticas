<?php

use Illuminate\Database\Seeder;
use App\Area;

class AreasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $areas = ['INFORMATICA','SALUD','COMERCIAL','INDUSTRIAL','SERVICIO'];

        foreach ($areas as $area) {
        	Area::create([
        		'area_descripcion' => $area,
        		'area_estado' => true
        	]);
        }
    }
}
