<?php

use Illuminate\Database\Seeder;
use App\Cargos;

class CargosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $cargos = [
            [
                'cargo' => 'administrador avatar',
                'visible' => false
            ],
            [
                'cargo' => 'gerencia',
                'visible' => true
            ],
            [
                'cargo' => 'coordinador',
                'visible' => true
            ],
            [
                'cargo' => 'recursos humanos',
                'visible' => true
            ],
            [
                'cargo' => 'supervisor',
                'visible' => true
            ],
            [
                'cargo' => 'Estudiante',
                'visible' => false
            ]
        ];

        foreach($cargos as $cargo){
            Cargos::create([
                'cargo_descripcion' => strtoupper($cargo['cargo']),
                'cargo_estado' => true,
                'cargo_visible' => $cargo['visible']
            ]);
        }
    }
}
