<?php

use Illuminate\Database\Seeder;
use App\EtapaEstudiante;

class EtapasEstudiantesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $etapas = ['PATROCINIO LECTIVO','PRODUCTIVA', 'GRUPO CERRADO DE FORMACION'];

        EtapaEstudiante::truncate();
        foreach ($etapas as $etapa) {
        	EtapaEstudiante::create([
        		'etapa_descripcion' => $etapa,
        		'etapa_estado' => true 
        	]);
        }
    }
}
