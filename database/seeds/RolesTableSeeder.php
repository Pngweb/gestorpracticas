<?php

use App\roles;
use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // roles::truncate();

        $roles = ['Admin','Coordinador','Empresa','Estudiante'];

        foreach($roles as $rol){
            roles::create([
                'rol_descripcion' => $rol,
                'rol_estado'=>true
                ]);
        }
    }
}
