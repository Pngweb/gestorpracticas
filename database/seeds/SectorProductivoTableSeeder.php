<?php

use App\SectorProductivo;
use Illuminate\Database\Seeder;

class SectorProductivoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        $sectores = ['Agropecuaria','Comercio','Industria','Manufactura','Minería','Otros','Servicios'];

        foreach($sectores as $sector){
            SectorProductivo::create([
                'sec_prod_descripcion' => strtoupper($sector),
                'sec_prod_estado' => true
            ]);
        }

    }
}
