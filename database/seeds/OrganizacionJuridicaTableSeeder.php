<?php

use App\OrganizacionJuridica;
use Illuminate\Database\Seeder;

class OrganizacionJuridicaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $organizaciones = ['Cooperativa','Corporación','Fundación','Oficial','Privada','S.A.S. Sociedad por Acciones Simplificada','Sociedad Anonima','Sociedad Comandita','Sociedad Ltda'];

        foreach($organizaciones as $organizacion){
            OrganizacionJuridica::create([
                'org_jur_descripcion' => strtoupper($organizacion),
                'org_jur_estado' => true
            ]);
        }
    }
}
