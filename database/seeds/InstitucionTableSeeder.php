<?php

use Illuminate\Database\Seeder;
use App\Institucion;

class InstitucionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $instituciones = [
            [
                'nombre' => 'CENTRO INCA',
                'direccion' => 'Cl. 57 #46-103',
                'telefono' => '300948859',
                'email' => 'admin@inca.com.co',
                'ciudad' => 1
            ],
            [
                'nombre' => 'INSTITUCION 1',
                'direccion' => 'CALLE FALSA 123',
                'telefono' => '302983990',
                'email' => 'admin@institucion1.com',
                'ciudad' => 2
            ],
            [
                'nombre' => 'INSTITUCION 2',
                'direccion' => 'CALLE 1234',
                'telefono' => '304995890',
                'email' => 'admin@institucion2.com.co',
                'ciudad' => 3
            ],
            [
                'nombre' => 'INSTITUCION 3',
                'direccion' => 'CALLE 56 CARRERA 20',
                'telefono' => '32084739',
                'email' => 'admin@institucion3.com.co',
                'ciudad' => 1
            ],
            [
                'nombre' => 'INSTITUCION 4',
                'direccion' => 'CALLE 25 43',
                'telefono' => '348930094',
                'email' => 'admin@institucion4.com.co',
                'ciudad' => 2
            ],
        ];

        foreach ($instituciones as $intitucion) {
            Institucion::create([
                'institucion_nombre'=> $intitucion['nombre'],
                'institucion_identificacion'=> rand(10000000,99999999),
                'institucion_direccion'=> $intitucion['direccion'],
                'institucion_telefono'=> $intitucion['telefono'],
                'institucion_email'=> $intitucion['email'],
                'ciudad_id'=> $intitucion['ciudad'],
                'institucion_estado'=>true,
                'created_user' => 1
            ]);
        }
    }
}
