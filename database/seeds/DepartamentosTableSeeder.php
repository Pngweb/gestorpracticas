<?php

use Illuminate\Database\Seeder;
use App\Departamentos;

class DepartamentosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $departamentos = ['ATLANTICO','BOLIVAR','ANTIOQUIA','SANTANDER','CUNDINAMARCA','VALLE DEL CAUCA'];

        foreach($departamentos as $deptos){
            Departamentos::create([
                'depto_descripcion' => $deptos,
                'depto_estado' => true
            ]);
        }
    }
}
