<?php

use Illuminate\Database\Seeder;
use App\EstudiantePrograma;

class EstudianteProgramaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $est_progs = [
        	[
        		'estudiante' => 3,
        		'programa' => 1
        	],
        	[
        		'estudiante' => 5,
        		'programa' => 4
        	],
            [
                'estudiante' => 6,
                'programa' => 1
            ]
        ];

        EstudiantePrograma::truncate();

        foreach ($est_progs as $est_prog) {
        	EstudiantePrograma::create([
        		'estudiante_id' => $est_prog['estudiante'],
        		'programa_id' => $est_prog['programa']
        	]);
        }
    }
}
