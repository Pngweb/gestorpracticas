<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSucursalesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('empresa_sucursal', function (Blueprint $table) {
            $table->bigIncrements('sucursal_id');
            $table->string('sucursal_descripcion');
            $table->string('sucursal_telefono');
            $table->string('sucursal_direccion');
            $table->bigInteger('ciudad_id')->unsigned();
            $table->foreign('ciudad_id')->references('ciudad_id')->on('ciudad');
            $table->bigInteger('empresa_id')->unsigned();
            $table->foreign('empresa_id')->references('empresa_id')->on('empresas');
            $table->boolean('sucursal_estado');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('empresa_sucursal');
    }
}
