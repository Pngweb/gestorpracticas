<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmpresaMensajeriaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('empresa_mensajeria', function (Blueprint $table) {
            $table->bigIncrements('empresa_mensajeria_id');
            $table->bigInteger('empresa_id')->unsigned();
            $table->foreign('empresa_id')->references('empresa_id')->on('empresas');
            $table->bigInteger('mensajeria_id')->unsigned();
            $table->foreign('mensajeria_id')->references('mensajeria_id')->on('sistemas_mensajeria');
            $table->string('mensajeria_usuario');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('empresa_mensajeria');
    }
}
