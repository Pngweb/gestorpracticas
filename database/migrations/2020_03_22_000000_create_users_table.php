<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('identificacion')->unique();
            $table->bigInteger('ciudad_id_expedicion')->unsigned();
            $table->foreign('ciudad_id_expedicion')->references('ciudad_id')->on('ciudad');
            $table->string('password');
            $table->string('user_nombre');
            $table->bigInteger('tercero_id')->unsigned();
            $table->bigInteger('rol_id')->unsigned();
            $table->foreign('rol_id')->references('rol_id')->on('roles_users');
            $table->bigInteger('cargo_id');
            $table->string('user_email');
            $table->string('user_telefono');
            $table->date('user_cumpleanos');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
