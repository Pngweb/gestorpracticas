<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSolicitudCupoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('solicitud_cupo', function (Blueprint $table) {
            $table->bigIncrements('solicitud_cupo_id');
            $table->bigInteger('estudiante_id')->unsigned();
            $table->foreign('estudiante_id')->references('estudiante_id')->on('estudiantes');
            $table->bigInteger('empresa_id')->unsigned();
            $table->foreign('empresa_id')->references('empresa_id')->on('empresas');
            $table->bigInteger('cupo_id')->unsigned();
            $table->foreign('cupo_id')->references('cupo_id')->on('cupos_practicas');
            $table->date('solicitud_fecha_inicio');
            $table->date('solicitud_fecha_culminacion');
            $table->text('solicitud_tipo_acuerdo');
            $table->enum('solicitud_estado',['pendiente', 'aceptada', 'rechazada']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('solicitud_cupo');
    }
}
