<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDependenciasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('empresa_dependencias', function (Blueprint $table) {
            $table->bigIncrements('dependencia_id');
            $table->string('dependencia_descripcion');
            $table->boolean('dependencia_estado');
            $table->bigInteger('empresa_id')->unsigned();
            $table->foreign('empresa_id')->references('empresa_id')->on('empresas');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('empresa_dependencias');
    }
}
