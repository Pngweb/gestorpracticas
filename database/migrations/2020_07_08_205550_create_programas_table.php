<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProgramasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('programas', function (Blueprint $table) {
            $table->bigIncrements('programa_id');
            $table->string('programa_descripcion');
            $table->bigInteger('area_id')->unsigned();
            $table->foreign('area_id')->references('area_id')->on('areas');
            $table->bigInteger('institucion_id')->unsigned();
            $table->foreign('institucion_id')->references('institucion_id')->on('institucion');
            $table->boolean('programa_estado');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('programas');
    }
}
