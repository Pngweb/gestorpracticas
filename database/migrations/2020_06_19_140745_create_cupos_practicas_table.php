<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCuposPracticasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cupos_practicas', function (Blueprint $table) {
            $table->bigIncrements('cupo_id');
            $table->bigInteger('dependencia_id')->unsigned();
            $table->bigInteger('sucursal_id')->unsigned();
            $table->bigInteger('cargo_id')->unsigned();
            $table->bigInteger('maestro_id')->unsigned();
            $table->text('cupo_competencias');
            $table->bigInteger('empresa_id')->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cupos_practicas');
    }
}
