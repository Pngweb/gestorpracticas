<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmpresasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('empresas', function (Blueprint $table) {
            $table->bigIncrements('empresa_id');
            $table->string('empresa_nombre');
            $table->string('empresa_nit');
            $table->string('empresa_logo')->nullable();
            $table->string('empresa_web');
            $table->boolean('empresa_mensajeria');
            $table->bigInteger('org_jur_id')->unsigned()->nullable();
            $table->foreign('org_jur_id')->references('org_jur_id')->on('organizacion_juridica');
            $table->bigInteger('sec_prod_id')->unsigned()->nullable();
            $table->foreign('sec_prod_id')->references('sec_prod_id')->on('sector_productivo');
            $table->bigInteger('jorn_trab_id')->unsigned()->nullable();
            $table->foreign('jorn_trab_id')->references('jorn_trab_id')->on('jornadas_trabajo');
            $table->string('empresa_objeto_social');
            $table->string('empresa_no_empleados');
            $table->boolean('empresa_conglomerado');
            $table->bigInteger('created_user');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('empresas');
    }
}
