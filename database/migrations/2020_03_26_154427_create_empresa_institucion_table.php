<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmpresaInstitucionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('empresa_institucion', function (Blueprint $table) {
            $table->bigIncrements('empresa_institucion_id');
            $table->bigInteger('empresa_id')->unsigned();
            $table->bigInteger('institucion_id')->unsigned();
            $table->bigInteger('empresa_institucion_estado')->unsigned();
            $table->bigInteger('created_user');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('empresa_institucion');
    }
}
