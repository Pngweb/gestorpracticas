<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEstudiantesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('estudiantes', function (Blueprint $table) {
            $table->bigIncrements('estudiante_id');
            $table->string('estudiante_identificacion');
            $table->string('estudiante_nombres');
            $table->string('estudiante_apellidos');
            $table->string('estudiante_foto')->nullable();
            $table->char('estudiante_sexo',1);
            $table->date('estudiante_fecha_nacimiento');
            $table->string('estudiante_direccion');
            $table->string('ciudad_id');
            $table->string('estudiante_email');
            $table->string('estudiante_telefono');
            $table->bigInteger('grupo_sanguineo_id')->unsigned();
            $table->foreign('grupo_sanguineo_id')->references('grupo_sanguineo_id')->on('grupos_sanguineos');
            $table->bigInteger('etapa_id');
            $table->foreign('etapa_id')->references('etapa_id')->on('estapas_estudiante');
            $table->boolean('estudiante_estado');
            $table->bigInteger('created_user');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('estudiantes');
    }
}
