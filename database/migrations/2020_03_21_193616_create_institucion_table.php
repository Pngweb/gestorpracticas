<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInstitucionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('institucion', function (Blueprint $table) {
            $table->bigIncrements('institucion_id');
            $table->string('institucion_identificacion');
            $table->string('institucion_logo')->nullable();
            $table->string('institucion_nombre');
            $table->string('institucion_direccion');
            $table->string('institucion_telefono');
            $table->string('institucion_email');
            $table->bigInteger('ciudad_id')->unsigned();
            $table->foreign('ciudad_id')->references('ciudad_id')->on('ciudad');
            $table->boolean('institucion_estado');
            $table->bigInteger('created_user');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('institucion');
    }
}
