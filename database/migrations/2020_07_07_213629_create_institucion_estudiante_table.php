<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInstitucionEstudianteTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('institucion_estudiante', function (Blueprint $table) {
            $table->bigIncrements('inst_est_id');
            $table->bigInteger('institucion_id')->unsigned();
            $table->foreign('institucion_id')->references('institucion_id')->on('institucion');
            $table->bigInteger('estudiante_id')->unsigned();
            $table->foreign('estudiante_id')->references('estudiante_id')->on('estudiantes');
            $table->date('inst_est_fecha_inicio');
            $table->boolean('inst_est_estado');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('institucion_estudiante');
    }
}
