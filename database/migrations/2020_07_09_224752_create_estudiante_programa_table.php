<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEstudianteProgramaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('estudiante_programa', function (Blueprint $table) {
            $table->bigIncrements('est_prog_id');
            $table->bigInteger('estudiante_id')->unsigned();
            $table->foreign('estudiante_id')->references('estudiante_id')->on('estudiantes');
            $table->bigInteger('programa_id')->unsigned();
            $table->foreign('programa_id')->references('programa_id')->on('programas');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('estudiante_programa');
    }
}
