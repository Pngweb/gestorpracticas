<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use App\Empresa;
use App\Departamentos;
use App\Institucion;
use App\Estudiante;
use App\EmpresaInstitucion;
use App\User;
use App\Dependencias;
use App\Sucursal;
use App\EmpresaMensajeria;
use App\CuposPractica;
use App\DocumentosEmpresa;
use App\Programas;
use App\AsignarEstudiante;
use App\ConfiguracionPasos;
use App\PasosXdependencias;
use App\ProcesoSeleccion;
use App\SolicitudCupo;
use App\InansistenciasPracticantes;
use App\ObservacionesPracticantes;
use App\AnexarArlPracticante;
use App\EstudianteSeleccionado;
use App\Http\Requests\filterEstudiantesDisponibles;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

DB::listen(function($query){
    // echo "<pre>{$query->sql}</pre>";
});

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('empresas', function(){
    $datos = Empresa::select('empresas.*','organizacion_juridica.org_jur_descripcion','sector_productivo.sec_prod_descripcion')
                                        ->join('organizacion_juridica','empresas.org_jur_id','=','organizacion_juridica.org_jur_id')
                                        ->join('sector_productivo','empresas.sec_prod_id','=','sector_productivo.sec_prod_id')->get();
    return array('data' => $datos);
});

Route::get('ciudades/{depto}', function($depto){
    $depto = Departamentos::findOrFail($depto);
    return $depto->ciudades()->get();
});

Route::get('instituciones', function(){
    $datos = Institucion::select('institucion.*','ciudad.ciudad_descripcion')
                                        ->join('ciudad','institucion.ciudad_id','=','ciudad.ciudad_id')->get();
    return array('data' => $datos);
});

Route::get('estudiantes', function(){
    $datos = Estudiante::where('estudiante_estado',true)->get();
    return array('data' => $datos);
});

Route::get('empresas-disponibles/{institucion}', function($institucion){
    $empresas = EmpresaInstitucion::select('empresa_id')->where('institucion_id',$institucion)
                                                                    ->whereIn('empresa_institucion_estado',[1,2])->get()->pluck('empresa_id');
    $datos = Empresa::select('empresas.*','sector_productivo.sec_prod_descripcion')
                                        ->join('sector_productivo','empresas.sec_prod_id','=','sector_productivo.sec_prod_id')
                                        ->whereNotIn('empresas.empresa_id',$empresas)
                                        ->get();

    return array('data' => $datos);
});

Route::get('convenios/{institucion}', function($institucion){
    $datos = Empresa::select('empresas.*','sector_productivo.sec_prod_descripcion','empresa_institucion.empresa_institucion_estado','empresa_institucion.empresa_institucion_id','empresa_institucion.created_user as usuario_solicita',DB::raw('(select rol_id from users where id = empresa_institucion.created_user) as rol_id'))
                                    ->join('sector_productivo','empresas.sec_prod_id','=','sector_productivo.sec_prod_id')
                                    ->join('empresa_institucion','empresas.empresa_id','=','empresa_institucion.empresa_id')
                                    ->where('empresa_institucion.institucion_id',$institucion)
                                    ->whereIn('empresa_institucion.empresa_institucion_estado',[1,2])
                                    ->get();
    return array('data' => $datos);
});

Route::get('empresas/convenios/{empresa}', function($empresa){
    $datos = Institucion::select('institucion.*','empresa_institucion.empresa_institucion_estado','ciudad.ciudad_descripcion','empresa_institucion.empresa_institucion_id','empresa_institucion.documento_empresa_id','documentos_empresas.documento_file','empresa_institucion.created_user as usuario_solicita',DB::raw('(select rol_id from users where id = empresa_institucion.created_user) as rol_id'))
                                        ->join('empresa_institucion','institucion.institucion_id','=','empresa_institucion.institucion_id')
                                        ->join('ciudad','institucion.ciudad_id','=','ciudad.ciudad_id')
                                        ->LeftJoin('documentos_empresas', 'empresa_institucion.documento_empresa_id', '=', 'documentos_empresas.doc_emp_id')
                                         ->where('empresa_institucion.empresa_id',$empresa)
                                         ->whereIn('empresa_institucion.empresa_institucion_estado',[1,2])
                                        ->get();
    return array('data' => $datos);
});

Route::get('instituciones-disponibles/{empresa}', function($empresa){
    $intituciones = EmpresaInstitucion::select('institucion_id')->where('empresa_id',$empresa)
                                                                        ->whereIn('empresa_institucion_estado',[1,2])->get()->pluck('institucion_id');
    $datos = Institucion::select('institucion.*','ciudad.ciudad_descripcion')
                                            ->whereNotIn('institucion_id',$intituciones)
                                            ->join('ciudad','institucion.ciudad_id','=','ciudad.ciudad_id')
                                            ->get();
    return array('data' => $datos);
});

Route::get('users/{tercero}/{rol}', function($tercero,$rol){
    $datos = User::select('users.*','cargos.cargo_descripcion','ciudad.ciudad_descripcion')
                                ->join('cargos','users.cargo_id','=','cargos.cargo_id')
                                ->join('ciudad','users.ciudad_id_expedicion','=','ciudad.ciudad_id')
                                ->where([['tercero_id',$tercero],['rol_id',$rol]])->get();
    return array('data' => $datos);
});

Route::get('dependencias/{empresa}', function($empresa){
    $datos = Dependencias::where('empresa_id', $empresa)->get();
    return array('data' => $datos);
});

Route::get('pasos-configurados/{empresa}', function($empresa){
    $datos = ConfiguracionPasos::where('empresa_id', $empresa)->orderBy('pasos_orden','ASC')->get();
    return array('data' => $datos);
});

Route::get('sucursales/{empresa}', function($empresa){
    $datos = Sucursal::select('empresa_sucursal.*','ciudad.ciudad_descripcion')
                                        ->join('ciudad','empresa_sucursal.ciudad_id','=','ciudad.ciudad_id')
                                        ->where('empresa_id', $empresa)->get();
    return array('data' => $datos);
});

Route::get('usermensajeria/{empresa}', function($empresa){
    $datos = EmpresaMensajeria::select('empresa_mensajeria.*','sistemas_mensajeria.mensajeria_descripcion')
                                        ->join('sistemas_mensajeria','empresa_mensajeria.mensajeria_id','=','sistemas_mensajeria.mensajeria_id')
                                        ->where('empresa_id', $empresa)->get();
    return array('data' => $datos);
});

Route::get('cupos/{empresa}', function($empresa){
    $datos = CuposPractica::select('cupos_practicas.*','empresa_dependencias.dependencia_descripcion','empresa_sucursal.sucursal_descripcion','cargos.cargo_descripcion','users.user_nombre')
                                                    ->join('empresa_dependencias', 'cupos_practicas.dependencia_id','=', 'empresa_dependencias.dependencia_id')
                                                    ->join('empresa_sucursal', 'cupos_practicas.sucursal_id','=', 'empresa_sucursal.sucursal_id')
                                                    ->join('cargos', 'cupos_practicas.cargo_id','=', 'cargos.cargo_id')
                                                    ->join('users', 'cupos_practicas.maestro_id','=', 'users.id')
                                                    ->where('cupos_practicas.empresa_id', $empresa)->get();
    return array('data' => $datos);
});

Route::post('cupos/{empresa}', function($empresa, Request $request){
    $datos = CuposPractica::select('cupos_practicas.*','empresa_dependencias.dependencia_descripcion','empresa_sucursal.sucursal_descripcion','cargos.cargo_descripcion','users.user_nombre')
                                                    ->join('empresa_dependencias', 'cupos_practicas.dependencia_id','=', 'empresa_dependencias.dependencia_id')
                                                    ->join('empresa_sucursal', 'cupos_practicas.sucursal_id','=', 'empresa_sucursal.sucursal_id')
                                                    ->join('cargos', 'cupos_practicas.cargo_id','=', 'cargos.cargo_id')
                                                    ->join('users', 'cupos_practicas.maestro_id','=', 'users.id')
                                                    ->when($request->cdDependencia, function($query, $dependencia){
                                                        return $query->where('cupos_practicas.dependencia_id',$dependencia);
                                                    })
                                                    ->when($request->cbSucursal, function($query, $sucursal){
                                                        return $query->where('cupos_practicas.sucursal_id',$sucursal);
                                                    })
                                                    ->when($request->cbCargo, function($query, $cargo){
                                                        return $query->where('cupos_practicas.cargo_id',$cargo);
                                                    })
                                                    ->when($request->cbMaestro, function($query, $maestro){
                                                        return $query->where('cupos_practicas.maestro_id',$maestro);
                                                    })->get();
    return array('data' => $datos);
});

Route::get('documentos/{empresa}', function($empresa){
    $datos = DocumentosEmpresa::select('documentos_empresas.*', 'documentos_legales.documento_descripcion')
                                        ->join('documentos_legales','documentos_empresas.documento_id','=','documentos_legales.documento_id')
                                        ->where('empresa_id', $empresa)->get();
    return array('data' => $datos);
});

Route::get('programas/{empresa}/{institucion}/{area}', function($empresa,$institucion, $area){
    return Programas::where('programa_estado',true)
        ->join('empresa_institucion','programas.institucion_id','=','empresa_institucion.institucion_id')
        ->where('empresa_institucion.empresa_id',$empresa)
        ->when($institucion != 0, function($query, $institucion){
            return $query->where('programas.institucion_id',$institucion);
        })
        ->when($area != 0, function($query, $area){
            return $query->where('area_id', $area);
        })->get();
});

Route::get('programas/{institucion}/{area}', function($institucion, $area){
    return Programas::where([['programa_estado',true],['institucion_id',$institucion]])
        ->when($area, function($query, $area){
            return $query->where('area_id', $area);
        })->get();
});

Route::post('estudiantes-disponibles/{empresa}', function(filterEstudiantesDisponibles $request,$empresa){
    return Estudiante::select('estudiantes.*','programas.programa_descripcion','ciudad.ciudad_descripcion','estapas_estudiante.etapa_descripcion','institucion.*','solicitud_cupo.solicitud_estado')
        ->selectRaw('timestampdiff(year, estudiante_fecha_nacimiento, curdate()) as edad')
        // ->selectRaw('(select solicitud_estado from  solicitud_cupo where estudiante_id=estudiantes.estudiante_id and empresa_id = '.$empresa.') as solicitud')
        // ->selectRaw('(select solicitud_estado from  solicitud_cupo where estudiante_id=estudiantes.estudiante_id and empresa_id = '.$empresa.') as estado_solicitud')
        ->join('institucion_estudiante','estudiantes.estudiante_id','=','institucion_estudiante.estudiante_id')
        ->join('estudiante_programa','estudiantes.estudiante_id','=','estudiante_programa.estudiante_id')
        ->join('programas','estudiante_programa.programa_id','=','programas.programa_id')
        ->join('institucion','programas.institucion_id','=','institucion.institucion_id')
        ->join('ciudad','estudiantes.ciudad_id','=','ciudad.ciudad_id')
        ->join('estapas_estudiante','estudiantes.etapa_id','=','estapas_estudiante.etapa_id')
        ->leftjoin('solicitud_cupo', function($join){
            $join->on('estudiantes.estudiante_id','=','solicitud_cupo.estudiante_id')
            ->where('solicitud_cupo.solicitud_estado','<>','rechazada')
            ->where('solicitud_cupo.solicitud_estado','<>','no seleccionada');
        })
        // ->where([['programas.area_id',$request->cbArea],['solicitud_cupo.solicitud_estado','pendiente']])
        ->where('programas.area_id',$request->cbArea)


        // ->orwhere(function($query) use ($request) {
        //     return $query->where([['institucion_estudiante.institucion_id',$request->cbInstitucion],['programas.area_id',$request->cbArea]]);
        // })
         ->when($request->cbInstitucion, function($query) use ($request){
            return $query->where('institucion_estudiante.institucion_id',$request->cbInstitucion);
        })
        ->when($request->cbPrograma, function($query) use ($request){
            return $query->where('programas.programa_id',$request->cbPrograma);
        })
        ->when($request->cbCiudad, function($query, $ciudad){
            return $query->where('ciudad.ciudad_id',$ciudad);
        })
        ->when($request->cbEdad, function($query, $edad){
            $edades = explode('-',$edad);
            return $query->whereRaw('timestampdiff(year, estudiante_fecha_nacimiento, curdate()) between '.$edades[0].' and '.$edades[1]);
        })
        ->when($request->cbSexo, function($query, $sexo){
            return $query->where('estudiantes.estudiante_sexo',$sexo);
        })
        ->when($request->cbModalidad, function($query, $modalidad){
            return $query->where('estudiantes.etapa_id',$modalidad);
        })
        ->get();
});

Route::get('info-cupo/{cupo}', function($cupo){
    return CuposPractica::select('cupos_practicas.*','cargos.cargo_descripcion','empresa_dependencias.dependencia_descripcion','empresa_sucursal.sucursal_descripcion','users.user_nombre')
                                                ->join('cargos','cupos_practicas.cargo_id','=','cargos.cargo_id')
                                                ->join('empresa_dependencias','cupos_practicas.dependencia_id','=','empresa_dependencias.dependencia_id')
                                                ->join('empresa_sucursal','cupos_practicas.sucursal_id','=','empresa_sucursal.sucursal_id')
                                                ->join('users', 'cupos_practicas.maestro_id','=', 'users.id')
                                                ->where('cupos_practicas.cupo_id', $cupo)
                                                ->get()->first();
});

Route::get('Asignados/{empresa}/{idcupo}', function($empresa,$idcupo){
    $datos = AsignarEstudiante::where([['empresa_id', $empresa],['cupo_id',$idcupo]])->get();
    return  $datos;
});

Route::post('estudiantes-preseleccionados/{empresa}', function(filterEstudiantesDisponibles $request,$empresa){
    return Estudiante::select('estudiantes.*','institucion.*','cupos_practicas.*','empresa_dependencias.dependencia_descripcion','solicitud_cupo.*','users.user_nombre','empresa_sucursal.sucursal_descripcion','programas.programa_descripcion','ciudad.ciudad_descripcion','estapas_estudiante.etapa_descripcion',DB::raw("(SELECT CONCAT(estudiante_nombres,' ',estudiante_apellidos) AS nombres FROM estudiantes WHERE estudiante_id = solicitud_cupo.solicitud_remplazo) as remplazo"))
        ->selectRaw('timestampdiff(year, estudiante_fecha_nacimiento, curdate()) as edad')
        ->join('institucion_estudiante','estudiantes.estudiante_id','=','institucion_estudiante.estudiante_id')
        ->join('estudiante_programa','estudiantes.estudiante_id','=','estudiante_programa.estudiante_id')
        ->join('programas','estudiante_programa.programa_id','=','programas.programa_id')
        ->join('ciudad','estudiantes.ciudad_id','=','ciudad.ciudad_id')
        ->join('estapas_estudiante','estudiantes.etapa_id','=','estapas_estudiante.etapa_id')
        ->join('solicitud_cupo','estudiantes.estudiante_id','=','solicitud_cupo.estudiante_id')
        ->join('cupos_practicas','cupos_practicas.cupo_id','=','solicitud_cupo.cupo_id')
        ->join('empresa_dependencias','cupos_practicas.dependencia_id','=','empresa_dependencias.dependencia_id')
        ->join('empresa_sucursal','cupos_practicas.sucursal_id','=','empresa_sucursal.sucursal_id')
        ->join('users','cupos_practicas.maestro_id','=','users.id')
        ->join('institucion','institucion_estudiante.institucion_id','=','institucion.institucion_id')
        
        ->where([['programas.area_id',$request->cbArea],['solicitud_cupo.solicitud_estado','aceptada']])


        // ->orwhere(function($query) use ($request) {
        //     return $query->where([['institucion_estudiante.institucion_id',$request->cbInstitucion],['solicitud_cupo.solicitud_estado','aceptada']]);
        // })
        ->when($request->cbInstitucion, function($query) use ($request){
            return $query->where('institucion_estudiante.institucion_id',$request->cbInstitucion);
        })
        ->when($request->cbPrograma, function($query) use ($request){
            return $query->where('programas.programa_id',$request->cbPrograma);
        })
        // ->when($request->cbCiudad, function($query, $ciudad){
        //     return $query->where('ciudad.ciudad_id',$ciudad);
        // })
        // ->when($request->cbEdad, function($query, $edad){
        //     $edades = explode('-',$edad);
        //     return $query->whereRaw('timestampdiff(year, estudiante_fecha_nacimiento, curdate()) between '.$edades[0].' and '.$edades[1]);
        // })
        // ->when($request->cbSexo, function($query, $sexo){
        //     return $query->where('estudiantes.estudiante_sexo',$sexo);
        // })
        // ->when($request->cbModalidad, function($query, $modalidad){
        //     return $query->where('estudiantes.etapa_id',$modalidad);
        // })
        ->get();
});

Route::get('consultar-pasos/{empresa}/{idependencia}/{idestudiante}', function($empresa,$idependencia,$idestudiante){
    $datos = PasosXdependencias::select('pasos_por_dependencias.*','users.*','pasos_por_dependencias.pasos_id','configuracion_pasos.pasos_nombre','configuracion_pasos.pasos_orden','configuracion_pasos.pasos_tipo',DB::raw('(select proceso_fecha  from proceso_seleccion where proceso_seleccion.pasos_id = configuracion_pasos.pasos_id and proceso_seleccion.estudiante_id = '.$idestudiante.') as proceso_fecha'),DB::raw('(select proceso_observaciones  from proceso_seleccion where proceso_seleccion.pasos_id = configuracion_pasos.pasos_id and proceso_seleccion.estudiante_id = '.$idestudiante.') as proceso_observaciones'),DB::raw('(select proceso_estado  from proceso_seleccion where proceso_seleccion.pasos_id = configuracion_pasos.pasos_id and proceso_seleccion.estudiante_id = '.$idestudiante.') as proceso_estado'),DB::raw('(select proceso_aplica  from proceso_seleccion where proceso_seleccion.pasos_id = configuracion_pasos.pasos_id and proceso_seleccion.estudiante_id = '.$idestudiante.') as proceso_aplica'),DB::raw('(select proceso_id  from proceso_seleccion where proceso_seleccion.pasos_id = configuracion_pasos.pasos_id and proceso_seleccion.estudiante_id = '.$idestudiante.') as proceso_id'),DB::raw('(select estudiante_foto  from estudiantes where estudiante_id = '.$idestudiante.') as foto'),DB::raw("(SELECT CONCAT(estudiante_nombres,' ',estudiante_apellidos) AS nombres FROM estudiantes WHERE estudiante_id =  ".$idestudiante.") as nombres"),DB::raw('(select estudiante_identificacion from estudiantes where estudiante_id = '.$idestudiante.') as cedula'))
    ->join('configuracion_pasos','pasos_por_dependencias.pasos_id','=','configuracion_pasos.pasos_id')
    ->join('users','configuracion_pasos.user_id','=','users.id')
    ->where([['pasos_por_dependencias.empresa_id', $empresa],['pasos_por_dependencias.dependencia_id',$idependencia]])->orderBy('configuracion_pasos.pasos_orden','ASC')->get();
    return  $datos;
});


Route::post('estudiantes-seleccionados/{empresa}', function( Request $request,$empresa){
    
    $datos = SolicitudCupo::select('solicitud_cupo.*','estudiantes.*','cupos_practicas.*','programas.*','users.*','empresa_sucursal.sucursal_descripcion','institucion.institucion_nombre','estudiantes_seleccionados.*')
        ->join('estudiantes','solicitud_cupo.estudiante_id','=','estudiantes.estudiante_id')
        ->join('estudiante_programa','estudiantes.estudiante_id','=','estudiante_programa.estudiante_id')
        ->join('programas','estudiante_programa.programa_id','=','programas.programa_id')
        ->join('cupos_practicas','solicitud_cupo.cupo_id','=','cupos_practicas.cupo_id')
        ->join('empresa_sucursal','cupos_practicas.sucursal_id','=','empresa_sucursal.sucursal_id')
        ->join('institucion','programas.institucion_id','=','institucion.institucion_id')
        ->join('users','cupos_practicas.maestro_id','=','users.id')
        ->join('estudiantes_seleccionados','estudiantes.estudiante_id','=','estudiantes_seleccionados.estudiante_id')
        ->where([['solicitud_cupo.empresa_id',$empresa],['solicitud_cupo.solicitud_estado','seleccionada']])

        ->when($request->cbInstitucion, function($query, $cbInstitucion){
            return $query->where('programas.institucion_id',$cbInstitucion);
        })
        ->when($request->cbSucursal, function($query, $cbSucursal){
            return $query->where('cupos_practicas.sucursal_id',$cbSucursal);
        })
        ->when($request->cdDependencia, function($query, $cdDependencia){
            return $query->where('cupos_practicas.dependencia_id',$cdDependencia);
        })
        ->when($request->cbCargo, function($query, $cbCargo){
            return $query->where('solicitud_cupo.cupo_id',$cbCargo);
        })
        ->get();


    return $datos; 
});

Route::get('info-solicitud/{cupo}', function($cupo){
    // return SolicitudCupo::select('solicitud_cupo.*','estudiantes.*','estudiantes_asignados.*')
    //     ->join('estudiantes','solicitud_cupo.estudiante_id','=','estudiantes.estudiante_id')
    //     ->leftjoin('estudiantes_asignados','estudiantes_asignados.cupo_id','solicitud_cupo.cupo_id')
    //     ->where('solicitud_cupo.cupo_id', $cupo)
    //     ->orwhere(function($query) use ($cupo) {
    //         return $query->where('estudiantes_asignados.cupo_id',$cupo);
    //     })
    //     ->get();

    return DB::select("select id as estudiante_id, asignado_nombres as estudiante_nombres from estudiantes_asignados where estudiantes_asignados.cupo_id =".$cupo);

    // return Estudiante::select('estudiantes.*','solicitud_cupo.*','estudiantes_asignados.*')
    //         ->join('institucion_estudiante','estudiantes.estudiante_id','=','institucion_estudiante.estudiante_id')
    //         ->join('empresa_institucion','institucion_estudiante.institucion_id','=','empresa_institucion.institucion_id')
    //         ->join('cupos_practicas','empresa_institucion.empresa_id','=','cupos_practicas.empresa_id')
    //         ->leftjoin('solicitud_cupo','solicitud_cupo.estudiante_id','=','estudiantes.estudiante_id')
    //         ->leftjoin('estudiantes_asignados','estudiantes_asignados.cupo_id','=','cupos_practicas.cupo_id')
    //         ->where('solicitud_cupo.cupo_id', $cupo)
    //         ->orwhere(function($query) use ($cupo) {
    //             return $query->where('estudiantes_asignados.cupo_id',$cupo);
    //         })
    //         ->get();            




});

Route::get('inasistencias/{empresa}/{idseleccionado}', function($empresa,$idseleccionado){
    $datos = InansistenciasPracticantes::where([['empresa_id',$empresa],['estudiante_Selec_id',$idseleccionado]])->get();
    return array('data' => $datos);
});

Route::get('observaciones-practicantes/{empresa}/{idseleccionado}', function($empresa,$idseleccionado){
    $datos = ObservacionesPracticantes::where([['empresa_id',$empresa],['estudiante_Selec_id',$idseleccionado]])->get();
    return array('data' => $datos);
});

Route::get('documentos-practicantes/{empresa}/{idseleccionado}', function($empresa,$idseleccionado){
    $datos = AnexarArlPracticante::select('documentos_practicante.*','documentos_legales.documento_descripcion')
    ->join('documentos_legales','documentos_practicante.documento_id','=','documentos_legales.documento_id')
    ->where([['empresa_id',$empresa],['estudiante_Selec_id',$idseleccionado]])->get();
    return array('data' => $datos);
});

Route::get('consultar-info-proceso/{empresa}/{idestudiante}', function($empresa,$idestudiante){
    return SolicitudCupo::select('solicitud_cupo.*','cupos_practicas.*','users.*','empresa_dependencias.*')
        ->join('cupos_practicas','solicitud_cupo.cupo_id','=','cupos_practicas.cupo_id')
        ->join('users','cupos_practicas.maestro_id','=','users.id')
        ->join('empresa_dependencias','cupos_practicas.dependencia_id','=','empresa_dependencias.dependencia_id')
        ->where([['solicitud_cupo.empresa_id', $empresa],['solicitud_cupo.estudiante_id',$idestudiante]])
        ->get();
});

// Información del Dashboard

Route::get('dashboard-cantidades/{empresa}', function($empresa){

    $Total    = DB::select('(select count(cupo_id) as Total from cupos_practicas  where empresa_id = '.$empresa.' )');
    $Ocupados = DB::select('(select count(cupo_id) as Ocupados from cupos_practicas where cupos_practicas.cupo_estado = 2 and empresa_id = '.$empresa.')');
    $Disponibles = DB::select('(select count(cupo_id) as Disponibles from cupos_practicas where cupos_practicas.cupo_estado = 1 and empresa_id = '.$empresa.')');

    $Empleados =  DB::select('(select empresa_no_empleados as Empleados from empresas where empresa_id = '.$empresa.')');

    $Solicitudes = DB::select('(select count(solicitud_cupo_id) as Solicitudes from solicitud_cupo where solicitud_estado = "pendiente" and empresa_id = '.$empresa.')');

    return array('total' => $Total,'ocupados' => $Ocupados,'disponibles' => $Disponibles,'solicitudes' => $Solicitudes,'empleados' => $Empleados);
});


Route::get('dashboard-dependencias-vencimiento/{empresa}', function($empresa){
   
     $datos = CuposPractica::select('cupos_practicas.*','empresa_dependencias.*','empresa_sucursal.sucursal_descripcion','cargos.cargo_descripcion','solicitud_cupo.solicitud_fecha_culminacion')
        ->join('empresa_dependencias', 'cupos_practicas.dependencia_id','=', 'empresa_dependencias.dependencia_id')
        ->join('empresa_sucursal', 'cupos_practicas.sucursal_id','=', 'empresa_sucursal.sucursal_id')
        ->join('cargos', 'cupos_practicas.cargo_id','=', 'cargos.cargo_id')
        ->join('solicitud_cupo','cupos_practicas.cupo_id','=','solicitud_cupo.cupo_id')
        ->where([['cupos_practicas.empresa_id',$empresa],['solicitud_cupo.solicitud_estado','seleccionada']])->get();

        return $datos;

});

Route::get('dashboard-dependencias-disponibles/{empresa}', function($empresa){
   
     $datos = CuposPractica::select('cupos_practicas.*','empresa_dependencias.dependencia_descripcion','empresa_sucursal.sucursal_descripcion','cargos.cargo_descripcion')
        ->join('empresa_dependencias', 'cupos_practicas.dependencia_id','=', 'empresa_dependencias.dependencia_id')
        ->join('empresa_sucursal', 'cupos_practicas.sucursal_id','=', 'empresa_sucursal.sucursal_id')
        ->join('cargos', 'cupos_practicas.cargo_id','=', 'cargos.cargo_id')
        ->where([['cupos_practicas.empresa_id',$empresa],['cupos_practicas.cupo_estado',1]])->get();

        return $datos;

});

Route::get('dashboard-dependencias-ocupados/{empresa}', function($empresa){
    $datos = Dependencias::select('empresa_dependencias.dependencia_id','empresa_dependencias.dependencia_descripcion')
   ->selectRaw('(select count(cupo_id) from  cupos_practicas where cupos_practicas.dependencia_id=empresa_dependencias.dependencia_id and cupos_practicas.cupo_estado = 2 and empresa_id = '.$empresa.') as cantidad')
    ->join('cupos_practicas','empresa_dependencias.dependencia_id','=','cupos_practicas.dependencia_id')
    // ->join('empresa_sucursal', 'cupos_practicas.sucursal_id','=', 'empresa_sucursal.sucursal_id')
    ->where([['empresa_dependencias.empresa_id', $empresa],['cupos_practicas.cupo_estado',2]])
    ->groupBy('empresa_dependencias.dependencia_id')
    ->get();
    return $datos;
});

Route::get('dashboard-ultimos-cupos/{empresa}', function($empresa){
   
   $datos = CuposPractica::select('cupos_practicas.*','cargos.cargo_descripcion','solicitud_cupo.solicitud_fecha_inicio','empresa_dependencias.*')
        ->join('cargos', 'cupos_practicas.cargo_id','=', 'cargos.cargo_id')
        ->join('solicitud_cupo','cupos_practicas.cupo_id','=','solicitud_cupo.cupo_id')
        ->join('empresa_dependencias','cupos_practicas.dependencia_id','=','empresa_dependencias.dependencia_id')
        ->where('cupos_practicas.empresa_id',$empresa)
        ->orderBy('solicitud_cupo.solicitud_fecha_inicio','DESC')->get();

   
    return $datos;

});

Route::get('dashboard-instituciones/{empresa}',function($empresa){

    $datos = Institucion::select('institucion.institucion_nombre','institucion.institucion_id')
            ->selectRaw('(select count(solicitud_cupo_id) from solicitud_cupo where solicitud_cupo.cupo_id=cupos_practicas.cupo_id and empresa_id = '.$empresa.') as cantidad')
            ->join('institucion_estudiante','institucion.institucion_id','=','institucion_estudiante.institucion_id')
            ->join('solicitud_cupo','institucion_estudiante.estudiante_id','=','solicitud_cupo.estudiante_id')
            ->join('cupos_practicas','solicitud_cupo.cupo_id','=','cupos_practicas.cupo_id')
            ->where([['solicitud_cupo.empresa_id',$empresa],['solicitud_estado','<>','pendiente']])
            ->groupBy('institucion.institucion_id')
            ->get();

    return $datos;

});


Route::get('dashboard-cantidades-inicio/{empresa}', function($empresa){

    $Total    = DB::select('(select count(cupo_id) as Total from cupos_practicas  where empresa_id = '.$empresa.' )');
    $Practicantes = DB::select('(select count(cupo_id) as Practicantes from solicitud_cupo where solicitud_estado <> "pendiente" and empresa_id = '.$empresa.')');
    $Disponibles = DB::select('(select count(cupo_id) as Disponibles from cupos_practicas where cupos_practicas.cupo_estado = 1 and empresa_id = '.$empresa.')');
    $Convenios = DB::select('(select count(empresa_institucion_id) as Convenios from empresa_institucion where empresa_institucion.empresa_institucion_estado = 2 and empresa_institucion.empresa_id = '.$empresa.')');

    $Solicitudes = DB::select('(select count(solicitud_cupo_id) as Solicitudes from solicitud_cupo where solicitud_estado = "pendiente" and empresa_id = '.$empresa.')');
    
    return array('total' => $Total,'practicantes' => $Practicantes,'disponibles' => $Disponibles,'convenios'=>$Convenios, 'solicitudes' => $Solicitudes);
});

Route::get('info-estudiante/{idestudiante}', function($idestudiante){

    $datos = Estudiante::select('estudiantes.*','programa_descripcion','grupos_sanguineos.grupo_sanguineo_descripcion')
     ->join('estudiante_programa','estudiantes.estudiante_id','=','estudiante_programa.estudiante_id')
    ->join('programas','estudiante_programa.programa_id','=','programas.programa_id')
    ->join('grupos_sanguineos','estudiantes.grupo_sanguineo_id','=','grupos_sanguineos.grupo_sanguineo_id')
    ->where([['estudiantes.estudiante_estado',true],['estudiantes.estudiante_id',$idestudiante]])->get();
    return $datos;
});


Route::get('info-estudiante-seleccionado/{idseleccionado}', function($idseleccionado){

    $datos = Estudiante::select('estudiantes.*','programa_descripcion','solicitud_cupo.*','empresas.empresa_nombre','empresa_dependencias.dependencia_descripcion')
    ->join('estudiante_programa','estudiantes.estudiante_id','=','estudiante_programa.estudiante_id')
    ->join('programas','estudiante_programa.programa_id','=','programas.programa_id')
    ->join('solicitud_cupo','estudiantes.estudiante_id','=','solicitud_cupo.estudiante_id')
    ->join('cupos_practicas','solicitud_cupo.cupo_id','=','cupos_practicas.cupo_id')
    ->join('empresas','solicitud_cupo.empresa_id','=','empresas.empresa_id')
    ->join('empresa_dependencias','cupos_practicas.dependencia_id','=','empresa_dependencias.dependencia_id')
    ->join('estudiantes_seleccionados','estudiantes_seleccionados.estudiante_id','=','estudiantes.estudiante_id')
    ->where([['estudiantes.estudiante_estado',true],['estudiantes_seleccionados.seleccion_id',$idseleccionado]])->get();
    return $datos;
});


Route::get('fechas-calendario/{empresa}', function($empresa){
   $datos = ProcesoSeleccion::select('proceso_seleccion.proceso_observaciones','proceso_seleccion.proceso_fecha as start','configuracion_pasos.pasos_nombre as title')
   ->join('configuracion_pasos','proceso_seleccion.pasos_id','=','configuracion_pasos.pasos_id')
   ->where([['proceso_seleccion.empresa_id',$empresa],['proceso_estado','Programada']])->get();
    return $datos;
});

Route::post('solicitud-practicantes/{empresa}', function(Request $request, $empresa){
    return SolicitudCupo::select('solicitud_cupo.*','cupos_practicas.*','users.*','empresa_dependencias.*','programas.programa_descripcion','empresa_sucursal.sucursal_descripcion')
        ->join('cupos_practicas','solicitud_cupo.cupo_id','=','cupos_practicas.cupo_id')
        ->join('users','cupos_practicas.maestro_id','=','users.id')
        ->join('estudiante_programa','solicitud_cupo.estudiante_id','=','estudiante_programa.estudiante_id')
        ->join('programas','estudiante_programa.programa_id','=','programas.programa_id')
        ->join('empresa_dependencias','cupos_practicas.dependencia_id','=','empresa_dependencias.dependencia_id')
        ->join('empresa_sucursal','cupos_practicas.empresa_id','=','empresa_sucursal.empresa_id')
        ->where('solicitud_cupo.empresa_id',$empresa)
        ->when(!$request->cbEstados, function($query){
                    return $query->where('solicitud_cupo.solicitud_estado','pendiente');
                })
                ->when($request->cbArea, function($query, $area){
                    return $query->where('programas.area_id',$area);
                })
                ->when($request->cbEstados, function($query, $estado){
                    return $query->where('solicitud_cupo.solicitud_estado',$estado);
                })
                ->when($request->cbPrograma, function($query, $programa){
                    return $query->where('programas.programa_id',$programa);
                })
                ->when($request->txtFecInicio, function($query, $fecha){
                    $fechaI = explode('/', $fecha);
                    $fechaInicio = $fechaI[2].'-'.$fechaI[1].'-'.$fechaI[0];
                    return $query->where('solicitud_cupo.created_at','>',$fechaInicio);
                })
                ->when($request->txtFecFin, function($query, $fecha){
                    $fechaF = explode('/', $fecha);
                    $fechaFin = $fechaF[2].'-'.$fechaF[1].'-'.$fechaF[0];
                    return $query->where('solicitud_cupo.created_at','<',$fechaFin);
                })
        ->get();

         // return array('data' => $datos);
});

Route::post('estudiantes-solicitados/{institucion}', function(Request $request, $institucion){
    return SolicitudCupo::select('estudiantes.*','programas.programa_descripcion','ciudad.ciudad_descripcion','estapas_estudiante.etapa_descripcion','solicitud_cupo.solicitud_cupo_id','empresas.*')
                    ->selectRaw('timestampdiff(year, estudiante_fecha_nacimiento, curdate()) as edad')
                    ->join('institucion_estudiante', 'solicitud_cupo.estudiante_id','=','institucion_estudiante.estudiante_id')
                    ->join('estudiantes','institucion_estudiante.estudiante_id','=','estudiantes.estudiante_id')
                    ->join('empresas','solicitud_cupo.empresa_id','=','empresas.empresa_id')
                    ->join('estudiante_programa','estudiantes.estudiante_id','=','estudiante_programa.estudiante_id')
                    ->join('programas','estudiante_programa.programa_id','=','programas.programa_id')
                    ->join('ciudad','estudiantes.ciudad_id','=','ciudad.ciudad_id')
                    ->join('estapas_estudiante','estudiantes.etapa_id','=','estapas_estudiante.etapa_id')
                    ->where('institucion_estudiante.institucion_id',$institucion)
                    ->when(!$request->cbEstados, function($query){
                        return $query->where('solicitud_cupo.solicitud_estado','pendiente');
                    })
                    ->when($request->cbArea, function($query, $area){
                        return $query->where('programas.area_id',$area);
                    })
                    ->when($request->cbEstados, function($query, $estado){
                        return $query->where('solicitud_cupo.solicitud_estado',$estado);
                    })
                    ->when($request->cbPrograma, function($query, $programa){
                        return $query->where('programas.programa_id',$programa);
                    })
                    ->when($request->txtFecInicio, function($query, $fecha){
                        $fechaI = explode('/', $fecha);
                        $fechaInicio = $fechaI[2].'-'.$fechaI[1].'-'.$fechaI[0];
                        return $query->where('solicitud_cupo.created_at','>',$fechaInicio);
                    })
                    ->when($request->txtFecFin, function($query, $fecha){
                        $fechaF = explode('/', $fecha);
                        $fechaFin = $fechaF[2].'-'.$fechaF[1].'-'.$fechaF[0];
                        return $query->where('solicitud_cupo.created_at','<',$fechaFin);
                    })
                    ->get();
});

Route::get('notificaciones-empresas/{empresa}', function($empresa){
    $notificaciones = [];
    $solicitudes = SolicitudCupo::select('solicitud_cupo.solicitud_estado','estudiantes.estudiante_nombres','estudiantes.estudiante_apellidos','empresas.empresa_nombre','institucion.institucion_nombre')
                    ->join('estudiantes','solicitud_cupo.estudiante_id','=','estudiantes.estudiante_id')
                    ->join('institucion_estudiante','estudiantes.estudiante_id','=','institucion_estudiante.estudiante_id')
                    ->join('institucion','institucion_estudiante.institucion_id','=','institucion.institucion_id')
                    ->join('empresas','solicitud_cupo.empresa_id','=','empresas.empresa_id')
                    ->where([['solicitud_cupo.empresa_id', $empresa],['solicitud_cupo.solicitud_fecha_respuesta','>',Carbon::now()->subMonth()->toDateString()]])
                    ->whereIn('solicitud_cupo.solicitud_estado',['aceptada','rechazada'])
                    ->get();
    foreach ($solicitudes as $sol) {
        $estado = ($sol['solicitud_estado'] == 'aceptada') ? 'aceptado' : 'rechazado';
        $notificaciones[] = ['Msj' => 'La institución '.$sol['institucion_nombre'].' ha '.$estado.' la solicitud del estudiante '.$sol['estudiante_nombres'].' '.$sol['estudiante_apellidos'], 'tipo' => (($estado == 'aceptado') ?  'solAceptada' : 'solRechazada')];
    }
    $cupos = EstudianteSeleccionado::select('estudiantes.estudiante_nombres','estudiantes.estudiante_apellidos','seleccion_FecFin_prd')
                    ->join('estudiantes','estudiantes_seleccionados.estudiante_id','=','estudiantes.estudiante_id')
                    ->where('estudiantes_seleccionados.empresa_id', $empresa)
                    ->whereBetween('seleccion_FecFin_prd',[Carbon::now()->toDateString(),Carbon::now()->addMonth()->toDateString()])
                    ->get();
    foreach ($cupos as $cupo) {
        $fecha = new DateTime($cupo['seleccion_FecFin_prd']);
        $notificaciones[] = ['Msj' => 'El estudiante '.$cupo['estudiante_nombres'].' '.$cupo['estudiante_apellidos'].' está proximo a terminar su proceso de prácticas el dia '.$fecha->format('d/m/Y'), 'tipo' => 'cupo'];
    }           
    return $notificaciones;
});

Route::get('documentos/{empresa}/{documento}', function($empresa, $documento){
    return DocumentosEmpresa::where([['documento_id', $documento],['empresa_id',$empresa]])->get();
});