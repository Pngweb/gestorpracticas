<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
})->name('raiz');

Route::get('/empresas/registro','EmpresasController@index')->name('registroEmpresas');
Route::get('/institucion/registro','InstitucionController@index')->name('registroInstituciones');
Route::get('/estudiantes/registro','EstudianteController@index')->name('registroEstudiantes');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::resource('institucion', 'InstitucionController');
Route::resource('estudiantes', 'EstudianteController');
Route::post('/estudiantes/importar','EstudianteController@importar')->name('importarEstudiantes');
Route::resource('empresas', 'EmpresasController');
Route::post('/empresas/importar','EmpresasController@importar')->name('importarEmpresas');
Route::resource('convenios', 'ConveniosController');
Route::resource('users', 'UsersController');

Route::post('/empresas/invitacion','EmpresasController@mailInvitacion')->name('InvitacionEmpresas');
Route::post('/institucion/invitacion','InstitucionController@mailInvitacion')->name('InvitacionInstitucion');

Route::resource('mensajeria','MensajeriaController');
Route::resource('asignacion','AsignarEstudianteController');
Route::resource('configuracionPasos','ConfiguracionPasosController');

Route::resource('evaluar-practicante','EvaluacionPracticanteController');

Route::get('asistencia-practicante/{id}','AsistenciaPracticanteController@index');
Route::resource('asistencia-practicante','AsistenciaPracticanteController');

Route::get('retiro-practicante/{id}','RetirarPracticanteController@index');
Route::resource('retiro-practicante','RetirarPracticanteController');

Route::get('observacion-practicante/{id}','ObservacionPracticanteController@index');
Route::resource('observacion-practicante','ObservacionPracticanteController');

Route::get('anexar-doc-practicantes/{id}','AnexarARLPracticanteController@index');
Route::resource('anexar-doc-practicantes','AnexarARLPracticanteController');


Route::get('mi-perfil','MiPerfilController@index')->name('miPerfil.index');
Route::post('mi-perfil','MiPerfilController@update')->name('miPerfil.update');

Route::resource('solicitud-practicantes','SolicitudesPracticantes');
Route::resource('encuestas','EncuestasController');


Route::resource('dependencias','DependenciasController');
Route::resource('sucursales','SucursalesController');
Route::resource('funcionarios','FuncionariosController');
Route::resource('cupos-practicas','CuposPracticasController');
Route::resource('documentos-legales','DocumentosLegalesController');

Route::post('cupos-practicas/importar','CuposPracticasController@importar')->name('cupos-practicas.importar');
Route::get('gestion-practicas','GestionPracticasController@index')->name('gestion-practicas.index');

Route::get('estudiantes-disponibles','EstudiantesDisponiblesController@index')->name('estudiantes-disponibles.index');
Route::post('estudiantes-disponibles','EstudiantesDisponiblesController@solicitudCupo')->name('estudiantes-disponibles.solicitudCupo');

Route::post('estudiantes-disponiblesRem','EstudiantesDisponiblesController@solicitudCupoRemplazo')->name('estudiantes-disponiblesRem.solicitudCupoRemplazo');

Route::get('estudiantes-preseleccionados','EstudiantesPreseleccionadosController@index')->name('estudiantes-preseleccionados.index');

// Route::get('proceso-seleccion/{idependencia}','ProcesoSeleccionController@index');
Route::get('proceso-seleccion/{idestudiante}/{idependencia}','ProcesoSeleccionController@index');
Route::post('proceso-seleccion','ProcesoSeleccionController@store')->name('proceso-seleccion.store');
Route::post('proceso-seleccion/update/{id}','ProcesoSeleccionController@update')->name('proceso-seleccion.update');
Route::post('proceso-seleccion/noseleccionar','ProcesoSeleccionController@noselect')->name('proceso-seleccion.noselect');
Route::post('proceso-seleccion/seleccionar','ProcesoSeleccionController@select')->name('proceso-seleccion.select');


Route::get('estudiantes-seleccionados','EstudiantesSeleccionadosController@index')->name('estudiantes-seleccionados.index');
Route::get('estudiantes-seleccionados-info/{id}/{idseleccionado}','EstudiantesSeleccionadosInfoController@index');

Route::get('estudiantes-solicitados', 'EstudiantesSolicitados@index')->name('estudiantes-solicitados.index');
Route::post('estudiantes-solicitados', 'EstudiantesSolicitados@cambiarEstado')->name('estudiantes-solicitados.cambiarEstado');

Route::resource('programas','ProgramasController');
Route::post('programas/importar', 'ProgramasController@importar')->name('importarProgramas');

Route::get('recuperar-contrasena/{identificacion}', 'UsersController@password')->name('recuperar-contrasena');
Route::get('nueva-contrasena/{identificacion}', 'UsersController@nuevaContrasena')->name('nueva-contrasena');
Route::post('guardar-contrasena', 'UsersController@guardarContrasena')->name('guardar-contrasena');

Route::post('documento-convenio/{convenio}','ConveniosController@guardarDocumento')->name('documento-convenio');
