<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CuposPractica extends Model
{
    protected $table = 'cupos_practicas';
    protected $primaryKey = 'cupo_id';
}
