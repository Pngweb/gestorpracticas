<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Dependencias extends Model
{
    protected $table = 'empresa_dependencias';
    protected $primaryKey = 'dependencia_id';
}
