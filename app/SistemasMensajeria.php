<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SistemasMensajeria extends Model
{
    protected $table = 'sistemas_mensajeria';
    protected $primaryKey = 'mensajeria_id';
}
