<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InstitucionEstudiante extends Model
{
    protected $table = 'institucion_estudiante';
    protected $primaryKey = 'inst_est_id';
}
