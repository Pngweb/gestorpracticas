<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sucursal extends Model
{
    protected $table = 'empresa_sucursal';
    protected $primaryKey = 'sucursal_id';
}
