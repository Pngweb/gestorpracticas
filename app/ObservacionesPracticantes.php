<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ObservacionesPracticantes extends Model
{
    //
    protected $table = 'observaciones_practicantes';
    protected $primaryKey = 'observacion_id';
}
