<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class systemAdmin extends Model
{
    protected $table = 'system_admin';
    protected $primaryKey = 'adm_id';
}
