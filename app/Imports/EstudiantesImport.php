<?php

namespace App\Imports;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\{Importable, ToModel, WithHeadingRow};
use Carbon\Carbon;
use App\Ciudad;
use App\GruposSanguineos;
use App\Estudiante;
use App\User;
use App\InstitucionEstudiante;
use App\Cargos;
use App\Programas;
use App\EstudiantePrograma;

class EstudiantesImport implements ToCollection,WithHeadingRow
{

	use Importable;

    /**
    * @param Collection $collection
    */
    public function collection(Collection $collection)
    {
        // print_r($collection);
        $sw = false;
        foreach ($collection as $excel) {
        	$ciudad = Ciudad::whereRaw('lower(ciudad_descripcion) = lower("'.$excel['ciudad'].'")')->first();
        	$grupo = GruposSanguineos::whereRaw('lower(grupo_sanguineo_descripcion) = lower("'.$excel['grupo_sanguineo'].'")')->first();
        	$estudiante = Estudiante::where('estudiante_identificacion',$excel['identificacion'])->first();
        	$programa = Programas::whereRaw('lower(programa_descripcion) = lower("'.$excel['programa'].'")')
        													->where('institucion_id',Auth::user()->tercero_id)->first();
        	if(!$ciudad ){
        		$sw = true;
        		$msj = 'La ciudad '.$excel['ciudad'].' no se encuentra registrada.';
    			break;
        	}else if(!$grupo){
        		$sw = true;
        		$msj = 'El grupo sanguineo '.$excel['grupo_sanguineo'].' no se encuentra registrado.';
    			break;
        	}else if(!$programa){
        		$sw = true;
        		$msj = 'El programa '.$excel['programa'].' no se encuentra registrado.';
    			break;
        	}
        	if($estudiante){
        		$sw = true;
        		$msj = 'El estudiante con identificacion '.$excel['identificacion'].' ya se encuentra registrado.';
    			break;
        	}
        }
        if($sw){
    		abort(403, $msj);
    	}
    	foreach ($collection as $excel) {
        	$ciudad = Ciudad::whereRaw('lower(ciudad_descripcion) = lower("'.$excel['ciudad'].'")')->first();
        	$grupo = GruposSanguineos::whereRaw('lower(grupo_sanguineo_descripcion) = lower("'.$excel['grupo_sanguineo'].'")')->first();
        	$programa = Programas::whereRaw('lower(programa_descripcion) = lower("'.$excel['programa'].'")')
        													->where('institucion_id',Auth::user()->tercero_id)->first();
        	$estudiante = new Estudiante;
        	$estudiante->estudiante_identificacion = $excel['identificacion'];
	        $estudiante->estudiante_nombres = strtoupper($excel['nombres']);
	        $estudiante->estudiante_apellidos = strtoupper($excel['apellidos']);
	        $estudiante->estudiante_sexo = strtoupper($excel['sexo']);
	        $estudiante->estudiante_fecha_nacimiento = \PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($excel['fecha_de_nacimiento']);
	        $estudiante->estudiante_direccion = strtoupper($excel['direccion']);
	        $estudiante->ciudad_id = $ciudad->ciudad_id;
	        $estudiante->estudiante_email = $excel['correo_electronico'];
	        $estudiante->estudiante_telefono = $excel['telefono'];
	        $estudiante->grupo_sanguineo_id = $grupo->grupo_sanguineo_id;
	        $estudiante->estudiante_estado = true;
	        $estudiante->created_user = Auth::user()->id;
	        $estudiante->save();
	        $user = new User;
            $user->identificacion = $excel['identificacion'];
            $user->ciudad_id_expedicion = $ciudad->ciudad_id;
            $user->password = bcrypt($excel['identificacion']);
            $user->user_nombre = strtoupper($excel['nombres'].' '.$excel['apellidos']);
            $user->tercero_id = $estudiante->estudiante_id;
            $user->rol_id = 4;
            $user->cargo_id = Cargos::where('cargo_descripcion','ESTUDIANTE')->get()->first()->cargo_id;
            $user->user_email = $excel['correo_electronico'];
            $user->user_telefono = $excel['telefono'];
            $user->user_cumpleanos = \PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($excel['fecha_de_nacimiento']);
            $user->save();
            $estudiante_programa = new EstudiantePrograma;
            $estudiante_programa->estudiante_id = $estudiante->estudiante_id;
            $estudiante_programa->programa_id = $programa->programa_id;
            $estudiante_programa->save();
            if(Auth::user()->rol_id == 2){
                $institucion_estudiante = new InstitucionEstudiante;
                $institucion_estudiante->institucion_id = Auth::user()->tercero_id;
                $institucion_estudiante->estudiante_id = $estudiante->estudiante_id;
                $institucion_estudiante->inst_est_fecha_inicio = Carbon::now();
                $institucion_estudiante->inst_est_estado = true;
                $institucion_estudiante->save();
            }
        }
    }
}
