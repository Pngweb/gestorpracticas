<?php

namespace App\Imports;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\{Importable, ToModel, WithHeadingRow};
use App\Area;
use App\Programas;

class ProgramasImport implements ToCollection,WithHeadingRow
{

	use Importable;

    /**
    * @param Collection $collection
    */
    public function collection(Collection $collection)
    {
    	 $sw = false;
         foreach ($collection as $excel) {
         	$area = Area::whereRaw('lower(area_descripcion) = lower("'.$excel['area'].'")')->first();
         	if(!$area ){
        		$sw = true;
        		$msj = 'El área '.$excel['area'].' no se encuentra registrada.';
    			break;
        	}
         }
         if($sw){
    		abort(403, $msj);
    	}
    	foreach ($collection as $excel) {
    		$area = Area::whereRaw('lower(area_descripcion) = lower("'.$excel['area'].'")')->first();
    		$programa = new Programas;
	        $programa->programa_descripcion = strtoupper($excel['programa']);
	        $programa->area_id = $area->area_id;
	        $programa->institucion_id = Auth::user()->tercero_id;
	        $programa->programa_estado = true;
	        $programa->save();
    	}
    }
}
