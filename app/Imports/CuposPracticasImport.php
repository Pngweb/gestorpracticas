<?php

namespace App\Imports;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\{Importable, ToModel, WithHeadingRow};
use App\Dependencias;
use App\Sucursal;
use App\Cargos;
use App\User;
use App\CuposPractica;

class CuposPracticasImport implements ToCollection,WithHeadingRow
{

	use Importable;


    /**
    * @param Collection $collection
    */
    public function collection(Collection $collection)
    {
    	$sw = false;
    	foreach ($collection as $cupos) {
    		$dependencia = Dependencias::whereRaw('lower(dependencia_descripcion) = lower("'.$cupos['dependencia'].'")')->get()->first();
    		$sucursal = Sucursal::whereRaw('lower(sucursal_descripcion) = lower("'.$cupos['sucursal'].'")')->get()->first();
    		$cargo = Cargos::whereRaw('lower(cargo_descripcion) = lower("'.$cupos['cargo'].'")')->get()->first();
    		$maestro = User::whereRaw('lower(user_nombre) = lower("'.$cupos['maestro_guia'].'")')
    											->where([['tercero_id', Auth::user()->tercero_id],['rol_id',3]])->get()->first();
    		// print_r($dependencia);
    		// echo count($dependencia).'-'.count($sucursal).'-'.count($cargo).'-'.count($maestro);
    		if(!$dependencia || !$sucursal || !$cargo || !$maestro){
    			$sw = true;
    			break;
    		}
    	}
    	if($sw){
    		abort(403, 'Error al importar los cupos. Valide que la información del archivo sea correcta.');
    	}
    	foreach ($collection as $cupos) {
    		$dependencia = Dependencias::whereRaw('lower(dependencia_descripcion) = lower("'.$cupos['dependencia'].'")')->get()->first();
    		$sucursal = Sucursal::whereRaw('lower(sucursal_descripcion) = lower("'.$cupos['sucursal'].'")')->get()->first();
    		$cargo = Cargos::whereRaw('lower(cargo_descripcion) = lower("'.$cupos['cargo'].'")')->get()->first();
    		$maestro = User::whereRaw('lower(user_nombre) = lower("'.$cupos['maestro_guia'].'")')
    											->where([['tercero_id', Auth::user()->tercero_id],['rol_id',3]])->get()->first();
    		$cupo = new CuposPractica;
    		$cupo->dependencia_id = $dependencia->dependencia_id;
	        $cupo->sucursal_id = $sucursal->sucursal_id;
	        $cupo->cargo_id = $cargo->cargo_id;
	        $cupo->maestro_id = $maestro->id;
	        $cupo->cupo_competencias = strtoupper($cupos['competencias']);
	        $cupo->empresa_id = Auth::user()->tercero_id;
	        $cupo->save();
    	}
    	// return 'mensaje';
    	// return json_encode(array('Msj'=>'Se han importado los cupos de practicas exitosamente.'));
        // return  implode(',',$collection);
    }
}
