<?php

namespace App\Imports;

use App\Empresa;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithMappedCells;

class EmpresasImport implements ToModel, WithMappedCells
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new Empresa([
            'empresa_nit' => $row[0],
            'empresa_nombre' => $row[1],
            'empresa_web' => $row[2],
            'empresa_mensajeria' => $row[3],
            'empresa_no_empleados' => $row[4],
            'empresa_objeto_social' => $row[5]
        ]);
    }

    public function mapping(): array
    {
        return [
            'name'  => 'B1',
            'email' => 'B2',
        ];
    }
}
