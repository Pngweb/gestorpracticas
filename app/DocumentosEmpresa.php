<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DocumentosEmpresa extends Model
{
    protected $table = 'documentos_empresas';
    protected $primaryKey = 'doc_emp_id';
}
