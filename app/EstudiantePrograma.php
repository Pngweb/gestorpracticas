<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EstudiantePrograma extends Model
{
    protected $table = 'estudiante_programa';
    protected $primaryKey = 'est_prog_id';
}
