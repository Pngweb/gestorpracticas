<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DocumentosLegales extends Model
{
    protected $table = 'documentos_legales';
    protected $primaryKey = 'documento_id';
}
