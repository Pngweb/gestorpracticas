<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JornadaTrabajo extends Model
{
    protected $table = 'jornadas_trabajo';
    protected $primaryKey = 'jorn_trab_id';
}
