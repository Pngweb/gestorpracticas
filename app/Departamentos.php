<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Ciudad;

class Departamentos extends Model
{
    protected $table = 'departamentos';
    protected $primaryKey = 'depto_id';

    public function ciudades(){
        return $this->hasMany(Ciudad::class, 'depto_id')->where('ciudad_estado',true);
    }
}
