<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;
use App\Http\Requests\createProgramaRequest;
use App\Http\Requests\importarProgramas;
use App\Imports\ProgramasImport;
use App\menu;
use App\Area;
use App\Programas;

class ProgramasController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $menus = menu::menu_submenu(Auth::user()->rol_id);
        $areas = Area::where('area_estado',true)->get();
        return view('programas', compact('menus','areas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(createProgramaRequest $request)
    {
        $programa = new Programas;
        $programa->programa_descripcion = strtoupper($request->txtNombre);
        $programa->area_id = $request->cbArea;
        $programa->institucion_id = Auth::user()->tercero_id;
        $programa->programa_estado = true;
        $programa->save();
        return  array( 'Msj' => 'Se ha guardado el programa exitosamente.');        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return Programas::find($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(createProgramaRequest $request, $id)
    {
        $programa = Programas::find($id);
        $programa->programa_descripcion = strtoupper($request->txtNombre);
        $programa->area_id = $request->cbArea;
        $programa->programa_estado = ($request->chkEstado == 'on') ? true : false ;
        $programa->save();
        return  array( 'Msj' => 'Se ha actualizado el programa exitosamente.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $programa = Programas::find($id);
        $programa->delete();
        return array('Msj'=>'Se ha eliminado el programa con éxito.');
    }

    public function importar(importarProgramas $request){
        $file = $request->file('file');
        Excel::import(new ProgramasImport, $file);
        return array('Msj' => 'Se ha importado la informacion de programas con éxito.');
    }
}
