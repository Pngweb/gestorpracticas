<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use illuminate\Support\Facades\Auth;
use App\menu;
use App\Dependencias;
use App\EmpresaMensajeria;
use App\Sucursal;
use App\CuposPractica;
use App\Departamentos;
use App\ConfiguracionPasos;
use App\User;

class EstudiantesSeleccionadosInfoController extends Controller
{
    
	public function __construct()
    {
        $this->middleware('auth');
    }

    public function index($id,$idseleccionado){
    	$menus = menu::menu_submenu(Auth::user()->rol_id);
    	$deptos = Departamentos::where('depto_estado',true)->get();
        $empresa = Auth::user()->tercero;
        $val_mensajeria = false;
        if($empresa->empresa_mensajeria){
            $val_mensajeria = true;
        }
        $mensajeria = EmpresaMensajeria::where('empresa_id', $empresa->empresa_id)->get()->count();
        $dependencias = Dependencias::where('empresa_id', $empresa->empresa_id)->get()->count();
        $sucursales = Sucursal::where('empresa_id', $empresa->empresa_id)->get()->count();
        $cupos = CuposPractica::where('empresa_id', $empresa->empresa_id)->get()->count();
        $pasos = ConfiguracionPasos::where('empresa_id', $empresa->empresa_id)->get()->count();
        $idestudiante = $id;
        $representantes = User::where([['tercero_id', $empresa->empresa_id],['cargo_id',11]])->get()->count();

        return view('estudiantesSeleccionadosInfo', compact(['menus','deptos','val_mensajeria','mensajeria','dependencias','sucursales','cupos','pasos','idestudiante','idseleccionado','representantes']));
    }

}
