<?php

namespace App\Http\Controllers;

use Mail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\menu;
use App\EmpresaInstitucion;
use App\Institucion;
use App\Empresa;
use App\EmpresaMensajeria;
use App\Dependencias;
use App\Sucursal;
use App\CuposPractica;
use App\ConfiguracionPasos;
use App\User;
use App\DocumentosLegales;

class ConveniosController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $menus = menu::menu_submenu(Auth::user()->rol_id);
        if(Auth::user()->rol_id == 2){
            return view('convenios', compact('menus'));
        }else{
            $empresa = Auth::user()->tercero;
            $val_mensajeria = false;
            if($empresa->empresa_mensajeria){
                $val_mensajeria = true;
            }
            $mensajeria = EmpresaMensajeria::where('empresa_id', $empresa->empresa_id)->get()->count();
            $dependencias = Dependencias::where('empresa_id', $empresa->empresa_id)->get()->count();
            $sucursales = Sucursal::where('empresa_id', $empresa->empresa_id)->get()->count();
            $cupos = CuposPractica::where('empresa_id', $empresa->empresa_id)->get()->count();
            $pasos = ConfiguracionPasos::where('empresa_id', $empresa->empresa_id)->get()->count();
            $representantes = User::where([['tercero_id', $empresa->empresa_id],['cargo_id',11]])->get()->count();
            $documentos = DocumentosLegales::where('documento_estado',true)->get();
            return view('conveniosEmpresas', compact(['menus','val_mensajeria','mensajeria','dependencias','sucursales','cupos','pasos','representantes','documentos']));
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $exists = EmpresaInstitucion::where([['empresa_id',$request->empresa],['institucion_id',$request->institucion]])->get();
        if($exists->count() == 0){
            $convenio = new EmpresaInstitucion;
            $convenio->empresa_id = $request->empresa;
            $convenio->institucion_id = $request->institucion;
            $convenio->empresa_institucion_estado = 1;
            $convenio->created_user = Auth::User()->id;
        }else{
            $convenio = EmpresaInstitucion::find($exists[0]->empresa_institucion_id);
            $convenio->empresa_institucion_estado = 1;
            $convenio->created_user = Auth::User()->id;
        }
        if($convenio->save()){
            if(Auth::User()->rol_id == 2){
                Mail::send('emails.emailSolicitudConvenioEmpresas',['institucion' => Institucion::find($convenio->institucion_id)], function($message){
                    $message->to('abarros@pngtechnology.co','Andres Barros')->subject('Solicitud de Convenio');
                });
            }else{
                Mail::send('emails.emailSolicitudConvenioInstitucion',['empresa' => Empresa::find($convenio->empresa_id)], function($message){
                    $message->to('abarros@pngtechnology.co','Andres Barros')->subject('Solicitud de Convenio');
                });
            }
            $resultado = array('ErrorStatus'=>false,'Msj'=>'Se ha registrado el convenio con éxito.');
        }else{
            $resultado = array('ErrorStatus'=>true,'Msj'=>'Error al registrar el convenio.');
        }
        return $resultado;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $convenio = EmpresaInstitucion::find($id);
        $convenio->empresa_institucion_estado = $request->estado;
        if($request->estado == 2){
            $msj = "Se ha aprobado el convenio con éxito";
        }elseif($request->estado == 3){
            $msj = "Se ha rechazado la solicitud de convenio con éxito";
        }elseif($request->estado == 4){
            $msj = "Se ha cancelado el convenio con éxito";
        }
        if($convenio->save()){
            $resultado = array('ErrorStatus'=>false,'Msj'=>$msj);
        }else{
            $resultado = array('ErrorStatus'=>true,'Msj'=>'Error al cancelar el convenio.');
        }
        return $resultado;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // 
    }

    public function guardarDocumento(Request $request, $id){
        $convenio = EmpresaInstitucion::find($id);
        $convenio->documento_empresa_id = $request->cbArchivo;
        $convenio->save();
        return array('Msj'=>'Se registró el documento con el convenio exitosamente.');
    }

}
