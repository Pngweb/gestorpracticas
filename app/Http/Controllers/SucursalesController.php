<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use illuminate\Support\Facades\Auth;
use App\Http\Requests\createSucursalRequest;
use App\menu;
use App\Departamentos;
use App\Sucursal;
use App\EmpresaMensajeria;
use App\Dependencias;
use App\CuposPractica;
use App\ConfiguracionPasos;
use App\User;


class SucursalesController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $menus = menu::menu_submenu(Auth::user()->rol_id);
        $deptos = Departamentos::where('depto_estado',true)->get();
        $empresa = Auth::user()->tercero;
        $val_mensajeria = false;
        if($empresa->empresa_mensajeria){
            $val_mensajeria = true;
        }
        $mensajeria = EmpresaMensajeria::where('empresa_id', $empresa->empresa_id)->get()->count();
        $dependencias = Dependencias::where('empresa_id', $empresa->empresa_id)->get()->count();
        $sucursales = Sucursal::where('empresa_id', $empresa->empresa_id)->get()->count();
        $cupos = CuposPractica::where('empresa_id', $empresa->empresa_id)->get()->count();
        $pasos = ConfiguracionPasos::where('empresa_id', $empresa->empresa_id)->get()->count();
        $representantes = User::where([['tercero_id', $empresa->empresa_id],['cargo_id',11]])->get()->count();

        return view('sucursales', compact(['menus','deptos','val_mensajeria','mensajeria','dependencias','sucursales','cupos','pasos','representantes']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(createSucursalRequest $request)
    {
        $sucursal = new Sucursal;
        $sucursal->sucursal_descripcion = strtoupper($request->txtNombre);
        $sucursal->sucursal_telefono = $request->txtTelefono;
        $sucursal->sucursal_direccion = $request->txtDireccion;
        $sucursal->ciudad_id = $request->cbCiudad;
        $sucursal->empresa_id = Auth::user()->tercero_id;
        $sucursal->sucursal_estado = true;
        $sucursal->save();
        return  array( 'Msj' => 'Se ha guardado la sucursal exitosamente.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return Sucursal::select('empresa_sucursal.*','ciudad.depto_id')
                                        ->join('ciudad','empresa_sucursal.ciudad_id','=','ciudad.ciudad_id')
                                        ->join('departamentos','ciudad.depto_id','=','departamentos.depto_id')
                                        ->where('sucursal_id', $id)->get()->first();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(createSucursalRequest $request, $id)
    {
        $sucursal = Sucursal::find($id);
        $sucursal->sucursal_descripcion = strtoupper($request->txtNombre);
        $sucursal->sucursal_telefono = $request->txtTelefono;
        $sucursal->sucursal_direccion = $request->txtDireccion;
        $sucursal->ciudad_id = $request->cbCiudad;
        $sucursal->sucursal_estado = ($request->chkEstado == 'on') ? true : false ;
        $sucursal->save();
        return  array( 'Msj' => 'Se ha actualizado la sucursal exitosamente.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $sucursal = Sucursal::find($id);
        $sucursal->delete();
        return array('Msj'=>'Se ha eliminado la sucursal con éxito.');
    }
}
