<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use illuminate\Support\Facades\Auth;
use App\menu;
use App\Dependencias;
use App\EmpresaMensajeria;
use App\Sucursal;
use App\CuposPractica;
use App\Institucion;
use App\Area;
use App\Departamentos;
use App\ConfiguracionPasos;
use App\User;

class EstudiantesPreseleccionadosController extends Controller
{
    

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
    	$menus = menu::menu_submenu(Auth::user()->rol_id);
    	$deptos = Departamentos::where('depto_estado',true)->get();
        $empresa = Auth::user()->tercero;
        $val_mensajeria = false;
        if($empresa->empresa_mensajeria){
            $val_mensajeria = true;
        }
        $mensajeria = EmpresaMensajeria::where('empresa_id', $empresa->empresa_id)->get()->count();
        $dependencias = Dependencias::where('empresa_id', $empresa->empresa_id)->get()->count();
        $sucursales = Sucursal::where('empresa_id', $empresa->empresa_id)->get()->count();
        $cupos = CuposPractica::where('empresa_id', $empresa->empresa_id)->get()->count();
        $pasos = ConfiguracionPasos::where('empresa_id', $empresa->empresa_id)->get()->count();
        $representantes = User::where([['tercero_id', $empresa->empresa_id],['cargo_id',11]])->get()->count();
        $instituciones = Institucion::select('institucion.*')
                                        ->join('empresa_institucion','institucion.institucion_id','=','empresa_institucion.institucion_id')
                                         ->where([['empresa_institucion.empresa_id', $empresa->empresa_id],['empresa_institucion.empresa_institucion_estado',2]])
                                        ->get();
        $areas = Area::where('area_estado',true)->get();
        return view('estudiantesPreseleccionados', compact(['menus','deptos','val_mensajeria','mensajeria','dependencias','sucursales','cupos','instituciones','areas','pasos','representantes']));
    }

}
