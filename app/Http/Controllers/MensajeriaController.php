<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\createUserMensajeriaRequest;
use App\menu;
use App\SistemasMensajeria;
use App\EmpresaMensajeria;
use App\Dependencias;
use App\Sucursal;
use App\CuposPractica;
use App\ConfiguracionPasos;
use App\User;

class MensajeriaController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $menus = menu::menu_submenu(Auth::user()->rol_id);
        $sistemasMensajeria = SistemasMensajeria::where('mensajeria_estado',true)->get();
        $empresa = Auth::user()->tercero;
        $val_mensajeria = false;
        if($empresa->empresa_mensajeria){
            $val_mensajeria = true;
        }
        $mensajeria = EmpresaMensajeria::where('empresa_id', $empresa->empresa_id)->get()->count();
        $dependencias = Dependencias::where('empresa_id', $empresa->empresa_id)->get()->count();
        $sucursales = Sucursal::where('empresa_id', $empresa->empresa_id)->get()->count();
        $cupos = CuposPractica::where('empresa_id', $empresa->empresa_id)->get()->count();
        $pasos = ConfiguracionPasos::where('empresa_id', $empresa->empresa_id)->get()->count();
        $representantes = User::where([['tercero_id', $empresa->empresa_id],['cargo_id',11]])->get()->count();

        return view('mensajeria', compact(['menus','sistemasMensajeria','val_mensajeria','mensajeria','dependencias','sucursales','cupos','pasos','representantes']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(createUserMensajeriaRequest $request)
    {
        $user_mensajeria = new EmpresaMensajeria;
        $user_mensajeria->empresa_id = Auth::user()->tercero_id;
        $user_mensajeria->mensajeria_id = $request->cbSistemaMensajeria;
        $user_mensajeria->mensajeria_usuario = $request->txtUsuario;
        $user_mensajeria->save();
        return  array( 'Msj' => 'Se ha guardado el usuario de mensajeria exitosamente.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return EmpresaMensajeria::find($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(createUserMensajeriaRequest $request, $id)
    {
        $user_mensajeria = EmpresaMensajeria::find($id);
        $user_mensajeria->mensajeria_id = $request->cbSistemaMensajeria;
        $user_mensajeria->mensajeria_usuario = $request->txtUsuario;
        $user_mensajeria->save();
        return  array( 'Msj' => 'Se ha actualizado el usuario de mensajeria exitosamente.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user_mensajeria = EmpresaMensajeria::find($id);
        $user_mensajeria->delete();
         return  array( 'Msj' => 'Se ha eliminado el usuario de mensajeria exitosamente.');
    }
}
