<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use Maatwebsite\Excel\Facades\Excel;
use App\Http\Requests\createEstudianteRequest;
use App\Http\Requests\importarEstudiantes;
use App\Imports\EstudiantesImport;
use Carbon\Carbon;
use App\menu;
use App\GruposSanguineos;
use App\Departamentos;
use App\Estudiante;
use App\User;
use App\Cargos;
use App\InstitucionEstudiante;

class EstudianteController extends Controller
{

    public function __construct()
    {
        if(Route::currentRouteName() == 'estudiantes.index'){
            $this->middleware('auth');
        }
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $grupos = GruposSanguineos::get();
        $deptos = Departamentos::where('depto_estado',true)->get();
        if(Route::currentRouteName() == 'registroEstudiantes'){
            return view('registroEstudiantes');
        }else{
            $menus = menu::menu_submenu(Auth::user()->rol_id);
            return view('estudiantes', compact('menus','grupos','deptos'));
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(createEstudianteRequest $request)
    {
        $imagen = $request->file('logo');
        $estudiante = new Estudiante;
        if($imagen){
            $image_name = rand() . '.' . $imagen->getClientOriginalExtension();
            $imagen->move(public_path('images/images_estudiantes'), $image_name);
            $estudiante->estudiante_foto = $image_name;
        }
        $fecha = explode('/',$request->txtFechaNac);
        $estudiante->estudiante_identificacion = $request->txtIdentificacion;
        $estudiante->estudiante_nombres = strtoupper($request->txtNombres);
        $estudiante->estudiante_apellidos = strtoupper($request->txtApellidos);
        $estudiante->estudiante_sexo = $request->cbSexo;
        $estudiante->estudiante_fecha_nacimiento = $fecha[2].'-'.$fecha[1].'-'.$fecha[0];
        $estudiante->estudiante_direccion = strtoupper($request->txtDireccion);
        $estudiante->ciudad_id = $request->cbCiudad;
        $estudiante->estudiante_email = $request->txtEmail;
        $estudiante->estudiante_telefono = $request->txtTelefono;
        $estudiante->grupo_sanguineo_id = $request->cbGrupoSanguineo;
        $estudiante->estudiante_estado = true;
        $estudiante->created_user = Auth::user()->id;
        if($estudiante->save()){
            $user = new User;
            $user->identificacion = $request->txtIdentificacion;
            $user->ciudad_id_expedicion = $request->cbCiudad;
            $user->password = bcrypt($request->txtIdentificacion);
            $user->user_nombre = strtoupper($request->txtNombres.' '.$request->txtApellidos);
            $user->tercero_id = $estudiante->estudiante_id;
            $user->rol_id = 4;
            $user->cargo_id = Cargos::where('cargo_descripcion','ESTUDIANTE')->get()->first()->cargo_id;
            $user->user_email = $request->txtEmail;
            $user->user_telefono = $request->txtTelefono;
            $user->user_cumpleanos = $fecha[2].'-'.$fecha[1].'-'.$fecha[0];
            $user->save();
            if(Auth::user()->rol_id == 2){
                $institucion_estudiante = new InstitucionEstudiante;
                $institucion_estudiante->institucion_id = Auth::user()->tercero_id;
                $institucion_estudiante->estudiante_id = $estudiante->estudiante_id;
                $institucion_estudiante->inst_est_fecha_inicio = Carbon::now();
                $institucion_estudiante->inst_est_estado = true;
                $institucion_estudiante->save();
            }
            $resultado = array('ErrorStatus'=>false,'Msj'=>'Se ha registrado el estudiante con éxito.');
        }else{
            $resultado = array('ErrorStatus'=>true,'Msj'=>'Error al registrar el estudiante.');
        }
        return $resultado;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return Estudiante::select('estudiantes.*','ciudad.depto_id')
                                            ->join('ciudad','estudiantes.ciudad_id','=','ciudad.ciudad_id')
                                            ->join('departamentos','ciudad.depto_id','=','departamentos.depto_id')
                                            ->where('estudiantes.estudiante_id',$id)->get()[0];
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(createEstudianteRequest $request, $id)
    {
        $imagen = $request->file('logo');
        $estudiante = Estudiante::find($id);
        if($imagen){
            if($estudiante->estudiante_foto){
                unlink(public_path().'/images/images_estudiantes/'.$estudiante->estudiante_foto);
            }
            $image_name = rand() . '.' . $imagen->getClientOriginalExtension();
            $imagen->move(public_path('images/images_estudiantes'), $image_name);
            $estudiante->estudiante_foto = $image_name;
        }else{
            if($request->hdRemImg == "true"){
                unlink(public_path().'/images/images_estudiantes/'.$estudiante->estudiante_foto);
                $estudiante->estudiante_foto = null;
            }
        }
        $fecha = explode('/',$request->txtFechaNac);
        $estudiante->estudiante_identificacion = $request->txtIdentificacion;
        $estudiante->estudiante_nombres = strtoupper($request->txtNombres);
        $estudiante->estudiante_apellidos = strtoupper($request->txtApellidos);
        $estudiante->estudiante_sexo = $request->cbSexo;
        $estudiante->estudiante_fecha_nacimiento = $fecha[2].'-'.$fecha[1].'-'.$fecha[0];
        $estudiante->estudiante_direccion = strtoupper($request->txtDireccion);
        $estudiante->ciudad_id = $request->cbCiudad;
        $estudiante->estudiante_email = $request->txtEmail;
        $estudiante->estudiante_telefono = $request->txtTelefono;
        $estudiante->grupo_sanguineo_id = $request->cbGrupoSanguineo;
        if($estudiante->save()){
            $resultado = array('ErrorStatus'=>false,'Msj'=>'Se ha actualizado el estudiante con éxito.');
        }else{
            $resultado = array('ErrorStatus'=>true,'Msj'=>'Error al actualizar el estudiante.');
        }
        return $resultado;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $estudiante = Estudiante::find($id);
        if($estudiante->delete()){
            if($estudiante->estudiante_foto){
                unlink(public_path().'/images/images_estudiantes/'.$estudiante->estudiante_foto);
            }
            $resultado = array('ErrorStatus'=>false,'Msj'=>'Se ha eliminado el estudiante con éxito.');
        }else{
            $resultado = array('ErrorStatus'=>true,'Msj'=>'Error al eliminar el estudiante.');
        }
        return $resultado;
    }

    public function importar(importarEstudiantes $request){
        $file = $request->file('file');
        Excel::import(new EstudiantesImport, $file);
        return array('Msj' => 'Se ha importado la informacion de estudiantes con éxito.');
    }
}
