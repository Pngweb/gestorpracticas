<?php

namespace App\Http\Controllers;

use Mail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Requests\createEmpresaRequest;
use App\Http\Requests\validateEmailInvitacionRequest;
use App\menu;
use App\OrganizacionJuridica;
use App\SectorProductivo;
use App\JornadaTrabajo;
use App\Empresa;
use App\Cargos;
use App\Departamentos;
use App\Imports\EmpresasImport;

class EmpresasController extends Controller
{

    public function __construct()
    {
        if(Route::currentRouteName() == 'empresas.index'){
            $this->middleware('auth');
        }
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $organizaciones = OrganizacionJuridica::where('org_jur_estado',true)->get();
        $sectores = SectorProductivo::where('sec_prod_estado',true)->get();
        $jornadas = JornadaTrabajo::where('jorn_trab_estado',true)->get();

        if(Route::currentRouteName() == 'registroEmpresas'){
            return view('registroEmpresas',compact(['organizaciones','sectores','jornadas']));
        }else{
            $menus = menu::menu_submenu(Auth::user()->rol_id);
            $cargos = Cargos::where([['cargo_estado',true],['cargo_visible',true]])->get();
            $deptos = Departamentos::where('depto_estado',true)->get();
            return view('empresas', compact('menus','organizaciones','sectores','jornadas','cargos','deptos'));
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(createEmpresaRequest $request)
    {
        $imagen = $request->file('logo');
        $empresa = new Empresa;
        if($imagen){
            $image_name = rand() . '.' . $imagen->getClientOriginalExtension();
            $imagen->move(public_path('images/images_empresas'), $image_name);
            $empresa->empresa_logo = $image_name;
        }
        $empresa->empresa_nombre = strtoupper($request->txtNombre);
        $empresa->empresa_nit = $request->txtNit;
        $empresa->empresa_web = $request->txtSitioWeb;
        $empresa->empresa_mensajeria = $request->cbMensajeria;
        $empresa->org_jur_id = $request->cbOrgJuridica;
        $empresa->sec_prod_id = $request->cbSectorProd;
        $empresa->jorn_trab_id = $request->cbJornada;
        $empresa->empresa_objeto_social = strtoupper($request->txtObjSocial);
        $empresa->empresa_no_empleados = $request->txtNoEmpleados;
        if(Auth::check()){
            $empresa->created_user = Auth::user()->id;
        }else{
             $empresa->created_user = 0;
        }
        if($empresa->save()){
            $resultado = array('ErrorStatus'=>false,'Msj'=>'Se ha registrado la empresa con éxito.');
        }else{
            $resultado = array('ErrorStatus'=>true,'Msj'=>'Error al registrar la empresa.');
        }
        return $resultado;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return Empresa::find($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(createEmpresaRequest $request, $id)
    {
        $imagen = $request->file('logo');
        $empresa = Empresa::find($id);
        if($imagen){
            if($empresa->empresa_logo){
                unlink(public_path().'/images/images_empresas/'.$empresa->empresa_logo);
            }
            $image_name = rand() . '.' . $imagen->getClientOriginalExtension();
            $imagen->move(public_path('images/images_empresas'), $image_name);
            $empresa->empresa_logo = $image_name;
        }else{
            if($request->hdRemImg == "true"){
                unlink(public_path().'/images/images_empresas/'.$empresa->empresa_logo);
                $empresa->empresa_logo = null;
            }
        }
        $empresa->empresa_nombre = strtoupper($request->txtNombre);
        $empresa->empresa_nit = $request->txtNit;
        $empresa->empresa_web = $request->txtSitioWeb;
        $empresa->empresa_mensajeria = $request->cbMensajeria;
        $empresa->org_jur_id = $request->cbOrgJuridica;
        $empresa->sec_prod_id = $request->cbSectorProd;
        $empresa->jorn_trab_id = $request->cbJornada;
        $empresa->empresa_objeto_social = strtoupper($request->txtObjSocial);
        $empresa->empresa_no_empleados = $request->txtNoEmpleados;
        if($empresa->save()){
            $resultado = array('ErrorStatus'=>false,'Msj'=>'Se ha actualizado la empresa con éxito.');
        }else{
            $resultado = array('ErrorStatus'=>true,'Msj'=>'Error al actualizar la empresa.');
        }
        return $resultado;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $empresa = Empresa::find($id);
        if($empresa->delete()){
            unlink(public_path().'/images/images_empresas/'.$empresa->empresa_logo);
            $resultado = array('ErrorStatus'=>false,'Msj'=>'Se ha eliminado la empresa con éxito.');
        }else{
            $resultado = array('ErrorStatus'=>true,'Msj'=>'Error al eliminar la empresa.');
        }
        return $resultado;
    }

    public function mailInvitacion(validateEmailInvitacionRequest $request)
    {
        $email = $request->txtEmail;
        Mail::send('emails.emailInvitacionEmpresa',[], function($message) use($email){
            $message->to($email)->subject('Invitación  Avatar');
        });

        return array('Msj'=>'Se envio el correo con exito');

    }

    public function importar(Request $request)
    {
        return \Excel::import(new EmpresasImport , $request->file('excelEmpresas'));

    }
}
