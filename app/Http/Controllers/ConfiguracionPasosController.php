<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\createConfiguracionPasos;
use App\menu;
use App\Dependencias;
use App\EmpresaMensajeria;
use App\Sucursal;
use App\CuposPractica;
use App\User;
use App\PasosXdependencias;
use App\ConfiguracionPasos;

class ConfiguracionPasosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $menus = menu::menu_submenu(Auth::user()->rol_id);
        $empresa = Auth::user()->tercero;
        $val_mensajeria = false;
        if($empresa->empresa_mensajeria){
            $val_mensajeria = true;
        }
        $mensajeria = EmpresaMensajeria::where('empresa_id', $empresa->empresa_id)->get()->count();
        $dependencias = Dependencias::where('empresa_id', $empresa->empresa_id)->get()->count();
        $sucursales = Sucursal::where('empresa_id', $empresa->empresa_id)->get()->count();
        $cupos = CuposPractica::where('empresa_id', $empresa->empresa_id)->get()->count();
        $funcionarios = User::where('tercero_id', $empresa->empresa_id)->get();
        $pasos = ConfiguracionPasos::where('empresa_id', $empresa->empresa_id)->get()->count();
         $representantes = User::where([['tercero_id', $empresa->empresa_id],['cargo_id',11]])->get()->count();
        return view('configuracionPasos', compact(['menus','val_mensajeria','mensajeria','dependencias','sucursales','cupos','funcionarios','pasos','representantes']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(createConfiguracionPasos $request)
    {
        //
        $empresa = Auth::user()->tercero;
        $pasos = new ConfiguracionPasos;

        $pasos->pasos_nombre = $request->txtNombre;
        $pasos->pasos_tipo   = $request->tipo;
        $pasos->pasos_tiempo = $request->tiempo;
        $pasos->user_id      = $request->responsable;
        $pasos->pasos_orden  = $request->orden;
        $pasos->Descripcion  = $request->descripcion;
        $pasos->empresa_id   = $empresa->empresa_id;
        $pasos->pasos_estado = ($request->chkEstado == 'on' ? 1 : 0);

        if($pasos->save()){
            $dependencias = explode(',',$request->CRid_dependencias);
            $ultimo = DB::table('configuracion_pasos')->latest()->first();
            $idpasos = $ultimo->pasos_id;
              foreach ($dependencias as $dep ) {
                $vbdependencia = '';
                $vbdependencia = 'depen'.$dep;
                
                    $depen = new PasosXdependencias;
                    $depen->pasos_id = $idpasos;
                    $depen->dependencia_id = $dep;
                    $depen->pasosdep_estado = ($request->$vbdependencia== 'on' ? 1 : 0);
                    $depen->empresa_id = $empresa->empresa_id;
                    if($depen->save()){

                    }else{
                     $resultado = array('ErrorStatus'=>true,'Msj'=>'Error al registrar la configuración.');
                    }
            }


            $resultado = array('ErrorStatus'=>false,'Msj'=>'Se ha registrado la configuración con éxito.');
        }else{
            $resultado = array('ErrorStatus'=>true,'Msj'=>'Error al registrar la configuración.');
        }
        return $resultado;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $respuesta = ConfiguracionPasos::select('configuracion_pasos.*','pasos_por_dependencias.*')->leftjoin('pasos_por_dependencias','configuracion_pasos.pasos_id','=','pasos_por_dependencias.pasos_id')->where('configuracion_pasos.pasos_id',$id)->get();
        return $respuesta;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $empresa = Auth::user()->tercero;
        $pasos = ConfiguracionPasos::find($id);

        $pasos->pasos_nombre = $request->ACtxtNombre;
        $pasos->pasos_tipo   = $request->ACtipo;
        $pasos->pasos_tiempo = $request->ACtiempo;
        $pasos->user_id      = $request->ACresponsable;
        $pasos->pasos_orden  = $request->ACorden;
        $pasos->Descripcion  = $request->ACdescripcion;
        $pasos->empresa_id   = $empresa->empresa_id;
        $pasos->pasos_estado = ($request->ACchkEstado == 'on' ? 1 : 0);
        if($pasos->save()){
            $dependencias = explode(',',$request->id_dependencias);

            $ultimo = DB::table('configuracion_pasos')->latest()->first();
            $idpasos = $ultimo->pasos_id;
            // $i = 1;
            foreach ($dependencias as $dep ) {
                 $vbdependencia = '';
                $vbdependencia = 'ACdepen'.$dep;

                $existe = PasosXdependencias::where([['pasos_id',$id],['dependencia_id',$dep],['empresa_id',$empresa->empresa_id]])->get();
                // return $existe->count();
                if($existe->count() == 0){
                    $depen = new PasosXdependencias;
                    $depen->pasos_id = $idpasos;
                    $depen->dependencia_id = $dep;
                    $depen->pasosdep_estado = ($request->$vbdependencia== 'on' ? 1 : 0);
                    $depen->empresa_id = $empresa->empresa_id;
                    if($depen->save()){

                    }else{

                    }
                }else{
                    $depen = PasosXdependencias::find($existe[0]->pasosdep_id);
                    $depen->pasos_id = $idpasos;
                    $depen->dependencia_id = $dep;
                    $depen->pasosdep_estado = ($request->$vbdependencia == 'on' ? 1 : 0);
                    if($depen->save()){

                    }else{
                        $resultado = array('ErrorStatus'=>true,'Msj'=>'Error al actualizar la configuración.');
                    }
                }
               
                // $i++;
            }

            $resultado = array('ErrorStatus'=>false,'Msj'=>'Se ha actualizado la configuración con éxito.');
        }else{
            $resultado = array('ErrorStatus'=>true,'Msj'=>'Error al actualizar la configuración.');
        }
        return $resultado;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $pasos = ConfiguracionPasos::find($id);
        $pasos->delete();
        return array('Msj'=>'Se ha eliminado la configuración con éxito.');
    }
}
