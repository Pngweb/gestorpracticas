<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;
use App\Imports\CuposPracticasImport;
use App\Http\Requests\createCuposPracticasRequest;
use App\menu;
use App\EmpresaMensajeria;
use App\Dependencias;
use App\Sucursal;
use App\CuposPractica;
use App\Cargos;
use App\User;
use App\ConfiguracionPasos;

class CuposPracticasController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $menus = menu::menu_submenu(Auth::user()->rol_id);
        $empresa = Auth::user()->tercero;
        $no_empleados = $empresa->empresa_no_empleados;
        $val_mensajeria = false;
        if($empresa->empresa_mensajeria){
            $val_mensajeria = true;
        }
        $mensajeria = EmpresaMensajeria::where('empresa_id', $empresa->empresa_id)->get()->count();
        $dependencias_combo = Dependencias::where('empresa_id', $empresa->empresa_id)->get();
        $sucursales_combo = Sucursal::where('empresa_id', $empresa->empresa_id)->get();
        $cupos = CuposPractica::where('empresa_id', $empresa->empresa_id)->get()->count();
        $dependencias = $dependencias_combo->count();
        $sucursales = $sucursales_combo->count();
        $cargos = Cargos::where([['cargo_estado',true],['cargo_visible',true]])->get();
        $maestros = User::where([['tercero_id', Auth::user()->tercero_id],[ 'rol_id',3]])->get();
        $pasos = ConfiguracionPasos::where('empresa_id', $empresa->empresa_id)->get()->count();
        $representantes = User::where([['tercero_id', $empresa->empresa_id],['cargo_id',11]])->get()->count();

        return view('cuposPracticas', compact(['menus','val_mensajeria','mensajeria','dependencias','sucursales','cupos','dependencias_combo','sucursales_combo','cargos','maestros','pasos','no_empleados','representantes']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(createCuposPracticasRequest $request)
    {
        $cupo = new CuposPractica;
        $cupo->dependencia_id = $request->cdDependencia;
        $cupo->sucursal_id = $request->cbSucursal;
        $cupo->cargo_id = $request->cbCargo;
        $cupo->maestro_id = $request->cbMaestro;
        $cupo->link_monitoreo = $request->txtMonitoreo;
        $cupo->cupo_competencias = strtoupper($request->txtCompetencias);
        $cupo->empresa_id = Auth::user()->tercero_id;
        $cupo->save();
        return  array( 'Msj' => 'Se ha guardado el cupo exitosamente.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return CuposPractica::find($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(createCuposPracticasRequest $request, $id)
    {
        $cupo = CuposPractica::find($id);
        $cupo->dependencia_id = $request->cdDependencia;
        $cupo->sucursal_id = $request->cbSucursal;
        $cupo->cargo_id = $request->cbCargo;
        $cupo->maestro_id = $request->cbMaestro;
        $cupo->link_monitoreo = $request->txtMonitoreo;
        $cupo->cupo_competencias = strtoupper($request->txtCompetencias);
        $cupo->save();
        return  array( 'Msj' => 'Se ha actualizado el cupo exitosamente.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $cupo = CuposPractica::find($id);
        $cupo->delete();
        return array('Msj'=>'Se ha eliminado el cupo con éxito.');
    }

    public function importar(Request $request){
        $file = $request->file('excelCupos');
        // return $file;
        Excel::import(new CuposPracticasImport, $file);
        return array('Msj' => 'Se realizo la importacion de los cupos de practica exitosamente.');
    }
}
