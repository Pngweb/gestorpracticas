<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use illuminate\Support\Facades\Auth;
use App\Http\Requests\createDocumentoEmpresaRequest;
use Carbon\Carbon;
use App\menu;
use App\Dependencias;
use App\EmpresaMensajeria;
use App\Sucursal;
use App\CuposPractica;
use App\DocumentosLegales;
use App\DocumentosEmpresa;
use App\ConfiguracionPasos;
use App\User;

class DocumentosLegalesController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $menus = menu::menu_submenu(Auth::user()->rol_id);
        $empresa = Auth::user()->tercero;
        $val_mensajeria = false;
        if($empresa->empresa_mensajeria){
            $val_mensajeria = true;
        }
        $mensajeria = EmpresaMensajeria::where('empresa_id', $empresa->empresa_id)->get()->count();
        $dependencias = Dependencias::where('empresa_id', $empresa->empresa_id)->get()->count();
        $sucursales = Sucursal::where('empresa_id', $empresa->empresa_id)->get()->count();
        $cupos = CuposPractica::where('empresa_id', $empresa->empresa_id)->get()->count();
        $documentos = DocumentosLegales::where('documento_estado',true)->get();
        $pasos = ConfiguracionPasos::where('empresa_id', $empresa->empresa_id)->get()->count();
        $representantes = User::where([['tercero_id', $empresa->empresa_id],['cargo_id',11]])->get()->count();

        return view('documentosLegales', compact(['menus','val_mensajeria','mensajeria','dependencias','sucursales','cupos','documentos','pasos','representantes']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(createDocumentoEmpresaRequest $request)
    {
        $file = $request->file('file_documento');
        $file_name = str_replace(' ', '', str_replace(':', '', str_replace('-','',Carbon::now()).'-'.$file->getClientOriginalName()));
        $existe_doc = DocumentosEmpresa::where('documento_id', $request->cbDocumento)->get();
        // return $existe_doc;
        if($existe_doc->count() ==  0){
            $documento = new DocumentosEmpresa;
            $documento->documento_id = $request->cbDocumento;
            $documento->empresa_id = Auth::user()->tercero_id;
            $documento->documento_file = $file_name;
        }else{
            $documento = DocumentosEmpresa::find($existe_doc->first()->doc_emp_id);
            unlink(public_path().'/documentos_empresas/'.$documento->documento_file);
            $documento->documento_id = $request->cbDocumento;
            $documento->documento_file = $file_name;
        }
        if($documento->save()){
            $file->move(public_path('documentos_empresas'), $file_name);
            return  array( 'Msj' => 'Se ha guardado el documento exitosamente.');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $documento = DocumentosEmpresa::find($id);
        unlink(public_path().'/documentos_empresas/'.$documento->documento_file);
        $documento->delete();
        return array('Msj'=>'Se ha eliminado el documento con éxito.');
    }
}
