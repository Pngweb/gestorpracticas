<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\createMiPerfilRequest;
use App\menu;
use App\OrganizacionJuridica;
use App\SectorProductivo;
use App\JornadaTrabajo;
use App\EmpresaMensajeria;
use App\Dependencias;
use App\Sucursal;
use App\CuposPractica;
use App\ConfiguracionPasos;
use App\User;

class MiPerfilController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $menus = menu::menu_submenu(Auth::user()->rol_id);
        $organizaciones = OrganizacionJuridica::where('org_jur_estado',true)->get();
        $sectores = SectorProductivo::where('sec_prod_estado',true)->get();
        $jornadas = JornadaTrabajo::where('jorn_trab_estado',true)->get();
        $empresa = Auth::user()->tercero;
        $val_mensajeria = false;
        if($empresa->empresa_mensajeria){
            $val_mensajeria = true;
        }
        $mensajeria = EmpresaMensajeria::where('empresa_id', $empresa->empresa_id)->get()->count();
        $dependencias = Dependencias::where('empresa_id', $empresa->empresa_id)->get()->count();
        $sucursales = Sucursal::where('empresa_id', $empresa->empresa_id)->get()->count();
        $cupos = CuposPractica::where('empresa_id', $empresa->empresa_id)->get()->count();
        $pasos = ConfiguracionPasos::where('empresa_id', $empresa->empresa_id)->get()->count();
        $representantes = User::where([['tercero_id', $empresa->empresa_id],['cargo_id',11]])->get()->count();

        return view('miPerfil', compact(['menus','organizaciones','sectores','jornadas','empresa','val_mensajeria','mensajeria','dependencias','sucursales','cupos','pasos','representantes']));
    }

    public function update(createMiPerfilRequest $request){
        $imagen = $request->file('logo');
        $empresa = Auth::user()->tercero;
        if($imagen){
            if($empresa->empresa_logo){
                unlink(public_path().'/images/images_empresas/'.$empresa->empresa_logo);
            }
            $image_name = rand() . '.' . $imagen->getClientOriginalExtension();
            $imagen->move(public_path('images/images_empresas'), $image_name);
            $empresa->empresa_logo = $image_name;
        }else{
            if($request->hdRemImg == "true"){
                unlink(public_path().'/images/images_empresas/'.$empresa->empresa_logo);
                $empresa->empresa_logo = null;
            }
        }
        $empresa->empresa_nombre = strtoupper($request->txtNombre);
        $empresa->empresa_nit = $request->txtNit;
        $empresa->empresa_web = $request->txtSitioWeb;
        $empresa->org_jur_id = $request->cbOrgJuridica;
        $empresa->sec_prod_id = $request->cbSectorProd;
        $empresa->jorn_trab_id = $request->cbJornada;
        $empresa->empresa_objeto_social = strtoupper($request->txtObjSocial);
        $empresa->empresa_no_empleados = $request->txtNoEmpleados;
        $empresa->empresa_conglomerado = $request->cbConglomerado;
        $empresa->save();
        return array('Msj' => 'Se ha actualizado la información de su perfil.');
    }
}
