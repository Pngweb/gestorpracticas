<?php

namespace App\Http\Controllers;

use Mail;
use Illuminate\Http\Request;
use illuminate\Support\Facades\Auth;
use App\Http\Requests\createSolicitudCupoRequest;
use App\Http\Requests\CreateSolicitudRemCupo;
use App\menu;
use App\Dependencias;
use App\EmpresaMensajeria;
use App\Sucursal;
use App\CuposPractica;
use App\Institucion;
use App\Area;
use App\Departamentos;
use App\EtapaEstudiante;
use App\SolicitudCupo;
use App\Estudiante;
use App\ConfiguracionPasos;
use App\User;

class EstudiantesDisponiblesController extends Controller
{


    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
    	$menus = menu::menu_submenu(Auth::user()->rol_id);
    	$deptos = Departamentos::where('depto_estado',true)->get();
        $empresa = Auth::user()->tercero;
        $val_mensajeria = false;
        if($empresa->empresa_mensajeria){
            $val_mensajeria = true;
        }
        $mensajeria = EmpresaMensajeria::where('empresa_id', $empresa->empresa_id)->get()->count();
        $dependencias = Dependencias::where('empresa_id', $empresa->empresa_id)->get()->count();
        $sucursales = Sucursal::where('empresa_id', $empresa->empresa_id)->get()->count();
        $cupos = CuposPractica::where('empresa_id', $empresa->empresa_id)->get()->count();
        $pasos = ConfiguracionPasos::where('empresa_id', $empresa->empresa_id)->get()->count();
        $representantes = User::where([['tercero_id', $empresa->empresa_id],['cargo_id',11]])->get()->count();

        $instituciones = Institucion::select('institucion.*')
                                        ->join('empresa_institucion','institucion.institucion_id','=','empresa_institucion.institucion_id')
                                         ->where([['empresa_institucion.empresa_id', $empresa->empresa_id],['empresa_institucion.empresa_institucion_estado',2]])
                                        ->get();
        $areas = Area::where('area_estado',true)->get();
        $modalidades = EtapaEstudiante::where('etapa_estado', true)->get();
        $cupos_practicas = CuposPractica::select('cupos_practicas.*','cargos.cargo_descripcion','empresa_dependencias.dependencia_descripcion')
									->join('cargos','cupos_practicas.cargo_id','=','cargos.cargo_id')
									->join('empresa_dependencias','cupos_practicas.dependencia_id','=','empresa_dependencias.dependencia_id')
									->where([['cupos_practicas.empresa_id', $empresa->empresa_id],['cupos_practicas.cupo_estado',1]])
									->get();
        $cupos_practicasrem = CuposPractica::select('cupos_practicas.*','cargos.cargo_descripcion','empresa_dependencias.dependencia_descripcion')
                                    ->join('cargos','cupos_practicas.cargo_id','=','cargos.cargo_id')
                                    ->join('empresa_dependencias','cupos_practicas.dependencia_id','=','empresa_dependencias.dependencia_id')
                                    ->where('cupos_practicas.empresa_id', $empresa->empresa_id)
                                    ->get();
        return view('estudiantesDisponibles', compact(['menus','deptos','val_mensajeria','mensajeria','dependencias','sucursales','cupos','instituciones','areas','modalidades','cupos_practicas','pasos','representantes','cupos_practicasrem']));
    }

    public function solicitudCupo(createSolicitudCupoRequest $request){
    	$solicitud = new SolicitudCupo;
    	$solicitud->empresa_id = Auth::user()->tercero_id;
    	$solicitud->estudiante_id = $request->hdEstudiante;
    	$solicitud->cupo_id = $request->cbCupo;
    	$fechaInicio = explode('/', $request->txtFechaInicio);
    	$solicitud->solicitud_fecha_inicio = $fechaInicio[2].'-'.$fechaInicio[1].'-'.$fechaInicio[0];
    	$fechaCulminacion = explode('/', $request->txtFechaCulminacion);
    	$solicitud->solicitud_fecha_culminacion = $fechaCulminacion[2].'-'.$fechaCulminacion[1].'-'.$fechaCulminacion[0];
    	$solicitud->solicitud_tipo_acuerdo = $request->txtTipoAcuerdo;
    	$solicitud->solicitud_estado = 'pendiente';
    	$solicitud->save();
    	$estudiante = Estudiante::find($solicitud->estudiante_id);

        $correoCordinador = User::where([['tercero_id', $request->institucion],['cargo_id',3]])->get();

        // print_r($correoCordinador[0]['user_email']);
    	$email = [$correoCordinador[0]['user_email']];
    	Mail::send('emails.emailSolicitudCupo',['estudiante' =>$estudiante, 'empresa' => Auth::user()->tercero->empresa_nombre], function($message) use($email){
            $message->to($email)->subject('Solicitud de Practicas');
        });
    	return  array( 'Msj' => 'Se ha realizado la solicitud de practicas exitosamente.');
    }

    public function solicitudCupoRemplazo(CreateSolicitudRemCupo $request){
        $solicitud = new SolicitudCupo;
        $solicitud->empresa_id = Auth::user()->tercero_id;
        $solicitud->estudiante_id = $request->hdEstudianteRem;
        $solicitud->cupo_id = $request->cbCupoRem;
        $fechaInicio = explode('/', $request->txtFechaInicioRem);
        $solicitud->solicitud_fecha_inicio = $fechaInicio[2].'-'.$fechaInicio[1].'-'.$fechaInicio[0];
        $fechaCulminacion = explode('/', $request->txtFechaCulminacionRem);
        $solicitud->solicitud_fecha_culminacion = $fechaCulminacion[2].'-'.$fechaCulminacion[1].'-'.$fechaCulminacion[0];
        $solicitud->solicitud_tipo_acuerdo = $request->txtTipoAcuerdoRem;
        $solicitud->solicitud_estado = 'pendiente';
        $solicitud->solicitud_remplazo = $request->cbPracticante;
        $solicitud->save();
        $estudiante = Estudiante::find($solicitud->estudiante_id);
        $email = [$estudiante->estudiante_email,'abarros@pngtechnology.co'];
        Mail::send('emails.emailSolicitudCupo',['estudiante' =>$estudiante, 'empresa' => Auth::user()->tercero->empresa_nombre], function($message) use($email){
            $message->to($email)->subject('Solicitud de Practicas');
        });
        return  array( 'Msj' => 'Se ha realizado la solicitud de practicas exitosamente.');
    }
}
