<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use illuminate\Support\Facades\Auth;
use App\menu;
use App\Cargos;
use App\Departamentos;
use App\EmpresaMensajeria;
use App\Dependencias;
use App\Sucursal;
use App\CuposPractica;
use App\ConfiguracionPasos;
use App\User;

class FuncionariosController extends Controller
{

     public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $menus = menu::menu_submenu(Auth::user()->rol_id);
        $deptos = Departamentos::where('depto_estado',true)->get();
        $cargos = Cargos::where([['cargo_estado',true],['cargo_visible',true]])->get();
        $empresa = Auth::user()->tercero;
        $val_mensajeria = false;
        if($empresa->empresa_mensajeria){
            $val_mensajeria = true;
        }
        $mensajeria = EmpresaMensajeria::where('empresa_id', $empresa->empresa_id)->get()->count();
        $dependencias = Dependencias::where('empresa_id', $empresa->empresa_id)->get()->count();
        $sucursales = Sucursal::where('empresa_id', $empresa->empresa_id)->get()->count();
        $cupos = CuposPractica::where('empresa_id', $empresa->empresa_id)->get()->count();
        $pasos = ConfiguracionPasos::where('empresa_id', $empresa->empresa_id)->get()->count();
        $representantes = User::where([['tercero_id', $empresa->empresa_id],['cargo_id',11]])->get()->count();

        return view('funcionarios', compact(['menus','deptos','cargos','val_mensajeria','mensajeria','dependencias','sucursales','cupos','pasos','representantes']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
