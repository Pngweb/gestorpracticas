<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use illuminate\Support\Facades\Auth;
use App\Http\Requests\createEstudianteSeleccionado;
use App\menu;
use App\Dependencias;
use App\EmpresaMensajeria;
use App\Sucursal;
use App\CuposPractica;
use App\Institucion;
use App\Area;
use App\Departamentos;
use App\ConfiguracionPasos;
use App\SolicitudCupo;
use App\ProcesoSeleccion;
use App\EstudianteSeleccionado;
use App\User;

class ProcesoSeleccionController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index($idestudiante,$idependencia){
    	$menus = menu::menu_submenu(Auth::user()->rol_id);
    	$deptos = Departamentos::where('depto_estado',true)->get();
        $empresa = Auth::user()->tercero;
        $val_mensajeria = false;
        if($empresa->empresa_mensajeria){
            $val_mensajeria = true;
        }
        $mensajeria = EmpresaMensajeria::where('empresa_id', $empresa->empresa_id)->get()->count();
        $dependencias = Dependencias::where('empresa_id', $empresa->empresa_id)->get()->count();
        $sucursales = Sucursal::where('empresa_id', $empresa->empresa_id)->get()->count();
        $cupos = CuposPractica::where('empresa_id', $empresa->empresa_id)->get()->count();
        $pasos = ConfiguracionPasos::where('empresa_id', $empresa->empresa_id)->get()->count();
        $representantes = User::where([['tercero_id', $empresa->empresa_id],['cargo_id',11]])->get()->count();

        $solicitud = SolicitudCupo::where([['estudiante_id',$idestudiante],['empresa_id',$empresa->empresa_id]])->get()->first();
        $idsolicitud =  $solicitud['solicitud_cupo_id'];

        return view('procesoSeleccion', compact(['menus','deptos','val_mensajeria','mensajeria','dependencias','sucursales','cupos','instituciones','areas','pasos','idependencia','idestudiante','idsolicitud','representantes']));
    }


     public function store(Request $request)
    {
        //
        $empresa = Auth::user()->tercero;
       
        $existe = ProcesoSeleccion::where([['estudiante_id',$request->id_estudiante],['pasos_id',$request->pasos_id],['empresa_id',$empresa->empresa_id]])->get();


        if($existe->count() == 0){

            $proceso =  new ProcesoSeleccion;
            $proceso->estudiante_id = $request->id_estudiante;
            $proceso->pasos_id = $request->pasos_id;
            $proceso->proceso_observaciones = $request->observacionProgramar;
            $proceso->proceso_estado = $request->estado;
            $proceso->empresa_id =  $empresa->empresa_id;
            if($request->actualizar && $request->fechaProgramar != 'null'){
                $proceso->proceso_fecha = $request->fechaProgramar;
            }else{
                if($request->fechaProgramar != 'null'){
                    $fecha = explode('/', $request->fechaProgramar);
                    $proceso->proceso_fecha = $fecha[2].'-'.$fecha[1].'-'.$fecha[0];
                }
            }

            if($request->aplica){
                $proceso->proceso_aplica = 0;
                $proceso->proceso_fecha = null;
            }else{
                $proceso->proceso_aplica = 1;
            }

            if($proceso->save()){
                return $resultado = array('ErrorStatus'=>false,'Msj'=>'Proceso guardado con éxito!');
            }else{
                return $resultado = array('ErrorStatus'=>true,'Msj'=>'Error al intentar programar la fecha');
            }


        }else{

            $proceso = ProcesoSeleccion::find($existe[0]->proceso_id);

            $proceso->estudiante_id = $request->id_estudiante;
            $proceso->pasos_id = $request->pasos_id;
            $proceso->proceso_observaciones = $request->observacionProgramar;
            $proceso->proceso_estado = $request->estado;
            $proceso->empresa_id =  $empresa->empresa_id;

            if($request->actualizar && $request->fechaProgramar != 'null'){
                $proceso->proceso_fecha = $request->fechaProgramar;
            }else{
                 if($request->fechaProgramar != 'null'){
                    $fecha = explode('/', $request->fechaProgramar);
                    $proceso->proceso_fecha = $fecha[2].'-'.$fecha[1].'-'.$fecha[0];
                }

            }
            if($request->aplica){
                 $proceso->proceso_aplica = 0;
                 $proceso->proceso_fecha = null;
            }else{
                 $proceso->proceso_aplica = 1;
            }

            if($proceso->save()){
                return $resultado = array('ErrorStatus'=>false,'Msj'=>'Proceso actualizado con éxito!');
            }else{
                return $resultado = array('ErrorStatus'=>true,'Msj'=>'Error al intentar programar la fecha');

            }


        }     
    
    }


    public function select(createEstudianteSeleccionado $request)
    {
         $empresa = Auth::user()->tercero;

        $selected = new EstudianteSeleccionado;
        
        $selected->dependencia_id = $request->dependencia;
        $selected->funcionario_id = $request->maestro;
        $selected->seleccion_funciones = $request->funciones;

        $fecha1 = explode('/', $request->fechaInicioLectiva);
        $selected->seleccion_FecIni_lec = $fecha1[0].'-'.$fecha1[1].'-'.$fecha1[2];

        $fecha2 = explode('/', $request->fechaFinLectiva);
        $selected->seleccion_FecFin_lec = $fecha2[0].'-'.$fecha2[1].'-'.$fecha2[2];

        $fecha3 = explode('/', $request->fechaInicioProductiva);
        $selected->seleccion_FecIni_prd = $fecha3[0].'-'.$fecha3[1].'-'.$fecha3[2];

        $fecha4 = explode('/', $request->fechaFinProductiva);
        $selected->seleccion_FecFin_prd = $fecha4[0].'-'.$fecha4[1].'-'.$fecha4[2];
        
         $selected->seleccion_jornada = $request->jornada;
         $selected->seleccion_horario = $request->horario;
         $selected->seleccion_seguridad = $request->seguridad;
         $selected->seleccion_riesgo = $request->riesgos;
         $selected->estudiante_id = $request->estudiante_id;
         $selected->empresa_id = $empresa->empresa_id;

        if($selected->save()){

            $soli = SolicitudCupo::find($request->idsolicitud);
            $soli->solicitud_estado = 'seleccionada';
            if($soli->save()){
               
            }
            return $resultado = array('ErrorStatus'=>false,'Msj'=>'Estudiante seleccionado con exito!');
        }else{
            return $resultado = array('ErrorStatus'=>true,'Msj'=>'Error al seleccionar estudiante');
        }

    }

    public function noselect(Request $request)
    {
        $empresa = Auth::user()->tercero;

        $soli = SolicitudCupo::find($request->idsolicitud);
        $soli->solicitud_estado = 'no seleccionada';
        $soli->solicitud_observaciones = $request->observaciones;


        if($soli->save()){
            return $resultado = array('ErrorStatus'=>false,'Msj'=>'Estudiante No seleccionado!');
        }else{
            return $resultado = array('ErrorStatus'=>true,'Msj'=>'Error al no seleccionar estudiante');
        }
    }

}
