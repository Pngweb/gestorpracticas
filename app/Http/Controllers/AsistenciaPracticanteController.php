<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\menu;
use App\Dependencias;
use App\EmpresaMensajeria;
use App\Sucursal;
use App\CuposPractica;
use App\User;
use App\ConfiguracionPasos;
use App\InansistenciasPracticantes;

class AsistenciaPracticanteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($idseleccionado)
    {
        //
        $menus = menu::menu_submenu(Auth::user()->rol_id);
        $empresa = Auth::user()->tercero;
        $val_mensajeria = false;
        if($empresa->empresa_mensajeria){
            $val_mensajeria = true;
        }
        $mensajeria = EmpresaMensajeria::where('empresa_id', $empresa->empresa_id)->get()->count();
        $dependencias = Dependencias::where('empresa_id', $empresa->empresa_id)->get()->count();
        $sucursales = Sucursal::where('empresa_id', $empresa->empresa_id)->get()->count();
        $cupos = CuposPractica::where('empresa_id', $empresa->empresa_id)->get()->count();
        $funcionarios = User::where('tercero_id', $empresa->empresa_id)->get();
        $pasos = ConfiguracionPasos::where('empresa_id', $empresa->empresa_id)->get()->count();
        $representantes = User::where([['tercero_id', $empresa->empresa_id],['cargo_id',11]])->get()->count();

        return view('inasistenciaPracticantes', compact(['menus','val_mensajeria','mensajeria','dependencias','sucursales','cupos','funcionarios','pasos','idseleccionado','representantes']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $empresa = Auth::user()->tercero;
        $inasistencia = new InansistenciasPracticantes;

        $fecha = explode('/', $request->fecha);
        $inasistencia->inasistencia_fecha = $fecha[2].'-'.$fecha[1].'-'.$fecha[0];
        $inasistencia->inasistencia_motivo = $request->motivo;
        $inasistencia->inasistencia_observacion = $request->observacion;
        $inasistencia->empresa_id = $empresa->empresa_id;
        $inasistencia->estudiante_Selec_id = $request->seleccionado_id;

        if($inasistencia->save()){
            return $resultado = array('ErrorStatus'=>false,'Msj'=>'Registro guardado con éxito!');
        }else{
            return $resultado = array('ErrorStatus'=>true,'Msj'=>'Error al intentar guardar el registro');

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        return $inasistencia = InansistenciasPracticantes::find($id)->first();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $empresa = Auth::user()->tercero;
        $inasistencia = InansistenciasPracticantes::find($id);

        $fecha = explode('/', $request->fechaAC);
        $inasistencia->inasistencia_fecha = $fecha[2].'-'.$fecha[1].'-'.$fecha[0];
        $inasistencia->inasistencia_motivo = $request->motivoAC;
        $inasistencia->inasistencia_observacion = $request->observacionAC;
        $inasistencia->empresa_id = $empresa->empresa_id;

        if($inasistencia->save()){
            return $resultado = array('ErrorStatus'=>false,'Msj'=>'Registro actualizado con éxito!');
        }else{
            return $resultado = array('ErrorStatus'=>true,'Msj'=>'Error al intentar actualizar el registro');

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $inasistencia = InansistenciasPracticantes::find($id);
        $inasistencia->delete();
        return array('Msj'=>'Se ha eliminado la Inasistencia con éxito.');
    }
}
