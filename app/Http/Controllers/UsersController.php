<?php

namespace App\Http\Controllers;

use Mail;
use Illuminate\Http\Request;
use App\Http\Requests\createUserRequest;
use App\User;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(createUserRequest $request)
    {
        $user = new User;
        $user->identificacion = $request->txtIdentificacion;
        $user->ciudad_id_expedicion = $request->cbCiudad;
        $user->password = bcrypt($request->txtPassword);
        $user->user_nombre = strtoupper($request->txtNombreUser);
        $user->tercero_id = $request->hdTercero;
        $user->rol_id = $request->hdRol;
        $user->cargo_id = $request->cbCargo;
        $user->user_email = $request->txtEmailUser;
        $user->user_telefono = $request->txtTelefonoUser;
        $fecha = explode('/', $request->txtFechaNac);
        $user->user_cumpleanos = $fecha[2].'-'.$fecha[1].'-'.$fecha[0];
        $user->user_notificaciones = ($request->notificacion == 'on' ? 'A': 'I');

        if($user->save()){
            $resultado = array('ErrorStatus'=>false,'Msj'=>'Se ha registrado el usuario con éxito.');
        }else{
            $resultado = array('ErrorStatus'=>true,'Msj'=>'Error al registrar el usuario.');
        }
        return $resultado;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return User::select('users.*','ciudad.depto_id')
                                ->join('ciudad','users.ciudad_id_expedicion','=','ciudad.ciudad_id')
                                ->join('departamentos','ciudad.depto_id','=','departamentos.depto_id')
                                ->where('id',$id)->get()->first();

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(createUserRequest $request, $id)
    {
        $user = User::find($id);
        $user->user_nombre = strtoupper($request->txtNombreUser);
        $user->identificacion = $request->txtIdentificacion;
        $user->ciudad_id_expedicion = $request->cbCiudad;
        $user->cargo_id = $request->cbCargo;
        $user->user_email = $request->txtEmailUser;
        $user->user_telefono = $request->txtTelefonoUser;
        $fecha = explode('/', $request->txtFechaNac);
        $user->user_cumpleanos = $fecha[2].'-'.$fecha[1].'-'.$fecha[0];
        $user->user_notificaciones = ($request->notificacionEdit == 'on' ? 'A': 'I');

        if($request->txtPassword != ""){
            $user->password = bcrypt($request->txtPassword);
        }
        if($user->save()){
            $resultado = array('ErrorStatus'=>false,'Msj'=>'Se ha actualizado el usuario con éxito.');
        }else{
            $resultado = array('ErrorStatus'=>true,'Msj'=>'Error al actualizar el usuario.');
        }
        return $resultado;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);

        if($user->cargo_id == '11'){
            $resultado = array('ErrorStatus'=>true,'Msj'=>'No puedes eliminar al Representante Legal');

        }else{
            $user->delete();
            $resultado = array('ErrorStatus'=>false,'Msj'=>'Se ha eliminado el usuario con éxito.');

        }
        return $resultado;
    }

    // Funcion para reestablecer la contraseña
    public function password($identificacion){
        $user = User::where('identificacion',  $identificacion)->get()->first();
        // $email = [$user->user_email, 'abarros@pngtechnology.co'];
        $email = ['abarros@pngtechnology.co'];
        Mail::send('emails.emailRecuperarContrasena',['id_user' => $user->id], function($message) use($email){
            $message->to($email)->subject('Recuperar Contraseña');
        });
        return array('Msj'=>'Hemos enviado el enlace para reestablecer tu contraseña al correo '.str_replace(substr(explode('@',$user->user_email)[0],0,4) , '******', $user->user_email));
    }

    // mostrar vista reestablecer contraseña
    public function nuevaContrasena($id){
        return view('nuevaContrasena', compact('id'));
    }

    // guardar nueva contraseña
    public function guardarContrasena(Request $request){
        $user = User::find($request->hdUsuario);
        $user->password = bcrypt($request->password);
        $user->save();
         return array('Msj'=>'Se ha cambiado la contraseña exitosamente.');
    }
}
