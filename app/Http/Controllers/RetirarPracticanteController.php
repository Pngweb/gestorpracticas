<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\menu;
use App\Dependencias;
use App\EmpresaMensajeria;
use App\Sucursal;
use App\CuposPractica;
use App\User;
use App\ConfiguracionPasos;
use App\RetirarPracticantes;

class RetirarPracticanteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($idseleccionado)
    {
        //
        $menus = menu::menu_submenu(Auth::user()->rol_id);
        $empresa = Auth::user()->tercero;
        $val_mensajeria = false;
        if($empresa->empresa_mensajeria){
            $val_mensajeria = true;
        }
        $mensajeria = EmpresaMensajeria::where('empresa_id', $empresa->empresa_id)->get()->count();
        $dependencias = Dependencias::where('empresa_id', $empresa->empresa_id)->get()->count();
        $sucursales = Sucursal::where('empresa_id', $empresa->empresa_id)->get()->count();
        $cupos = CuposPractica::where('empresa_id', $empresa->empresa_id)->get()->count();
        $funcionarios = User::where('tercero_id', $empresa->empresa_id)->get();
        $pasos = ConfiguracionPasos::where('empresa_id', $empresa->empresa_id)->get()->count();
        $representantes = User::where([['tercero_id', $empresa->empresa_id],['cargo_id',11]])->get()->count();

        return view('retirarPracticante', compact(['menus','val_mensajeria','mensajeria','dependencias','sucursales','cupos','funcionarios','pasos','idseleccionado','representantes']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $empresa = Auth::user()->tercero;
        $retiro = new RetirarPracticantes;

        $fecha = explode('/', $request->fecha);
        $retiro->retiro_fecha = $fecha[2].'-'.$fecha[1].'-'.$fecha[0];
        $retiro->retiro_motivo = $request->motivo;
        $retiro->retiro_observacion = $request->observacion;
        $retiro->empresa_id = $empresa->empresa_id;
        $retiro->estudiante_Selec_id = $request->seleccionado_id;

        if($retiro->save()){
            return $resultado = array('ErrorStatus'=>false,'Msj'=>'Registro guardado con éxito!');
        }else{
            return $resultado = array('ErrorStatus'=>true,'Msj'=>'Error al intentar guardar el registro');

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
