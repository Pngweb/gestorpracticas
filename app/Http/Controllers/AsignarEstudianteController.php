<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\AsignarEstudiante;
use App\CuposPractica;
use App\Http\Requests\createAsignacionEstudiante;

class AsignarEstudianteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(createAsignacionEstudiante $request)
    {
        $asignar = new AsignarEstudiante;
        $asignar->cupo_id = $request->idcupo;
        $asignar->asignado_nombres = strtoupper($request->nombres);
        $fechaInicio = explode('/', $request->fechaingreso);
        $asignar->asignado_fecha_inicio = $fechaInicio[0].'-'.$fechaInicio[1].'-'.$fechaInicio[2];

        $fechaFinal = explode('/', $request->fechasalida);
        $asignar->asignado_fehca_fin = $fechaFinal[0].'-'.$fechaFinal[1].'-'.$fechaFinal[2];
        $asignar->empresa_id = Auth::user()->tercero_id;
        if($asignar->save()){
             $cupo = CuposPractica::find($request->idcupo);
             $cupo->cupo_estado = 2;
              if($cupo->save()){
                return  array( 'Msj' => 'Se ha asigando el estudiante exitosamente.');
              }else{
                return  array( 'Msj' => 'Error al asignar estudiante.');
              }
            
        }else{
            return  array( 'Msj' => 'Error al asignar estudiante.');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
