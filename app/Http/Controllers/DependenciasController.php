<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use illuminate\Support\Facades\Auth;
use App\Http\Requests\createDependenciaRequest;
use App\menu;
use App\Dependencias;
use App\EmpresaMensajeria;
use App\Sucursal;
use App\CuposPractica;
use App\ConfiguracionPasos;
use App\User;

class DependenciasController extends Controller
{

     
    public function __construct()
    {
        $this->middleware('auth');
    }
     

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $menus = menu::menu_submenu(Auth::user()->rol_id);
        $empresa = Auth::user()->tercero;
        $val_mensajeria = false;
        if($empresa->empresa_mensajeria){
            $val_mensajeria = true;
        }
        $mensajeria = EmpresaMensajeria::where('empresa_id', $empresa->empresa_id)->get()->count();
        $dependencias = Dependencias::where('empresa_id', $empresa->empresa_id)->get()->count();
        $sucursales = Sucursal::where('empresa_id', $empresa->empresa_id)->get()->count();
        $cupos = CuposPractica::where('empresa_id', $empresa->empresa_id)->get()->count();
        $pasos = ConfiguracionPasos::where('empresa_id', $empresa->empresa_id)->get()->count();
        $representantes = User::where([['tercero_id', $empresa->empresa_id],['cargo_id',11]])->get()->count();
        return view('dependencias', compact(['menus','val_mensajeria','mensajeria','dependencias','sucursales','cupos','pasos','representantes']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(createDependenciaRequest $request)
    {
        $dependencia = new Dependencias;
        $dependencia->dependencia_descripcion = strtoupper($request->txtNombre);
        $dependencia->dependencia_estado = true;
        $dependencia->empresa_id = Auth::user()->tercero_id;
        $dependencia->save();
        return  array( 'Msj' => 'Se ha guardado la dependencia exitosamente.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return Dependencias::find($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(createDependenciaRequest $request, $id)
    {
        $dependencia = Dependencias::find($id);
        $dependencia->dependencia_descripcion = strtoupper($request->txtNombre);
        $dependencia->dependencia_estado = ($request->chkEstado == 'on') ? true : false ;
        $dependencia->save();
        return  array( 'Msj' => 'Se ha actualizado la dependencia exitosamente.');
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $dependencia = Dependencias::find($id);
        $dependencia->delete();
        return array('Msj'=>'Se ha eliminado la dependencia con éxito.');
    }
}
