<?php

namespace App\Http\Controllers;

use Mail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Requests\createInstitucionRequest;
use App\Http\Requests\validateEmailInvitacionRequest;
use App\menu;
use App\Departamentos;
use App\Institucion;
use App\Cargos;

class InstitucionController extends Controller
{

    public function __construct()
    {
        if(Route::currentRouteName() == 'institucion.index'){
            $this->middleware('auth');
        }
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $deptos = Departamentos::where('depto_estado',true)->get();

        if(Route::currentRouteName() == 'registroInstituciones'){
            return view('registroIntituciones');
        }else{
            $menus = menu::menu_submenu(Auth::user()->rol_id);
            $cargos = Cargos::where([['cargo_estado',true],['cargo_visible',true]])->get();
            return view('institucion', compact('menus','deptos','cargos'));
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(createInstitucionRequest $request)
    {
        $imagen = $request->file('logo');
        $institucion = new Institucion;
        if($imagen){
            $image_name = rand() . '.' . $imagen->getClientOriginalExtension();
            $imagen->move(public_path('images/images_institucion'), $image_name);
            $institucion->institucion_logo = $image_name;
        }
        $institucion->institucion_nombre = strtoupper($request->txtNombre);
        $institucion->institucion_identificacion = $request->txtNit;
        $institucion->institucion_direccion = strtoupper($request->txtDireccion);
        $institucion->institucion_telefono = $request->txtTelefono;
        $institucion->institucion_email = $request->txtEmail;
        $institucion->ciudad_id = $request->cbCiudad;
        $institucion->institucion_estado = true;
        $institucion->created_user = Auth::user()->id;
        if($institucion->save()){
            $resultado = array('ErrorStatus'=>false,'Msj'=>'Se ha registrado la institucion con éxito.');
        }else{
            $resultado = array('ErrorStatus'=>true,'Msj'=>'Error al registrar la institucion.');
        }
        return $resultado;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return Institucion::select('institucion.*','ciudad.depto_id')
                                            ->join('ciudad','institucion.ciudad_id','=','ciudad.ciudad_id')
                                            ->join('departamentos','ciudad.depto_id','=','departamentos.depto_id')
                                            ->where('institucion.institucion_id',$id)->get()[0];
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(createInstitucionRequest $request, $id)
    {
        $imagen = $request->file('logo');
        $institucion = Institucion::find($id);
        if($imagen){
            if($institucion->institucion_logo){
                unlink(public_path().'/images/images_institucion/'.$institucion->institucion_logo);
            }
            $image_name = rand() . '.' . $imagen->getClientOriginalExtension();
            $imagen->move(public_path('images/images_institucion'), $image_name);
            $institucion->institucion_logo = $image_name;
        }else{
            if($request->hdRemImg == "true"){
                unlink(public_path().'/images/images_institucion/'.$institucion->institucion_logo);
                $institucion->institucion_logo = null;
            }
        }
        $institucion->institucion_nombre = strtoupper($request->txtNombre);
        $institucion->institucion_identificacion = $request->txtNit;
        $institucion->institucion_direccion = strtoupper($request->txtDireccion);
        $institucion->institucion_telefono = $request->txtTelefono;
        $institucion->ciudad_id = $request->cbCiudad;
        $institucion->created_user = Auth::user()->id;
        if($institucion->save()){
            $resultado = array('ErrorStatus'=>false,'Msj'=>'Se ha actualizado la institucion con éxito.');
        }else{
            $resultado = array('ErrorStatus'=>true,'Msj'=>'Error al actualizar la institucion.');
        }
        return $resultado;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $institucion = Institucion::find($id);
        if($institucion->delete()){
            if($institucion->institucion_logo){
                unlink(public_path().'/images/images_institucion/'.$institucion->institucion_logo);
            }
            $resultado = array('ErrorStatus'=>false,'Msj'=>'Se ha eliminado la institucion con éxito.');
        }else{
            $resultado = array('ErrorStatus'=>true,'Msj'=>'Error al eliminar la institucion.');
        }
        return $resultado;
    }

    public function mailInvitacion(validateEmailInvitacionRequest $request)
    {
        $email = $request->txtEmail;
        Mail::send('emails.emailInvitacionInstitucion',[], function($message) use($email){
            $message->to($email)->subject('Invitación  Avatar');
        });

        return array('Msj'=>'Se envio el correo con exito');

    }
}

