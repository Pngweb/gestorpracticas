<?php

namespace App\Http\Controllers;

use Mail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use App\menu;
use App\SolicitudCupo;
use App\Empresa;
use App\Estudiante;
use App\Area;
use App\User;
use App\CuposPractica;

class EstudiantesSolicitados extends Controller
{

	public function __construct()
    {
        $this->middleware('auth');
    }
    
	public function index(){
		$menus = menu::menu_submenu(Auth::user()->rol_id);
		$areas = Area::where('area_estado',true)->get();
		return view('estudiantesSolicitados', compact('menus','areas'));
	}
	public function cambiarEstado(Request $request){
		$solicitud = SolicitudCupo::find($request->solicitud);
		$solicitud->solicitud_estado = $request->estado;
		$solicitud->solicitud_observaciones = $request->observaciones;
		$solicitud->solicitud_fecha_respuesta = Carbon::now();
		$solicitud->save();
		$empresa = Empresa::find($solicitud->empresa_id);
		$funcionario = User::where([['rol_id',3],['tercero_id',Auth::user()->tercero_id],['user_notificaciones','A']])->get()->first();
		$estudiante = Estudiante::find($solicitud->estudiante_id);

		$cupo = CuposPractica::find($solicitud->cupo_id);
		if ($request->estado == 'aceptada') {
			$cupo->cupo_estado = 2;
		}else{
			$cupo->cupo_estado = 1;
		}
		$cupo->save();

		$email_func = '';
		if($funcionario->user_email){
			$email_func = $funcionario->user_email;
		}
		$email = ['abarros@pngtechnology.co',$estudiante->estudiante_email, $email_func];
		$institucion = Auth::user()->tercero;
        Mail::send('emails.emailRespuestaSolicitudEstudiante',['institucion' => $institucion, 'estudiante' => $estudiante, 'estado' => $request->estado], function($message) use($email){
            $message->to($email)->subject('Solicitud de Estudiante');
        });
        return array('ErrorStatus'=>false,'Msj'=>'Se ha respondido a la solicitud con éxito.');
	}

}
