<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class createDependenciaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'txtNombre' => 'Required'
        ];
    }

    public function messages(){
        return[
            'txtNombre.required' => 'Debe Ingresar el nombre.'
        ];
    }
}
