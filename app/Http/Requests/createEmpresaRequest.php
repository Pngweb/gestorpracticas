<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class createEmpresaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        
        if($this->route('empresa')){
            return [
                'txtNit' => 'required|min:8|numeric|unique:empresas,empresa_nit,'.$this->route('empresa').',empresa_id',
                'txtNombre' => 'required|min:5',
                'txtSitioWeb' => 'required',
                'cbMensajeria' => 'required',
                'cbOrgJuridica' => 'required',
                'cbSectorProd' => 'required',
                'cbJornada' => 'required',
                'txtObjSocial' => 'required',
                'txtNoEmpleados' => 'required|numeric'
            ];
        }else{
            return [
                'txtNit' => 'required|min:8|numeric|unique:empresas,empresa_nit',
                'txtNombre' => 'required|min:5',
                'txtSitioWeb' => 'required',
                'cbMensajeria' => 'required',
                'cbOrgJuridica' => 'required',
                'cbSectorProd' => 'required',
                'cbJornada' => 'required',
                'txtObjSocial' => 'required',
                'txtNoEmpleados' => 'required|numeric'
            ];
        }
    }
}
