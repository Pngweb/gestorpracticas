<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class createSolicitudCupoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'hdEstudiante' => 'required',
            'cbCupo' => 'required',
            'txtFechaInicio' => 'required',
            'txtFechaCulminacion' => 'required',
            'txtTipoAcuerdo' => 'required'
        ];
    }

    public function messages(){
        return [
            'hdEstudiante.required' => 'Debe seleccionar un estudiante.',
            'cbCupo.required' => 'Debe seleccionar el cupo de practica.',
            'txtFechaInicio.required' => 'Debe seleccionar la fecha de inicio',
            'txtFechaCulminacion.required' => 'Debe seleccionar la fecha de culminacion.',
            'txtTipoAcuerdo.required' => 'Debe ingresar el tipo de acuerdo.'
        ];
    }
}
