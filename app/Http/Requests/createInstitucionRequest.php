<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class createInstitucionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if($this->route('institucion')){
            return [
                'txtNit' => 'required|min:8|numeric|unique:institucion,institucion_identificacion,'.$this->route('institucion').',institucion_id',
                'txtNombre' => 'required|min:5',
                'txtDireccion' => 'required',
                'cbDepto' => 'required',
                'cbCiudad' => 'required',
                'txtTelefono' => 'required',
                'txtEmail' => 'required|email'
            ];

        }else{
            return [
                'txtNit' => 'required|min:8|numeric|unique:institucion,institucion_identificacion',
                'txtNombre' => 'required|min:5',
                'txtDireccion' => 'required',
                'cbDepto' => 'required',
                'cbCiudad' => 'required',
                'txtTelefono' => 'required',
                'txtEmail' => 'required|email'
            ];
        }
    }
}
