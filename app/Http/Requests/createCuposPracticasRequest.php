<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class createCuposPracticasRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'cdDependencia' => 'required',
            'cbSucursal' => 'required',
            'cbCargo' => 'required',
            'cbMaestro' => 'required',
            'txtCompetencias' => 'required'
        ];
    }

    public function messages(){
        return[
            'cdDependencia.required' => 'Debe seleccionar la dependencia.',
            'cbSucursal.required' => 'Debe seleccionar la sucursal',
            'cbCargo.required' => 'Debe seleccionar el cargo',
            'cbMaestro.required' => 'Debe seleccionar el maestro guia.',
            'txtCompetencias.required' => 'Debe ingresar las competencias.'
        ];
    }
}
