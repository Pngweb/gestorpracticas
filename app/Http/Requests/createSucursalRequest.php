<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class createSucursalRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'txtNombre' => 'required',
            'cbDepto' => 'required',
            'cbCiudad' => 'required',
            'txtDireccion' => 'required',
            'txtTelefono' => 'required'
        ];
    }

    public function messages(){
        return [
            'txtNombre.required' => 'Debe ingresar el nombre.',
            'cbDepto.required' => 'Debe seleccionar el departamento.',
            'cbCiudad.required' => 'Debe seleccionar la ciudad.',
            'txtDireccion.required' => 'Debe ingresar la dirección.',
            'txtTelefono.required' => 'Debe ingresar el telefono.'
        ];
    }
}
