<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class filterEstudiantesDisponibles extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            // 'cbInstitucion' => 'required',
            // 'cbPrograma' => 'required',
            'cbArea' => 'required'
        ];
    }

    public function messages()
    {
        return [
            // 'cbInstitucion.required' => 'Debe seleccionar una institucion.',
            // 'cbPrograma.required' => 'Debe seleccionar un programa',
            'cbArea.required' => 'Debe seleccionar un Area',
        ];
    }
}
