<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class createEstudianteRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if($this->route('estudiante')){
            return [
                'txtIdentificacion' => 'required|min:8|numeric|unique:estudiantes,estudiante_identificacion,'.$this->route('estudiante').',estudiante_id',
                'txtNombres' => 'required',
                'txtApellidos' => 'required',
                'txtDireccion' => 'required',
                'cbDepto' => 'required',
                'cbCiudad' => 'required',
                'txtTelefono' => 'required',
                'cbSexo' => 'required',
                'txtFechaNac' => 'required',
                'txtEmail' => 'required|email',
                'cbGrupoSanguineo' => 'required',
            ];
        }else{
            return [
                'txtIdentificacion' => 'required|min:8|numeric|unique:estudiantes,estudiante_identificacion',
                'txtNombres' => 'required',
                'txtApellidos' => 'required',
                'txtDireccion' => 'required',
                'cbDepto' => 'required',
                'cbCiudad' => 'required',
                'txtTelefono' => 'required',
                'cbSexo' => 'required',
                'txtFechaNac' => 'required',
                'txtEmail' => 'required|email',
                'cbGrupoSanguineo' => 'required',
            ];
        }
    }
}
