<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateSolicitudRemCupo extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'cbPracticante' => 'required',
            'cbCupoRem' => 'required',
            'txtFechaInicioRem' => 'required',
            'txtFechaCulminacionRem' => 'required',
            'txtTipoAcuerdoRem' => 'required'
        ];
    }

    public function messages(){
        return [
            'cbPracticante.required' => 'Debe seleccionar un practicante.',
            'cbCupoRem.required' => 'Debe seleccionar el cupo de practica.',
            'txtFechaInicioRem.required' => 'Debe seleccionar la fecha de inicio',
            'txtFechaCulminacionRem.required' => 'Debe seleccionar la fecha de culminacion.',
            'txtTipoAcuerdoRem.required' => 'Debe ingresar el tipo de acuerdo.'
        ];
    }
}
