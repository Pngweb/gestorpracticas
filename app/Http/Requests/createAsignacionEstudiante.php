<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class createAsignacionEstudiante extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
         return [
            'nombres' => 'required',
            'fechaingreso' => 'required',
            'fechasalida' => 'required',
            
        ];
    }

     public function messages(){
        return[
            'nombres.required' => 'Debe digitar el nombre del estudiante.',
            'fechaingreso.required' => 'Debe seleccionar la fecha inicial.',
            'fechasalida.required' => 'Debe seleccionar la fecha final.',
        ];
    }
}
