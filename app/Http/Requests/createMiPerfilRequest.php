<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class createMiPerfilRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'txtNit' => 'required|unique:empresas,empresa_nit,'.Auth::user()->tercero_id.',empresa_id',
            'txtNombre' => 'required',
            'txtSitioWeb' => 'required',
            'cbOrgJuridica' => 'required',
            'cbSectorProd' => 'required',
            'cbJornada' => 'required',
            'cbConglomerado' => 'required',
            'txtNoEmpleados' => 'required|numeric',
            'txtObjSocial' => 'required',
        ];
    }

    public function messages(){
        return [
            'txtNit.required' => 'Debe Ingresar el NIT.',
            'txtNit.unique' => 'El NIT ingresado ya esta registrado.',
            'txtNombre.required' => 'Debe Ingresar el nombre.',   
            'txtSitioWeb.required' => 'Debe Ingresar el sitio web.',   
            'cbOrgJuridica.required' => 'Debe seleccionar la organizacion juridica.',   
            'cbSectorProd.required' => 'Debe seleccionar el sector productivo.',   
            'cbJornada.required' => 'Debe seleccionar la jornada de trabajo.',   
            'cbConglomerado.required' => 'Debe seleccionar si su empresa pertenece a un conglomerado.',
            'txtNoEmpleados.required' => 'Debe ingresar el numero de empleados.',
            'txtNoEmpleados.numeric' => 'El numero de empleados ingresado es invalido.',
            'txtObjSocial.required' => 'Debe ingresar el objeto social.'
        ];   
    }
}
