<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class createUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if($this->route('user')){
            return [
                    'txtNombreUser' => 'required',
                    'cbCargo' => 'required',
                    'txtIdentificacion' => 'required|min:8|numeric|unique:users,identificacion,'.$this->route('user'),
                    'cbDepto' => 'required',
                    'cbCiudad' => 'required',
                    'txtEmailUser' => 'required|email',
                    'txtTelefonoUser' => 'required',
                    'txtFechaNac' => 'required',
                    'txtPassword' => 'exclude_if:txtPassword,""|required|same:txtPasswordConfirm',
                    'txtPasswordConfirm' => 'exclude_if:txtPassword,""|required'
                ];
        }else{
            return [
                    'txtNombreUser' => 'required',
                    'cbCargo' => 'required',
                    'txtIdentificacion' => 'required|min:8|numeric|unique:users,identificacion',
                    'cbDepto' => 'required',
                    'cbCiudad' => 'required',
                    'txtEmailUser' => 'required|email',
                    'txtTelefonoUser' => 'required',
                    'txtFechaNac' => 'required',
                    'txtPassword' => 'required|same:txtPasswordConfirm',
                    'txtPasswordConfirm' => 'required'
                ];
            }
    }
}
