<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class createUserMensajeriaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'cbSistemaMensajeria' => 'required',
            'txtUsuario' => 'required'
        ];
    }

    public function messages(){
        return [
            'cbSistemaMensajeria.required' => 'Debe Seleccionar el sistema de mensajeria.',
            'txtUsuario.required' => 'Debe ingresar el usuario de mensajeria.'
        ];
    }
}
