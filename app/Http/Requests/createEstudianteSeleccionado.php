<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class createEstudianteSeleccionado extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'fechaInicioLectiva' => 'required',
            'fechaFinLectiva' => 'required',
            'fechaInicioProductiva' => 'required',
            'fechaFinProductiva' => 'required',
            'jornada' => 'required',
            'horario' => 'required',
            'seguridad' => 'required',
            'riesgos' => 'required',
            
        ];
    }

    public function messages(){
        return[

            'fechaInicioLectiva.required' => 'Debes seleccionar una fecha de Inicio lectiva',
            'fechaFinLectiva.required' => 'Debes seleccionar una fecha Final lectiva',
            'fechaInicioProductiva.required' => 'Debes seleccionar una fecha de Inicio de etapa Productiva',
            'fechaFinProductiva.required' => 'Debes seleccionar una fecha Final de etapa Productiva',
            'jornada.required' => 'Debes seleccionar una jornada',
            'horario.required' => 'Debes digitar un horario',
            'seguridad.required' => 'Debes seleccionar un tipo de seguridad social',
            'riesgos.required' => 'Debes seleccionar un tipo de entidad de riesgos,'
        ];
    }
}
