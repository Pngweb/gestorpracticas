<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class createConfiguracionPasos extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
       return [
            'txtNombre' => 'required',
            'tipo' => 'required',
            'tiempo' => 'required',
            'orden' => 'required',
            'responsable' => 'required',

            
        ];
    }

     public function messages(){
        return[
            'txtNombre.required' => 'Debe digitar el nombre.',
            'tipo.required' => 'Debe seleccionar un tipo.',
            'tiempo.required' => 'Debe digitar el tiempo que se requiere.',
            'orden.required' => 'Debe digitar el orden del proceso.',
            'responsable.required' => 'Debe seleccionar una funcionario a cargo',
        ];
    }
}
