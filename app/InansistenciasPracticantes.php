<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InansistenciasPracticantes extends Model
{
    //
    protected $table = 'inasistencia_practicantes';
    protected $primaryKey = 'inasistencia_id';
}
