<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PasosXdependencias extends Model
{
    //
    protected $table = 'pasos_por_dependencias';
    protected $primaryKey = 'pasosdep_id';
}
