<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SolicitudCupo extends Model
{
    protected $table = 'solicitud_cupo';
    protected $primaryKey = 'solicitud_cupo_id';
}
