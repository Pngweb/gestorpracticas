<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RetirarPracticantes extends Model
{
    //
    protected $table = 'retiro_practicante';
    protected $primaryKey = 'retiro_id';
}
