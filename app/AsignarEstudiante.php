<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AsignarEstudiante extends Model
{
    //
    protected $table = 'estudiantes_asignados';
    protected $primaryKey = 'id';
}
