<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EstudianteSeleccionado extends Model
{
    //
    protected $table = 'estudiantes_seleccionados';
    protected $primaryKey = 'seleccion_id';
}
