<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmpresaInstitucion extends Model
{
     protected $table = 'empresa_institucion';
     protected $primaryKey = 'empresa_institucion_id';
}
