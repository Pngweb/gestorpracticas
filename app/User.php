<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use App\roles;
use App\systemAdmin;
use App\Institucion;
use App\Empresa;
use App\Cargos;
use App\Estudiante;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'identificacion', 'password','tercero_id', 'rol_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
    ];
    
    public function rol(){
        return $this->belongsTo(roles::class,'rol_id');
    }

    public function tercero(){
        if($this->rol_id == 1){
            return $this->belongsTo(systemAdmin::class,'tercero_id');
        }elseif($this->rol_id == 2){
            return $this->belongsTo(Institucion::class,'tercero_id');
        }elseif($this->rol_id == 3){
            return $this->belongsTo(Empresa::class,'tercero_id');
        }elseif($this->rol_id == 4){
            return $this->belongsTo(Estudiante::class,'tercero_id');
        }
    }

    public function cargo(){
        return $this->belongsTo(Cargos::class, 'cargo_id');
    }

}
