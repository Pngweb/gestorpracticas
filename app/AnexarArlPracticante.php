<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AnexarArlPracticante extends Model
{
    //
    protected $table = 'documentos_practicante';
    protected $primaryKey = 'doc_prac_id';
}
