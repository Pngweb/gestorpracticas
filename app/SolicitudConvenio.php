<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SolicitudConvenio extends Model
{
    protected $table = 'solicitud_convenio';
}
