<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SectorProductivo extends Model
{
    protected $table = 'sector_productivo';
    protected $primaryKey = 'sec_prod_id';
}
