<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmpresaMensajeria extends Model
{
    protected $table = 'empresa_mensajeria';
    protected $primaryKey = 'empresa_mensajeria_id';
}
