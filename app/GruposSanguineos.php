<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GruposSanguineos extends Model
{
    protected $table = 'grupos_sanguineos';
    protected $primaryKey = 'grupo_sanguineo_id';
}
