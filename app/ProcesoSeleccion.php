<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProcesoSeleccion extends Model
{
    //
    protected $table = 'proceso_seleccion';
    protected $primaryKey = 'proceso_id';
}
