<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ConfiguracionPasos extends Model
{
    //
    protected $table = 'configuracion_pasos';
    protected $primaryKey = 'pasos_id';
}
