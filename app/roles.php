<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class roles extends Model
{

    protected $table = 'roles_users';
    protected $primaryKey = 'rol_id';
    
    protected $fillable = [
        'descripcion',
    ];
}
