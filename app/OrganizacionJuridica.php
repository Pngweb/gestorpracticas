<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrganizacionJuridica extends Model
{
    protected $table = 'organizacion_juridica';
    protected $primaryKey = 'org_jur_id';
}
