<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Empresa extends Model
{
    protected $table = 'empresas';
    protected $primaryKey = 'empresa_id';

    protected $fillable = [
        'empresa_nit', 'empresa_nombre','empresa_web', 'empresa_mensajeria','empresa_no_empleados','empresa_objeto_social'
    ];
}
