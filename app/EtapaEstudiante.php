<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EtapaEstudiante extends Model
{
    protected $table = 'estapas_estudiante';
    protected $primaryKey = 'etapa_id';
}
