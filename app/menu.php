<?php

namespace App;

use App\submenu;
use Illuminate\Database\Eloquent\Model;

class menu extends Model
{
    protected $primaryKey = 'id_menu';

    public static function menu_submenu($rol){
        $menus = menu::where([['rol_id',$rol],['estado','A']])->orderBy('orden','ASC')->get();
        foreach($menus as $menu){
            $menu->smenu = submenu::where('id_menu',$menu->id_menu)->get();
        }
        return $menus;
    }

}
