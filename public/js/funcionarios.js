let language={
    "sProcessing":     "Procesando...",
    "sLengthMenu":     "Mostrar _MENU_ registros",
    "sZeroRecords":    "No se encontraron resultados",
    "sEmptyTable":     "Ningún dato disponible en esta tabla",
    "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
    "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
    "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
    "sInfoPostFix":    "",
    "sSearch":         "Buscar:",
    "sUrl":            "",
    "sInfoThousands":  ",",
    "sLoadingRecords": "Cargando...",
    "oPaginate": {
        "sFirst":    "Primero",
        "sLast":     "Último",
        "sNext":     "Siguiente",
        "sPrevious": "Anterior"
    },
    "oAria": {
        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
    }
};

var table;
$(document).ready(function() {

    $('.bootstrap-datepicker').bsdatepicker({
        format: 'dd/mm/yyyy'
    });

	table = $('#datatable-example').DataTable(
        {
            "language" : language,
            "ajax": `api/users/${$('#hdTercero').val()}/${$('#hdRol').val()}`,
            columns : [
                { data: "user_nombre" },
                { data: "identificacion" },
                { data: "ciudad_descripcion" },
                { data: "user_telefono" },
                { data: "user_email" },
                { data: "cargo_descripcion"},
                { data: "id", render: (data) => { return `<div class="group-2-btn">
                                                                                                    <button class="btn btn-round btn-primary btnEdit" onClick="formEdit(${data})">
                                                                                                        <i class="glyph-icon icon-edit"></i>
                                                                                                    </button>
                                                                                                <button class="btn btn-round btn-primary" onClick="EliminarFuncionario(${data})">
                                                                                                    <i class="glyph-icon icon-trash"></i>
                                                                                                </button>
                                                                                                </div>` }, width: '13%'}
            ]
        }
    );

    $('#frmCrearFuncionario').on('submit', function(e){
        e.preventDefault();
        let form = $(this)[0];
        let datos = new FormData(form);
        axios.post(form.action, datos).then((res) => {
            $.jGrowl(res.data.Msj, {
                theme: 'bg-green' 
            });
            table.ajax.reload();
            form.reset();
            $('#mdlCrear').modal('hide');
        }, (err) => {
            if(err.response.status == 422){
                $.jGrowl(Object.values(err.response.data.errors)[0], {
                    theme: 'bg-red' 
                });
            }else{
                $.jGrowl('Error en el servidor.', {
                    theme: 'bg-red' 
                });
            }
        });
    });

    $('#frmEditarFuncionario').on('submit', function(e){
        e.preventDefault();
        let form = $(this)[0];
        let datos = new FormData(form);
        let url = `${form.action}/${$('#hdUser').val()}`
        axios.post(url, datos).then((res) => {
            $.jGrowl(res.data.Msj, {
                theme: 'bg-green' 
            });
            table.ajax.reload();
            form.reset();
            $('#mdlEditar').modal('hide');
        }, (err) => {
            if(err.response.status == 422){
                $.jGrowl(Object.values(err.response.data.errors)[0], {
                    theme: 'bg-red' 
                });
            }else{
                $.jGrowl('Error en el servidor.', {
                    theme: 'bg-red' 
                });
            }
        });
    });

	
});

function abrirModal(e){
    $($(e).data('target')).modal();
}

function consultarCiudades(e,ciudadVal=0){
    let ciudadCb = '';
    if($(e).attr('id') == 'cbDepto'){
        ciudadCb = 'cbCiudad';
    }else{
        ciudadCb = 'cbCiudadEdit';
    }
    $(`#${ciudadCb} option`).remove();
    $(`#${ciudadCb}`).append(`<option value="">Seleccione..</option>`);
    axios.get(`api/ciudades/${$(e).val()}`).then((res) => {
        if(res.data.length > 0){
            for(ciudad of res.data){
                $(`#${ciudadCb}`).append(`<option value="${ciudad.ciudad_id}">${ciudad.ciudad_descripcion}</option>`);
            }
            if(ciudadVal != 0){
                $(`#${ciudadCb}`).val(ciudadVal);
            }
        }
    });
}

function formEdit(id){
    axios.get(`/users/${id}/edit`).then((res) => {
        $('#hdUser').val(res.data.id);
        $('#txtNombreUserEdit').val(res.data.user_nombre);
        $('#cbCargoEdit').val(res.data.cargo_id);
        $('#txtIdentificacionEdit').val(res.data.identificacion);
        $('#cbDeptoEdit').val(res.data.depto_id);
        consultarCiudades($('#cbDeptoEdit'),res.data.ciudad_id_expedicion);
        $('#txtTelefonoUserEdit').val(res.data.user_telefono);
        $('#txtEmailUserEdit').val(res.data.user_email);
        $('#txtFechaNacEdit').val(moment(res.data.user_cumpleanos).format('DD/MM/YYYY'));
        if(res.data.user_notificaciones == 'A'){
            $('#notificacionEdit').prop('checked','checked');
        }
        $('#mdlEditar').modal();
    });
}

function EliminarFuncionario(id){

    swal({
          title: "Eliminar Funcionario?",
          text: "Estas a punto de Eliminar este Funcionario",
          icon: "warning",
          buttons: true,
          dangerMode: true,
        })
        .then((willDelete) => {
          if (willDelete) {

                axios.delete(`/users/${id}`).then((res) => {
                        $.jGrowl(res.data.Msj, {
                            theme: 'bg-green' 
                        });
                        table.ajax.reload();
                }, (err) => {
                    $.jGrowl('Error en el servidor.', {
                            theme: 'bg-red' 
                        });
                });
            }
        })

}