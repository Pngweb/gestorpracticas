let language={
    "sProcessing":     "Procesando...",
    "sLengthMenu":     "Mostrar _MENU_ registros",
    "sZeroRecords":    "No se encontraron resultados",
    "sEmptyTable":     "Ningún dato disponible en esta tabla",
    "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
    "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
    "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
    "sInfoPostFix":    "",
    "sSearch":         "Buscar:",
    "sUrl":            "",
    "sInfoThousands":  ",",
    "sLoadingRecords": "Cargando...",
    "oPaginate": {
        "sFirst":    "Primero",
        "sLast":     "Último",
        "sNext":     "Siguiente",
        "sPrevious": "Anterior"
    },
    "oAria": {
        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
    }
};

var table;
$(document).ready(function() {

     $('#back').click(function(){
         window.history.back();
    });

     consultar_info_seleccionado($('#seleccionado_id').val());

   $('.bootstrap-datepicker').bsdatepicker({
        format: 'dd/mm/yyyy',
         autoclose: true,
    }).on('changeDate', function (ev) {
         $(this).bsdatepicker('hide');
    });

	table = $('#datatable-example').DataTable(
        {
            "language" : language,
            "ajax": `http://inca.pngtechnology.co/api/observaciones-practicantes/${$('#hdTercero').val()}/${$('#seleccionado_id').val()}`,
            columns : [
                { data: "observacion_descripcion" },
                { data: "created_at", render: (data) => { return moment(data).format('DD/MM/YYYY') } },
                { data: "observacion_id", render: (data) => { return `<div class="group-2-btn">
                                                        <button class="btn btn-round btn-primary" onClick="EliminarFuncionario(${data})">
                                                            <i class="glyph-icon icon-trash"></i>
                                                        </button>
                                                        </div>` }, width: '13%'}
            ]
        }
    );

    $('#frmCrearObservacion').on('submit', function(e){
        e.preventDefault();
        let form = $(this)[0];
        let datos = new FormData(form);
        axios.post(form.action, datos).then((res) => {
          if(!res.ErrorStatus){
                $.jGrowl(res.data.Msj, {
                    theme: 'bg-green' 
                });
                table.ajax.reload();
                form.reset();
                $('#mdlCrear').modal('hide');
            }else{
                $.jGrowl(res.data.Msj, {
                    theme: 'bg-red' 
                });
            }
        }, (err) => {
            if(err.response.status == 422){
                $.jGrowl(Object.values(err.response.data.errors)[0], {
                    theme: 'bg-red' 
                });
            }else{
                $.jGrowl('Error en el servidor.', {
                    theme: 'bg-red' 
                });
            }
        });
    });
	
});

function abrirModal(e){
    $($(e).data('target')).modal();
}

function EliminarFuncionario(id){

    swal({
          title: "Eliminar Observación?",
          text: "Estas a punto de Eliminar esta Observación",
          icon: "warning",
          buttons: true,
          dangerMode: true,
        })
        .then((willDelete) => {
          if (willDelete) {

            axios.delete(`/observacion-practicante/${id}`).then((res) => {
                    $.jGrowl(res.data.Msj, {
                        theme: 'bg-green' 
                    });
                    table.ajax.reload();
            }, (err) => {
                $.jGrowl('Error en el servidor.', {
                        theme: 'bg-red' 
                    });
            });

        }
    });
}

function consultar_info_seleccionado(idseleccionado){
    axios.get(`http://inca.pngtechnology.co/api/info-estudiante-seleccionado/${idseleccionado}`).then(res => {
        // console.log(res.data);
        $('#cedula').text('C.C '+res.data[0].estudiante_identificacion);
        $('#nombres').text(res.data[0].estudiante_nombres+' '+res.data[0].estudiante_apellidos);
        $('#programa').text(res.data[0].programa_descripcion);
        $('#tipo').text(res.data[0].solicitud_tipo_acuerdo);
        $('#empresa').text(res.data[0].empresa_nombre);
        $('#dependencia').text(res.data[0].dependencia_descripcion);
        $('#fechaI').text(res.data[0].solicitud_fecha_inicio);
        $('#fechaF').text(res.data[0].solicitud_fecha_culminacion);
    });
}