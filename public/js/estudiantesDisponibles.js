let language={
    "sProcessing":     "Procesando...",
    "sLengthMenu":     "Mostrar _MENU_ registros",
    "sZeroRecords":    "No se encontraron resultados",
    "sEmptyTable":     "Ningún dato disponible en esta tabla",
    "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
    "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
    "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
    "sInfoPostFix":    "",
    "sSearch":         "Buscar:",
    "sUrl":            "",
    "sInfoThousands":  ",",
    "sLoadingRecords": "Cargando...",
    "oPaginate": {
        "sFirst":    "Primero",
        "sLast":     "Último",
        "sNext":     "Siguiente",
        "sPrevious": "Anterior"
    },
    "oAria": {
        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
    }
};

var table;
$(document).ready(function() {

    // $('.bootstrap-datepicker').bsdatepicker({
    //     format: 'dd/mm/yyyy',
    //      autoclose: true,
    // }).on('changeDate', function (ev) {
    //      $(this).bsdatepicker('hide');
    // });

    $('.bootstrap-datepicker').bsdatepicker({
        format: 'dd/mm/yyyy'
    }).on('changeDate',function(e){
        var fec = $(this).val().split('/');
        var fec2 = $('.bootstrap-datepicker2').val().split('/');
        if(moment(moment(fec2[2]+'-'+fec2[1]+'-'+fec2[0])).diff(moment(fec[2]+'-'+fec[1]+'-'+fec[0]),'days') < 0){
            $.jGrowl('La fecha de Inicio no puede ser mayor a la fecha final.', {
                  sticky: false,
                  position: 'top-right',
                  theme: 'bg-red'
              });
            $(this).val($('.bootstrap-datepicker2').val());
        }
        $(this).bsdatepicker('hide');
    });

    $('.bootstrap-datepicker2').bsdatepicker({
        format: 'dd/mm/yyyy'
    }).on('changeDate',function(e){
        var fec = $(this).val().split('/');
        var fec2 = $('.bootstrap-datepicker').val().split('/');
        if(moment(moment(fec2[2]+'-'+fec2[1]+'-'+fec2[0])).diff(moment(fec[2]+'-'+fec[1]+'-'+fec[0]),'days') > 0){
            $.jGrowl('La fecha final no puede ser menor a la fecha de inicio.', {
                  sticky: false,
                  position: 'top-right',
                  theme: 'bg-red'
              });
            $(this).val($('.bootstrap-datepicker').val());
        }
        $(this).bsdatepicker('hide');
    });






    $('#datatable-example').DataTable({"language" : language,"columnDefs": [{ "width": "150px", "targets": 0 }]});

    $('#cbArea').on('change', function(){
        let institucion = (($('#cbInstitucion').val() != '') ? $('#cbInstitucion').val()  : 0 );
        let area = (($(this).val() != '') ? $(this).val()  : 0 );
        consultarProgramas($('#hdEmpresa').val(),institucion, area);
    });

    $('#cbInstitucion').on('change', function(){
        let institucion = (($(this).val() != '') ? $(this).val()  : 0 );
        let area = (($('#cbArea').val() != '') ? $('#cbArea').val()  : 0 );
        consultarProgramas($('#hdEmpresa').val(),institucion, area);
    });

    $('#frmFiltrarEstudiantes').on('submit', function(e){
        e.preventDefault();
        consultarEstudiantes();
    });

    $('#cbCupo').on('change', function(){
        let cupo = $(this).val();
        if(cupo != ''){
            consultarInfoCupo(cupo);
        }else{
            $('#panel_info_cupo').html(`Seleccione el cupo de practica.`);
        }
    });

    $('#cbCupoRe').on('change', function(){
        let cupo = $(this).val();
        if(cupo != ''){
            consultarInfoCupoRem(cupo);
            consultarPracticante(cupo);
        }else{
            $('#cbPracticante option').remove();
            $('#panel_info_cupo_Rem').html(`Seleccione el cupo de practica.`);
            $('#cbPracticante').append('<option value="">Seleccione..</option>')
        }
    });

    $('#frmSolicitudCupo').on('submit', function(e){
        e.preventDefault();
        let form = $(this)[0];
        let datos = new FormData(form);
        axios.post(form.action, datos).then((res) => {
            $.jGrowl(res.data.Msj, {
                theme: 'bg-green' 
            });
            form.reset();
            $('#mdlSolicitar').modal('hide');
            consultarEstudiantes();
        }, (err) => {
            if(err.response.status == 422){
                $.jGrowl(Object.values(err.response.data.errors)[0], {
                    theme: 'bg-red' 
                });
            }else{
                $.jGrowl('Error en el servidor.', {
                    theme: 'bg-red' 
                });
            }
        });
    });


    // formulario de remplazo para practicante

    $('#frmRemplazoCupo').on('submit', function(e){
        e.preventDefault();
        let form = $(this)[0];
        let datos = new FormData(form);
        axios.post(form.action, datos).then((res) => {
            $.jGrowl(res.data.Msj, {
                theme: 'bg-green' 
            });
            form.reset();
            $('#mdlSolicitar').modal('hide');
            consultarEstudiantes();
        }, (err) => {
            if(err.response.status == 422){
                $.jGrowl(Object.values(err.response.data.errors)[0], {
                    theme: 'bg-red' 
                });
            }else{
                $.jGrowl('Error en el servidor.', {
                    theme: 'bg-red' 
                });
            }
        });
    });
	
});

function abrirModal(e){
    $($(e).data('target')).modal();
    $('#hdEstudiante').val($(e).data('estudiante'));
    $('#hdEstudianteRem').val($(e).data('estudiante'));
    $('#institucion').val($(e).data('institucion'));
    $('#institucionRem').val($(e).data('institucion'));
}

function consultarProgramas(empresa,institucion, area){
     axios.get(`api/programas/${institucion}/${area}`).then(res => {
        $('#cbPrograma option').remove();
        if(res.data.length > 0){
            $('#cbPrograma').append(`<option value="">Seleccione..</option>`);
            for(programa of res.data){
                $('#cbPrograma').append(`<option value="${programa.programa_id}">${programa.programa_descripcion}</option>`);
            }
        }else{
            $('#cbPrograma').append(`<option value="">No hay Informacion.</option>`);
        }
     });
}

function consultarCiudades(e,ciudadVal=0){
    let ciudadCb = '';
    if($(e).attr('id') == 'cbDepto'){
        ciudadCb = 'cbCiudad';
    }else{
        ciudadCb = 'cbCiudadEdit';
    }
    $(`#${ciudadCb} option`).remove();
    $(`#${ciudadCb}`).append(`<option value="">Seleccione..</option>`);
    axios.get(`api/ciudades/${$(e).val()}`).then((res) => {
        if(res.data.length > 0){
            for(ciudad of res.data){
                $(`#${ciudadCb}`).append(`<option value="${ciudad.ciudad_id}">${ciudad.ciudad_descripcion}</option>`);
            }
            if(ciudadVal != 0){
                $(`#${ciudadCb}`).val(ciudadVal);
            }
        }
    });
}

function consultarEstudiantes(){
    let datos = $('#frmFiltrarEstudiantes').serialize();
    let url = `api/estudiantes-disponibles/${$('#hdEmpresa').val()}`;
    axios.post(url, datos).then(res => {
        let table = $('#datatable-example').DataTable();
        table.clear().draw();
        for(estudiante of res.data){
            console.log(estudiante.estado_solicitud);
            if(estudiante.estado_solicitud != 'aceptada' && estudiante.estado_solicitud != 'seleccionada' ) {
            table.row.add([`<img class="img-thumbnail" src="/images/${((estudiante.estudiante_foto) ? `images_estudiantes/${estudiante.estudiante_foto}` : 'no-image.png')}" width="100">`,`<strong>${estudiante.estudiante_nombres} ${estudiante.estudiante_apellidos}</strong><br>C.C. ${numeral(estudiante.estudiante_identificacion).format('0,0')}<br>Edad: ${estudiante.edad}<br>${estudiante.estudiante_direccion}<br>${estudiante.ciudad_descripcion}<br></a> <a href="mailto:${estudiante.estudiante_email}"><button class="btn btn-round btn-info"><i class="glyph-icon icon-envelope"></i></button></a> ${((!estudiante.solicitud_estado) ? `<button class="btn btn-primary" data-target="#mdlSolicitar" data-institucion="${estudiante.institucion_id}" data-estudiante="${estudiante.estudiante_id}" onclick="abrirModal(this)">Solicitar</button>` :  '<button class="btn btn-success">Solicitado</button>')}`,`Institución: ${estudiante.institucion_nombre} <br>Teléfono:${estudiante.institucion_telefono} <br>Correo: ${estudiante.institucion_email} <br> Dirección: ${estudiante.institucion_direccion} <br> ${estudiante.programa_descripcion}<br>Modalidad: ${estudiante.etapa_descripcion}`,`<div style="text-align: center; padding: 20px 0;"><button style="font-size: 30pt; height: 60px;" class="btn btn-default"><i class="glyph-icon icon-file-text-o"></i></button><span>Hoja de Vida</span></div>`]).draw();
            }
        }
    }, (err) => {
            if(err.response.status == 422){
                $.jGrowl(Object.values(err.response.data.errors)[0], {
                    theme: 'bg-red' 
                });
            }else{
                $.jGrowl('Error en el servidor.', {
                    theme: 'bg-red' 
                });
            }
        });
}

function consultarInfoCupo(cupo){
    axios.get(`api/info-cupo/${cupo}`).then(res => {
        $('#panel_info_cupo').html(`<strong>Cargo : </strong>${res.data.cargo_descripcion}<br>
                                                                <strong>Dependencia : </strong>${res.data.dependencia_descripcion}<br>
                                                                <strong>Sucursal : </strong>${res.data.sucursal_descripcion}<br>
                                                                <strong>Maestro : </strong>${res.data.user_nombre}<br>
                                                                <strong>Competencias : </strong>${res.data.cupo_competencias}<br>`);
    });
}

function consultarInfoCupoRem(cupo){
    axios.get(`api/info-cupo/${cupo}`).then(res => {
        $('#panel_info_cupo_Rem').html(`<strong>Cargo : </strong>${res.data.cargo_descripcion}<br>
                                                                <strong>Dependencia : </strong>${res.data.dependencia_descripcion}<br>
                                                                <strong>Sucursal : </strong>${res.data.sucursal_descripcion}<br>
                                                                <strong>Maestro : </strong>${res.data.user_nombre}<br>
                                                                <strong>Competencias : </strong>${res.data.cupo_competencias}<br>`);
    });
}

function consultarPracticante(cupo){
    axios.get(`api/info-solicitud/${cupo}`).then((res) => {
        console.log('res ',res.data);
        $('#cbPracticante option').remove();
        if(res.data.length > 0){    
            for(pract of res.data){
                $(`#cbPracticante`).append(`<option value="${pract.estudiante_id}"> ${pract.estudiante_nombres} ${pract.estudiante_apellidos ? pract.estudiante_apellidos : ''}</option>`);
            }
        }else{
            $('#cbPracticante').append('<option value="">Seleccione..</option>');
        }
    });  
}

// Validaciones fecha 
var bindDateRangeValidation = function (f, s, e) {
    if(!(f instanceof jQuery)){
            console.log("Not passing a jQuery object");
    }
  
    var jqForm = f,
        startDateId = s,
        endDateId = e;
  
    var checkDateRange = function (startDate, endDate) {
        var isValid = (startDate != "" && endDate != "") ? startDate <= endDate : true;
        return isValid;
    }

    var bindValidator = function () {
        var bstpValidate = jqForm.data('bootstrapValidator');
        var validateFields = {
            startDate: {
                validators: {
                    notEmpty: { message: 'Este campo es requerido.' },
                    callback: {
                        message: 'Start Date must less than or equal to End Date.',
                        callback: function (startDate, validator, $field) {
                            return checkDateRange(startDate, $('#' + endDateId).val())
                        }
                    }
                }
            },
            endDate: {
                validators: {
                    notEmpty: { message: 'This field is required.' },
                    callback: {
                        message: 'End Date must greater than or equal to Start Date.',
                        callback: function (endDate, validator, $field) {
                            return checkDateRange($('#' + startDateId).val(), endDate);
                        }
                    }
                }
            },
            customize: {
                validators: {
                    customize: { message: 'customize.' }
                }
            }
        }
        if (!bstpValidate) {
            jqForm.bootstrapValidator({
                excluded: [':disabled'], 
            })
        }
      
        jqForm.bootstrapValidator('addField', startDateId, validateFields.startDate);
        jqForm.bootstrapValidator('addField', endDateId, validateFields.endDate);
      
    };

    var hookValidatorEvt = function () {
        var dateBlur = function (e, bundleDateId, action) {
            jqForm.bootstrapValidator('revalidateField', e.target.id);
        }

        $('#' + startDateId).on("dp.change dp.update blur", function (e) {
            $('#' + endDateId).data("DateTimePicker").setMinDate(e.date);
            dateBlur(e, endDateId);
        });

        $('#' + endDateId).on("dp.change dp.update blur", function (e) {
            $('#' + startDateId).data("DateTimePicker").setMaxDate(e.date);
            dateBlur(e, startDateId);
        });
    }

    bindValidator();
    hookValidatorEvt();
};

// validar rango de fechas etapa productiva

var bindDateRangeValidationPrd = function (f, s, e) {
    if(!(f instanceof jQuery)){
            console.log("Not passing a jQuery object");
    }
  
    var jqForm = f,
        startDateId = s,
        endDateId = e;
  
    var checkDateRange2 = function (startDate, endDate) {
        var isValid = (startDate != "" && endDate != "") ? startDate <= endDate : true;
        return isValid;
    }

    var bindValidator2 = function () {
        var bstpValidate = jqForm.data('bootstrapValidator');
        var validateFields = {
            startDate: {
                validators: {
                    notEmpty: { message: 'Este campo es requerido.' },
                    callback: {
                        message: 'Start Date must less than or equal to End Date.',
                        callback: function (startDate, validator, $field) {
                            return checkDateRange2(startDate, $('#' + endDateId).val())
                        }
                    }
                }
            },
            endDate: {
                validators: {
                    notEmpty: { message: 'This field is required.' },
                    callback: {
                        message: 'End Date must greater than or equal to Start Date.',
                        callback: function (endDate, validator, $field) {
                            return checkDateRange2($('#' + startDateId).val(), endDate);
                        }
                    }
                }
            },
            customize: {
                validators: {
                    customize: { message: 'customize.' }
                }
            }
        }
        if (!bstpValidate) {
            jqForm.bootstrapValidator({
                excluded: [':disabled'], 
            })
        }
      
        jqForm.bootstrapValidator('addField', startDateId, validateFields.startDate);
        jqForm.bootstrapValidator('addField', endDateId, validateFields.endDate);
      
    };

    var hookValidatorEvt2 = function () {
        var dateBlur = function (e, bundleDateId, action) {
            jqForm.bootstrapValidator('revalidateField', e.target.id);
        }

        $('#' + startDateId).on("dp.change dp.update blur", function (e) {
            $('#' + endDateId).data("DateTimePicker").setMinDate(e.date);
            dateBlur(e, endDateId);
        });

        $('#' + endDateId).on("dp.change dp.update blur", function (e) {
            $('#' + startDateId).data("DateTimePicker").setMaxDate(e.date);
            dateBlur(e, startDateId);
        });
    }

    bindValidator2();
    hookValidatorEvt2();
};


$(function () {
  
    // $('#txtFechaInicio').datetimepicker({ 
    //   pickTime: false, 
    //   format: "YYYY/MM/DD", 
      
    // });
  
    // $('#txtFechaCulminacion').datetimepicker({ 
    //   pickTime: false, 
    //   format: "YYYY/MM/DD", 
      
    // });

    //passing 1.jquery form object, 2.start date dom Id, 3.end date dom Id
    // bindDateRangeValidation($("#frmSolicitudCupo"), 'txtFechaInicio', 'txtFechaCulminacion');

});

// funcion validar fecha productivo

$(function () {
  
    // $('#txtFechaInicioRem').datetimepicker({ 
    //   pickTime: false, 
    //   format: "YYYY/MM/DD", 
    // });
  
    // $('#txtFechaCulminacionRem').datetimepicker({ 
    //   pickTime: false, 
    //   format: "YYYY/MM/DD", 
    // });

    //passing 1.jquery form object, 2.start date dom Id, 3.end date dom Id
    // bindDateRangeValidationPrd($("#frmRemplazoCupo"), 'txtFechaInicioRem', 'txtFechaCulminacionRem');

});