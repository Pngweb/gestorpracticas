let language={
    "sProcessing":     "Procesando...",
    "sLengthMenu":     "Mostrar _MENU_ registros",
    "sZeroRecords":    "No se encontraron resultados",
    "sEmptyTable":     "Ningún dato disponible en esta tabla",
    "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
    "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
    "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
    "sInfoPostFix":    "",
    "sSearch":         "Buscar:",
    "sUrl":            "",
    "sInfoThousands":  ",",
    "sLoadingRecords": "Cargando...",
    "oPaginate": {
        "sFirst":    "Primero",
        "sLast":     "Último",
        "sNext":     "Siguiente",
        "sPrevious": "Anterior"
    },
    "oAria": {
        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
    }
};

var table;

$(document).ready(function() {
    
    $('.bootstrap-datepicker').bsdatepicker({
        format: 'dd/mm/yyyy'
    });

    $('.bootstrap-datepicker').val(moment().format('DD/MM/YYYY'));

    table = $('#datatable-example').DataTable(
        {
            "language" : language,
            "ajax": "api/estudiantes",
            columns : [
                { data : "estudiante_foto", render: (data) => { return `<img src="/images/${((data) ? `images_estudiantes/${data}` : 'no-image.png')}" width="50">` }},
                { data: "estudiante_identificacion" },
                { render: (data,type,row) => {
                    return `${row.estudiante_nombres} ${row.estudiante_apellidos}`;
                } },
                { data: "estudiante_email" },
                { data: "estudiante_telefono" },
                { data: "estudiante_id", render: (data) => { return `<button class="btn btn-round btn-primary btnEdit" onClick="formEdit(${data})">
                                                                            <i class="glyph-icon icon-edit"></i>
                                                                        </button>
                                                                    <button class="btn btn-round btn-primary" onClick="EliminarEstudiante(${data})">
                                                                        <i class="glyph-icon icon-trash"></i>
                                                                    </button>` }}
            ]
        }
    );

    $('#logo').on('change', function(e){
        $('#removeImg').removeClass('hide');
        $('#img-logo').removeClass('hide');
        let TmpPath = URL.createObjectURL(e.target.files[0])
        $('#img-logo').attr('src',TmpPath);
    });

    $('#logoEdit').on('change', function(e){
        $('#removeImgEdit').removeClass('hide');
        $('#img-logo-edit').removeClass('hide');
        let TmpPath = URL.createObjectURL(e.target.files[0])
        $('#img-logo-edit').attr('src',TmpPath);
    });

});

function guardarEstudiante(){
    let form = $('#frmCrearEstudiante')[0];
    let datos = new FormData(form);
    axios.post(form.action, datos).then((res) => {
        if(!res.ErrorStatus){
            $.jGrowl(res.data.Msj, {
                theme: 'bg-green' 
            });
            table.ajax.reload();
            form.reset();
            $('#mdlCrear').modal('hide');
        }else{
            $.jGrowl(res.data.Msj, {
                theme: 'bg-red' 
            });
        }
    }, (err) => {
        $.jGrowl(Object.values(err.response.data.errors)[0], {
            theme: 'bg-red' 
        });
    });
    quitarImagen();
}

function EditarEstudiante(){
    let form = $('#frmEditarEstudiante')[0];
    let datos = new FormData(form);
    let url = `${form.action}/${$('#hdEstudianteEdit').val()}`;
    axios.post(url, datos).then((res) => {
        if(!res.ErrorStatus){
            $.jGrowl(res.data.Msj, {
                theme: 'bg-green' 
            });
            table.ajax.reload();
            form.reset();
            $('#mdlEditar').modal('hide');
        }else{
            $.jGrowl(res.data.Msj, {
                theme: 'bg-red' 
            });
        }
    }, (err) => {
        $.jGrowl(Object.values(err.response.data.errors)[0], {
            theme: 'bg-red' 
        });
    });
}

function EliminarEstudiante(id){
    axios.delete(`/estudiantes/${id}`).then((res) => {
        if(!res.ErrorStatus){
            $.jGrowl(res.data.Msj, {
                theme: 'bg-green' 
            });
            table.ajax.reload();
        }else{
            $.jGrowl(res.data.Msj, {
                theme: 'bg-red' 
            });
        }
    });
}

function importarExcel(){
    let form = $('#frmImportarEstudiantes')[0];
    let datos = new FormData(form);
    axios.post(form.action, datos).then(res => {
        $.jGrowl(res.data.Msj, {
            theme: 'bg-green' 
        });
        table.ajax.reload();
        form.reset();
        $('#mdlImportar').modal('hide');
    }, err => {
        if(err.response.status == 422){
            $.jGrowl(Object.values(err.response.data.errors)[0], {
                theme: 'bg-red' 
            });
        }else if(err.response.status == 403){
            $.jGrowl(err.response.data.message, {
                theme: 'bg-red' 
            });
        }else{
            $.jGrowl('Error en el servidor.', {
                theme: 'bg-red' 
            });
        }
    });
}

function enviarInvitacion(){
    $.jGrowl(`Se ha enviado un correo electrónico a ${$('#txtEmailInvitacion').val()}.`, {
        theme: 'bg-green' 
    });
    $('#txtEmailInvitacion').val('');
}

function consultarCiudades(e,ciudadVal=0){
    let ciudadCb = '';
    if($(e).attr('id') == 'cbDepto'){
        ciudadCb = 'cbCiudad';
    }else{
        ciudadCb = 'cbCiudadEdit';
    }
    $(`#${ciudadCb} option`).remove();
    $(`#${ciudadCb}`).append(`<option value="">Seleccione..</option>`);
    axios.get(`api/ciudades/${$(e).val()}`).then((res) => {
        if(res.data.length > 0){
            for(ciudad of res.data){
                $(`#${ciudadCb}`).append(`<option value="${ciudad.ciudad_id}">${ciudad.ciudad_descripcion}</option>`);
            }
            if(ciudadVal != 0){
                $(`#${ciudadCb}`).val(ciudadVal);
            }
        }
    });
}

function quitarImagen(){
    $('#img-logo').attr('src','');
    $('#img-logo').addClass('hide');
    $('#logo').val('');
}

function quitarImagenEdit(){
    $('#img-logo-edit').attr('src','');
    $('#img-logo-edit').addClass('hide');
    $('#hdRemImg').val(true);
    $('#logoEdit').val('');
}

function abrirModal(e){
    $($(e).data('target')).modal();
}

function formEdit(id){
    $('#img-logo-edit').removeClass('hide');
    axios.get(`/estudiantes/${id}/edit`).then((res) => {
        if(res.data.estudiante_foto){
            $('#removeImgEdit').removeClass('hide');
            $('#img-logo-edit').attr('src',`/images/images_estudiantes/${res.data.estudiante_foto}`);
        }else{
            $('#img-logo-edit').attr('src','');
        }
        $('#txtIdentificacionEdit').val(res.data.estudiante_identificacion);
        $('#hdEstudianteEdit').val(res.data.estudiante_id);
        $('#txtNombresEdit').val(res.data.estudiante_nombres);
        $('#txtApellidosEdit').val(res.data.estudiante_apellidos);
        $('#txtDireccionEdit').val(res.data.estudiante_direccion);
        $('#cbDeptoEdit').val(res.data.depto_id);
        consultarCiudades($('#cbDeptoEdit'),res.data.ciudad_id);
        $('#txtTelefonoEdit').val(res.data.estudiante_telefono);
        $('#cbSexoEdit').val(res.data.estudiante_sexo);
        $('#txtFechaNacEdit').val(moment(res.data.estudiante_fecha_nacimiento).format('DD/MM/YYYY'));
        $('#txtEmailEdit').val(res.data.estudiante_email);
        $('#cbGrupoSanguineoEdit').val(res.data.grupo_sanguineo_id);
        $('#hdRemImg').val(false);
        $('#mdlEditar').modal();
    });
}