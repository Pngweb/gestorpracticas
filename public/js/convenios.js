let language={
    "sProcessing":     "Procesando...",
    "sLengthMenu":     "Mostrar _MENU_ registros",
    "sZeroRecords":    "No se encontraron resultados",
    "sEmptyTable":     "Ningún dato disponible en esta tabla",
    "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
    "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
    "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
    "sInfoPostFix":    "",
    "sSearch":         "Buscar:",
    "sUrl":            "",
    "sInfoThousands":  ",",
    "sLoadingRecords": "Cargando...",
    "oPaginate": {
        "sFirst":    "Primero",
        "sLast":     "Último",
        "sNext":     "Siguiente",
        "sPrevious": "Anterior"
    },
    "oAria": {
        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
    }
};
var tableConvenios;
var tableDisponibles;

$(document).ready(function() {
    
    tableConvenios = $('#tbConvenios').DataTable(
        {
            "language" : language,
            "ajax": `api/convenios/${$('#hdInstitucion').val()}`,
             columns : [
                { data : "empresa_logo", render: (data) => { return `<img src="/images/${((data) ? `images_empresas/${data}` : 'no-image.png')}" width="50">` }},
                { data: "empresa_nit" },
                { data: "empresa_nombre" },
                { data: "empresa_web", render: (data) => { return `<a href="http://${data}" target="_blank">${data}</a>` } },
                { data: "sec_prod_descripcion" },
                { data: "empresa_institucion_estado", render: (data) => { return ((data == 1) ? `<span class="bs-label label-yellow">Solicitado</span>` : `<span class="bs-label label-success">Aprobado</span>` ) } },
                { data: (row) => { return ((row.rol_id == 2) ? `<button class="btn btn-round btn-primary" onClick="responderSolicitud(${row.empresa_institucion_id},4)">
                                                                            <i class="glyph-icon icon-minus"></i>
                                                                        </button>` : (( row.empresa_institucion_estado == 1) ? `<button class="btn btn-round btn-primary" onClick="responderSolicitud(${row.empresa_institucion_id},3)">
                                                                            <i class="glyph-icon icon-close"></i>
                                                                        </button>
                                                                        <button class="btn btn-round btn-primary" onClick="responderSolicitud(${row.empresa_institucion_id},2)">
                                                                            <i class="glyph-icon icon-check"></i>
                                                                        </button>` : `<button class="btn btn-round btn-primary" onClick="responderSolicitud(${row.empresa_institucion_id},4)">
                                                                            <i class="glyph-icon icon-minus"></i>
                                                                        </button>`  ))  }}
            ]
        }
    );

    tableDisponibles = $('#tbDisponibles').DataTable(
        {
            "language" : language,
            "ajax": `api/empresas-disponibles/${$('#hdInstitucion').val()}`,
            columns : [
                { data : "empresa_logo", render: (data) => { return `<img src="/images/${((data) ? `images_empresas/${data}` : 'no-image.png')}" width="50">` }},
                { data: "empresa_nit" },
                { data: "empresa_nombre" },
                { data: "empresa_web", render: (data) => { return `<a href="http://${data}" target="_blank">${data}</a>` } },
                { data: "sec_prod_descripcion" },
                { data: "empresa_id", render: (data) => { return `<button class="btn btn-round btn-primary" onClick="solicitarConvenio(${data})" title="Solicitar Convenio">
                                                                            <i class="glyph-icon icon-envelope"></i>
                                                                        </button>` }}
            ]
        }
    );

});

function solicitarConvenio(empresa){
    axios.post('convenios',{institucion: $('#hdInstitucion').val(), empresa}).then((res) => {
         if(!res.ErrorStatus){
            $.jGrowl(res.data.Msj, {
                theme: 'bg-green' 
            });
            tableDisponibles.ajax.reload();
            tableConvenios.ajax.reload();
         }else{
            $.jGrowl(res.data.Msj, {
                theme: 'bg-red' 
            });
         }
    });
}

// function EliminarConvenio(convenio){
//     axios.put(`convenios/${convenio}`).then((res) => {
//          if(!res.ErrorStatus){
//             $.jGrowl(res.data.Msj, {
//                 theme: 'bg-green' 
//             });
//             tableDisponibles.ajax.reload();
//             tableConvenios.ajax.reload();
//          }else{
//             $.jGrowl(res.data.Msj, {
//                 theme: 'bg-red' 
//             });
//          }
//     });
// }

function responderSolicitud(convenio,estado){
    axios.put(`convenios/${convenio}`,{estado}).then((res) => {
         if(!res.ErrorStatus){
            $.jGrowl(res.data.Msj, {
                theme: 'bg-green' 
            });
            tableDisponibles.ajax.reload();
            tableConvenios.ajax.reload();
         }else{
            $.jGrowl(res.data.Msj, {
                theme: 'bg-red' 
            });
         }
    });
}