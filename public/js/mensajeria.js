let language={
    "sProcessing":     "Procesando...",
    "sLengthMenu":     "Mostrar _MENU_ registros",
    "sZeroRecords":    "No se encontraron resultados",
    "sEmptyTable":     "Ningún dato disponible en esta tabla",
    "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
    "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
    "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
    "sInfoPostFix":    "",
    "sSearch":         "Buscar:",
    "sUrl":            "",
    "sInfoThousands":  ",",
    "sLoadingRecords": "Cargando...",
    "oPaginate": {
        "sFirst":    "Primero",
        "sLast":     "Último",
        "sNext":     "Siguiente",
        "sPrevious": "Anterior"
    },
    "oAria": {
        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
    }
};

var table;
$(document).ready(function() {

	table = $('#datatable-example').DataTable(
        {
            "language" : language,
            "ajax": `api/usermensajeria/${$('#hdEmpresa').val()}`,
            columns : [
                { data: "mensajeria_descripcion" },
                { data: "mensajeria_usuario" },
                { data: "empresa_mensajeria_id", render: (data) => { return `<button class="btn btn-round btn-primary btnEdit" onClick="formEdit(${data})">
                                                                            <i class="glyph-icon icon-edit"></i>
                                                                        </button>
                                                                    <button class="btn btn-round btn-primary" onClick="EliminarUserMensajeria(${data})">
                                                                        <i class="glyph-icon icon-trash"></i>
                                                                    </button>` }}
            ]
        }
    );

    $('#frmCrearUserMensajeria').on('submit', function(e){
        e.preventDefault();
        let form = $(this)[0];
        let datos = new FormData(form);
        axios.post(form.action, datos).then((res) => {
            $.jGrowl(res.data.Msj, {
                theme: 'bg-green' 
            });
            table.ajax.reload();
            form.reset();
            $('#mdlCrear').modal('hide');
        }, (err) => {
            if(err.response.status == 422){
                $.jGrowl(Object.values(err.response.data.errors)[0], {
                    theme: 'bg-red' 
                });
            }else{
                $.jGrowl('Error en el servidor.', {
                    theme: 'bg-red' 
                });
            }
        });
    });

    $('#frmEditarUserMensajeria').on('submit', function(e){
        e.preventDefault();
        let form = $(this)[0];
        let datos = new FormData(form);
        let url = `${form.action}/${$('#hdUserMensajeriaId').val()}`
        axios.post(url, datos).then((res) => {
            $.jGrowl(res.data.Msj, {
                theme: 'bg-green' 
            });
            table.ajax.reload();
            form.reset();
            $('#mdlEditar').modal('hide');
        }, (err) => {
            if(err.response.status == 422){
                $.jGrowl(Object.values(err.response.data.errors)[0], {
                    theme: 'bg-red' 
                });
            }else{
                $.jGrowl('Error en el servidor.', {
                    theme: 'bg-red' 
                });
            }
        });
    });
	
});

function abrirModal(e){
    $($(e).data('target')).modal();
}

function formEdit(id){
    axios.get(`/mensajeria/${id}/edit`).then((res) => {
        $('#hdUserMensajeriaId').val(res.data.empresa_mensajeria_id);
        $('#cbSistemaMensajeriaEdit').val(res.data.mensajeria_id);
        $('#txtUsuarioEdit').val(res.data.mensajeria_usuario);
        $('#mdlEditar').modal();
    });
}

function EliminarUserMensajeria(id){
    axios.delete(`/mensajeria/${id}`).then((res) => {
            $.jGrowl(res.data.Msj, {
                theme: 'bg-green' 
            });
            table.ajax.reload();
    }, (err) => {
        $.jGrowl('Error en el servidor.', {
                theme: 'bg-red' 
            });
    });
}