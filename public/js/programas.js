let language={
    "sProcessing":     "Procesando...",
    "sLengthMenu":     "Mostrar _MENU_ registros",
    "sZeroRecords":    "No se encontraron resultados",
    "sEmptyTable":     "Ningún dato disponible en esta tabla",
    "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
    "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
    "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
    "sInfoPostFix":    "",
    "sSearch":         "Buscar:",
    "sUrl":            "",
    "sInfoThousands":  ",",
    "sLoadingRecords": "Cargando...",
    "oPaginate": {
        "sFirst":    "Primero",
        "sLast":     "Último",
        "sNext":     "Siguiente",
        "sPrevious": "Anterior"
    },
    "oAria": {
        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
    }
};

var table;
$(document).ready(function() {
    
    table = $('#datatable-example').DataTable(
        {
            "language" : language,
            "ajax": `api/programas/${$('#hdInstitucion').val()}`,
            columns : [
                { data: "programa_descripcion" },
                { data: "area_descripcion" },
                { data: "programa_estado", render: (data) => { return ((data) ? '<span class="bs-label label-success">Activo</span>' : '<span class="bs-label label-danger">Inactivo</span>' ) } },
                { data: "programa_id", render: (data) => { return `<button class="btn btn-round btn-primary btnEdit" onClick="formEdit(${data})">
                                                                            <i class="glyph-icon icon-edit"></i>
                                                                        </button>
                                                                    <button class="btn btn-round btn-primary" onClick="EliminarDependencia(${data})">
                                                                        <i class="glyph-icon icon-trash"></i>
                                                                    </button>` }}
            ]
        }
    );

    $('#frmCrearPrograma').on('submit', function(e){
        e.preventDefault();
        let form = $(this)[0];
        let datos = new FormData(form);
        axios.post(form.action, datos).then((res) => {
            $.jGrowl(res.data.Msj, {
                theme: 'bg-green' 
            });
            table.ajax.reload();
            form.reset();
            $('#mdlCrear').modal('hide');
        }, (err) => {
            if(err.response.status == 422){
                $.jGrowl(Object.values(err.response.data.errors)[0], {
                    theme: 'bg-red' 
                });
            }else{
                $.jGrowl('Error en el servidor.', {
                    theme: 'bg-red' 
                });
            }
        });
    });

    $('#frmEditarPrograma').on('submit', function(e){
        e.preventDefault();
        let form = $(this)[0];
        let datos = new FormData(form);
        let url = `${form.action}/${$('#hdProgramaId').val()}`
        axios.post(url, datos).then((res) => {
            $.jGrowl(res.data.Msj, {
                theme: 'bg-green' 
            });
            table.ajax.reload();
            form.reset();
            $('#mdlEditar').modal('hide');
        }, (err) => {
            if(err.response.status == 422){
                $.jGrowl(Object.values(err.response.data.errors)[0], {
                    theme: 'bg-red' 
                });
            }else{
                $.jGrowl('Error en el servidor.', {
                    theme: 'bg-red' 
                });
            }
        });
    });

});

function abrirModal(e){
    $($(e).data('target')).modal();
}

function formEdit(id){
    axios.get(`/programas/${id}/edit`).then((res) => {
        $('#txtNombreEdit').val(res.data.programa_descripcion);
        $('#hdProgramaId').val(res.data.programa_id);
        $('#cbAreaEdit').val(res.data.area_id);
        if(res.data.programa_estado == 1){
            $('#chkEstado').prop('checked','checked');
            $('.switch-toggle').addClass('switch-on');
        }
        $('#mdlEditar').modal();
    });
}

function EliminarDependencia(id){
    axios.delete(`/programas/${id}`).then((res) => {
            $.jGrowl(res.data.Msj, {
                theme: 'bg-green' 
            });
            table.ajax.reload();
    }, (err) => {
        $.jGrowl('Error en el servidor.', {
                theme: 'bg-red' 
            });
    });
}

function importarExcel(){
    let form = $('#frmImportarProgramas')[0];
    let datos = new FormData(form);
    axios.post(form.action, datos).then(res => {
        $.jGrowl(res.data.Msj, {
            theme: 'bg-green' 
        });
        table.ajax.reload();
        form.reset();
        $('#mdlImportar').modal('hide');
    }, err => {
        if(err.response.status == 422){
            $.jGrowl(Object.values(err.response.data.errors)[0], {
                theme: 'bg-red' 
            });
        }else if(err.response.status == 403){
            $.jGrowl(err.response.data.message, {
                theme: 'bg-red' 
            });
        }else{
            $.jGrowl('Error en el servidor.', {
                theme: 'bg-red' 
            });
        }
    });
}