let language={
    "sProcessing":     "Procesando...",
    "sLengthMenu":     "Mostrar _MENU_ registros",
    "sZeroRecords":    "No se encontraron resultados",
    "sEmptyTable":     "Ningún dato disponible en esta tabla",
    "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
    "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
    "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
    "sInfoPostFix":    "",
    "sSearch":         "Buscar:",
    "sUrl":            "",
    "sInfoThousands":  ",",
    "sLoadingRecords": "Cargando...",
    "oPaginate": {
        "sFirst":    "Primero",
        "sLast":     "Último",
        "sNext":     "Siguiente",
        "sPrevious": "Anterior"
    },
    "oAria": {
        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
    }
};

var table;
$(document).ready(function() {

   $('#datatable-example').DataTable({"language" : language,"columnDefs": [{ "width": "150px", "targets": 0 }]});

     $('#frmFiltrarSeleccionados').on('submit', function(e){
        e.preventDefault();
        
        if($('#cbInstitucion').val() == '' &&  $('#fltSucursal').val() == '' && $('#fltDependencia').val() == '' && $('#fltCargo').val() == ''){
        	$.jGrowl('Selecciona un campo para filtrar.', {
                    theme: 'bg-orange' 
            });

        }else{
       		 consultarEstudiantes();
        }
    });

});

function consultarEstudiantes(){

    let datos = $('#frmFiltrarSeleccionados').serialize();
    let url = `api/estudiantes-seleccionados/${$('#hdEmpresa').val()}`;
    axios.post(url, datos).then(res => {
    	// console.log('Res ',res);
        let table = $('#datatable-example').DataTable();
        table.clear().draw();
        for(estudiante of res.data){
            table.row.add([`${estudiante.cupo_id}`,`${estudiante.sucursal_descripcion}`,`${estudiante.cupo_competencias}`,`${estudiante.user_nombre}`,`${estudiante.estudiante_nombres}  ${estudiante.estudiante_apellidos}`,
                    `${estudiante.programa_descripcion}`,`${estudiante.solicitud_tipo_acuerdo}`,`<div style="text-align: center; padding: 20px 0;"><a href='/estudiantes-seleccionados-info/${estudiante.estudiante_id}/${estudiante.seleccion_id}'><button class="btn btn-default">Consultar</i></button></div>`
                ]).draw();
            
           $('#title').text(`${estudiante.institucion_nombre}`);
        }
        if (res.data.length == 0) {
             $('#title').text('');
        }
    }, (err) => {
            if(err.response.status == 422){
                $.jGrowl(Object.values(err.response.data.errors)[0], {
                    theme: 'bg-red' 
                });
            }else{
                $.jGrowl('Error en el servidor.', {
                    theme: 'bg-red' 
                });
            }
        });
}