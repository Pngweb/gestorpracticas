let language={
    "sProcessing":     "Procesando...",
    "sLengthMenu":     "Mostrar _MENU_ registros",
    "sZeroRecords":    "No se encontraron resultados",
    "sEmptyTable":     "Ningún dato disponible en esta tabla",
    "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
    "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
    "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
    "sInfoPostFix":    "",
    "sSearch":         "Buscar:",
    "sUrl":            "",
    "sInfoThousands":  ",",
    "sLoadingRecords": "Cargando...",
    "oPaginate": {
        "sFirst":    "Primero",
        "sLast":     "Último",
        "sNext":     "Siguiente",
        "sPrevious": "Anterior"
    },
    "oAria": {
        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
    }
};

var table;
$(document).ready(function() {

    $('#datatable-example').DataTable({"language" : language,"columnDefs": [{ "width": "150px", "targets": 0 }]});

    $('#cbArea').on('change', function(){
        let institucion = (($('#cbInstitucion').val() != '') ? $('#cbInstitucion').val()  : 0 );
        let area = (($(this).val() != '') ? $(this).val()  : 0 );
        consultarProgramas($('#hdEmpresa').val(),institucion, area);
    });

    $('#cbInstitucion').on('change', function(){
        let institucion = (($(this).val() != '') ? $(this).val()  : 0 );
        let area = (($('#cbArea').val() != '') ? $('#cbArea').val()  : 0 );
        consultarProgramas($('#hdEmpresa').val(),institucion, area);
    });

     $('#frmFiltrarEstudiantes').on('submit', function(e){
        e.preventDefault();
        consultarEstudiantes();
    });

     $('#btnNoSelect').click(function(){
        let datos = {
            idsolicitud: $('#hdCupoId').val(),
            observaciones: $('#txtObservaciones').val()
        }
         axios.post('/proceso-seleccion/noseleccionar', datos).then((res) => {
                if(!res.ErrorStatus){
                    $.jGrowl(res.data.Msj, {
                        theme: 'bg-green' 
                    });
                    window.location.href = '/estudiantes-disponibles'
                }else{
                    $.jGrowl(res.data.Msj, {
                        theme: 'bg-red' 
                    });
                }
            }, (err) => {
                $.jGrowl(Object.values(err.response.data.errors)[0], {
                    theme: 'bg-red' 
                });
            }); 
     });

});

function consultarProgramas(empresa,institucion, area){
     axios.get(`api/programas/${institucion}/${area}`).then(res => {
        $('#cbPrograma option').remove();
        if(res.data.length > 0){
            $('#cbPrograma').append(`<option value="">Seleccione..</option>`);
            for(programa of res.data){
                $('#cbPrograma').append(`<option value="${programa.programa_id}">${programa.programa_descripcion}</option>`);
            }
        }else{
            $('#cbPrograma').append(`<option value="">No hay Informacion.</option>`);
        }
     });
}



function consultarEstudiantes(){

    let datos = $('#frmFiltrarEstudiantes').serialize();
    let url = `api/estudiantes-preseleccionados/${$('#hdEmpresa').val()}`;
    axios.post(url, datos).then(res => {
        let table = $('#datatable-example').DataTable();
        table.clear().draw();
        for(estudiante of res.data){
            // table.row.add([`<img class="img-thumbnail" src="/images/${((estudiante.estudiante_foto) ? `images_estudiantes/${estudiante.estudiante_foto}` : 'no-image.png')}" width="100">`,`<strong>${estudiante.estudiante_nombres} ${estudiante.estudiante_apellidos}</strong><br>C.C. ${numeral(estudiante.estudiante_identificacion).format('0,0')}<br>Edad: ${estudiante.edad}<br>${estudiante.estudiante_direccion}<br>${estudiante.ciudad_descripcion}<br>${estudiante.estudiante_telefono}<br><a href="https://web.whatsapp.com/"><button class="btn btn-round btn-success"><i class="fab fa-whatsapp"></i></button></a> <a href="mailto:${estudiante.estudiante_email}"><button class="btn btn-round btn-info"><i class="glyph-icon icon-envelope"></i></button></a> ${((estudiante.solicitud) ? '<button class="btn btn-success">Solicitado</button>' : `<button class="btn btn-primary" data-target="#mdlSolicitar" data-estudiante="${estudiante.estudiante_id}" onclick="abrirModal(this)">Solicitar</button>` )}`,`${estudiante.programa_descripcion}<br>Modalidad: ${estudiante.etapa_descripcion}`,`<div style="text-align: center; padding: 20px 0;"><button style="font-size: 30pt; height: 60px;" class="btn btn-default"><i class="glyph-icon icon-file-text-o"></i></button><span>Hoja de Vida</span></div>`]).draw();
            
            table.row.add([`<img class="img-thumbnail" src="/images/${((estudiante.estudiante_foto) ? `images_estudiantes/${estudiante.estudiante_foto}` : 'no-image.png')}" width="100">`,`Cupo de Practica<br> <strong>${estudiante.cupo_competencias}</strong> <br><strong>Fecha de inicio:</strong> ${estudiante.solicitud_fecha_inicio}<br> <strong>Sucursal:</strong> ${estudiante.sucursal_descripcion}<br><strong>Dependencia:</strong> ${estudiante.dependencia_descripcion}<br><strong>Maestro:</strong> ${estudiante.user_nombre}<br><strong>Paso actual:</strong> <br> ${estudiante.solicitud_remplazo ? `<strong>Remplazará a:</strong> ${estudiante.remplazo}` : ''} `,`<strong>${estudiante.estudiante_nombres} ${estudiante.estudiante_apellidos}</strong><br><strong>C.C.</strong> ${numeral(estudiante.estudiante_identificacion).format('0,0')}<br> <strong>Institución: </strong>${estudiante.institucion_nombre} <br> <strong>Programa:</strong> ${estudiante.programa_descripcion} <br><strong>Edad:</strong> ${estudiante.edad}<br><strong>Telefono:</strong>${estudiante.estudiante_telefono}<br><strong>Modalidad:</strong> ${estudiante.etapa_descripcion} <br>${estudiante.estudiante_direccion}<br>${estudiante.ciudad_descripcion}<br><a href="https://web.whatsapp.com/"><button class="btn btn-round btn-success"><i class="fab fa-whatsapp"></i></button></a> <a href="mailto:${estudiante.estudiante_email}"><button class="btn btn-round btn-info"><i class="glyph-icon icon-envelope"></i></button></a><br>
                            <a href= "proceso-seleccion/${estudiante.estudiante_id}/${estudiante.dependencia_id}"> <button class="btn btn-sm btn-primary mrg5T">Proceso de Selección</button></a> <button class="btn btn-sm btn-danger mrg5T" onclick="noseleccionar(${estudiante.solicitud_cupo_id})">No Seleccionar</button>`,`<div style="text-align: center; padding: 20px 0;"><button style="font-size: 30pt; height: 60px;" class="btn btn-default"><i class="glyph-icon icon-file-text-o"></i></button><span>Hoja de Vida</span></div>`]).draw();
        }
    }, (err) => {
            if(err.response.status == 422){
                $.jGrowl(Object.values(err.response.data.errors)[0], {
                    theme: 'bg-red' 
                });
            }else{
                $.jGrowl('Error en el servidor.', {
                    theme: 'bg-red' 
                });
            }
        });
}

function noseleccionar(cupo_id){


        swal({
              title: "Descartar Estudiante?",
              text: "Estas a punto de descartar este estudiante",
              icon: "warning",
              buttons: true,
              dangerMode: true,
            })
            .then((willDelete) => {
              if (willDelete) {
                // let id_estudiante = $('#estudiante_id').val();
                
                $('#hdCupoId').val(cupo_id);

                $('#mdlNoSeleccionar').modal();

                
              }
            }); 
}