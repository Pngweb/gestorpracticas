let language={
    "sProcessing":     "Procesando...",
    "sLengthMenu":     "Mostrar _MENU_ registros",
    "sZeroRecords":    "No se encontraron resultados",
    "sEmptyTable":     "Ningún dato disponible en esta tabla",
    "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
    "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
    "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
    "sInfoPostFix":    "",
    "sSearch":         "Buscar:",
    "sUrl":            "",
    "sInfoThousands":  ",",
    "sLoadingRecords": "Cargando...",
    "oPaginate": {
        "sFirst":    "Primero",
        "sLast":     "Último",
        "sNext":     "Siguiente",
        "sPrevious": "Anterior"
    },
    "oAria": {
        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
    }
};

var table;
$(document).ready(function() {


    $('.bootstrap-datepicker').bsdatepicker({
        format: 'dd/mm/yyyy'
    }).on('changeDate', function (ev) {
         $(this).bsdatepicker('hide');
    });

    $('#cerrar').click(function(){
        $('#notification').hide();
    })

	table = $('#datatable-example').DataTable(
        {
            "language" : language,
            "ajax": { url :`api/cupos/${$('#hdEmpresa').val()}`,
                            type: 'post',
                            dataType: 'json',
                            data: function(d){
                                return $('#frmFiltrarCupos').serialize();
                            }
                            // data: {dependencia: $('#fltDependencia').val(), sucursal: $('#fltSucursal').val(), cargo: $('#fltCargo').val(), maestro: $('#fltMaestro').val()}
            },
            columns : [
                { data: "dependencia_descripcion" },
                { data: "sucursal_descripcion" },
                { data: "cargo_descripcion" },
                { data: "user_nombre" },
                { data: "cupo_competencias" },
                { data: "cupo_estado", render: (data) => { return `${data == '1' ? '<span class="bs-label label-success">Disponible</span>' : '<span class="bs-label label-primary">Asignado</span>'}` } },
                { data: "cupo_id", render: (data) => { return `<button class="btn btn-round btn-primary btnEdit" title="Editar" onClick="formEdit(${data})">
                                                                            <i class="glyph-icon icon-edit"></i>
                                                                        </button>
                                                                    <button class="btn btn-round btn-primary" title="Eliminar Cupo" onClick="EliminarCupo(${data})">
                                                                        <i class="glyph-icon icon-trash"></i>
                                                                    </button>
                                                                    <button class="btn btn-round btn-primary" title="Asignar Estudiante" onClick="Asignar(${data})">
                                                                        <i class="glyphicon glyphicon-check"></i>
                                                                    </button>` }, width: '20%'}
            ]
        }
    );

    $('#frmCrearCupo').on('submit', function(e){
        e.preventDefault();
        let form = $(this)[0];
        let datos = new FormData(form);
        axios.post(form.action, datos).then((res) => {
            $.jGrowl(res.data.Msj, {
                theme: 'bg-green' 
            });
            table.ajax.reload();
            form.reset();
            $('#mdlCrear').modal('hide');
        }, (err) => {
            if(err.response.status == 422){
                $.jGrowl(Object.values(err.response.data.errors)[0], {
                    theme: 'bg-red' 
                });
            }else{
                $.jGrowl('Error en el servidor.', {
                    theme: 'bg-red' 
                });
            }
        });
    });

    $('#frmEditarCupo').on('submit', function(e){
        e.preventDefault();
        let form = $(this)[0];
        let datos = new FormData(form);
        let url = `${form.action}/${$('#hdCupoId').val()}`
        axios.post(url, datos).then((res) => {
            $.jGrowl(res.data.Msj, {
                theme: 'bg-green' 
            });
            table.ajax.reload();
            form.reset();
            $('#mdlEditar').modal('hide');
        }, (err) => {
            if(err.response.status == 422){
                $.jGrowl(Object.values(err.response.data.errors)[0], {
                    theme: 'bg-red' 
                });
            }else{
                $.jGrowl('Error en el servidor.', {
                    theme: 'bg-red' 
                });
            }
        });
    });

    // Asignar Estudiante a una vacante

     $('#frmAsignar').on('submit', function(e){
        e.preventDefault();
        let form = $(this)[0];
        let datos = new FormData(form);
        let url = `${form.action}`
        axios.post(url, datos).then((res) => {
            $.jGrowl(res.data.Msj, {
                theme: 'bg-green' 
            });
            table.ajax.reload();
            form.reset();
            $('#mdlAsignar').modal('hide');
        }, (err) => {
            if(err.response.status == 422){
                $.jGrowl(Object.values(err.response.data.errors)[0], {
                    theme: 'bg-red' 
                });
            }else{
                $.jGrowl('Error en el servidor.', {
                    theme: 'bg-red' 
                });
            }
        });
    });

    $('#frmExcel').on('submit', function(e){
        e.preventDefault();
        let form = $(this)[0];
        let datos = new FormData(form);
        axios.post(form.action, datos).then((res) => {
            $.jGrowl(res.data.Msj, {
                theme: 'bg-green' 
            });
            table.ajax.reload();
            form.reset();
            $('#mdlImportar').modal('hide');
        }, (err) => {
            if(err.response.status == 422){
                $.jGrowl(Object.values(err.response.data.errors)[0], {
                    theme: 'bg-red' 
                });
            }else if(err.response.status == 403){
                $.jGrowl(err.response.data.message, {
                    theme: 'bg-red' 
                });
            }else{
                $.jGrowl('Error en el servidor.', {
                    theme: 'bg-red' 
                });
            }
        });
    });

    $('#frmFiltrarCupos').on('submit', function(e){
        e.preventDefault();
        // console.log($('#fltDependencia').val());
        table.ajax.reload();
    });
	
});

function Asignar(id){
    $('#idcupo').val(id);
    $('#mdlAsignar').modal();
    let empresa = $('#hdEmpresa').val();
    axios.get(`api/Asignados/${empresa}/${id}`).then(res=>{

       // console.log(res.data)
       if(res.data.length > 0){
        $('#nombres').val(res.data[0].asignado_nombres);
        $('#fechaingreso').val(res.data[0].asignado_fecha_inicio);
        $('#fechasalida').val(res.data[0].asignado_fehca_fin);
        $('#btnguardar').hide();
       }else{
        $('#nombres').val('');
        $('#fechaingreso').val('');
        $('#fechasalida').val('');
        $('#btnguardar').show();
       }
    })
}

function abrirModal(e){
    $($(e).data('target')).modal();
}

function formEdit(id){
    axios.get(`/cupos-practicas/${id}/edit`).then((res) => {
        console.log(res);
        $('#cdDependenciaEdit').val(res.data.dependencia_id);
        $('#cbSucursalEdit').val(res.data.sucursal_id);
        $('#cbSucursalEdit').val(res.data.sucursal_id);
        $('#cbCargoEdit').val(res.data.cargo_id);
        $('#cbMaestroEdit').val(res.data.maestro_id);
        $('#txtCompetenciasEdit').val(res.data.cupo_competencias);
        $('#hdCupoId').val(res.data.cupo_id);
        $('#txtMonitoreoEdit').val(res.data.link_monitoreo);
        $('#mdlEditar').modal();
    });
}

function EliminarCupo(id){
    axios.delete(`/cupos-practicas/${id}`).then((res) => {
            $.jGrowl(res.data.Msj, {
                theme: 'bg-green' 
            });
            table.ajax.reload();
    }, (err) => {
        $.jGrowl('Error en el servidor.', {
                theme: 'bg-red' 
            });
    });
}

// Validaciones fecha 
var bindDateRangeValidation = function (f, s, e) {
    if(!(f instanceof jQuery)){
            console.log("Not passing a jQuery object");
    }
  
    var jqForm = f,
        startDateId = s,
        endDateId = e;
  
    var checkDateRange = function (startDate, endDate) {
        var isValid = (startDate != "" && endDate != "") ? startDate <= endDate : true;
        return isValid;
    }

    var bindValidator = function () {
        var bstpValidate = jqForm.data('bootstrapValidator');
        var validateFields = {
            startDate: {
                validators: {
                    notEmpty: { message: 'Este campo es requerido.' },
                    callback: {
                        message: 'Start Date must less than or equal to End Date.',
                        callback: function (startDate, validator, $field) {
                            return checkDateRange(startDate, $('#' + endDateId).val())
                        }
                    }
                }
            },
            endDate: {
                validators: {
                    notEmpty: { message: 'This field is required.' },
                    callback: {
                        message: 'End Date must greater than or equal to Start Date.',
                        callback: function (endDate, validator, $field) {
                            return checkDateRange($('#' + startDateId).val(), endDate);
                        }
                    }
                }
            },
            customize: {
                validators: {
                    customize: { message: 'customize.' }
                }
            }
        }
        if (!bstpValidate) {
            jqForm.bootstrapValidator({
                excluded: [':disabled'], 
            })
        }
      
        jqForm.bootstrapValidator('addField', startDateId, validateFields.startDate);
        jqForm.bootstrapValidator('addField', endDateId, validateFields.endDate);
      
    };

    var hookValidatorEvt = function () {
        var dateBlur = function (e, bundleDateId, action) {
            jqForm.bootstrapValidator('revalidateField', e.target.id);
        }

        $('#' + startDateId).on("dp.change dp.update blur", function (e) {
            $('#' + endDateId).data("DateTimePicker").setMinDate(e.date);
            dateBlur(e, endDateId);
        });

        $('#' + endDateId).on("dp.change dp.update blur", function (e) {
            $('#' + startDateId).data("DateTimePicker").setMaxDate(e.date);
            dateBlur(e, startDateId);
        });
    }

    bindValidator();
    hookValidatorEvt();
};

$(function () {
  
    $('#fechaingreso').datetimepicker({ 
      pickTime: false, 
      format: "YYYY/MM/DD", 
      
    });
  
    $('#fechasalida').datetimepicker({ 
      pickTime: false, 
      format: "YYYY/MM/DD", 
      
    });

    //passing 1.jquery form object, 2.start date dom Id, 3.end date dom Id
    bindDateRangeValidation($("#frmAsignar"), 'fechaingreso', 'fechasalida');

});