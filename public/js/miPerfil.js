$(document).ready(function() {

	$('.nav-responsive').tabdrop();

    $('#logoEdit').on('change', function(e){
        $('#removeImgEdit').removeClass('hide');
        $('#img-logo-edit').removeClass('hide');
        let TmpPath = URL.createObjectURL(e.target.files[0])
        $('#img-logo-edit').attr('src',TmpPath);
    });

    $('#frmGuardarMiPerfil').on('submit', function(e){
        e.preventDefault();
        let form = $(this)[0];
        let datos = new FormData(form);
        axios.post(form.action, datos).then((res) => {
            console.log($('#logoEdit').get(0).files.length);
            if($('#logoEdit').get(0).files.length > 0){
                let TmpPath = URL.createObjectURL($('#logoEdit')[0].files[0]);
                console.log(TmpPath);
                $('.header-imagen').attr('src', TmpPath);
            }
            $.jGrowl(res.data.Msj, {
                theme: 'bg-green' 
            });
        }, (err) => {
            if(err.response.status == 422){
                $.jGrowl(Object.values(err.response.data.errors)[0], {
                    theme: 'bg-red' 
                });
            }else{
                $.jGrowl('Error en el servidor.', {
                    theme: 'bg-red' 
                });
            }
        });
    });

});

function quitarImagenEdit(){
    $('#img-logo-edit').attr('src','');
    $('#img-logo-edit').addClass('hide');
    $('#hdRemImg').val(true);
    $('#logoEdit').val('');
}