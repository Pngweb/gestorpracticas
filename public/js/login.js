$(document).ready(function(){

    if( $('#error-login').length > 0 ){
       
        $.jGrowl($('#error-login').val(), {
            sticky: false,
            position: 'top-right',
            theme: 'bg-red'
        });
       
    }


    $('.combo').click(function(){
        if($(this).data('perfil') == 'empresa'){
            $('#registroEmpresas').removeClass('hidden');
        }else{
            $('#registroEmpresas').addClass('hidden');
        }
		$('.login').show();
		$('.inicial').hide();
	})

	$('#volver').click(function(){
		$('.login').hide();
		$('.inicial').show();	
	});

    $('#volver_pass').click(function(){
        $('.password').hide();
        $('.login').show();   
    });

    $('#recuperarPass').click(function(){
        $('.password').show();
        $('.login').hide();
    });

    $('#btnPassword').click(function(){
        if($('#txtIdentificacionPass').val() == ''){
            $.jGrowl('Debe ingresar el numero de identificación', {
                sticky: false,
                position: 'top-right',
                theme: 'bg-red'
            });
            return;
        }
        axios.get('/recuperar-contrasena/'+$('#txtIdentificacionPass').val()).then(res => {
            swal("Reestablecer Contraseña", res.data.Msj, "success");
            $('#txtIdentificacionPass').val('');
            $('.password').hide();
            $('.login').show();   
        });
    });

    $('#reset-password').submit(function(e){
        e.preventDefault();
        if($('#password').val() != $('#cPassword').val()){
            $.jGrowl('Las contraseñas no coinciden.', {
                sticky: false,
                position: 'top-right',
                theme: 'bg-red'
            });
            return;
        }
        let form = $(this)[0];
        let datos = new FormData(form);
        axios.post('/guardar-contrasena', datos).then(res => {
            swal({
                        title : "Reestablecer Contraseña", 
                        text : res.data.Msj, 
                        icon : "success",
                        button : 'Aceptar'
                    }).then(value => {
                        location.href = '/';
                    });
        });
    })

});