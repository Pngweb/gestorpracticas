let language={
    "sProcessing":     "Procesando...",
    "sLengthMenu":     "Mostrar _MENU_ registros",
    "sZeroRecords":    "No se encontraron resultados",
    "sEmptyTable":     "Ningún dato disponible en esta tabla",
    "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
    "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
    "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
    "sInfoPostFix":    "",
    "sSearch":         "Buscar:",
    "sUrl":            "",
    "sInfoThousands":  ",",
    "sLoadingRecords": "Cargando...",
    "oPaginate": {
        "sFirst":    "Primero",
        "sLast":     "Último",
        "sNext":     "Siguiente",
        "sPrevious": "Anterior"
    },
    "oAria": {
        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
    }
};

var table;
$(document).ready(function() {

	table = $('#datatable-example').DataTable(
        {
            "language" : language,
            "ajax": `api/pasos-configurados/${$('#hdEmpresa').val()}`,
            columns : [
                {data: "pasos_orden" },
                { data: "pasos_nombre" },
                { data: "pasos_estado", render: (data) => { return ((data) ? '<span class="bs-label label-success">Activo</span>' : '<span class="bs-label label-danger">Inactivo</span>' ) } },
                { data: "pasos_id", render: (data) => { return `<button class="btn btn-round btn-primary btnEdit" onClick="formEdit(${data})">
                                                                            <i class="glyph-icon icon-edit"></i>
                                                                        </button>
                                                                    <button class="btn btn-round btn-primary" onClick="EliminarDependencia(${data})">
                                                                        <i class="glyph-icon icon-trash"></i>
                                                                    </button>` }}
            ]
        }
    );

    $('#frmCrearPasos').on('submit', function(e){
        e.preventDefault();

        if($('#id_dependencias').val() == ''){
        	 $.jGrowl('Seleccione una dependencia', {
                theme: 'bg-orange' 
            });
        }else{
		        let form = $(this)[0];
		        let datos = new FormData(form);

		        axios.post(form.action, datos).then((res) => {
		        if(!res.ErrorStatus){
		            $.jGrowl(res.data.Msj, {
		                theme: 'bg-green' 
		            });
		            table.ajax.reload();
		            form.reset();
		            $('#mdlCrear').modal('hide');
		        }else{
		            $.jGrowl(res.data.Msj, {
		                theme: 'bg-red' 
		            });
		        }
		    }, (err) => {
		        $.jGrowl(Object.values(err.response.data.errors)[0], {
		            theme: 'bg-red' 
		        });
		    });

	   }
    });

    $('#frmConfiguracionPasos').on('submit', function(e){
        e.preventDefault();

        if($('#id_dependencias').val() == ''){
        	 $.jGrowl('Seleccione una dependencia', {
                theme: 'bg-orange' 
            });
        }else{
	        let form = $(this)[0];
	        let datos = new FormData(form);
	        let url = `${form.action}/${$('#idconfiguracion').val()}`
	        axios.post(url, datos).then((res) => {

	        	if(!res.ErrorStatus){
		            $.jGrowl(res.data.Msj, {
		                theme: 'bg-green' 
		            });
		            table.ajax.reload();
		            form.reset();
		             $('#mdlEditar').modal('hide');
		        }else{
		            $.jGrowl(res.data.Msj, {
		                theme: 'bg-red' 
		            });
		        }
	           
	        }, (err) => {
	            if(err.response.status == 422){
	                $.jGrowl(Object.values(err.response.data.errors)[0], {
	                    theme: 'bg-red' 
	                });
	            }else{
	                $.jGrowl('Error en el servidor.', {
	                    theme: 'bg-red' 
	                });
	            }
	        });

        }
    });
	
});

function abrirModal(e){
    $($(e).data('target')).modal();
    consultardependencias();
}

function formEdit(id){
	consultardependenciasAC();
    axios.get(`/configuracionPasos/${id}/edit`).then((res) => {
    	// console.log(res.data);
    	if (res.data[0]) {
    		
	        $('#ACtxtNombre').val(res.data[0].pasos_nombre);
	        $('#idconfiguracion').val(res.data[0].pasos_id);
	        $('#ACtipo').val(res.data[0].pasos_tipo);
	        $('#ACtiempo').val(res.data[0].pasos_tiempo);
	        $('#ACorden').val(res.data[0].pasos_orden);
	        $('#ACresponsable').val(res.data[0].user_id);
	        $('#ACdescripcion').val(res.data[0].Descripcion);

	        if(res.data[0].pasos_estado == 1){
	            $('#ACchkEstado').prop('checked','checked');
	            $('.switch-toggle').addClass('switch-on');
	        }
    	}

    	setTimeout(() => {
    		let newArr2; 
	    	for (depens of res.data) {
	    		// console.log(depens);
	    		ids_dependencias.push(depens.dependencia_id);
				 newArr2 = [...new Set(ids_dependencias)]

	    		if(depens.pasosdep_estado == 1){
	    		 $(`#ACdepen${depens.dependencia_id}`).prop('checked','checked');
	    		}
	    	}

	    	$('#id_dependencias').val(newArr2);
  		}, 100);
    	
        $('#mdlEditar').modal();
    });
}

function EliminarDependencia(id){
    axios.delete(`/configuracionPasos/${id}`).then((res) => {
            $.jGrowl(res.data.Msj, {
                theme: 'bg-green' 
            });
            table.ajax.reload();
    }, (err) => {
        $.jGrowl('Error en el servidor.', {
                theme: 'bg-red' 
            });
    });
}

Array.prototype.remove = function(x) { 
    var i;
    for(i in this){
        if(this[i].toString() == x.toString()){
            this.splice(i,1)
        }
    }
}

let ids_dependencias = [];
function adddependencia(id){
	ids_dependencias.push(id);
	const result = [...new Set(ids_dependencias)]
	
	$('#id_dependencias').val(result);
	$('#CRid_dependencias').val(result);

}

function consultardependencias(){

	axios.get(`api/dependencias/${$('#hdEmpresa').val()}`).then((res) => {
          
           if(res.data.data.length > 0){
           		$('#depe div').remove();
	           	for(depen of res.data.data){
	           		// console.log(depen)
	           		$('#depe').append(`<div class="col-3 col-md-4">
                                    <div class="form-group">
                                        <div class="checkbox">
                                                ${depen.dependencia_descripcion}
                                              <label> 
                                                 <input name="depen${depen.dependencia_id}" onclick="adddependencia(${depen.dependencia_id})" type="checkbox" class="custom-checkbox" id="depen${depen.dependencia_id}" style="margin: 2px;">
                                              </label>
                                        </div>
                                    </div>
                                </div>`);

	           		
	           	}

           }	
    });
}

function consultardependenciasAC(){

	axios.get(`api/dependencias/${$('#hdEmpresa').val()}`).then((res) => {
          
           if(res.data.data.length > 0){
           		$('#dependencias div').remove();
	           	for(depen of res.data.data){
	           		$('#dependencias').append(`<div class="col-3 col-md-4">
                                    <div class="form-group">
                                        <div class="checkbox">
                                                ${depen.dependencia_descripcion}
                                              <label> 
                                                 <input name="ACdepen${depen.dependencia_id}" onclick="adddependencia(${depen.dependencia_id})" type="checkbox" class="custom-checkbox" id="ACdepen${depen.dependencia_id}" style="margin: 2px;">
                                              </label>
                                        </div>
                                    </div>
                                </div>`);
	           	}

           }	
    });
}