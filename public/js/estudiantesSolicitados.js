let language={
    "sProcessing":     "Procesando...",
    "sLengthMenu":     "Mostrar _MENU_ registros",
    "sZeroRecords":    "No se encontraron resultados",
    "sEmptyTable":     "Ningún dato disponible en esta tabla",
    "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
    "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
    "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
    "sInfoPostFix":    "",
    "sSearch":         "Buscar:",
    "sUrl":            "",
    "sInfoThousands":  ",",
    "sLoadingRecords": "Cargando...",
    "oPaginate": {
        "sFirst":    "Primero",
        "sLast":     "Último",
        "sNext":     "Siguiente",
        "sPrevious": "Anterior"
    },
    "oAria": {
        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
    }
};

var table;
$(document).ready(function() {
	
	$('#datatable-example').DataTable({"language" : language,"columnDefs": [{ "width": "150px", "targets": 0 },{ "width": "150px", "targets": 3 }]});

    $('.bootstrap-datepicker').bsdatepicker({
        format: 'dd/mm/yyyy'
    }).on('changeDate',function(e){
        var fec = $(this).val().split('/');
        var fec2 = $('.bootstrap-datepicker2').val().split('/');
        if(moment(moment(fec2[2]+'-'+fec2[1]+'-'+fec2[0])).diff(moment(fec[2]+'-'+fec[1]+'-'+fec[0]),'days') < 0){
            $.jGrowl('La fecha de Inicio no puede ser mayor a la fecha final.', {
                  sticky: false,
                  position: 'top-right',
                  theme: 'bg-red'
              });
            $(this).val($('.bootstrap-datepicker2').val());
        }
    });

    $('.bootstrap-datepicker2').bsdatepicker({
        format: 'dd/mm/yyyy'
    }).on('changeDate',function(e){
        var fec = $(this).val().split('/');
        var fec2 = $('.bootstrap-datepicker').val().split('/');
        if(moment(moment(fec2[2]+'-'+fec2[1]+'-'+fec2[0])).diff(moment(fec[2]+'-'+fec[1]+'-'+fec[0]),'days') > 0){
            $.jGrowl('La fecha final no puede ser menor a la fecha de inicio.', {
                  sticky: false,
                  position: 'top-right',
                  theme: 'bg-red'
              });
            $(this).val($('.bootstrap-datepicker').val());
        }
    });

	consultarEstudiantes();

	$(document).on('click','.btnCambiarEstado', function(){
		$('#hdEstado').val($(this).data('estado'));
		$('#hdSolicitud').val($(this).data('solicitud'));
		$('#mdlEstado').modal();
	});

	$('#btnResponder').on('click', function(){
		let observaciones = $('#txtObservaciones').val();
		if(observaciones == ''){
			$.jGrowl('Debe ingresar las observaciones.', {
                theme: 'bg-red' 
            });
		}else{
			cambiarEstado($('#hdEstado').val(), $('#hdSolicitud').val(), observaciones);
		}
	});

    $('#cbArea').on('change', function(){
        let area = (($(this).val() != '') ? $(this).val()  : 0 );
        consultarProgramas($('#hdInstitucion').val(), area);
    });

    $('#frmFiltros').on('submit', function(e){
        e.preventDefault();
        let form = $(this)[0];
        let datos = new FormData(form);
        consultarEstudiantes(datos);
    });

});

function consultarProgramas(institucion, area){
     axios.get(`api/programas/${institucion}/${area}`).then(res => {
        $('#cbPrograma option').remove();
        if(res.data.length > 0){
            $('#cbPrograma').append(`<option value="">Seleccione..</option>`);
            for(programa of res.data){
                $('#cbPrograma').append(`<option value="${programa.programa_id}">${programa.programa_descripcion}</option>`);
            }
        }else{
            $('#cbPrograma').append(`<option value="">No hay Informacion.</option>`);
        }
     });
}

function cambiarEstado(estado, solicitud, observaciones){
	let url = `estudiantes-solicitados`;
	let datos = {estado, solicitud, observaciones};
	axios.post(url, datos).then(res => {
		$.jGrowl(res.data.Msj, {
                theme: 'bg-green' 
            });
		$('#mdlEstado').modal('hide');
		consultarEstudiantes();
		$('#txtObservaciones').val('');
	}, (err) => {
		 $.jGrowl('Error en el servidor.', {
                theme: 'bg-red' 
            });
	});
}

function consultarEstudiantes(datos = null){
	let url = `api/estudiantes-solicitados/${$('#hdInstitucion').val()}`;
	axios.post(url, datos).then(res => {
        let table = $('#datatable-example').DataTable();
        table.clear().draw();
        for(estudiante of res.data){
            table.row.add([`<img class="img-thumbnail" src="/images/${((estudiante.estudiante_foto) ? `images_estudiantes/${estudiante.estudiante_foto}` : 'no-image.png')}" width="100">`,`<strong>${estudiante.estudiante_nombres} ${estudiante.estudiante_apellidos}</strong><br>C.C. ${numeral(estudiante.estudiante_identificacion).format('0,0')}<br>Edad: ${estudiante.edad}<br>${estudiante.estudiante_direccion}<br>${estudiante.ciudad_descripcion}<br>${estudiante.estudiante_telefono}<br><a href="https://web.whatsapp.com/"><button class="btn btn-round btn-success"><i class="fab fa-whatsapp"></i></button></a> <a href="mailto:${estudiante.estudiante_email}"><button class="btn btn-round btn-info"><i class="glyph-icon icon-envelope"></i></button></a>`,`${estudiante.programa_descripcion}<br>Modalidad: ${estudiante.etapa_descripcion}<br>Empresa: ${estudiante.empresa_nombre} <br>Empresa Nit: ${estudiante.empresa_nit} <br>Empresa Sitio web: ${estudiante.empresa_web}`,`<div style="text-align: center; padding: 20px 0;"><div class="group-2-btn">
                                                                                                    <button class="btn btn-round btn-primary btnCambiarEstado" data-estado="aceptada" data-solicitud="${estudiante.solicitud_cupo_id}" title="Aceptar">
                                                                                                        <i class="glyph-icon icon-check"></i>
                                                                                                    </button>
                                                                                                <button class="btn btn-round btn-primary btnCambiarEstado" data-estado="rechazada" data-solicitud="${estudiante.solicitud_cupo_id}" title="Rechazar">
                                                                                                    <i class="glyph-icon icon-close"></i>
                                                                                                </button>
                                                                                                </div></div>`]).draw();
        }
    }, (err) => {
                $.jGrowl('Error en el servidor.', {
                    theme: 'bg-red' 
                });
        });
}