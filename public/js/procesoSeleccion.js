var fechasregistradas = [];
$(document).ready(function() {

	$('.bootstrap-datepicker').bsdatepicker({
        format: 'dd/mm/yyyy',
         autoclose: true,
    }).on('changeDate', function (ev) {
	     $(this).bsdatepicker('hide');
	});
    // console.log('id estudiante ',$('#estudiante_id').val());
	consultarPasos($('#hdEmpresa').val(),$('#idpendencia').val(),$('#estudiante_id').val());

    consultar_info_cupo($('#hdEmpresa').val(),$('#estudiante_id').val());

	$('#frmCrearVancantes').on('submit', function(e){
        e.preventDefault();
        let form = $(this)[0];
        let datos = new FormData(form);
        let url = `${form.action}}`
        axios.post('/proceso-seleccion/seleccionar', datos).then((res) => {
            $.jGrowl(res.data.Msj, {
                theme: 'bg-green' 
            });
           window.location.href = '/estudiantes-seleccionados'
        }, (err) => {
            if(err.response.status == 422){
                $.jGrowl(Object.values(err.response.data.errors)[0], {
                    theme: 'bg-red' 
                });
            }else{
                $.jGrowl('Error en el servidor.', {
                    theme: 'bg-red' 
                });
            }
        });
    });

});

function abrirModal(id,orden){
    // console.log(e,' - ',id);
	$('#idprogram').val(id);
    $('#numero').val(orden);
	$('#fechaProgramar').val('')
	$('#observacionProgramar').val('');
    $('#mdlCrear').modal('show');
}

function Seleccionar(){
	 $('#mdlSeleccion').modal();
}

function noaplica(pasos_id,fechaProgramar){

		swal({
			  title: "Marcar como no aplica?",
			  text: "Estas a punto de marcar como aplica este paso",
			  icon: "warning",
			  buttons: true,
			  dangerMode: true,
			})
			.then((willDelete) => {
			  if (willDelete) {
			  	let observacionProgramar = $(`#observacion${pasos_id}`).val();
				let id_estudiante = $('#estudiante_id').val();
				let estado = 'No aplica';
				let datos = {
					pasos_id,
					fechaProgramar,
					observacionProgramar,
					estado,
					id_estudiante,
					aplica:true,
					actualizar:true
				}

				 axios.post('/proceso-seleccion', datos).then((res) => {
			        if(!res.ErrorStatus){
			            $.jGrowl(res.data.Msj, {
			                theme: 'bg-green' 
			            });
			            consultarPasos($('#hdEmpresa').val(),$('#idpendencia').val(),$('#estudiante_id').val());
			            $('#mdlCrear').modal('hide');
			        }else{
			            $.jGrowl(res.data.Msj, {
			                theme: 'bg-red' 
			            });
			        }
			    }, (err) => {
			        $.jGrowl(Object.values(err.response.data.errors)[0], {
			            theme: 'bg-red' 
			        });
			    });

			   	
			  }//cierre if
			});
	
}

function programar(){

	let pasos_id = $('#idprogram').val();
	let fechaProgramar = $('#fechaProgramar').val();
    var z = fechaProgramar.split('/');
    let fechavalida = z[1] + '-' + z[0] + '-' + z[2];
    let estado =  true;

    if(fechaProgramar){
            for (var i = 0; i < fechasregistradas.length; i++) {
                if(fechasregistradas[i].pasos_id == pasos_id){
                let fecha1 =  moment(fechasregistradas[i].proceso_fecha).format('MM-DD-YYYY');
                    if(fechasregistradas[i+1]){ 
                        let fecha2 =  moment(fechasregistradas[i+1].proceso_fecha).format('MM-DD-YYYY');
                         // console.log('Fecha ',fecha1,' rango ',fechavalida,' Next ',fecha2)

                        if (Date.parse(fechavalida) > Date.parse(fecha1) && Date.parse(fechavalida) <= Date.parse(fecha2)) {
                            estado = true;
                        }else{
                            estado = false
                        }
                    }
                    // else{
                    //     estado = true
                    // }
                    // else if(fechasregistradas[i-1]){
                    //     var fecha2 =  moment(fechasregistradas[i-1].proceso_fecha).format('MM-DD-YYYY');
                    //      // console.log('rango ',fechavalida,' Next ',fecha2)

                    //     if (Date.parse(fechavalida) < Date.parse(fecha2)) {
                    //         estado = false;
                    //     }
               
                }

            }

            if(estado){

                let observacionProgramar =  $('#observacionProgramar').val();
                let estado = 'Programada';
                let id_estudiante = $('#estudiante_id').val();
                let datos = {
                    pasos_id,
                    fechaProgramar,
                    observacionProgramar,
                    estado,
                    id_estudiante,
                }

                 axios.post('/proceso-seleccion', datos).then((res) => {
                    if(!res.ErrorStatus){
                        $.jGrowl(res.data.Msj, {
                            theme: 'bg-green' 
                        });
                        consultarPasos($('#hdEmpresa').val(),$('#idpendencia').val(),$('#estudiante_id').val(),$('#numero').val());
                        $('#mdlCrear').modal('hide');
                    }else{
                        $.jGrowl(res.data.Msj, {
                            theme: 'bg-red' 
                        });
                    }
                }, (err) => {
                    $.jGrowl(Object.values(err.response.data.errors)[0], {
                        theme: 'bg-red' 
                    });
                });

                $('#mdlCrear').modal('hide');


            }else{
                $.jGrowl('La fecha ingresada debe ser mayor a la programada y menor o igual a la fecha del siguiente paso', {
                    theme: 'bg-orange' 
                });
            } //Fin If
    }else{
        $.jGrowl('Debe seleccionar una fecha', {
                    theme: 'bg-orange' 
                });
    }

}

function realizarPaso(pasos_id,fechaProgramar){

	let observacionProgramar =  $(`#observacion${pasos_id}`).val();
	let estado = 'Realizado';
	let id_estudiante = $('#estudiante_id').val();
	let datos = {
		pasos_id,
		fechaProgramar,
		observacionProgramar,
		estado,
		id_estudiante,
		actualizar:true
	}

	 axios.post('/proceso-seleccion', datos).then((res) => {
        if(!res.ErrorStatus){
            $.jGrowl(res.data.Msj, {
                theme: 'bg-green' 
            });
            consultarPasos($('#hdEmpresa').val(),$('#idpendencia').val(),$('#estudiante_id').val());
            $('#mdlCrear').modal('hide');
        }else{
            $.jGrowl(res.data.Msj, {
                theme: 'bg-red' 
            });
        }
    }, (err) => {
        $.jGrowl(Object.values(err.response.data.errors)[0], {
            theme: 'bg-red' 
        });
    });

}

function consultar_info_cupo(empresa,idestudiante){

    axios.get(`http://inca.pngtechnology.co/api/consultar-info-proceso/${empresa}/${idestudiante}`).then(res => {
        // console.log(res.data);
        $('#namecupo').text(res.data[0].cupo_competencias);
        $('#dependencianame').val(res.data[0].dependencia_descripcion);
        $('#maestroname').val(res.data[0].user_nombre);
        $('#maestro').val(res.data[0].maestro_id);
    });

}


function consultarPasos(empresa,idpendencia,idestudiante,activo=null){
     axios.get(`http://inca.pngtechnology.co/api/consultar-pasos/${empresa}/${idpendencia}/${idestudiante}`).then(res => {
        // console.log(res.data);
        if(res.data){
            
            $('#foto').attr('src','/images/images_estudiantes/'+res.data[0].foto);
            $('#nombres').text(res.data[0].nombres)
            $('#cedula').text('C.C. '+res.data[0].cedula);
        	 $('#tab_info div').remove();
        	 $('#pasos li').remove();
        	 // let active = true;
        	 let i = 1;
        	 for(paso of res.data){
                if (paso.proceso_fecha){fechasregistradas.push(paso);}
                
	        	$('#pasos').append(`
					<li id="li${paso.pasos_id}" class="act">
					
	                    <a href="#step-${i}" data-toggle="tab">
	                            <label class="wizard-step ${paso.proceso_estado == 'Realizado' ? 'complete' :  paso.proceso_aplica == 0 ? 'noaplica' : ''}"><i class="glyph-icon icon-check"></i></label>
	                      <span class="wizard-description">
	                         ${paso.pasos_nombre} - ${paso.pasos_tipo}
	                         <small class="fecha${paso.pasos_id}">${paso.proceso_fecha ? paso.proceso_fecha : '' }</small>
	                      </span>
	                    </a>
	                </li>
					
	        		`);

	        	$('#tab_info').append(`

					<div class="tab-pane " id="step-${i}">
                        <div class="row">
                            <div class="col-md-4">
                                Paso: <strong>${paso.pasos_nombre}</strong><br>
                                Estado: <strong id="estado${paso.pasos_id}">${paso.proceso_estado ? paso.proceso_estado : 'Pendiente' }</strong><br>
                                Fecha: <strong class="fecha${paso.pasos_id}">${paso.proceso_fecha ? paso.proceso_fecha : '' }</strong><br>
                                
                                ${paso.proceso_fecha == null && paso.proceso_aplica != 0 ? `<button class="btn btn-sm btn-primary mrg5T" id="btnpro${paso.pasos_id}" data-target="#mdlCrear" onclick="abrirModal(${paso.pasos_id},${paso.pasos_orden})">
                                        <span>Programar</span>
                                </button>` : ''}
                                

                                ${paso.proceso_fecha && paso.proceso_aplica != 0 ? `<button class="btn btn-sm btn-primary mrg5T" id="btnrepro${paso.pasos_id}" data-target="#mdlCrear" onclick="abrirModal(${paso.pasos_id},${paso.pasos_orden})">
                                        <span>Reprogramar</span>
                                </button>` : ''}
                                
           
                                ${paso.proceso_fecha && paso.proceso_aplica != 0 ? `<button class="btn btn-sm btn-primary mrg5T" id="btnrealizado${paso.pasos_id}" onclick="realizarPaso(${paso.pasos_id},'${paso.proceso_fecha}')" >Paso Realizado</button>` : '' }

                                ${paso.proceso_aplica != 0 ? `<br><button class="btn btn-sm btn-warning" style="margin-top: 10px;" onclick="noaplica(${paso.pasos_id},'${paso.proceso_fecha}')" ><span style="margin: 5px;">Omitir Paso</span></button>` : ''}
                            
                            </div>
                            <div class="col-md-4">
                                <strong>Responsable</strong><br>
                                <p>
                                   ${paso.user_nombre}<br>
                                    Movil: (57)${paso.user_telefono} <br>
                                    E-mail: ${paso.user_email} <br>
                                    Resposable de ${paso.pasos_nombre}<br>
                                    Whtasapp: (57)${paso.user_telefono} <br>
                                </p>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Observaciones</label>
                                    <textarea rows="4" id="observacion${paso.pasos_id}" class="form-control">${paso.proceso_observaciones ? paso.proceso_observaciones : ''}</textarea>
                                </div>
                            </div>
                        </div>
                    </div>
					

	        	`);
                // console.log('estado ',paso.proceso_estado);
	        	if(paso.proceso_estado == 'Realizado' || paso.proceso_estado == 'No aplica' ){
	        		var id = i+1;
	        		setTimeout(() => {
                     // console.log(`#step-${id}`);   
	        		$(`#step-${id}`).addClass('active');
	        		$(`ul li:nth-child(${id})`).addClass('active');
  					}, 100);
	        	}

	        	i++;

        	 }//cierre for

        	 	let state;
        	 	let validacion = res.data.filter(res=>{
        	 		state = (paso.proceso_estado ? true : false);
        	 		return (res.proceso_estado == 'Realizado' ||  res.proceso_estado == 'No aplica' || res.proceso_estado == 'Programada' );
        	 	})
        	 	
        	 	if(!state){
        	 		$('#btnGuardarVacantes').hide();
        	 		$('#notification').show();
        	 	}else{
        	 		$('#notification').hide();
        	 		$('#btnGuardarVacantes').show();
        	 	}
        	 	 
                 
        	 	if(validacion.length == 0){
        	 		$(`#step-1`).addClass('active');
        			$(`ul li:nth-child(1)`).addClass('active');
        	 	}
                if (activo) {
                    // console.log('paso ',activo)
                    $(`#step-${activo}`).addClass('active');
                    $(`ul li:nth-child(${activo})`).addClass('active');
                }else{
                    for (var j = 0; j < validacion.length; j++) {
                        if(validacion[j].proceso_estado == 'Programada'){
                            $(`#step-${validacion[j].pasos_orden}`).addClass('active');
                            $(`ul li:nth-child(${validacion[j].pasos_orden})`).addClass('active');
                            return false;
                        }
                       
                    }
                }


        }
      	
     });
}

function pasoActual(paso){
    $(`#step-${paso}`).addClass('active');
    $(`ul li:nth-child(${paso})`).addClass('active');
}

function programadas(){
     let j = 1;
        for(step of fechasregistradas){
            console.log('Inicial',step.proceso_estado)
            if(step.proceso_estado == 'Programada'){
                console.log('programada ',`#step-${j}`);
                $(`#step-${j}`).addClass('active');
                $(`ul li:nth-child(${j})`).addClass('active');
                return false;
            }
            j++;
        }
}

function Noseleccionar(){

		swal({
			  title: "Descartar Estudiante?",
			  text: "Estas a punto de descartar este estudiante",
			  icon: "warning",
			  buttons: true,
			  dangerMode: true,
			})
			.then((willDelete) => {
			  if (willDelete) {
			   	let idsolicitud = $('#idsolicitud').val();

				let datos = {
					idsolicitud,
				}

				 axios.post('/proceso-seleccion/noseleccionar', datos).then((res) => {
			        if(!res.ErrorStatus){
			            $.jGrowl(res.data.Msj, {
			                theme: 'bg-green' 
			            });
			            window.location.href = '/estudiantes-disponibles';
			        }else{
			            $.jGrowl(res.data.Msj, {
			                theme: 'bg-red' 
			            });
			        }
			    }, (err) => {
			        $.jGrowl(Object.values(err.response.data.errors)[0], {
			            theme: 'bg-red' 
			        });
			    }); 
			  }
			});
	
}


// Validaciones fecha 
var bindDateRangeValidation = function (f, s, e) {
    if(!(f instanceof jQuery)){
			console.log("Not passing a jQuery object");
    }
  
    var jqForm = f,
        startDateId = s,
        endDateId = e;
  
    var checkDateRange = function (startDate, endDate) {
        var isValid = (startDate != "" && endDate != "") ? startDate <= endDate : true;
        return isValid;
    }

    var bindValidator = function () {
        var bstpValidate = jqForm.data('bootstrapValidator');
        var validateFields = {
            startDate: {
                validators: {
                    notEmpty: { message: 'Este campo es requerido.' },
                    callback: {
                        message: 'Start Date must less than or equal to End Date.',
                        callback: function (startDate, validator, $field) {
                            return checkDateRange(startDate, $('#' + endDateId).val())
                        }
                    }
                }
            },
            endDate: {
                validators: {
                    notEmpty: { message: 'This field is required.' },
                    callback: {
                        message: 'End Date must greater than or equal to Start Date.',
                        callback: function (endDate, validator, $field) {
                            return checkDateRange($('#' + startDateId).val(), endDate);
                        }
                    }
                }
            },
          	customize: {
                validators: {
                    customize: { message: 'customize.' }
                }
            }
        }
        if (!bstpValidate) {
            jqForm.bootstrapValidator({
                excluded: [':disabled'], 
            })
        }
      
        jqForm.bootstrapValidator('addField', startDateId, validateFields.startDate);
        jqForm.bootstrapValidator('addField', endDateId, validateFields.endDate);
      
    };

    var hookValidatorEvt = function () {
        var dateBlur = function (e, bundleDateId, action) {
            jqForm.bootstrapValidator('revalidateField', e.target.id);
        }

        $('#' + startDateId).on("dp.change dp.update blur", function (e) {
            $('#' + endDateId).data("DateTimePicker").setMinDate(e.date);
            dateBlur(e, endDateId);
        });

        $('#' + endDateId).on("dp.change dp.update blur", function (e) {
            $('#' + startDateId).data("DateTimePicker").setMaxDate(e.date);
            dateBlur(e, startDateId);
        });
    }

    bindValidator();
    hookValidatorEvt();
};

// validar rango de fechas etapa productiva

var bindDateRangeValidationPrd = function (f, s, e) {
    if(!(f instanceof jQuery)){
			console.log("Not passing a jQuery object");
    }
  
    var jqForm = f,
        startDateId = s,
        endDateId = e;
  
    var checkDateRange2 = function (startDate, endDate) {
        var isValid = (startDate != "" && endDate != "") ? startDate <= endDate : true;
        return isValid;
    }

    var bindValidator2 = function () {
        var bstpValidate = jqForm.data('bootstrapValidator');
        var validateFields = {
            startDate: {
                validators: {
                    notEmpty: { message: 'Este campo es requerido.' },
                    callback: {
                        message: 'Start Date must less than or equal to End Date.',
                        callback: function (startDate, validator, $field) {
                            return checkDateRange2(startDate, $('#' + endDateId).val())
                        }
                    }
                }
            },
            endDate: {
                validators: {
                    notEmpty: { message: 'This field is required.' },
                    callback: {
                        message: 'End Date must greater than or equal to Start Date.',
                        callback: function (endDate, validator, $field) {
                            return checkDateRange2($('#' + startDateId).val(), endDate);
                        }
                    }
                }
            },
          	customize: {
                validators: {
                    customize: { message: 'customize.' }
                }
            }
        }
        if (!bstpValidate) {
            jqForm.bootstrapValidator({
                excluded: [':disabled'], 
            })
        }
      
        jqForm.bootstrapValidator('addField', startDateId, validateFields.startDate);
        jqForm.bootstrapValidator('addField', endDateId, validateFields.endDate);
      
    };

    var hookValidatorEvt2 = function () {
        var dateBlur = function (e, bundleDateId, action) {
            jqForm.bootstrapValidator('revalidateField', e.target.id);
        }

        $('#' + startDateId).on("dp.change dp.update blur", function (e) {
            $('#' + endDateId).data("DateTimePicker").setMinDate(e.date);
            dateBlur(e, endDateId);
        });

        $('#' + endDateId).on("dp.change dp.update blur", function (e) {
            $('#' + startDateId).data("DateTimePicker").setMaxDate(e.date);
            dateBlur(e, startDateId);
        });
    }

    bindValidator2();
    hookValidatorEvt2();
};


$(function () {
  
    $('#fechaInicioLectiva').datetimepicker({ 
      pickTime: false, 
      format: "YYYY/MM/DD", 
      
    });
  
    $('#fechaFinLectiva').datetimepicker({ 
      pickTime: false, 
      format: "YYYY/MM/DD", 
      
    });

    //passing 1.jquery form object, 2.start date dom Id, 3.end date dom Id
    bindDateRangeValidation($("#frmCrearVancantes"), 'fechaInicioLectiva', 'fechaFinLectiva');

});

// funcion validar fecha productivo

$(function () {
  
    $('#fechaInicioProductiva').datetimepicker({ 
      pickTime: false, 
      format: "YYYY/MM/DD", 
    });
  
    $('#fechaFinProductiva').datetimepicker({ 
      pickTime: false, 
      format: "YYYY/MM/DD", 
    });

    //passing 1.jquery form object, 2.start date dom Id, 3.end date dom Id
    bindDateRangeValidationPrd($("#frmCrearVancantes"), 'fechaInicioProductiva', 'fechaFinProductiva');

});