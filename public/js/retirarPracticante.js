$(document).ready(function() {
   $('#back').click(function(){
         window.history.back();
    });

   $('.bootstrap-datepicker').bsdatepicker({
        format: 'dd/mm/yyyy',
         autoclose: true,
    }).on('changeDate', function (ev) {
         $(this).bsdatepicker('hide');
    });
    consultar_info_seleccionado($('#seleccionado_id').val());
	// table = $('#datatable-example').DataTable(
 //        {
 //            "language" : language,
 //            "ajax": `api/inasistencias/${$('#hdTercero').val()}/${$('#seleccionado_id').val()}`,
 //            columns : [
 //                { data: "inasistencia_id" },
 //                { data: "inasistencia_fecha", render: (data) => { return moment(data).format('DD/MM/YYYY') } },
 //                { data: "inasistencia_motivo" },
 //                { data: "inasistencia_observacion" },
 //                { data: "inasistencia_id", render: (data) => { return `<div class="group-2-btn">
 //                                                            <button class="btn btn-round btn-primary btnEdit" onClick="formEdit(${data})">
 //                                                                <i class="glyph-icon icon-edit"></i>
 //                                                            </button>
 //                                                        <button class="btn btn-round btn-primary" onClick="EliminarFuncionario(${data})">
 //                                                            <i class="glyph-icon icon-trash"></i>
 //                                                        </button>
 //                                                        </div>` }, width: '13%'}
 //            ]
 //        }
 //    );

    $('#frmCrearRetiro').on('submit', function(e){
        e.preventDefault();

        swal({
          title: "Retirar Practicante?",
          text: "Estas a punto de retirar este Practicante",
          icon: "warning",
          buttons: true,
          dangerMode: true,
        })
        .then((willDelete) => {
          if (willDelete) {

            let form = $(this)[0];
            let datos = new FormData(form);
            axios.post(form.action, datos).then((res) => {
              if(!res.ErrorStatus){
                    $.jGrowl(res.data.Msj, {
                        theme: 'bg-green' 
                    });
                   
                    form.reset();
                     window.location.href = '/estudiantes-seleccionados-info'
                }else{
                    $.jGrowl(res.data.Msj, {
                        theme: 'bg-red' 
                    });
                }
            }, (err) => {
                if(err.response.status == 422){
                    $.jGrowl(Object.values(err.response.data.errors)[0], {
                        theme: 'bg-red' 
                    });
                }else{
                    $.jGrowl('Error en el servidor.', {
                        theme: 'bg-red' 
                    });
                }
            });

        }
     });
    });
	
});

function consultar_info_seleccionado(idseleccionado){
    axios.get(`http://inca.pngtechnology.co/api/info-estudiante-seleccionado/${idseleccionado}`).then(res => {
        // console.log(res.data);
        $('#cedula').text('C.C '+res.data[0].estudiante_identificacion);
        $('#nombres').text(res.data[0].estudiante_nombres+' '+res.data[0].estudiante_apellidos);
        $('#programa').text(res.data[0].programa_descripcion);
        $('#tipo').text(res.data[0].solicitud_tipo_acuerdo);
        $('#empresa').text(res.data[0].empresa_nombre);
        $('#dependencia').text(res.data[0].dependencia_descripcion);
        $('#fechaI').text(res.data[0].solicitud_fecha_inicio);
        $('#fechaF').text(res.data[0].solicitud_fecha_culminacion);
    });
}