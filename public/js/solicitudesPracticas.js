let language={
    "sProcessing":     "Procesando...",
    "sLengthMenu":     "Mostrar _MENU_ registros",
    "sZeroRecords":    "No se encontraron resultados",
    "sEmptyTable":     "Ningún dato disponible en esta tabla",
    "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
    "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
    "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
    "sInfoPostFix":    "",
    "sSearch":         "Buscar:",
    "sUrl":            "",
    "sInfoThousands":  ",",
    "sLoadingRecords": "Cargando...",
    "oPaginate": {
        "sFirst":    "Primero",
        "sLast":     "Último",
        "sNext":     "Siguiente",
        "sPrevious": "Anterior"
    },
    "oAria": {
        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
    }
};

var table;
$(document).ready(function() {

    $('#datatable-example').DataTable({"language" : language,"columnDefs": [{ "width": "150px", "targets": 0 },{ "width": "150px", "targets": 3 }]});

    $('.bootstrap-datepicker').bsdatepicker({
        format: 'dd/mm/yyyy'
    }).on('changeDate',function(e){
        var fec = $(this).val().split('/');
        var fec2 = $('.bootstrap-datepicker2').val().split('/');
        if(moment(moment(fec2[2]+'-'+fec2[1]+'-'+fec2[0])).diff(moment(fec[2]+'-'+fec[1]+'-'+fec[0]),'days') < 0){
            $.jGrowl('La fecha de Inicio no puede ser mayor a la fecha final.', {
                  sticky: false,
                  position: 'top-right',
                  theme: 'bg-red'
              });
            $(this).val($('.bootstrap-datepicker2').val());
        }
    });

    $('.bootstrap-datepicker2').bsdatepicker({
        format: 'dd/mm/yyyy'
    }).on('changeDate',function(e){
        var fec = $(this).val().split('/');
        var fec2 = $('.bootstrap-datepicker').val().split('/');
        if(moment(moment(fec2[2]+'-'+fec2[1]+'-'+fec2[0])).diff(moment(fec[2]+'-'+fec[1]+'-'+fec[0]),'days') > 0){
            $.jGrowl('La fecha final no puede ser menor a la fecha de inicio.', {
                  sticky: false,
                  position: 'top-right',
                  theme: 'bg-red'
              });
            $(this).val($('.bootstrap-datepicker').val());
        }
    });


	// table = $('#datatable-example').DataTable(
 //        {
 //            "language" : language,
 //            "ajax": `api/solicitud-practicantes/${$('#hdEmpresa').val()}`,
 //            columns : [
 //                { data: "solicitud_cupo_id" },
 //                { data: "solicitud_estado" },
 //                { data: "programa_descripcion" },
 //                { data: "sucursal_descripcion" },
 //                { data: "dependencia_descripcion" },
 //                { data: "user_nombre" },
 //                { data: "created_at" },
 //                { data: "solicitud_fecha_respuesta" },
 //            ]
 //        }
 //    );

    consultarEstudiantes();

    $('#cbArea').on('change', function(){
        let area = (($(this).val() != '') ? $(this).val()  : 0 );
        consultarProgramas($('#cbInstitucion').val(), area);
    });

    $('#frmFiltros').on('submit', function(e){
        e.preventDefault();
        let form = $(this)[0];
        let datos = new FormData(form);
        consultarEstudiantes(datos);
    });


	
});



function consultarProgramas(institucion, area){
     axios.get(`api/programas/${institucion}/${area}`).then(res => {
        $('#cbPrograma option').remove();
        if(res.data.length > 0){
            $('#cbPrograma').append(`<option value="">Seleccione..</option>`);
            for(programa of res.data){
                $('#cbPrograma').append(`<option value="${programa.programa_id}">${programa.programa_descripcion}</option>`);
            }
        }else{
            $('#cbPrograma').append(`<option value="">No hay Informacion.</option>`);
        }
     });
}

function consultarEstudiantes(datos = null){
    let url = `api/solicitud-practicantes/${$('#hdEmpresa').val()}`;
    axios.post(url, datos).then(res => {
        // console.log(res.data);
        let table = $('#datatable-example').DataTable();
        table.clear().draw();
        for(estudiante of res.data){
            table.row.add([estudiante.solicitud_cupo_id,estudiante.solicitud_estado.toUpperCase(),estudiante.programa_descripcion,estudiante.sucursal_descripcion,estudiante.dependencia_descripcion,estudiante.user_nombre,estudiante.solicitud_fecha_inicio,estudiante.solicitud_fecha_respuesta, ((estudiante.solicitud_observaciones) ? '<button class="btn btn-round btn-info" onclick="mostrarObs(`'+estudiante.solicitud_observaciones+'`)"><i class="glyph-icon icon-eye"></i></button>' : '')]).draw();
        }
    }, (err) => {
            $.jGrowl('Error en el servidor.', {
                theme: 'bg-red' 
            });
        });
}

function mostrarObs(obs){
    swal("Solicitud de Practicantes", obs, "info");
}