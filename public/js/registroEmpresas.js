$(document).ready(function() {
    
    $('#logo').on('change', function(e){
        $('#removeImg').removeClass('hide');
        $('#img-logo').removeClass('hide');
        let TmpPath = URL.createObjectURL(e.target.files[0])
        $('#img-logo').attr('src',TmpPath);
    });

});

function guardarEmpresas(){
	let form = $('#frmCrearEmpresa')[0];
    let datos = new FormData(form);
    axios.post(form.action, datos).then((res) => {
        if(!res.ErrorStatus){
             swal({
		        title: "Registro de Empresas",
		        text: res.data.Msj,
		        icon: "success",
		        closeOnClickOutside: false
		    }).then(() => {
		        location.href = '/';
		    });
            table.ajax.reload();
            form.reset();
            $('#mdlCrear').modal('hide');
        }else{
            $.jGrowl(res.data.Msj, {
                theme: 'bg-red' 
            });
        }
    }, (err) => {
        $.jGrowl(Object.values(err.response.data.errors)[0], {
            theme: 'bg-red' 
        });
    });
   
}

function quitarImagen(){
    $('#img-logo').attr('src','');
    $('#img-logo').addClass('hide');
    $('#logo').val('');
}