let language={
    "sProcessing":     "Procesando...",
    "sLengthMenu":     "Mostrar _MENU_ registros",
    "sZeroRecords":    "No se encontraron resultados",
    "sEmptyTable":     "Ningún dato disponible en esta tabla",
    "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
    "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
    "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
    "sInfoPostFix":    "",
    "sSearch":         "Buscar:",
    "sUrl":            "",
    "sInfoThousands":  ",",
    "sLoadingRecords": "Cargando...",
    "oPaginate": {
        "sFirst":    "Primero",
        "sLast":     "Último",
        "sNext":     "Siguiente",
        "sPrevious": "Anterior"
    },
    "oAria": {
        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
    }
};
var tableConvenios;
var tableDisponibles;

$(document).ready(function() {
    
    tableConvenios = $('#tbConvenios').DataTable(
        {
            "language" : language,
            "ajax": `api/empresas/convenios/${$('#hdEmpresa').val()}`,
             columns : [
                { data : "institucion_logo", render: (data) => { return `<img src="/images/${((data) ? `images_institucion/${data}` : 'no-image.png')}" width="50">` }},
                { data: "institucion_nombre" },
                { data: "ciudad_descripcion" },
                { data: "institucion_email" },
                { data: (row) => { return ((row.documento_empresa_id) ? `<a href="../documentos_empresas/${row.documento_file}" target="_blank"><button class="btn btn-round btn-primary"><i class="glyph-icon icon-download"></i></button></a>` : `<button class="btn btn-round btn-primary btnDoc" onClick="selectDocumento(${row.empresa_institucion_id})"><i class="glyph-icon icon-file-text"></i></button>` )}},
                { data: "empresa_institucion_estado", render: (data) => { return ((data == 1) ? `<span class="bs-label label-yellow">Solicitado</span>` : `<span class="bs-label label-success">Aprobado</span>` ) } },
                {data: (row) => { return ((row.rol_id == 3) ? `<button class="btn btn-round btn-primary" onClick="responderSolicitud(${row.empresa_institucion_id},4)">
                                                                            <i class="glyph-icon icon-minus"></i>
                                                                        </button>` :((row.empresa_institucion_estado == 2) ? `<button class="btn btn-round btn-primary" onClick="responderSolicitud(${row.empresa_institucion_id},4)">
                                                                            <i class="glyph-icon icon-minus"></i>
                                                                        </button>`  :  `<button class="btn btn-round btn-primary" onClick="responderSolicitud(${row.empresa_institucion_id},3)">
                                                                            <i class="glyph-icon icon-close"></i>
                                                                        </button>
                                                                        <button class="btn btn-round btn-primary" onClick="responderSolicitud(${row.empresa_institucion_id},2)">
                                                                            <i class="glyph-icon icon-check"></i>
                                                                        </button>`)) }}
            ]
        }
    );

    tableDisponibles = $('#tbDisponibles').DataTable(
        {
            "language" : language,
            "ajax": `api/instituciones-disponibles/${$('#hdEmpresa').val()}`,
             columns : [
                { data : "institucion_logo", render: (data) => { return `<img src="/images/${((data) ? `images_institucion/${data}` : 'no-image.png')}" width="50">` }},
                { data: "institucion_nombre" },
                { data: "ciudad_descripcion" },
                { data: "institucion_email" },
                { data: "institucion_id", render: (data) => { return `<button class="btn btn-round btn-primary" onClick="solicitarConvenio(${data})">
                                                                            <i class="glyph-icon icon-envelope"></i>
                                                                        </button>` }}
            ]
        }
    );

    $('#cbDocumento').change(function(){
        axios.get(`api/documentos/${$('#hdEmpresa').val()}/${$(this).val()}`).then(res => {
            $('#cbArchivo option').remove();
            $('#cbArchivo').append(`<option value="">Seleccione..</option>`);
            for(archivo of res.data)
            $('#cbArchivo').append(`<option value="${archivo.doc_emp_id}">${archivo.documento_file}</option>`);
        });
    });

    $('#frmConvenioDoc').submit(function(e){
        e.preventDefault();
        if($('#cbDocumento').val() == ''){
            $.jGrowl('Debe seleccionar el documento.', {
                theme: 'bg-red' 
            });
            return;
        }else if($('#cbArchivo').val() == ''){
            $.jGrowl('Debe seleccionar el archivo.', {
                theme: 'bg-red' 
            });
            return;
        }
        let form = $(this)[0];
        let datos = new FormData(form);
        axios.post(`${form.action}/${ $('#hdConvenio').val()}`, datos).then(res => {
            $.jGrowl(res.data.Msj, {
                theme: 'bg-green' 
            });
            tableConvenios.ajax.reload();
            form.reset();
            $('#mdlDocumento').modal('hide');
        });
    });

});

function responderSolicitud(convenio,estado){
    axios.put(`convenios/${convenio}`,{estado}).then((res) => {
         if(!res.ErrorStatus){
            $.jGrowl(res.data.Msj, {
                theme: 'bg-green' 
            });
            tableDisponibles.ajax.reload();
            tableConvenios.ajax.reload();
         }else{
            $.jGrowl(res.data.Msj, {
                theme: 'bg-red' 
            });
         }
    });
}

// function EliminarConvenio(convenio){
//     axios.put(`convenios/${convenio}`).then((res) => {
//          if(!res.ErrorStatus){
//             $.jGrowl(res.data.Msj, {
//                 theme: 'bg-green' 
//             });
//             tableDisponibles.ajax.reload();
//             tableConvenios.ajax.reload();
//          }else{
//             $.jGrowl(res.data.Msj, {
//                 theme: 'bg-red' 
//             });
//          }
//     });
// }

function solicitarConvenio(institucion){
    axios.post('convenios',{institucion, empresa: $('#hdEmpresa').val()}).then((res) => {
         if(!res.ErrorStatus){
            $.jGrowl(res.data.Msj, {
                theme: 'bg-green' 
            });
            tableDisponibles.ajax.reload();
            tableConvenios.ajax.reload();
         }else{
            $.jGrowl(res.data.Msj, {
                theme: 'bg-red' 
            });
         }
    });
}

function selectDocumento(convenio){
    $('#hdConvenio').val(convenio);
    $('#mdlDocumento').modal();
}