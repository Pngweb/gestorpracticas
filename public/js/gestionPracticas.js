$(document).ready(function() {

	consultar_cupos($('#hdEmpresa').val());
	consultar_cupos_por_vencer($('#hdEmpresa').val());
	consultar_cupos_disponibles($('#hdEmpresa').val());
	consultar_cuposXarea($('#hdEmpresa').val());
	consultar_ultimos_cupos_asigandos($('#hdEmpresa').val());
	consultar_instituciones($('#hdEmpresa').val());
});

// Cargar Dashboard

function consultar_cupos(empresa){
    axios.get(`api/dashboard-cantidades/${empresa}`).then(res => {
        // console.log(res.data);
        $('#solicitudes').text(res.data['solicitudes'][0].Solicitudes);
        $('#total').text(res.data['total'][0].Total);
        $('#ocupados').text(res.data['ocupados'][0].Ocupados);
        $('#disponibles').text(res.data['disponibles'][0].Disponibles);
        $('#empleados').text(res.data['empleados'][0].Empleados);

    });

}

function consultar_instituciones(empresa){
	axios.get(`api/dashboard-instituciones/${empresa}`).then(res => {
		// console.log(res.data);
        if(res.data.length > 0){
        	$('#practicantesinstituciones div').remove();
        	for (datos of res.data) {
        		 $('#practicantesinstituciones').append(`
	        		<div class="col-md-6">
                        <div class="tile-box bg-blue-alt">
                            <div class="tile-header text-center">
                                ${datos.institucion_nombre}
                            </div>
                            <div class="tile-content-wrapper">
                                <div class="tile-content font-size-20">
                                    ${datos.cantidad}
                                </div>
                                <small>
                                    Practicantes
                                </small>
                            </div>
                        </div>
                    </div>
	        	`);

        	}
        }
    });	
}

function consultar_ultimos_cupos_asigandos(empresa){
	axios.get(`api/dashboard-ultimos-cupos/${empresa}`).then(res => {
		// console.log(res.data.length);
        if(res.data.length > 0){
        	$('#ultimosCupos div').remove();
        	for (datos of res.data) {
        		 $('#ultimosCupos').append(`
	        		<div class="tl-row">
                            <div class="tl-item float-right">
                                <div class="tl-icon bg-red">
                                    <i class="glyph-icon icon-info"></i>
                                </div>
                                <div class="popover right">
                                    <div class="arrow"></div>
                                    <div class="popover-content">
                                        <div class="tl-label bs-label label-info">${datos.cargo_descripcion}</div>
                                        <p class="tl-content">Cupo de práctica asigando a <strong>${datos.dependencia_descripcion}</strong> </p>
                                        <div class="tl-time">
                                            <i class="glyph-icon icon-clock-o"></i>
                                             ${datos.solicitud_fecha_inicio} 
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
	        	`);

        	}
        }
    });
}

function consultar_cupos_por_vencer(empresa){
	axios.get(`api/dashboard-dependencias-vencimiento/${empresa}`).then(res => {
		// console.log(res.data);
        if(res.data.length > 0){
        	$('#tbvencimiento tr').remove();
            let i = 1;
        	for (datos of res.data) {
        		 $('#tbvencimiento').append(`
	        		<tr>
	                    <td>${i}</td>
	                    <td>${datos.dependencia_descripcion}</td>
	                    <td>${datos.sucursal_descripcion}</td>
	                    <td>${datos.cargo_descripcion}</td>
	                </tr>
	        	`);
        	}
            i++;
        }
    });
}

function consultar_cupos_disponibles(empresa){
	axios.get(`api/dashboard-dependencias-disponibles/${empresa}`).then(res => {
		// console.log(res.data);
        if(res.data.length > 0){
        	$('#tbdisponibles tr').remove();
            let i = 1;
        	for (datos of res.data) {
        		 $('#tbdisponibles').append(`
	        		<tr>
	                    <td>${i}</td>
	                    <td>${datos.dependencia_descripcion}</td>
	                    <td>${datos.sucursal_descripcion}</td>
	                    <td>${datos.cargo_descripcion}</td>
	                </tr>
	        	`);

                i++;
        	}
        }
    });
}

function consultar_cuposXarea(empresa){
    axios.get(`api/dashboard-dependencias-ocupados/${empresa}`).then(res => {
        if(res.data.length > 0){
        	$('#tbareas tr').remove();
            let i = 1;
        	for (datos of res.data) {
        		 $('#tbareas').append(`
	        		<tr>
	                    <td>${i}</td>
	                    <td>${datos.dependencia_descripcion}</td>
	                    <td>SUCURSAL PRINCIPAL</td>
	                    <td>${datos.cantidad}</td>
	                </tr>
	        	`);

        	}
            i++;
        }
    });
}