var initialLocaleCode = 'es';
$(document).ready(function() {
	
consultar_cupos_inicio($('#hdEmpresa').val());
armarCalendario($('#hdEmpresa').val());

consultar_cupos($('#hdEmpresa').val());
consultar_instituciones($('#hdEmpresa').val());
consultar_cupos_por_vencer($('#hdEmpresa').val());
consultar_cupos_disponibles($('#hdEmpresa').val());
consultar_cuposXarea($('#hdEmpresa').val());
consultar_ultimos_cupos_asigandos($('#hdEmpresa').val());



});

function consultar_cupos_inicio(empresa){
    axios.get(`api/dashboard-cantidades-inicio/${empresa}`).then(res => {
        // console.log(res.data);
        $('#convenios').text(res.data['convenios'][0].Convenios);
        $('#solicitudes').text(res.data['solicitudes'][0].Solicitudes);
        $('#total').text(res.data['total'][0].Total);
        $('#practicantes').text(res.data['practicantes'][0].Practicantes);
        $('#disponibles').text(res.data['disponibles'][0].Disponibles+' Disponibles');

    });

}

function armarCalendario(empresa){

	let fechas = [];
     axios.get(`api/fechas-calendario/${empresa}`).then(res => {
     
    $('#calendar-example-1').fullCalendar({
    	 monthNames: ['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'],
	    monthNamesShort: ['Ene','Feb','Mar','Abr','May','Jun','Jul','Ago','Sep','Oct','Nov','Dic'],
	    dayNames: ['Domingo','Lunes','Martes','Miércoles','Jueves','Viernes','Sábado'],
	    dayNamesShort: ['Dom','Lun','Mar','Mié','Jue','Vie','Sáb'],
        header: {
            left: 'prev,next',
            center: 'title',
            right: 'month,agendaWeek,agendaDay',
          
        },
        locale: initialLocaleCode,
        
        // defaultDate: '2014-01-12',
        editable: false,
        events:res.data
        // [{
        //     title: 'All Day Event',
        //     start: '2020-08-12'
        // }, {
        //     title: 'Long Event',
        //     start: '2020-08-31'
        // }]
        });
         // $('.fc-agenda-axis').text('Todo el día');
         $('.fc-button-month').text('Mes');
         $('.fc-button-agendaWeek').text('Semana');
         $('.fc-button-agendaDay').text('Día');

     });

}

function consultar_cupos(empresa){
    axios.get(`api/dashboard-cantidades/${empresa}`).then(res => {
        // console.log(res.data);
        $('#c_solicitudes').text(res.data['solicitudes'][0].Solicitudes);
        $('#c_total').text(res.data['total'][0].Total);
        $('#ocupados').text(res.data['ocupados'][0].Ocupados);
        $('#c_disponibles').text(res.data['disponibles'][0].Disponibles);
        $('#empleados').text(res.data['empleados'][0].Empleados);

    });

}

function consultar_instituciones(empresa){
    axios.get(`api/dashboard-instituciones/${empresa}`).then(res => {
        // console.log(res.data);
        if(res.data.length > 0){
            $('#practicantesinstituciones div').remove();
            for (datos of res.data) {
                 $('#practicantesinstituciones').append(`
                    <div class="col-md-3">
                        <div class="tile-box bg-blue-alt">
                            <div class="tile-header text-center">
                                ${datos.institucion_nombre}
                            </div>
                            <div class="tile-content-wrapper">
                                <div class="tile-content font-size-20">
                                    ${datos.cantidad}
                                </div>
                                <small>
                                    Practicantes
                                </small>
                            </div>
                        </div>
                    </div>
                `);

            }
        }
    }); 
}

function consultar_cupos_por_vencer(empresa){
    axios.get(`api/dashboard-dependencias-vencimiento/${empresa}`).then(res => {
        // console.log(res.data);
        if(res.data.length > 0){
            $('#tbvencimiento tr').remove();
            let i = 1;
            for (datos of res.data) {
                 $('#tbvencimiento').append(`
                    <tr>
                        <td>${i}</td>
                        <td>${datos.dependencia_descripcion}</td>
                        <td>${datos.sucursal_descripcion}</td>
                        <td>${datos.cargo_descripcion}</td>
                    </tr>
                `);
            }
            i++;
        }
    });
}

function consultar_cupos_disponibles(empresa){
    axios.get(`api/dashboard-dependencias-disponibles/${empresa}`).then(res => {
        // console.log(res.data);
        if(res.data.length > 0){
            $('#tbdisponibles tr').remove();
            let i = 1;
            for (datos of res.data) {
                 $('#tbdisponibles').append(`
                    <tr>
                        <td>${i}</td>
                        <td>${datos.dependencia_descripcion}</td>
                        <td>${datos.sucursal_descripcion}</td>
                        <td>${datos.cargo_descripcion}</td>
                    </tr>
                `);

                i++;
            }
        }
    });
}

function consultar_cuposXarea(empresa){
    axios.get(`api/dashboard-dependencias-ocupados/${empresa}`).then(res => {
        if(res.data.length > 0){
            $('#tbareas tr').remove();
            let i = 1;
            for (datos of res.data) {
                 $('#tbareas').append(`
                    <tr>
                        <td>${i}</td>
                        <td>${datos.dependencia_descripcion}</td>
                        <td>SUCURSAL PRINCIPAL</td>
                        <td>${datos.cantidad}</td>
                    </tr>
                `);

            }
            i++;
        }
    });
}

function consultar_ultimos_cupos_asigandos(empresa){
    axios.get(`api/dashboard-ultimos-cupos/${empresa}`).then(res => {
        // console.log(res.data.length);
        if(res.data.length > 0){
            $('#ultimosCupos div').remove();
            for (datos of res.data) {
                 $('#ultimosCupos').append(`
                    <div class="tl-row">
                            <div class="tl-item float-right">
                                <div class="tl-icon bg-red">
                                    <i class="glyph-icon icon-info"></i>
                                </div>
                                <div class="popover right">
                                    <div class="arrow"></div>
                                    <div class="popover-content">
                                        <div class="tl-label bs-label label-info">${datos.cargo_descripcion}</div>
                                        <p class="tl-content">Cupo de práctica asigando a <strong>${datos.dependencia_descripcion}</strong> </p>
                                        <div class="tl-time">
                                            <i class="glyph-icon icon-clock-o"></i>
                                             ${datos.solicitud_fecha_inicio} 
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                `);

            }
        }
    });
}