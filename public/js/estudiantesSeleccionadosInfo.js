$(document).ready(function() {
  consultar_info_estudiante($('#idEstudiante').val());
  // $(".tabs").tabs();

});	

function consultar_info_estudiante(idestudiante){
	axios.get(`http://inca.pngtechnology.co/api/info-estudiante/${idestudiante}`).then(res => {
		// console.log(res.data);
       	$('#foto').attr('src','/images/images_estudiantes/'+res.data[0].estudiante_foto);
       	$('#cedula').text('C.C '+res.data[0].estudiante_identificacion);
        $('#programa').text(res.data[0].programa_descripcion);
       	$('#nombres').text(res.data[0].estudiante_nombres+' '+res.data[0].estudiante_apellidos);
        $('#sangre').text(res.data[0].grupo_sanguineo_descripcion);
       	$('#direccion').text(res.data[0].estudiante_direccion);
       	$('#telefono').text(res.data[0].estudiante_telefono)
       	$('#correo').append(`<a href="mailto:lauraperez@centroinca.com" class="textos-info" target="_blank">${res.data[0].estudiante_email} </a>`)
    });
}