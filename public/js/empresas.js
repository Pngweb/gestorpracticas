let language={
    "sProcessing":     "Procesando...",
    "sLengthMenu":     "Mostrar _MENU_ registros",
    "sZeroRecords":    "No se encontraron resultados",
    "sEmptyTable":     "Ningún dato disponible en esta tabla",
    "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
    "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
    "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
    "sInfoPostFix":    "",
    "sSearch":         "Buscar:",
    "sUrl":            "",
    "sInfoThousands":  ",",
    "sLoadingRecords": "Cargando...",
    "oPaginate": {
        "sFirst":    "Primero",
        "sLast":     "Último",
        "sNext":     "Siguiente",
        "sPrevious": "Anterior"
    },
    "oAria": {
        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
    }
};
var table;
var tableUsers;
$(document).ready(function() {

    $('.bootstrap-datepicker').bsdatepicker({
        format: 'dd/mm/yyyy'
    });
    
    table = $('#datatable-example').DataTable(
        {
            "language" : language,
            "ajax": "api/empresas",
            columns : [
                { data : "empresa_logo", render: (data) => { return `<img src="/images/${((data) ? `images_empresas/${data}` : 'no-image.png')}" width="50">` }},
                { data: "empresa_nit" },
                { data: "empresa_nombre" },
                { data: "org_jur_descripcion" },
                { data: "empresa_web", render: (data) => { return `<a href="http://${data}" target="_blank">${data}</a>` } },
                { data: "sec_prod_descripcion" },
                { data: "empresa_id", render: (data) => { 
                    return `<div class="group-2-btn">
                                    <button class="btn btn-round btn-primary btnEdit" onClick="formEdit(${data})" title="Editar">
                                        <i class="glyph-icon icon-edit"></i>
                                    </button>
                                    <button class="btn btn-round btn-primary btnUsuarios" data-empresa="${data}" title="Usuarios">
                                        <i class="glyph-icon icon-user"></i>
                                    </button>
                                </div>` }}
            ]
        }
    );

    tableUsers = $('#datatable-users').DataTable(
        {
            "language" : language,
            "ajax": `api/users/${$('#hdTercero').val()}/${$('#hdRol').val()}`,
            columns : [
                { data: "user_nombre" },
                { data: "cargo_descripcion" },
                { data: "identificacion" },
                { data: "user_email" },
                { data: "id", render: (data) => { return `<button class="btn btn-round btn-primary" onClick="formEditUser(${data})" title="Editar">
                                                                            <i class="glyph-icon icon-edit"></i>
                                                                        </button>`} }
            ]
        }
    );

    $('#logo').on('change', function(e){
        $('#removeImg').removeClass('hide');
        $('#img-logo').removeClass('hide');
        let TmpPath = URL.createObjectURL(e.target.files[0])
        $('#img-logo').attr('src',TmpPath);
    });

    $('#logoEdit').on('change', function(e){
        $('#removeImgEdit').removeClass('hide');
        $('#img-logo-edit').removeClass('hide');
        let TmpPath = URL.createObjectURL(e.target.files[0])
        $('#img-logo-edit').attr('src',TmpPath);
    });

    $(document).on('click','.btnUsuarios', function(){
        $('#hdTercero').val($(this).data('empresa'));
        $('#mdlUsuarios').modal();
        tableUsers.ajax.url(`api/users/${$('#hdTercero').val()}/${$('#hdRol').val()}`).load();
        $('#frmUsuario').removeClass('hide');
        $('#frmEditUsuario').addClass('hide');
    });
    
});

function guardarEmpresas(){
    let form = $('#frmCrearEmpresa')[0];
    let datos = new FormData(form);
    axios.post(form.action, datos).then((res) => {
        if(!res.ErrorStatus){
            $.jGrowl(res.data.Msj, {
                theme: 'bg-green' 
            });
            table.ajax.reload();
            form.reset();
            $('#mdlCrear').modal('hide');
        }else{
            $.jGrowl(res.data.Msj, {
                theme: 'bg-red' 
            });
        }
    }, (err) => {
        $.jGrowl(Object.values(err.response.data.errors)[0], {
            theme: 'bg-red' 
        });
    });
}

function EditarEmpresas(){
// let form = document.querySelector('#frmEditarEmpresa')
//    let url = form.getAttribute('action')+ '/' + $('#hdEmpresaEdit').val();
//    let data = new FormData(form)
    let form = $('#frmEditarEmpresa')[0];
    let datos = new FormData(form);
    let url = `${form.action}/${$('#hdEmpresaEdit').val()}`;
    axios.post(url, datos).then((res) => {
        if(!res.ErrorStatus){
            $.jGrowl(res.data.Msj, {
                theme: 'bg-green' 
            });
            table.ajax.reload();
            form.reset();
            $('#mdlEditar').modal('hide');
        }else{
            $.jGrowl(res.data.Msj, {
                theme: 'bg-red' 
            });
        }
    }, (err) => {
        $.jGrowl(Object.values(err.response.data.errors)[0], {
            theme: 'bg-red' 
        });
    });
}

function importarExcel(){
    let form = $('#frmExcel')[0];
    let datos = new FormData(form);
    axios.post(form.action, datos).then((res) => {
        
    }, (err) => {
        $.jGrowl(Object.values(err.response.data.errors)[0], {
            theme: 'bg-red' 
        });
    });
}

function enviarInvitacion(){
    let form = new FormData($('#frmInvitacionEmpresas')[0]);
    axios.post($('#frmInvitacionEmpresas')[0].action, form).then((res)=>{
        $.jGrowl(`Se ha enviado un correo electrónico a ${$('#txtEmailInvitacion').val()}.`, {
            theme: 'bg-green' 
        });
        $('#txtEmailInvitacion').val('');
        $('#mdlEmail').modal('hide');
    }, (err) => {
        $.jGrowl(Object.values(err.response.data.errors)[0], {
            theme: 'bg-red' 
        });
    });
}

function quitarImagen(){
    $('#img-logo').attr('src','');
    $('#img-logo').addClass('hide');
    $('#logo').val('');
}

function quitarImagenEdit(){
    $('#img-logo-edit').attr('src','');
    $('#img-logo-edit').addClass('hide');
    $('#hdRemImg').val(true);
    $('#logoEdit').val('');
}

function abrirModal(e){
    $($(e).data('target')).modal();
}

function formEdit(id){
    $('#img-logo-edit').removeClass('hide');
    axios.get(`/empresas/${id}/edit`).then((res) => {
        if(res.data.empresa_logo){
            $('#removeImgEdit').removeClass('hide');
            $('#img-logo-edit').attr('src',`/images/images_empresas/${res.data.empresa_logo}`);
        }else{
            $('#img-logo-edit').attr('src','');
        }
        $('#txtNitEdit').val(res.data.empresa_nit);
        $('#hdEmpresaEdit').val(res.data.empresa_id);
        $('#txtNombreEdit').val(res.data.empresa_nombre);
        $('#txtSitioWebEdit').val(res.data.empresa_web);
        $('#cbMensajeriaEdit').val(res.data.empresa_mensajeria);
        $('#cbOrgJuridicaEdit').val(res.data.org_jur_id);
        $('#cbSectorProdEdit').val(res.data.sec_prod_id);
        $('#cbJornadaEdit').val(res.data.jorn_trab_id);
        $('#txtNoEmpleadosEdit').val(res.data.empresa_no_empleados);
        $('#txtObjSocialEdit').val(res.data.empresa_objeto_social);
        $('#hdRemImg').val(false);
        $('#mdlEditar').modal();
    });
}

function guardarUsuario(){
    let form = $('#frmUsuario')[0];
    let datos = new FormData(form);
    axios.post(form.action, datos).then((res) => {
        if(!res.ErrorStatus){
            $.jGrowl(res.data.Msj, {
                theme: 'bg-green' 
            });
            tableUsers.ajax.reload();
            form.reset();
        }else{
            $.jGrowl(res.data.Msj, {
                theme: 'bg-red' 
            });
        }
    }, (err) => {
        $.jGrowl(Object.values(err.response.data.errors)[0], {
            theme: 'bg-red' 
        });
    });
}

function formEditUser(id){
    $('#frmUsuario').addClass('hide');
    $('#frmEditUsuario').removeClass('hide');
    axios.get(`users/${id}/edit`).then((res) => {
        $('#txtNombreUserEdit').val(res.data.user_nombre);
        $('#txtEmailUserEdit').val(res.data.user_email);
        $('#txtIdentificacionEdit').val(res.data.identificacion);
        $('#cbCargoEdit').val(res.data.cargo_id);
        $('#cbDeptoEdit').val(res.data.depto_id);
        consultarCiudades($('#cbDeptoEdit'),res.data.ciudad_id_expedicion);
        $('#txtTelefonoUserEdit').val(res.data.user_telefono);
        $('#txtFechaNacEdit').val(moment(res.data.user_cumpleanos).format('DD/MM/YYYY'));
        $('#hdUser').val(res.data.id);
    });
}

function editarUsuario(){
    let form = $('#frmEditUsuario')[0];
    let datos = new FormData(form);
    let url = `${form.action}/${$('#hdUser').val()}`
    axios.post(url, datos).then((res) => {
        if(!res.ErrorStatus){
            $.jGrowl(res.data.Msj, {
                theme: 'bg-green' 
            });
            $('#frmUsuario').removeClass('hide');
            $('#frmEditUsuario').addClass('hide');
            tableUsers.ajax.reload();
            form.reset();
        }else{
            $.jGrowl(res.data.Msj, {
                theme: 'bg-red' 
            });
        }
    }, (err) => {
        $.jGrowl(Object.values(err.response.data.errors)[0], {
            theme: 'bg-red' 
        });
    });

}

function consultarCiudades(e,ciudadVal=0){
    let ciudadCb = '';
    if($(e).attr('id') == 'cbDepto'){
        ciudadCb = 'cbCiudad';
    }else{
        ciudadCb = 'cbCiudadEdit';
    }
    $(`#${ciudadCb} option`).remove();
    $(`#${ciudadCb}`).append(`<option value="">Seleccione..</option>`);
    axios.get(`api/ciudades/${$(e).val()}`).then((res) => {
        if(res.data.length > 0){
            for(ciudad of res.data){
                $(`#${ciudadCb}`).append(`<option value="${ciudad.ciudad_id}">${ciudad.ciudad_descripcion}</option>`);
            }
            if(ciudadVal != 0){
                $(`#${ciudadCb}`).val(ciudadVal);
            }
        }
    });
}