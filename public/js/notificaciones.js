$(document).ready(function() {
	
	consultarNotificaciones($('#hdRolNotificaciones').val());

});

function consultarNotificaciones(rol){
	let url = '';
	if(rol == 3){
		url = `/api/notificaciones-empresas/${$('#hdTerceroNotificaciones').val()}`;
	}
	if(url != ''){
		axios.get(url).then(res => {
			for(notif of res.data){
				let clases = '';
				switch(notif.tipo){
					case 'solAceptada':
						clases = 'bg-green icon-check';
					break;
					case 'solRechazada':
						clases = 'bg-danger icon-close';
					break;
					case 'cupo':
						clases = 'bg-warning icon-exclamation-triangle';
					break;
				}
				$('#notification-list').append(`<li>
				                            <span class="icon-notification glyph-icon ${clases}"></span>
				                            <span class="notification-text float-none">${notif.Msj}</span>
				                        </li>`);
			}
		},(err) => {
			$.jGrowl('Error en el servidor.', {
                    theme: 'bg-red' 
                });
		})
	}
}