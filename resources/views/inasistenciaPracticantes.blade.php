@extends('layouts.site')

@section('title')
    Registrar Inasistencia
@endsection

@section('content')
<div id="page-title"  style="display: flex; margin-bottom: 10px;">
    <h2>REGISTRAR INASISTENCIA</h2>
     <a id="back" style="margin-bottom: auto;margin-top: auto;margin-left: auto;font-size: 1.2em;text-decoration: none;color: black;cursor: pointer;"> 
            <i class="glyph-icon icon-arrow-left"></i>
        Volver
    </a>
</div>
<div class="panel">
    <div class="panel-body">
    <div class="row">
        <div class="col-md-12 mrg20B text-right">
            <button class="btn btn-alt btn-hover btn-primary" data-target="#mdlCrear" onclick="abrirModal(this)">
                    <span>Agregar Inasistencia</span>
                    <i class="glyph-icon icon-plus"></i>
            </button>
        </div>
    </div>

      {{-- Modal Creacion --}}
        <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="mdlCrear">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Registro de Inasistencia</h4>
                    </div>
                    <div class="modal-body">
                        <form action="{{ route('asistencia-practicante.store') }}" id="frmCrearInasistencia">
                            @csrf
                            <input type="hidden" name="hdTercero" id="hdTercero" value="{{ Auth::user()->tercero_id }}">
                            <input type="hidden" name="seleccionado_id" id="seleccionado_id" value="{{ $idseleccionado }}">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Fecha de Inasistencia</label>
                                        <input type="text" name="fecha" class="bootstrap-datepicker form-control" autocomplete="off">
                                    </div>
                                </div>
                                
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Motivo</label>
                                        <select class="form-control" name="motivo">
                                            <option value="">Seleccione</option>
                                            <option value="Inasistencia justificada">Inasistencia justificada</option>
                                            <option value="Inasistencia no justificada">Inasistencia no justificada</option>
                                            <option value="Inasistencia por incapacidad">Inasistencia por incapacidad</option>
                                            <option value="Motivos personales o familiares">Motivos personales o familiares</option>
                                            <option value="Otro">Otro</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label">Observaciones</label>
                                        <textarea class="form-control" name="observacion"></textarea>
                                    </div>
                                </div>
                                
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        <button type="button" class="btn btn-primary" onclick="$('#frmCrearInasistencia').submit()">Guardar</button>
                    </div>
                </div>
            </div>
        </div>
        {{-- Modal Creacion --}}

        {{-- Modal Editar --}}
        <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="mdlEditar">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Editar Inasistencia</h4>
                    </div>
                    <div class="modal-body">
                        <form action="{{ route('asistencia-practicante.update','') }}" id="frmEditarInasistencia">
                            @csrf
                             {{method_field('PUT')}}
                            <input type="hidden" name="hdTercero" id="hdTercero" value="{{ Auth::user()->tercero_id }}">
                            <input type="hidden" name="idseleccionado" id="idseleccionado" >
                            <div class="row">
                            <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Fecha de Inasistencia</label>
                                        <input type="text" name="fechaAC" id="fechaAC" class="bootstrap-datepicker form-control" autocomplete="off">
                                    </div>
                                </div>
                                
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="control-label">Motivo</label>
                                        <select class="form-control" name="motivoAC" id="motivoAC">
                                            <option value="">Seleccione</option>
                                            <option value="Falta de compromiso">Falta de compromiso</option>
                                            <option value="Salir sin permiso">Salir sin permiso</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Observaciones</label>
                                        <textarea class="form-control" name="observacionAC" id="observacionAC"></textarea>
                                    </div>
                                </div>
                                
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        <button type="button" class="btn btn-primary" onclick="$('#frmEditarInasistencia').submit()">Guardar</button>
                    </div>
                </div>
            </div>
        </div>
        {{-- Modal Editar --}}


        {{-- tabla --}}
        <div class="example-box-wrapper">
            <input type="hidden" id="hdEmpresa" value="{{ Auth::user()->tercero_id }}">
            <table class="table">
            <tbody>
                <tr>
                    <td>Documento</td>
                    <td id="cedula"></td>
                </tr>
                <tr>
                    <td>Estudiante</td>
                    <td id="nombres"></td>
                  
                </tr>
                <tr>
                    <td>Programa</td>
                    <td id="programa"></td>
                  
                </tr>
                <tr>
                    <td>Tipo de Práctica</td>
                    <td id="tipo"></td>
                  
                </tr>
                <tr>
                    <td>Empresa</td>
                    <td id="empresa"></td>
                </tr>
                <tr>
                    <td>Dependencia</td>
                    <td id="dependencia"></td>
                  
                </tr>
                <tr>
                    <td>Fecha de Inicio</td>
                    <td id="fechaI"></td>
                  
                </tr>
                <tr>
                    <td>Fecha de Finalización</td>
                    <td id="fechaF"></td>
                  
                </tr>
                <!--  <tr>
                    <td>Estado</td>
                    <td>Legalizado</td>
                  
                </tr> -->
            </tbody>
            </table>
        </div>
        {{-- end tabla --}}


         {{-- tabla --}}
        <div class="example-box-wrapper">

            <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="datatable-example">
            <thead>
            <tr>
                <th>N°</th>
                <th>Fecha de Inasistencia</th>
                <th>Motivo</th>
                <th>Observacion</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            
            </tbody>
            </table>
        </div>
        {{-- end tabla --}}
    </div>
</div>
@endsection

@section('scripts')
    <script type="text/javascript" src="{{ asset('template/widgets/datatable/datatable.js') }}"></script>
    <script type="text/javascript" src="{{ asset('template/widgets/datatable/datatable-bootstrap.js') }}"></script>
    <script type="text/javascript" src="{{ asset('template/widgets/datatable/datatable-tabletools.js') }}"></script>
    <script type="text/javascript" src="{{ asset('template/widgets/jgrowl-notifications/jgrowl.js') }}"></script>
    <script type="text/javascript" src="{{ asset('template/widgets/datepicker/datepicker.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/inasistenciaPracticantes.js') }}"></script>
@endsection
