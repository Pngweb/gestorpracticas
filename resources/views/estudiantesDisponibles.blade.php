@extends('layouts.site')

@section('title')
    Estudiantes Disponibles
@endsection

@section('content')
<div id="page-title">
    <h2>Estudiantes Disponibles</h2>
</div>
<div class="panel">
    <div class="panel-body">
        @include('layouts.partials.lineaProceso')
        <h3 class="title-hero">
            Buscar Prácticantes
        </h3>
        <form id="frmFiltrarEstudiantes">
            <div class="row">
                <div class="col-md-3">
                    <div class="form-group">
                        <label>Institución</label>
                        <select name="cbInstitucion" class="form-control" id="cbInstitucion">
                            <option value="">Seleccione..</option>
                            @foreach ($instituciones as $institucion)
                                <option value="{{ $institucion->institucion_id }}">{{ $institucion->institucion_nombre }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label>Area</label>*
                        <select name="cbArea" class="form-control" id="cbArea">
                            <option value="">Seleccione..</option>
                            @foreach ($areas as $area)
                                <option value="{{ $area->area_id }}">{{ $area->area_descripcion }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label>Programa</label>
                        <select name="cbPrograma" class="form-control" id="cbPrograma">
                            <option value="">Seleccione..</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label class="control-label">Departamento</label>
                        <select class="form-control" name="cbDepto" id="cbDepto" onchange="consultarCiudades(this)">
                            <option value="">Seleccione..</option>
                            @foreach ($deptos as $depto)
                            <option value="{{ $depto->depto_id }}">{{ $depto->depto_descripcion }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label class="control-label">Ciudad</label>
                        <select class="form-control" name="cbCiudad" id="cbCiudad">
                            <option value="">Seleccione..</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label class="control-label">Edad</label>
                        <select class="form-control" name="cbEdad">
                            <option value="">Seleccione..</option>
                            <option value="18-20">18 a 20</option>
                            <option value="21-25">21 a 25</option>
                            <option value="26-30">26 a 30</option>
                            <option value="31-200">Mayor de 30</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label class="control-label">Sexo</label>
                        <select class="form-control" name="cbSexo">
                            <option value="">Seleccione..</option>
                            <option value="M">M</option>
                            <option value="F">F</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-3">
                    <button type="button" class="btn btn-default mrg20T" data-toggle="collapse" data-target="#filtros_avanzados">
                        Más Filtros
                    </button>
                </div>
            </div>
            <div class="row collapse" id="filtros_avanzados">
                <div class="col-md-3">
                    <div class="form-group">
                        <label class="control-label">Modalidad</label>
                        <select class="form-control" name="cbModalidad">
                            <option value="">Seleccione..</option>
                            @foreach ($modalidades as $modalidad)
                                <option value="{{ $modalidad->etapa_id }}">{{ $modalidad->etapa_descripcion }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label class="control-label">Disponible en</label>
                        <select class="form-control" name="cbDisponible">
                            <option value="">Seleccione..</option>
                            <option value="">1 Mes</option>
                            <option value="">2 Meses</option>
                            <option value="">3 Meses</option>
                            <option value="">4 Meses</option>
                            <option value="">5 Meses</option>
                            <option value="">6 Meses</option>
                            <option value="">Más de 7 meses</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label class="control-label">Experiencia Laboral</label>
                        <select class="form-control" name="cbDisponible">
                            <option value="">Seleccione..</option>
                            <option value="">3 Meses</option>
                            <option value="">4 Meses</option>
                            <option value="">5 Meses</option>
                            <option value="">6 Meses</option>
                            <option value="">Más de 7 meses</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label class="control-label">Tipo de Practica</label>
                        <select class="form-control" name="cbDisponible">
                            <option value="">Seleccione..</option>
                            <option value="">Convenio Institucional</option>
                            <option value="">Duales</option>
                            <option value="">Contrato de Aprendizaje</option>
                            <option value="">Convenio Docencia Servicio</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 text-right">
                    <button class="btn btn-alt btn-primary" id="btnFiltro" type="submit">
                        <span>Filtrar</span>
                        <i class="glyph-icon icon-filter"></i>
                    </button>
                </div>
            </div>
        </form>

         {{-- Modal Solicitar --}}
        <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="mdlSolicitar">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Solicitar Practicante</h4>
                    </div>
                    <div class="modal-body">
                        <ul class="nav-responsive nav nav-tabs">
                            <li class="active"><a href="#tabNuevo" data-toggle="tab">Nuevo</a></li>
                            <li><a href="#tabReemplazo" data-toggle="tab">Reemplazo</a></li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane active" id="tabNuevo">
                                <form id="frmSolicitudCupo" action="{{ route('estudiantes-disponibles.solicitudCupo') }}">
                                    @csrf
                                    <input type="hidden" name="hdEstudiante"  id="hdEstudiante">
                                    <input type="hidden" name="institucion" id="institucion">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Cupo de Practica</label>
                                                <select name="cbCupo" class="form-control" id="cbCupo">
                                                    <option value="">Seleccione..</option>
                                                    @foreach ($cupos_practicas as $cupo_practica)
                                                        <option value="{{ $cupo_practica->cupo_id }}">{{ $cupo_practica->cargo_descripcion }} - {{ $cupo_practica->dependencia_descripcion }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label">Fecha Inicio</label>
                                                        <div class="input-prepend input-group">
                                                            <span class="add-on input-group-addon">
                                                                <i class="glyph-icon icon-calendar"></i>
                                                            </span>
                                                            <input type="text" class="form-control bootstrap-datepicker" name="txtFechaInicio" id="txtFechaInicio" autocomplete="off">

                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label">Fecha Culminación</label>
                                                        <div class="input-prepend input-group">
                                                            <span class="add-on input-group-addon">
                                                                <i class="glyph-icon icon-calendar"></i>
                                                            </span>
                                                            <input type="text" class="form-control bootstrap-datepicker2" name="txtFechaCulminacion" id="txtFechaCulminacion" autocomplete="off">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label>Tipo de Acuerdo</label>
                                                <textarea name="txtTipoAcuerdo" class="form-control"></textarea>
                                            </div>

                                            <a href="{{ route('cupos-practicas.index') }}" class="btn btn-primary">Ir a crear cupos</a>

                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Descripcion Cupo</label>
                                                <div class="panel">
                                                    <div class="panel-body" id="panel_info_cupo">
                                                        Seleccione el cupo de practica.
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="row">
                                        <div class="col-md-12 text-right">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                                            <button type="submit" class="btn btn-primary">Guardar</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="tab-pane" id="tabReemplazo">
                                <form action="{{ route('estudiantes-disponiblesRem.solicitudCupoRemplazo') }}" id="frmRemplazoCupo">
                                    @csrf
                                    <input type="hidden" name="hdEstudianteRem"  id="hdEstudianteRem">
                                    <input type="hidden" name="institucionRem" id="institucionRem">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Cupo de Practica</label>
                                                <select name="cbCupoRem" class="form-control" id="cbCupoRe">
                                                    <option value="">Seleccione..</option>
                                                    @foreach ($cupos_practicasrem as $cupo_practica)
                                                        <option value="{{ $cupo_practica->cupo_id }}">{{ $cupo_practica->cargo_descripcion }} - {{ $cupo_practica->dependencia_descripcion }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label>Practicante a reemplazar</label>
                                                <select name="cbPracticante" id="cbPracticante" class="form-control">
                                                    <option value="">Seleccione..</option>
                                                </select>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label">Fecha Inicio</label>
                                                        <div class="input-prepend input-group">
                                                            <span class="add-on input-group-addon">
                                                                <i class="glyph-icon icon-calendar"></i>
                                                            </span>
                                                            <input type="text" class="form-control bootstrap-datepicker" name="txtFechaInicioRem" id="txtFechaInicioRem">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label">Fecha Culminación</label>
                                                        <div class="input-prepend input-group">
                                                            <span class="add-on input-group-addon">
                                                                <i class="glyph-icon icon-calendar"></i>
                                                            </span>
                                                            <input type="text" class="form-control bootstrap-datepicker2" name="txtFechaCulminacionRem" id="txtFechaCulminacionRem">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                             <div class="form-group">
                                                <label>Tipo de Acuerdo</label>
                                                <textarea name="txtTipoAcuerdoRem"  class="form-control"></textarea>
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Descripcion Cupo</label>
                                                <div class="panel">
                                                    <div class="panel-body" id="panel_info_cupo_Rem">
                                                        Seleccione el cupo de practica.
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="row">
                                        <div class="col-md-12 text-right">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                                            <button type="submit" class="btn btn-primary">Guardar</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {{-- Modal Solicitar --}}

        <hr>
        {{-- tabla --}}
        <div class="example-box-wrapper">
            <input type="hidden" id="hdEmpresa" value="{{ Auth::user()->tercero_id }}">
            <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="datatable-example">
            <thead>
            <tr>
                <th></th>
                <th>Datos Personales</th>
                <th>Datos Académicos</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            
            </tbody>
            </table>
        </div>
        {{-- end tabla --}}
    </div>
</div>
@endsection

@section('scripts')
    <script type="text/javascript" src="{{ asset('template/widgets/datatable/datatable.js') }}"></script>
    <script type="text/javascript" src="{{ asset('template/widgets/accordion-ui/accordion.js') }}"></script>
    <script type="text/javascript" src="{{ asset('template/widgets/datatable/datatable-bootstrap.js') }}"></script>
    <script type="text/javascript" src="{{ asset('template/widgets/datatable/datatable-tabletools.js') }}"></script>
    <script type="text/javascript" src="{{ asset('template/widgets/jgrowl-notifications/jgrowl.js') }}"></script>
    <script type="text/javascript" src="{{ asset('template/widgets/datepicker/datepicker.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/estudiantesDisponibles.js') }}"></script>

    <!-- Importaciones para validar fechas -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.3/js/bootstrapValidator.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/3.1.3/js/bootstrap-datetimepicker.min.js"></script>
@endsection
