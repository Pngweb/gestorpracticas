@extends('layouts.site')

@section('title')
    Estudiantes Pre-seleccionados
@endsection

@section('content')
<div id="page-title">
    <h2>Estudiantes Pre-seleccionados</h2>
</div>
<div class="panel">
    <div class="panel-body">
        @include('layouts.partials.lineaProceso')
        <h3 class="title-hero">
            Buscar Prácticantes
        </h3>
        <form id="frmFiltrarEstudiantes">
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Institución</label>
                        <select name="cbInstitucion" class="form-control" id="cbInstitucion">
                            <option value="">Seleccione..</option>
                            @foreach ($instituciones as $institucion)
                                <option value="{{ $institucion->institucion_id }}">{{ $institucion->institucion_nombre }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Area</label>*
                        <select name="cbArea" class="form-control" id="cbArea">
                            <option value="">Seleccione..</option>
                            @foreach ($areas as $area)
                                <option value="{{ $area->area_id }}">{{ $area->area_descripcion }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Programa</label>
                        <select name="cbPrograma" class="form-control" id="cbPrograma">
                            <option value="">Seleccione..</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 text-right">
                    <button class="btn btn-alt btn-primary" id="btnFiltro" type="submit">
                        <span>Filtrar</span>
                        <i class="glyph-icon icon-filter"></i>
                    </button>
                </div>
            </div>
        </form>

        {{-- Modal no seleccionar --}}
        <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="mdlNoSeleccionar">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">No Seleccionar</h4>
                    </div>
                    <div class="modal-body">
                        <input type="hidden" id="hdCupoId">
                        <div class="row">
                            <div class="col-md-6 col-md-offset-3">
                                <div class="form-group">
                                    <label>Por favor escriba la justificación</label>
                                    <textarea class="form-control" rows="6" id="txtObservaciones"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        <button type="button" class="btn btn-primary" id="btnNoSelect">Enviar</button>
                    </div>
                </div>
            </div>
        </div>
        {{-- Modal no seleccionar --}}

        <hr>
        {{-- tabla --}}
        <div class="example-box-wrapper">
            <input type="hidden" id="hdEmpresa" value="{{ Auth::user()->tercero_id }}">
            <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="datatable-example">
            <thead>
            <tr>
                <th></th>
                <th>Cupo de Práctica</th>
                <th>Datos Personales</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
               <!--  <tr>
                    <td>
                        <img class="img-thumbnail" src="/images/images_estudiantes/1449286552.jpg" width="100">
                    </td>
                    <td>
                        Cupo de Practica <br>
                        <strong>AUXILIAR DE SISTEMAS</strong> <br>
                        Fecha de Inicio; 17/07/2020 <br>
                        Sucursal: SUCURSAL PRINCIPAL <br>
                        Dependencia: DEPARTAMENTO DE SISTEMAS <br>
                        Maestro: JUAN PEREZ <br>
                        Paso Actual: ENTREVISTA PRESENCIAL
                    </td>
                    <td>
                        <strong>LUISA PEREZ JIMENEZ</strong><br>
                        C.C. 1,234,567 <br>
                        Programa: TECNICO EN DESARROLLO DE SOFTWARE <br>
                        Edad: 20 <br>
                        Modalidad: PATROCINIO LECTIVO <br>
                        CALLE 15 CRA 20 <br>
                        SOLEDAD <br>
                        <a href="https://web.whatsapp.com/"><button class="btn btn-round btn-success"><i class="fab fa-whatsapp"></i></button></a> <a href="mailto:luisaperez@gmail.com"><button class="btn btn-round btn-info"><i class="glyph-icon icon-envelope"></i></button> <br>
                        <a href="#">
                            <button class="btn btn-sm btn-primary mrg5T">Proceso de Selección</button></a> <button class="btn btn-sm btn-primary mrg5T">No Seleccionar</button>
                     </td>
                     <td><div style="text-align: center; padding: 20px 0;"><button style="font-size: 30pt; height: 60px;" class="btn btn-default"><i class="glyph-icon icon-file-text-o"></i></button><br><span>Hoja de Vida</span></div></td>
                </tr> -->
            </tbody>
            </table>
        </div>
        {{-- end tabla --}}
    </div>
</div>
@endsection

@section('scripts')
    <script type="text/javascript" src="{{ asset('template/widgets/datatable/datatable.js') }}"></script>
    <script type="text/javascript" src="{{ asset('template/widgets/accordion-ui/accordion.js') }}"></script>
    <script type="text/javascript" src="{{ asset('template/widgets/datatable/datatable-bootstrap.js') }}"></script>
    <script type="text/javascript" src="{{ asset('template/widgets/datatable/datatable-tabletools.js') }}"></script>
    <script type="text/javascript" src="{{ asset('template/widgets/jgrowl-notifications/jgrowl.js') }}"></script>
    <script type="text/javascript" src="{{ asset('template/widgets/datepicker/datepicker.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/estudiantesPreseleccionados.js') }}"></script>
@endsection
