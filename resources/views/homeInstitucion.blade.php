@extends('layouts.site')

@section('title')
    Home
@endsection

@section('content')
<div id="page-title">
    <h2>Dashboard</h2>
</div>
<div class="panel">
    <div class="panel-body">
        <div class="row">
        	<div class="col-md-4">
                    <div class="tile-box bg-white content-box">
                        <div class="tile-header">
                            Convenios
                        </div>
                        <div class="tile-content-wrapper">
                            <i class="glyph-icon icon-building" style="font-size: 50px;"></i>
                            <div class="tile-content">
                                12
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="tile-box bg-white content-box">
                        <div class="tile-header">
                            Estudiantes
                        </div>
                        <div class="tile-content-wrapper">
                            <i class="glyph-icon icon-group" style="font-size: 50px;"></i>
                            <div class="tile-content">
                                120
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="tile-box bg-white content-box">
                        <div class="tile-header">
                            Practicantes
                        </div>
                        <div class="tile-content-wrapper">
                            <i class="glyph-icon icon-gear" style="font-size: 50px;"></i>
                            <div class="tile-content">
                                60
                            </div>
                        </div>
                    </div>
                </div>
        </div>
    </div>
</div>
@endsection
