@extends('layouts.site')

@section('title')
    Mensajeria
@endsection

@section('content')
<div id="page-title">
    <h2>Mensajeria</h2>
</div>
<div class="panel">
    <div class="panel-body">
        <div class="row">
            <div class="col-md-12 mrg20B text-right">
                <button class="btn btn-alt btn-hover btn-primary" data-target="#mdlCrear" onclick="abrirModal(this)">
                        <span>Nuevo</span>
                        <i class="glyph-icon icon-plus"></i>
                </button>
            </div>
        </div>
        {{-- Modal Creacion --}}
        <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="mdlCrear">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Nuevo usuario de mensajeria</h4>
                    </div>
                    <div class="modal-body">
                        <form action="{{ route('mensajeria.store') }}" id="frmCrearUserMensajeria">
                            @csrf
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Sistema de Mensajeria</label>
                                        <select name="cbSistemaMensajeria" class="form-control">
                                            <option value="">Seleccione..</option>
                                            @foreach ($sistemasMensajeria as $men)
                                                <option value="{{ $men->mensajeria_id }}">{{ $men->mensajeria_descripcion }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Usuario sistema de mensajeria</label>
                                        <input type="text" name="txtUsuario" class="form-control" autocomplete="off">
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        <button type="button" class="btn btn-primary" onclick="$('#frmCrearUserMensajeria').submit()">Guardar</button>
                    </div>
                </div>
            </div>
        </div>
        {{-- Modal Creacion --}}

        {{-- Modal Editar --}}
        <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="mdlEditar">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Nuevo usuario de mensajeria</h4>
                    </div>
                    <div class="modal-body">
                        <form action="{{ route('mensajeria.update','') }}" id="frmEditarUserMensajeria">
                            @csrf
                            {{method_field('PUT')}}
                            <input type="hidden" name="hdUserMensajeriaId" id="hdUserMensajeriaId">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Sistema de Mensajeria</label>
                                        <select name="cbSistemaMensajeria" id="cbSistemaMensajeriaEdit" class="form-control">
                                            <option value="">Seleccione..</option>
                                            @foreach ($sistemasMensajeria as $men)
                                                <option value="{{ $men->mensajeria_id }}">{{ $men->mensajeria_descripcion }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Usuario sistema de mensajeria</label>
                                        <input type="text" name="txtUsuario" id="txtUsuarioEdit" class="form-control" autocomplete="off">
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        <button type="button" class="btn btn-primary" onclick="$('#frmEditarUserMensajeria').submit()">Guardar</button>
                    </div>
                </div>
            </div>
        </div>
        {{-- Modal Editar --}}

        {{-- tabla --}}
        <div class="example-box-wrapper">
            <input type="hidden" id="hdEmpresa" value="{{ Auth::user()->tercero_id }}">
            <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="datatable-example">
            <thead>
            <tr>
                <th>Sistema de mensajeria</th>
                <th>Usuario</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            
            </tbody>
            </table>
        </div>
        {{-- end tabla --}}
    </div>
</div>
@endsection

@section('scripts')
    <script type="text/javascript" src="{{ asset('template/widgets/datatable/datatable.js') }}"></script>
    <script type="text/javascript" src="{{ asset('template/widgets/datatable/datatable-bootstrap.js') }}"></script>
    <script type="text/javascript" src="{{ asset('template/widgets/datatable/datatable-tabletools.js') }}"></script>
    <script type="text/javascript" src="{{ asset('template/widgets/jgrowl-notifications/jgrowl.js') }}"></script>
    <script type="text/javascript" src="{{ asset('template/widgets/datepicker/datepicker.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/mensajeria.js') }}"></script>
@endsection