@extends('layouts.site')

@section('title')
    Estudiantes Seleccionados
@endsection

@section('content')
<div id="page-title">
    <h2>Estudiantes Seleccionados</h2>
</div>
<div class="panel">
    <div class="panel-body">
        @include('layouts.partials.lineaProceso')
        <hr>
        {{-- tabla --}}

        <form id="frmFiltrarSeleccionados">
            @csrf
            <div class="row">
                <div class="col-md-3">
                    <div class="form-group">
                        <label>Institución</label>
                        <select name="cbInstitucion" class="form-control" id="cbInstitucion">
                            <option value="">Seleccione..</option>
                            @foreach ($instituciones as $institucion)
                                <option value="{{ $institucion->institucion_id }}">{{ $institucion->institucion_nombre }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="form-group">
                        <label>Sucursal</label>
                        <select name="cbSucursal" class="form-control" id="fltSucursal">
                            <option value="">Seleccione..</option>
                            @foreach ($sucursales_combo as $sucursal)
                                <option value="{{ $sucursal->sucursal_id }}">{{ $sucursal->sucursal_descripcion }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="form-group">
                        <label>Dependencia</label>
                        <select name="cdDependencia" class="form-control" id="fltDependencia">
                            <option value="">Seleccione..</option>
                            @foreach ($dependencias_combo as $dependencia)
                                <option value="{{ $dependencia->dependencia_id }}">{{ $dependencia->dependencia_descripcion }}</option>}
                            @endforeach
                        </select>
                    </div>
                </div>
                
                <div class="col-md-3">
                    <div class="form-group">
                        <label>Cargo</label>
                        <select name="cbCargo" class="form-control" id="fltCargo">
                            <option value="">Seleccione..</option>
                            @foreach ($cargos as $cargo)
                                <option value="{{ $cargo->cupo_id }}">{{ $cargo->cupo_competencias }}</option>}
                            @endforeach
                        </select>
                    </div>
                </div>
                
            </div>
            <div class="row" style="margin-bottom: 15px;">
                <div class="col-md-12 text-right">
                    <button class="btn btn-alt btn-primary" id="btnFiltro" type="submit">
                        <span>Filtrar</span>
                        <i class="glyph-icon icon-filter icon-spin"></i>
                    </button>
                </div>
            </div>
        </form>


        <h3 class="text-center font-primary font-bold" id="title"></h3><br>
        <div class="example-box-wrapper">
            <input type="hidden" id="hdEmpresa" value="{{ Auth::user()->tercero_id }}">
            <table class="table" id="datatable-example">
            <thead>
            <tr>
                <th>Código Cupo</th>
                <th>Sucursal</th>
                <th>Cargo</th>
                <th>Maestro Guia</th>
                <th>Practicante</th>
                <th>Programa</th>
                <th>Tipo de Acuerdo</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            </tbody>
            </table>
        </div>
        {{-- end tabla --}}
    </div>
</div>
@endsection

@section('scripts')
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script type="text/javascript" src="{{ asset('js/estudiantesSeleccionados.js') }}"></script>
    <script type="text/javascript" src="{{ asset('template/widgets/datatable/datatable.js') }}"></script>
    <script type="text/javascript" src="{{ asset('template/widgets/accordion-ui/accordion.js') }}"></script>
    <script type="text/javascript" src="{{ asset('template/widgets/datatable/datatable-bootstrap.js') }}"></script>
    <script type="text/javascript" src="{{ asset('template/widgets/datatable/datatable-tabletools.js') }}"></script>
    <script type="text/javascript" src="{{ asset('template/widgets/jgrowl-notifications/jgrowl.js') }}"></script>
    <script type="text/javascript" src="{{ asset('template/widgets/datepicker/datepicker.js') }}"></script>
     
@endsection
