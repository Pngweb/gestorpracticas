<!DOCTYPE html>
<html lang="en">
<head>
    <style>
        /* Loading Spinner */
        .spinner{margin:0;width:70px;height:18px;margin:-35px 0 0 -9px;position:absolute;top:50%;left:50%;text-align:center}.spinner > div{width:18px;height:18px;background-color:#333;border-radius:100%;display:inline-block;-webkit-animation:bouncedelay 1.4s infinite ease-in-out;animation:bouncedelay 1.4s infinite ease-in-out;-webkit-animation-fill-mode:both;animation-fill-mode:both}.spinner .bounce1{-webkit-animation-delay:-.32s;animation-delay:-.32s}.spinner .bounce2{-webkit-animation-delay:-.16s;animation-delay:-.16s}@-webkit-keyframes bouncedelay{0%,80%,100%{-webkit-transform:scale(0.0)}40%{-webkit-transform:scale(1.0)}}@keyframes bouncedelay{0%,80%,100%{transform:scale(0.0);-webkit-transform:scale(0.0)}40%{transform:scale(1.0);-webkit-transform:scale(1.0)}}
    </style>
    <meta charset="UTF-8">
<!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
<title> @yield('title') </title>
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

<!-- Favicons -->

<link rel="apple-touch-icon-precomposed" sizes="144x144" href="{{ asset('template/images/icons/apple-touch-icon-144-precomposed.png') }}">
<link rel="apple-touch-icon-precomposed" sizes="114x114" href="{{ asset('template/images/icons/apple-touch-icon-114-precomposed.png') }}">
<link rel="apple-touch-icon-precomposed" sizes="72x72" href="{{ asset('template/images/icons/apple-touch-icon-72-precomposed.png') }}">
<link rel="apple-touch-icon-precomposed" href="{{ asset('template/images/icons/apple-touch-icon-57-precomposed.png') }}">
<link rel="shortcut icon" href="{{ asset('images/favicon.png') }}">



    <link rel="stylesheet" type="text/css" href="{{ asset('template/bootstrap/css/bootstrap.css') }}">


<!-- HELPERS -->

<link rel="stylesheet" type="text/css" href="{{ asset('template/helpers/animate.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('template/helpers/backgrounds.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('template/helpers/boilerplate.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('template/helpers/border-radius.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('template/helpers/grid.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('template/helpers/page-transitions.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('template/helpers/spacing.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('template/helpers/typography.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('template/helpers/utils.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('template/helpers/colors.css') }}">

<!-- ELEMENTS -->

<link rel="stylesheet" type="text/css" href="{{ asset('template/elements/badges.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('template/elements/buttons.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('template/elements/content-box.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('template/elements/dashboard-box.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('template/elements/forms.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('template/elements/images.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('template/elements/info-box.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('template/elements/invoice.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('template/elements/loading-indicators.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('template/elements/menus.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('template/elements/panel-box.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('template/elements/response-messages.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('template/elements/responsive-tables.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('template/elements/ribbon.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('template/elements/social-box.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('template/elements/tables.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('template/elements/tile-box.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('template/elements/timeline.css') }}">



<!-- ICONS -->

<link rel="stylesheet" type="text/css" href="{{ asset('template/icons/fontawesome/fontawesome.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('template/icons/linecons/linecons.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('template/icons/spinnericon/spinnericon.css') }}">


<!-- WIDGETS -->

<link rel="stylesheet" type="text/css" href="{{ asset('template/widgets/accordion-ui/accordion.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('template/widgets/calendar/calendar.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('template/widgets/carousel/carousel.css') }}">

<link rel="stylesheet" type="text/css" href="{{ asset('template/widgets/charts/justgage/justgage.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('template/widgets/charts/morris/morris.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('template/widgets/charts/piegage/piegage.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('template/widgets/charts/xcharts/xcharts.css') }}">

<link rel="stylesheet" type="text/css" href="{{ asset('template/widgets/chosen/chosen.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('template/widgets/colorpicker/colorpicker.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('template/widgets/datatable/datatable.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('template/widgets/datepicker/datepicker.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('template/widgets/datepicker-ui/datepicker.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('template/widgets/daterangepicker/daterangepicker.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('template/widgets/dialog/dialog.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('template/widgets/dropdown/dropdown.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('template/widgets/dropzone/dropzone.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('template/widgets/file-input/fileinput.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('template/widgets/input-switch/inputswitch.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('template/widgets/input-switch/inputswitch-alt.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('template/widgets/ionrangeslider/ionrangeslider.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('template/widgets/jcrop/jcrop.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('template/widgets/jgrowl-notifications/jgrowl.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('template/widgets/loading-bar/loadingbar.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('template/widgets/maps/vector-maps/vectormaps.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('template/widgets/markdown/markdown.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('template/widgets/modal/modal.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('template/widgets/multi-select/multiselect.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('template/widgets/multi-upload/fileupload.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('template/widgets/nestable/nestable.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('template/widgets/noty-notifications/noty.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('template/widgets/popover/popover.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('template/widgets/pretty-photo/prettyphoto.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('template/widgets/progressbar/progressbar.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('template/widgets/range-slider/rangeslider.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('template/widgets/slidebars/slidebars.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('template/widgets/slider-ui/slider.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('template/widgets/summernote-wysiwyg/summernote-wysiwyg.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('template/widgets/tabs-ui/tabs.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('template/widgets/theme-switcher/themeswitcher.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('template/widgets/timepicker/timepicker.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('template/widgets/tocify/tocify.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('template/widgets/tooltip/tooltip.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('template/widgets/touchspin/touchspin.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('template/widgets/uniform/uniform.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('template/widgets/wizard/wizard.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('template/widgets/xeditable/xeditable.css') }}">

<!-- SNIPPETS -->

<link rel="stylesheet" type="text/css" href="{{ asset('template/snippets/chat.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('template/snippets/files-box.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('template/snippets/login-box.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('template/snippets/notification-box.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('template/snippets/progress-box.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('template/snippets/todo.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('template/snippets/user-profile.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('template/snippets/mobile-navigation.css') }}">

<!-- APPLICATIONS -->

<link rel="stylesheet" type="text/css" href="{{ asset('template/applications/mailbox.css') }}">

<!-- Admin theme -->

<link rel="stylesheet" type="text/css" href="{{ asset('template/themes/admin/layout.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('template/themes/admin/color-schemes/default.css') }}">

<!-- Components theme -->

<link rel="stylesheet" type="text/css" href="{{ asset('template/themes/components/default.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('template/themes/components/border-radius.css') }}">

<!-- Admin responsive -->

<link rel="stylesheet" type="text/css" href="{{ asset('template/helpers/responsive-elements.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('template/helpers/admin-responsive.css') }}">

<!-- Css -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.1/css/all.min.css" integrity="sha256-2XFplPlrFClt0bIdPgpz8H7ojnk10H69xRqd9+uTShA=" crossorigin="anonymous" />
<link rel="stylesheet" href="{{ asset('css/app.css') }}">
    <!-- JS Core -->

    <script type="text/javascript" src="{{ asset('template/js-core/jquery-core.js') }}"></script>
    <script type="text/javascript" src="{{ asset('template/js-core/jquery-ui-core.js') }}"></script>
    <script type="text/javascript" src="{{ asset('template/js-core/jquery-ui-widget.js') }}"></script>
    <script type="text/javascript" src="{{ asset('template/js-core/jquery-ui-mouse.js') }}"></script>
    <script type="text/javascript" src="{{ asset('template/js-core/jquery-ui-position.js') }}"></script>
    <!--<script type="text/javascript" src="{{ asset('template/js-core/transition.js') }}"></script>-->
    <script type="text/javascript" src="{{ asset('template/js-core/modernizr.js') }}"></script>
    <script type="text/javascript" src="{{ asset('template/js-core/jquery-cookie.js') }}"></script>





    <script type="text/javascript">
        $(window).load(function(){
            setTimeout(function() {
                $('#loading').fadeOut( 400, "linear" );
            }, 300);
        });
    </script>



</head>
<body>
<div id="loading">
    <div class="spinner">
        <div class="bounce1"></div>
        <div class="bounce2"></div>
        <div class="bounce3"></div>
    </div>
</div>

<style type="text/css">

    html,body {
        height: 100%;
        background: #fff;
    }

</style>

@yield('content')



    <!-- WIDGETS -->

<script type="text/javascript" src="{{ asset('template/bootstrap/js/bootstrap.js') }}"></script>

<!-- Bootstrap Dropdown -->

<!-- <script type="text/javascript" src="{{ asset('template/widgets/dropdown/dropdown.js') }}"></script> -->

<!-- Bootstrap Tooltip -->

<!-- <script type="text/javascript" src="{{ asset('template/widgets/tooltip/tooltip.js') }}"></script> -->

<!-- Bootstrap Popover -->

<!-- <script type="text/javascript" src="{{ asset('template/widgets/popover/popover.js') }}"></script> -->

<!-- Bootstrap Progress Bar -->

<script type="text/javascript" src="{{ asset('template/widgets/progressbar/progressbar.js') }}"></script>

<!-- Bootstrap Buttons -->

<!-- <script type="text/javascript" src="{{ asset('template/widgets/button/button.js') }}"></script> -->

<!-- Bootstrap Collapse -->

<!-- <script type="text/javascript" src="{{ asset('template/widgets/collapse/collapse.js') }}"></script> -->

<!-- Superclick -->

<script type="text/javascript" src="{{ asset('template/widgets/superclick/superclick.js') }}"></script>

<!-- Input switch alternate -->

<script type="text/javascript" src="{{ asset('template/widgets/input-switch/inputswitch-alt.js') }}"></script>

<!-- Slim scroll -->

<script type="text/javascript" src="{{ asset('template/widgets/slimscroll/slimscroll.js') }}"></script>

<!-- Slidebars -->

<script type="text/javascript" src="{{ asset('template/widgets/slidebars/slidebars.js') }}"></script>
<script type="text/javascript" src="{{ asset('template/widgets/slidebars/slidebars-demo.js') }}"></script>

<!-- PieGage -->

{{-- <script type="text/javascript" src="{{ asset('template/widgets/charts/piegage/piegage.js') }}"></script> --}}
{{-- <script type="text/javascript" src="{{ asset('template/widgets/charts/piegage/piegage-demo.js') }}"></script> --}}

<!-- Screenfull -->

<script type="text/javascript" src="{{ asset('template/widgets/screenfull/screenfull.js') }}"></script>

<!-- Content box -->

<script type="text/javascript" src="{{ asset('template/widgets/content-box/contentbox.js') }}"></script>

<!-- Overlay -->

<script type="text/javascript" src="{{ asset('template/widgets/overlay/overlay.js') }}"></script>

<!-- Widgets init for demo -->

{{-- <script type="text/javascript" src="{{ asset('template/js-init/widgets-init.js') }}"></script> --}}

<!-- Theme layout -->

{{-- <script type="text/javascript" src="{{ asset('template/themes/admin/layout.js') }}"></script> --}}

<!-- Theme switcher -->

{{-- <script type="text/javascript" src="{{ asset('template/widgets/theme-switcher/themeswitcher.js') }}"></script> --}}

<script type="text/javascript" src="{{ asset('template/widgets/jgrowl-notifications/jgrowl.js') }}"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

{{-- JS --}}
<script type="text/javascript" src="{{ asset('js/app.js') }}"></script>
@yield('scripts')

</body>
</html>