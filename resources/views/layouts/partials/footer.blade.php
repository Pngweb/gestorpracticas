<div class="center-contect row" style="display: table-footer-group;">
        <div class="col-md-12 col-sm-5 col-xs-11 center-margin">
            <h6 class="text-center pad15B pad25T pad25L pad25R text-transform-upr font-size-21">
            SU PRIVACIDAD Y CONFIANZA SON MUY IMPORTANTES PARA NOSOTROS. POR ELLO, QUEREMOS ASEGURARNOS QUE CONOZCA CÓMO SALVAGUARDAMOS LA INTEGRIDAD, CONFIDENCIALIDAD Y DISPONIBILIDAD, DE SUS DATOS PERSONALES. AVATAR TECHNOLOGY S.A.S 
            </h6>
            <div class="row pad15B pad25L pad25R text-center">
                <div class="col-md-3"></div>

                <div class="col-md-2"  style="display: flex;">
                    <a href="https://web.whatsapp.com/" class="btn btn-round btn-success">
                        <i class="fab fa-whatsapp"></i>
                    </a>
                    <p style="margin: auto; margin-left: 10px;">3003508928</p>
                   
                </div>
                <div class="col-md-2" style="display: flex;">
                    <button class="btn btn-round btn-warning center-margin" style="margin-right: 0;">
                        <i class="fas fa-comment-dots"></i>
                    </button>
                    <p style="margin: auto; margin-left: 10px;">PQR</p>
                </div> 
                <div class="col-md-2" style="display: flex; padding:0;">
                    <button class="btn btn-round btn-default">
                        <i class="fas fa-headset"></i>
                    </button>
                    <p style="margin: auto;">SOPORTE TÉCNICO</p>
                </div> 
                <div class="col-md-2"></div>

            </div>
       
        </div>
    </div>