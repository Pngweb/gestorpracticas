<?php 
    $user = Auth::user();
    if($user->rol_id == 1){
        $session = array('name'=>$user->tercero->adm_nombre,'imagen'=> null, 'nit' => $user->tercero->adm_identificacion);
    }elseif($user->rol_id == 2){
        $session = array('name'=>$user->tercero->institucion_nombre,'imagen' => $user->tercero->institucion_logo,'folder' => 'images_institucion', 'nit' => $user->tercero->institucion_identificacion);
    }elseif($user->rol_id == 3){
        $session = array('name'=>$user->tercero->empresa_nombre,'imagen' => $user->tercero->empresa_logo, 'folder' => 'images_empresas','nit' => $user->tercero->empresa_nit);
    }else{
        $session = array('name'=>$user->user_nombre,'imagen' => $user->tercero->estudiante_foto, 'folder' => 'images_estudiantes', 'nit' => $user->tercero->estudiante_identificacion);
    }
?>
<div id="page-header" class="bg-azul-inca">
    <div id="mobile-navigation">
        <button id="nav-toggle" class="collapsed" data-toggle="collapse" data-target="#page-sidebar"><span></span></button>
        <a href="{{ route('home') }}" class="logo-content-small" title="MonarchUI"></a>
    </div>
    <div id="header-logo" class="logo-bg">
        <a href="{{ route('home') }}" class="logo-content-big" title="Centro Inca">
            <div style="float: left; margin-left: 20px; margin-right: 10px;">
                <img class="img-thumbnail header-imagen" src="{{ asset(($session['imagen'] == null) ? 'images/no-image.png' : 'images/'.$session['folder'].'/'.$session['imagen'] ) }}"  style="width: 50px;">
            </div>
            <div class="font-size-12 text-left font-white" style="white-space: nowrap; overflow: hidden;">
                <span>{{ $session['name'] }}</span><br>
                <span>Nit. {{ $session['nit'] }}</span>
            </div>
        </a>
        <a href="index.html" class="logo-content-small" title="Centro Inca">
            <div style="float: left;">
                <img class="img-thumbnail header-imagen" src="{{ asset(($session['imagen'] == null) ? 'images/no-image.png' : 'images/'.$session['folder'].'/'.$session['imagen'] ) }}"  style="width: 50px;">
            </div>
        </a>
        <a id="close-sidebar" href="#" title="Close sidebar">
            <i class="glyph-icon icon-angle-left"></i>
        </a>
    </div>


    <div id="header-nav-left" style="float: right;">
        <div class="user-account-btn dropdown">
            <a href="#" title="My Account" class="user-profile clearfix" data-toggle="dropdown" id="btn-myacount">
                {{-- <img width="28" src="{{ asset(($session['imagen'] == null) ? 'images/no-image.png' : 'images/'.$session['folder'].'/'.$session['imagen'] ) }}" alt="Profile image"> --}}
                <span>{{ $user->user_nombre }}</span>
                <i class="glyph-icon icon-angle-down"></i>
            </a>
            <div class="dropdown-menu float-left" id="div-myAcount">
                <div class="box-sm">
                    <div class="login-box clearfix">
                        <div class="user-img">
                            {{-- <a href="#" title="" class="change-img">Change photo</a> --}}
                            <img src="{{ asset(($session['imagen'] == null) ? 'images/no-image.png' : 'images/'.$session['folder'].'/'.$session['imagen'] ) }}" alt="">
                        </div>
                        <div class="user-info">
                            <span>
                                {{ $user->user_nombre }}
                                <i>Cargo : {{ $user->cargo->cargo_descripcion }}</i>
                            </span>
                        </div>
                    </div>
                    {{-- <div class="divider"></div> --}}
                    
                    <div class="pad5A button-pane button-pane-alt text-center">
                        <a class="btn display-block font-normal btn-danger" href="{{ route('logout') }}"
                            onclick="event.preventDefault();
                            document.getElementById('logout-form').submit();">
                            <i class="glyph-icon icon-power-off"></i>
                            Cerrar Sesión
                        </a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- #header-nav-left -->

    <div id="header-nav-right">
        <input type="hidden" id="hdRolNotificaciones" value="{{ Auth::user()->rol_id }}">
        <input type="hidden" id="hdTerceroNotificaciones" value="{{ Auth::user()->tercero_id }}">
        <div class="dropdown" id="notifications-btn">
            <a data-toggle="dropdown" href="#" title="">
                <span class="small-badge bg-yellow"></span>
                <i class="glyph-icon icon-linecons-megaphone"></i>
            </a>
            <div class="dropdown-menu box-md float-right">

                <div class="popover-title display-block clearfix pad10A">
                    Notificaciones
                    {{-- <a class="text-transform-cap font-primary font-normal btn-link float-right" href="#" title="View more options">
                        More options...
                    </a> --}}
                </div>
                <div class="scrollable-content scrollable-slim-box">
                    <ul class="no-border notifications-box" id="notification-list">
{{--                         <li>
                            <span class="bg-danger icon-notification glyph-icon icon-bullhorn"></span>
                            <span class="notification-text">This is an error notification</span>
                            <div class="notification-time">
                                a few seconds ago
                                <span class="glyph-icon icon-clock-o"></span>
                            </div>
                        </li>
                        <li>
                            <span class="bg-warning icon-notification glyph-icon icon-users"></span>
                            <span class="notification-text font-blue">This is a warning notification</span>
                            <div class="notification-time">
                                <b>15</b> minutes ago
                                <span class="glyph-icon icon-clock-o"></span>
                            </div>
                        </li>
                        <li>
                            <span class="bg-green icon-notification glyph-icon icon-sitemap"></span>
                            <span class="notification-text font-green">A success message example.</span>
                            <div class="notification-time">
                                <b>2 hours</b> ago
                                <span class="glyph-icon icon-clock-o"></span>
                            </div>
                        </li>
                        <li>
                            <span class="bg-azure icon-notification glyph-icon icon-random"></span>
                            <span class="notification-text">This is an error notification</span>
                            <div class="notification-time">
                                a few seconds ago
                                <span class="glyph-icon icon-clock-o"></span>
                            </div>
                        </li>
                        <li>
                            <span class="bg-warning icon-notification glyph-icon icon-ticket"></span>
                            <span class="notification-text">This is a warning notification</span>
                            <div class="notification-time">
                                <b>15</b> minutes ago
                                <span class="glyph-icon icon-clock-o"></span>
                            </div>
                        </li>
                        <li>
                            <span class="bg-blue icon-notification glyph-icon icon-user"></span>
                            <span class="notification-text font-blue">Alternate notification styling.</span>
                            <div class="notification-time">
                                <b>2 hours</b> ago
                                <span class="glyph-icon icon-clock-o"></span>
                            </div>
                        </li>
                        <li>
                            <span class="bg-purple icon-notification glyph-icon icon-user"></span>
                            <span class="notification-text">This is an error notification</span>
                            <div class="notification-time">
                                a few seconds ago
                                <span class="glyph-icon icon-clock-o"></span>
                            </div>
                        </li>
                        <li>
                            <span class="bg-warning icon-notification glyph-icon icon-user"></span>
                            <span class="notification-text">This is a warning notification</span>
                            <div class="notification-time">
                                <b>15</b> minutes ago
                                <span class="glyph-icon icon-clock-o"></span>
                            </div>
                        </li>
                        <li>
                            <span class="bg-green icon-notification glyph-icon icon-user"></span>
                            <span class="notification-text font-green">A success message example.</span>
                            <div class="notification-time">
                                <b>2 hours</b> ago
                                <span class="glyph-icon icon-clock-o"></span>
                            </div>
                        </li>
                        <li>
                            <span class="bg-purple icon-notification glyph-icon icon-user"></span>
                            <span class="notification-text">This is an error notification</span>
                            <div class="notification-time">
                                a few seconds ago
                                <span class="glyph-icon icon-clock-o"></span>
                            </div>
                        </li>
                        <li>
                            <span class="bg-warning icon-notification glyph-icon icon-user"></span>
                            <span class="notification-text">This is a warning notification</span>
                            <div class="notification-time">
                                <b>15</b> minutes ago
                                <span class="glyph-icon icon-clock-o"></span>
                            </div>
                        </li> --}}
                    </ul>
                </div>
                {{-- <div class="pad10A button-pane button-pane-alt text-center">
                    <a href="#" class="btn btn-primary" title="View all notifications">
                        View all notifications
                    </a>
                </div> --}}
            </div>
        </div>
    </div>

</div>