<div class="example-box-wrapper">
    <div id="form-wizard-3" class="form-wizard">
        <ul>
          @if (route::current()->getName() == 'cupos-practicas.index')
            <li class="active">
          @else
            <li>
          @endif
                <a href="{{ route('cupos-practicas.index') }}">
                    <label class="wizard-step"><i class="glyph-icon icon-bars"></i></label>
              <span class="wizard-description">
                 Cupos de Practicas
              </span>
                </a>
            </li>
          @if (route::current()->getName() == 'estudiantes-disponibles.index')
            <li class="active">
          @else
            <li>
          @endif
                <a href="{{ route('estudiantes-disponibles.index') }}">
                    <label class="wizard-step"><i class="glyph-icon icon-group"></i></label>
              <span class="wizard-description">
                 Estudiantes Disponibles
              </span>
                </a>
            </li>
            @if (route::current()->getName() == 'estudiantes-preseleccionados.index')
            <li class="active">
          @else
            <li>
          @endif
                <a href="{{ route('estudiantes-preseleccionados.index') }}">
                    <label class="wizard-step"><i class="glyph-icon icon-file-text"></i></label>
              <span class="wizard-description">
                 Estudiantes Pre-seleccionados
              </span>
                </a>
            </li>
            @if (route::current()->getName() == 'estudiantes-seleccionados.index')
            <li class="active">
          @else
            <li>
          @endif
                <a href="{{ route('estudiantes-seleccionados.index') }}">
                    <label class="wizard-step"><i class="glyph-icon icon-check"></i></label>
              <span class="wizard-description">
                 Estudiantes Selecionados
              </span>
                </a>
            </li>
        </ul>
    </div>
</div>