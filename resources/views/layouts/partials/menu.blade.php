<div id="page-sidebar">
    <div class="scroll-sidebar">
        <ul id="sidebar-menu">
            <li class="header"><span>MENÚ</span></li>
            
            <li>
                <a href="{{ route('home') }}" title="Inicio">
                    <i class="glyph-icon icon-home"></i>
                    <span>Inicio</span>
                </a>
            </li>
            <li class="divider"></li>
            {{-- {{ dd($menus) }} --}}
            @foreach ($menus as $menu)
            <li>
                <a href="{{ (($menu->url_menu) ? route($menu->url_menu)  : '#' ) }}" title="{{ $menu->descripcion_menu }}">
                    <i class="glyph-icon {{ $menu->icon_menu }}"></i>
                    <span>{{ $menu->descripcion_menu }}</span>
                </a>
                
                @if (!$menu->url_menu)                
                    <div class="sidebar-submenu">

                        @if (count($menu->smenu) > 0)
                            <ul>
                                @foreach ($menu->smenu as $smenu)
                                    <li><a href="{{ route($smenu->url_smenu) }}"><span>{{ $smenu->descripcion_smenu }}</span>
                                        @if (Auth::user()->rol_id == 3)
                                            @php
                                                $complete = true;
                                                $visible = false;
                                                if($smenu->url_smenu == 'mensajeria.index'){
                                                    if($val_mensajeria){
                                                        $visible = true;
                                                        if($mensajeria == 0){
                                                            $complete = false;
                                                        }
                                                    }
                                                }else if($smenu->url_smenu == 'dependencias.index'){
                                                    $visible = true;
                                                    if($dependencias == 0){
                                                        $complete = false;
                                                    }
                                                }else if($smenu->url_smenu == 'sucursales.index'){
                                                    $visible = true;
                                                    if($sucursales == 0){
                                                        $complete = false;
                                                    }
                                                }else if($smenu->url_smenu == 'cupos-practicas.index'){
                                                    $visible = true;
                                                    if($cupos == 0){
                                                        $complete = false;
                                                    }
                                                }else if($smenu->url_smenu == 'configuracionPasos.index'){
                                                    $visible = true;
                                                    if($pasos == 0){
                                                        $complete = false;
                                                    }
                                                }else if($smenu->url_smenu == 'funcionarios.index'){
                                                    $visible = true;
                                                    if($representantes == 0){
                                                        $complete = false;
                                                    }
                                            }
                                            @endphp
                                            @if ($visible)
                                                <i class="fas fa-check-circle float-right mrg10A {{ (($complete) ? 'font-green' : 'font-gray-dark' ) }}"></i>
                                            @endif
                                        @endif
                                    </a></li>
                                @endforeach
                            </ul>
                        @endif
            
                    </div><!-- .sidebar-submenu -->
                @endif
            </li>
            @endforeach
        </ul><!-- #sidebar-menu -->
    </div>
</div>