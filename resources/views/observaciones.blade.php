@extends('layouts.site')

@section('title')
    Observaciones Practicante
@endsection

@section('content')
<div id="page-title"  style="display: flex; margin-bottom: 10px;">
    <h2>REGISTRAR OBSERVACIONES</h2>
     <a id="back" style="margin-bottom: auto;margin-top: auto;margin-left: auto;font-size: 1.2em;text-decoration: none;color: black;cursor: pointer;"> 
            <i class="glyph-icon icon-arrow-left"></i>
        Volver
    </a>
</div>
<div class="panel">
    <div class="panel-body">
    <div class="row">
        <div class="col-md-12 mrg20B text-right">
            <button class="btn btn-alt btn-hover btn-primary" data-target="#mdlCrear" onclick="abrirModal(this)">
                    <span>Agregar Observacion</span>
                    <i class="glyph-icon icon-plus"></i>
            </button>
        </div>
    </div>

      {{-- Modal Creacion --}}
        <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="mdlCrear">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Registro de Observacion</h4>
                    </div>
                    <div class="modal-body">
                        <form action="{{ route('observacion-practicante.store') }}" id="frmCrearObservacion">
                            @csrf
                            <input type="hidden" name="hdTercero" id="hdTercero" value="{{ Auth::user()->tercero_id }}">
                             <input type="hidden" name="seleccionado_id" id="seleccionado_id" value="{{ $idseleccionado }}">
                            <div class="row">
                                
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label">Observaciones</label>
                                        <textarea class="form-control" name="observacion"></textarea>
                                    </div>
                                </div>
                                
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        <button type="button" class="btn btn-primary" onclick="$('#frmCrearObservacion').submit()">Guardar</button>
                    </div>
                </div>
            </div>
        </div>
        {{-- Modal Creacion --}}

        <div class="example-box-wrapper">

            <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="datatable-example">
            <thead>
            <tr>
                <th>Observación</th>
                <th>Fecha de Observación</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            
            </tbody>
            </table>
        </div>
        {{-- end tabla --}}
    </div>
</div>
@endsection

@section('scripts')
    <script type="text/javascript" src="{{ asset('template/widgets/datatable/datatable.js') }}"></script>
    <script type="text/javascript" src="{{ asset('template/widgets/datatable/datatable-bootstrap.js') }}"></script>
    <script type="text/javascript" src="{{ asset('template/widgets/datatable/datatable-tabletools.js') }}"></script>
    <script type="text/javascript" src="{{ asset('template/widgets/jgrowl-notifications/jgrowl.js') }}"></script>
    <script type="text/javascript" src="{{ asset('template/widgets/datepicker/datepicker.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/observacion.js') }}"></script>
@endsection
