@extends('layouts.site')

@section('title')
    Proceso de Selección
@endsection

@section('content')
<div id="page-title" style="display: flex; margin-bottom: 10px;">
    <h2>Proceso de Selección</h2>
    <a href="{{ route('estudiantes-preseleccionados.index') }}" style="margin-bottom: auto;margin-top: auto;margin-left: auto;font-size: 1.2em;text-decoration: none;color: black;cursor: pointer;"> 
            <i class="glyph-icon icon-arrow-left"></i>
        Volver
    </a>
</div>
<div class="panel">
    <div class="panel-body">
        <h3 class="title-hero" style="opacity: 1;">
            Proceso de Selección para el cupo <strong style="color: black;font-size: 1.2em;" id="namecupo"></strong> 
        </h3>
        
        <div class="example-box-wrapper">
            <input type="hidden" id="hdEmpresa" value="{{ Auth::user()->tercero_id }}">
            <input type="hidden" name="estudiante_id" id="estudiante_id" value="{{ $idestudiante }}">
            <input type="hidden" name="idpendencia" id="idpendencia" value="{{ $idependencia }}">
            <input type="hidden" name="idsolicitud" id="idsolicitud" value="{{ $idsolicitud }}">
            <div id="form-wizard-3" class="form-wizard">
                <ul id="pasos">
                    <!-- <li>
                        <a href="#step-1" data-toggle="tab">
                            <label class="wizard-step complete"><i class="glyph-icon icon-check"></i></label>
                      <span class="wizard-description">
                         Prueba Psicotécnica
                         <small>17/07/2020</small>
                      </span>
                        </a>
                    </li>
                    <li class="active">
                        <a href="#step-2" data-toggle="tab">
                            <label class="wizard-step "><i class="glyph-icon icon-check"></i></label>
                      <span class="wizard-description">
                         Entrevista Presencial
                         <small>20/07/2020</small>
                      </span>
                        </a>
                    </li>
                    <li >
                        <a href="#step-3" data-toggle="tab">
                            <label class="wizard-step"><i class="glyph-icon icon-check"></i></label>
                      <span class="wizard-description">
                         Entrevista Virtual
                         <small>22/07/2020</small>
                      </span>
                        </a>
                    </li>
                    <li>
                        <a href="#step-4" data-toggle="tab">
                            <label class="wizard-step"><i class="glyph-icon icon-check"></i></label>
                      <span class="wizard-description">
                         Entrevista Talento Humano
                         <small>25/07/2020</small>
                      </span>
                        </a>
                    </li>
                    <li>
                        <a href="#step-5" data-toggle="tab">
                            <label class="wizard-step"><i class="glyph-icon icon-check"></i></label>
                      <span class="wizard-description">
                         Visita Domiciliaria
                         <small>28/07/2020</small>
                      </span>
                        </a>
                    </li> -->
                </ul>
                <hr>
                <div class="row">
                    <div class="col-md-2">

                        <img class="img-thumbnail" id="foto" width="100"><br>
                        <strong id="nombres"></strong><br>
                        <label id="cedula"></label>
                    </div>
                    <div class="col-md-5">
                        <h3>Perfil Ocupacional</h3>
                        <p>Excelente manejo de sus relaciones interpersonales habilidad para el manejo de operaciones comerciales, bancarias y empresariales, capacidad de adaptación a las innovaciones tecnológicas e idoneidad en la ejecución de procedimientos operativos en las distintas dependencias de una entidad bancaria o comercial.</p>
                    </div>
                    <div class="col-md-5">
                        <h3>Areas de Desempeño</h3>
                        <p>Auxiliar de cartera, auxiliar de oficina, auxiliar administrativo de instituciones o entidades de financiamiento, auxiliar de departamento de crédito, cobranza o financiero, auxiliar contable, cajero recibidor-pagador, transcriptor de datos en computador, auxiliar de nómina, promotor de servicios bancarios.</p>
                    </div>
                </div>
                <hr>
                <div class="tab-content" id="tab_info">
                    <div class="tab-pane" id="step-1">
                        <div class="row">
                            <div class="col-md-4">
                                Paso: <strong>Prueba Psicotécnica</strong><br>
                                Estado: <strong>Realizada</strong><br>
                                Fecha: <strong>17/07/2020</strong><br>
                            </div>
                            <div class="col-md-4">
                                <strong>Responsable</strong><br>
                                <p>
                                    Maria Cardona<br>
                                    Tel: (575) 3697600 ext 1654 <br>
                                    Movil: (57)3003508928 <br>
                                    E-mail: correo@empresa.com <br>
                                    Resposable de Entrevistas<br>
                                    Direccion: Calle 57 No 46-103 Barranquilla <br>
                                    Whtasapp: (57)3003508928 <br>
                                </p>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Observaciones</label>
                                    <textarea rows="4" class="form-control"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="step-2">
                        <div class="row">
                            <div class="col-md-4">
                                Paso: <strong>Entrevista Presencial</strong><br>
                                Estado: <strong>Realizada</strong><br>
                                Fecha: <strong>20/07/2020</strong><br>
                            </div>
                            <div class="col-md-4">
                                <strong>Responsable</strong><br>
                                <p>
                                    Maria Cardona<br>
                                    Tel: (575) 3697600 ext 1654 <br>
                                    Movil: (57)3003508928 <br>
                                    E-mail: correo@empresa.com <br>
                                    Resposable de Entrevistas<br>
                                    Direccion: Calle 57 No 46-103 Barranquilla <br>
                                    Whtasapp: (57)3003508928 <br>
                                </p>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Observaciones</label>
                                    <textarea rows="4" class="form-control"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                   <!--  <div class="tab-pane active" id="step-3">
                        <div class="row">
                            <div class="col-md-4">
                                Paso: <strong>Entrevista</strong><br>
                                Estado: <strong id="estado">Pendiente</strong><br>
                                Fecha: <strong id="fecha"></strong><br>
                                
                                <button class="btn btn-sm btn-primary mrg5T" id="btnpro" data-target="#mdlCrear" onclick="abrirModal(this)">
                                        <span>Programar</span>
                                </button>

                                <button class="btn btn-sm btn-primary mrg5T" id="btnrepro" data-target="#mdlCrear" onclick="abrirModal(this)" style="display: none;">
                                        <span>Reprogramar</span>
                                </button>
           
                                <button class="btn btn-sm btn-primary mrg5T" id="btnrealizado" style="display: none">Paso Realizado</button>
                            </div>
                            <div class="col-md-4">
                                <strong>Responsable</strong><br>
                                <p>
                                    Maria Cardona<br>
                                    Tel: (575) 3697600 ext 1654 <br>
                                    Movil: (57)3003508928 <br>
                                    E-mail: correo@empresa.com <br>
                                    Resposable de Entrevistas<br>
                                    Direccion: Calle 57 No 46-103 Barranquilla <br>
                                    Whtasapp: (57)3003508928 <br>
                                </p>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Observaciones</label>
                                    <textarea rows="4" id="observacion" class="form-control"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="step-4">
                        <div class="row">
                            <div class="col-md-4">
                                Paso: <strong>Entrevista Talento Humano</strong><br>
                                Estado: <strong>Pendiente</strong><br>
                                Fecha: <strong>25/07/2020</strong><br>
                                <button class="btn btn-sm btn-primary mrg5T">Reprogramar</button> <button class="btn btn-sm btn-primary mrg5T">Paso Realizado</button>
                            </div>
                            <div class="col-md-4">
                                <strong>Responsable</strong><br>
                                <p>
                                    Maria Cardona<br>
                                    Tel: (575) 3697600 ext 1654 <br>
                                    Movil: (57)3003508928 <br>
                                    E-mail: correo@empresa.com <br>
                                    Resposable de Entrevistas<br>
                                    Direccion: Calle 57 No 46-103 Barranquilla <br>
                                    Whtasapp: (57)3003508928 <br>
                                </p>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Observaciones</label>
                                    <textarea rows="4" class="form-control"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="step-5">
                        <div class="row">
                            <div class="col-md-4">
                                Paso: <strong>Visita Domiciliaria</strong><br>
                                Estado: <strong>Pendiente</strong><br>
                                Fecha: <strong>25/07/2020</strong><br>
                                <button class="btn btn-sm btn-primary mrg5T">Reprogramar</button> <button class="btn btn-sm btn-primary mrg5T">Paso Realizado</button>
                            </div>
                            <div class="col-md-4">
                                <strong>Responsable</strong><br>
                                <p>
                                    Maria Cardona<br>
                                    Tel: (575) 3697600 ext 1654 <br>
                                    Movil: (57)3003508928 <br>
                                    E-mail: correo@empresa.com <br>
                                    Resposable de Entrevistas<br>
                                    Direccion: Calle 57 No 46-103 Barranquilla <br>
                                    Whtasapp: (57)3003508928 <br>
                                </p>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Observaciones</label>
                                    <textarea rows="4" class="form-control"></textarea>
                                </div>
                            </div>
                        </div>
                    </div> -->
                    
                </div>
                    <div style="text-align: right;" >
                        <button class="btn btn-sm btn-success mrg5T" onclick="Seleccionar()">Seleccionar</button> 
                        <button class="btn btn-sm btn-danger mrg5T" onclick="Noseleccionar()">No Seleccionar</button>
                    </div>
            </div>
        </div>
    
        {{-- Modal Programar --}}
        <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="mdlCrear">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="titulo">Programar paso</h4>
                    </div>
                    <div class="modal-body">
                        <form action="#" id="frmCrearDependencia">
                            @csrf
                            <div class="row">
                                <div class="col-md-6 col-md-offset-3">
                                    <input type="hidden" name="idprogram" id="idprogram">
                                    <input type="hidden" name="numero" id="numero">
                                    <div class="form-group">
                                        <label>Fecha</label>
                                        <input type="text" name="fechaProgramar" id="fechaProgramar" class="bootstrap-datepicker form-control" autocomplete="off">
                                    </div>
                                </div>
                                <div class="col-md-6 col-md-offset-3">
                                    <div class="form-group">
                                        <label>Observación</label>
                                        <textarea class="form-control" name="observacionProgramar" id="observacionProgramar"></textarea>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        <button type="button" class="btn btn-primary" onclick="programar()" >Guardar</button>
                    </div>
                </div>
            </div>
        </div>
        {{-- Modal Creacion --}}
        
        {{-- Modal seleccion --}}
        <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="mdlSeleccion">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Selección de estudiante</h4>
                    </div>
                    <div class="modal-body">
                        <form action="#" id="frmCrearVancantes">
                            @csrf

                            <div class="alert alert-warning" id="notification">
                                <div class="bg-yellow alert-icon" data-dismiss="modal" style="cursor: pointer;">
                                    <i class="glyph-icon icon-times"></i>
                                </div>
                                <div class="alert-content" >
                                    <h4 class="alert-title">Información Pendiente</h4>
                                    <ul style="list-style: none; padding-left: 0;">
                                        <li>
                                        </i>Hay pasos en estado <strong>Pendiente</strong>
                                        </li>
                                    </ul>
                                </div>
                            </div>

                            <div class="panel-group" id="accordion">
                                <div class="panel">
                                    <div class="panel-heading">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" class="">
                                            <h4 class="panel-title">
                                                    Información Inicial
                                            </h4>
                                        </a>
                                    </div>
                                    <div id="collapseOne" class="panel-collapse collapse in" aria-expanded="true" style="">
                                        <div class="panel-body">
                                           <div class="row">
                                               <div class="col-md-6">
                                                   <div class="form-group">
                                                    <input type="hidden" name="estudiante_id" id="estudiante_id" value="{{ $idestudiante }}">
                                                     <input type="hidden" name="idsolicitud" id="idsolicitud" value="{{ $idsolicitud }}">
                                                     <input type="hidden" name="dependencia" id="dependencia" value="{{ $idependencia }}">
                                                       <label>Dependencia Asignada</label>
                                                       <input type="text" class="form-control" name="dependencianame" id="dependencianame" readonly>
                                                   </div>
                                               </div>
                                                <div class="col-md-6">
                                                   <div class="form-group">
                                                       <label>Maestro Guía</label>
                                                       <input type="hidden" name="maestro" id="maestro">
                                                       <input type="text" class="form-control" name="maestroname" id="maestroname"readonly>
                                                   </div>
                                               </div>
                                               <div class="col-md-12">
                                                   <div class="form-group">
                                                       <label>Función del Practicante</label>
                                                       <!-- <textarea></textarea> -->
                                                       <textarea name="funciones" id="funciones" class="form-control"></textarea>
                                                   </div>
                                               </div>
                                           </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="panel">
                                    <div class="panel-heading">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" class="collapsed" aria-expanded="false">
                                            <h4 class="panel-title">
                                               Etapa lectiva
                                            </h4>
                                        </a>
                                    </div>
                                    <div id="collapseTwo" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                                        <div class="panel-body">
                                           <div class="row">
                                               <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Fecha de Inicio</label>
                                                        <input type="text" name="fechaInicioLectiva" id="fechaInicioLectiva" class="form-control" autocomplete="off">
                                                    </div>
                                               </div>
                                               <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Fecha de Finalización</label>
                                                        <input type="text" name="fechaFinLectiva" id="fechaFinLectiva" class="form-control" autocomplete="off">
                                                    </div>
                                               </div>
                                           </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="panel">
                                    <div class="panel-heading">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree" class="collapsed" aria-expanded="false">
                                            <h4 class="panel-title">
                                                Etapa productiva
                                            </h4>
                                        </a>
                                    </div>
                                    <div id="collapseThree" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                                        <div class="panel-body">
                                           <div class="row">
                                               <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Fecha de Inicio</label>
                                                        <input type="text" name="fechaInicioProductiva" id="fechaInicioProductiva" class="form-control" autocomplete="off">
                                                    </div>
                                               </div>
                                               <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Fecha de Finalización</label>
                                                        <input type="text" name="fechaFinProductiva" id="fechaFinProductiva" class="form-control" autocomplete="off">
                                                    </div>
                                               </div>
                                           </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="panel">
                                    <div class="panel-heading">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapsefour" aria-expanded="false">
                                            <h4 class="panel-title">
                                                Información de Jornada
                                            </h4>
                                        </a>
                                    </div>
                                    <div id="collapsefour" class="panel-collapse collapse " aria-expanded="false" style="height: 0px;">
                                        <div class="panel-body">
                                           <div class="row">
                                               <div class="col-md-6">
                                                   <div class="form-group">
                                                        <label>Jornada de Prácticas</label>
                                                          <select class="form-control" name="jornada" id="jornada">
                                                            <option value="">Seleccionar</option>
                                                              <option value="1">Diurna (Mañana y Tarde)</option>
                                                              <option value="2">Diurna (Mañana)</option>
                                                              <option value="3">Diurna (Tarde)</option>
                                                              <option value="4">Diurna y/o Nocturna</option>
                                                              <option value="5">Nocturna</option>


                                                          </select>
                                                   </div>
                                               </div>
                                                <div class="col-md-6">
                                                   <div class="form-group">
                                                       <label>Horario de Prácticas</label>
                                                       <input type="text" class="form-control" name="horario" id="horario" value="" placeholder="Ejemplo: 8:00 am a 12:00 pm - 2:00 a 6:00 pm">
                                                   </div>
                                               </div>
                                               <div class="col-md-6">
                                                   <div class="form-group">
                                                        <label>Entidad Seguridad Social</label>
                                                          <select class="form-control" name="seguridad" id="seguridad">
                                                           <option value="">Seleccionar</option>
                                                           <option value="27">ALIANSALUD</option>
                                                           <option value="2">CAFESALUD MEDICINA PREPAGADA S.A.</option>
                                                           <option value="34">CAJACOPI</option><option value="3">CAPRECOM ENTIDAD PROMOTORA DE SALUD</option><option value="33">CLINICA GENERAL DEL NORTE</option><option value="4">COLMÉDICA EPS</option><option value="5">COMFENALCO</option><option value="6">COMPENSAR</option><option value="7">COOMEVA</option><option value="8">CRUZ BLANCA ENTIDAD PROMOTORA DE SALUD S.A.</option><option value="29">EPS COLSANITAS</option><option value="9">FAMISANAR LIMITADA CAFAM-COLSUBSIDIO</option><option value="30">FOGYSALUD</option><option value="10">FONDO DE PASIVO SOCIAL DE FERROCARRILES NACIONALES DE COLOMBIA</option><option value="11">FONDO DE SOLIDARIDAD Y GARANTÍA - MINISTERIO DE SALUD</option><option value="28">GOLDEN GROUP</option><option value="12">HUMANA VIVIR S.A.</option><option value="35">IPS FUNCOLS</option><option value="13">LA NUEVA EPS</option><option value="31">MEDIEPS</option><option value="26">MULTIMEDICA</option><option value="1">Ninguno</option><option value="14">RED SALUD ATENCIÓN HUMANA E.P.S. S.A.</option><option value="15">SALUD TOTAL S.A.</option><option value="16">SALUDCOLOMBIA ENTIDAD PROMOTORA DE SALUD S.A.</option><option value="17">SALUDCOOP</option><option value="18">SALUDVIDA S.A. ENTIDAD PROMOTORA DE SALUD</option><option value="32">SANIDAD MILITAR- SIST SALUD</option><option value="19">SANITAS S.A.</option><option value="20">SERVICIO OCCIDENTAL DE SALUD S.A. S.O.S.</option><option value="21">SERVICIOS MÉDICOS COLPATRIA S.A.</option><option value="25">SISBEN</option><option value="22">SOLSALUD E.P.S. S.A.</option><option value="23">SURA EPS</option><option value="24">UNISALUD</option>
                                                          </select>
                                                   </div>
                                               </div>

                                               <div class="col-md-6">
                                                   <div class="form-group">
                                                        <label>Entidad Riesgos Laborales</label>
                                                          <select class="form-control" name="riesgos" id="riesgos">
                                                            <option value="">Seleccionar</option>
                                                            <option value="18">AGENCIA DE SEGUROS SALUD PLUS LTDA</option>
                                                            <option value="11">ALFA S.A.</option>
                                                            <option value="9">ARL SURA</option>
                                                            <option value="5">AURORA</option>
                                                            <option value="8">BBVA SEGUROS DE VIDA COLOMBIA S.A</option>
                                                            <option value="4">BOLIVAR S.A.</option>
                                                            <option value="6">COLMENA S.A</option>
                                                            <option value="7">COLPATRIA S.A.</option>
                                                            <option value="2">COLSEGUROS</option>
                                                            <option value="3">COMPAÑIA AGRICOLA DE SEGUROS DE VIDA S.A.</option>
                                                            <option value="15">INSTITUTO DE SEGUROS SOCIALES I.S.S. RIESGOS PROFESIONALES</option>
                                                            <option value="14">LA EQUIDAD</option>
                                                            <option value="10">LA PREVISORA VIDA S.A.</option>
                                                            <option value="13">LIBERTY</option>
                                                            <option value="16">MAPFRE</option><option value="1">Ninguno</option>
                                                            <option value="17">POSITIVA</option>
                                                            <option value="12">SEGUROS DE VIDA DEL ESTADO S.A.</option>
                                                          </select>
                                                   </div>
                                               </div>
                                           </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        <button type="button" class="btn btn-primary" id="btnGuardarVacantes" onclick="$('#frmCrearVancantes').submit()" >Guardar</button>
                    </div>
                </div>
            </div>
        </div>
        {{-- Modal Creacion --}}

    </div>
</div>
@endsection


@section('scripts')
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script type="text/javascript" src="{{ asset('template/widgets/jgrowl-notifications/jgrowl.js') }}"></script>
<script type="text/javascript" src="{{ asset('template/widgets/datepicker/datepicker.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/procesoSeleccion.js') }}"></script>

<!-- Importaciones para validar fechas -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.3/js/bootstrapValidator.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/3.1.3/js/bootstrap-datetimepicker.min.js"></script>


@endsection