@extends('layouts.app')

@section('title')
    Practicas Inca
@endsection

@section('content')
<div class="center-vertical" style="background-color: #358fc0;">
    <div class="center-content row">
        <form id="login-validation" class="col-md-4 col-sm-5 col-xs-11 col-lg-3 center-margin" method="POST" action="{{ route('login') }}">
            @csrf
            <div id="login-form" class="content-box bg-default login" style="display: none;">
                
                <span class="input-group-addon addon-inside bg-gray" title="Volver" style="cursor: pointer;" id="volver">
                    <i class="glyph-icon icon-arrow-left" style="font-size: 20px;"></i>
                </span>
                 
                <div class="content-box-wrapper pad20A">
                <img class="mrg25B center-margin display-block" src="{{ asset('images/Logo-práctica-gestor-1.png') }}" alt="">
                    <div class="form-group">
                        <div class="input-group">
                            <span class="input-group-addon addon-inside bg-gray">
                                <i class="glyph-icon icon-user"></i>
                            </span>
                            <input type="text" class="form-control" name="identificacion" value="{{ old('identificacion') }}" placeholder="Digite su Identificación" required>
                            @error('identificacion')
                                <input type="hidden" id="error-login" value="{{ $message }}">
                            @enderror
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-group">
                            <span class="input-group-addon addon-inside bg-gray">
                                <i class="glyph-icon icon-unlock-alt"></i>
                            </span>
                            <input type="password" class="form-control" name="password" placeholder="Password" required>
                        </div>
                    </div>
                    <div class="row">
                        <div class="text-right col-md-12">
                            <a href="#" class="switch-button"  title="'¿Olvidaste tu Contraseña?" id="recuperarPass">¿Olvidaste tu Contraseña?</a>
                        </div>
                    </div>
                    <div class="form-group" style="margin-top: 15px;">
                        <button type="submit" class="btn btn-block btn-primary">Login</button>
                    </div>
                    <p class="text-right hidden" id="registroEmpresas"><a href="{{ route('registroEmpresas') }}"><label style="cursor: pointer;">Registrarse</label></a></p>
                </div>
            </div>

            <div id="login-form" class="content-box bg-default password" style="display: none;">
                
                <span class="input-group-addon addon-inside bg-gray" title="Volver" style="cursor: pointer;" id="volver_pass">
                    <i class="glyph-icon icon-arrow-left" style="font-size: 20px;"></i>
                </span>
                 
                <div class="content-box-wrapper pad20A">
                <img class="mrg25B center-margin display-block" src="{{ asset('images/Logo-práctica-gestor-1.png') }}" alt="">
                    <div class="form-group">
                        <div class="input-group">
                            <span class="input-group-addon addon-inside bg-gray">
                                <i class="glyph-icon icon-user"></i>
                            </span>
                            <input type="text" class="form-control" name="identificacion_pass" id="txtIdentificacionPass" placeholder="Digite su Identificación">
                        </div>
                    </div>
                    <div class="form-group" style="margin-top: 15px;">
                        <button type="button" class="btn btn-block btn-primary" id="btnPassword">Consultar</button>
                    </div>
                    </div>
            </div>

            <!-- Inicial  -->

            <div id="login-form" class="content-box bg-default inicial">
                <div class="content-box-wrapper pad20A">
                <img class="mrg25B center-margin display-block" src="{{ asset('images/Logo-práctica-gestor-1.png') }}" alt="">
                    <div class="form-group">
                        <div class="input-group">
                            <div class="row">
                                <div class="col-md-4 text-center combo" data-perfil="empresa" title="EMPRESA" style="cursor: pointer;">
                                 <img src="{{ asset('images/empresa.jpeg') }}" width="70" height="70">
                                    <label>EMPRESA</label>
                                </div>
                                <div class="col-md-4 text-center combo" title="INSTITUCION" style="cursor: pointer;">
                                 <img src="{{ asset('images/colegio.jpeg') }}" width="70" height="70">
                                    <label>INSTITUCION</label>
                                </div>
                                <div class="col-md-4 text-center combo" title="ESTUDIANTE" style="cursor: pointer;">
                                 <img src="{{ asset('images/estudiantes.jpeg') }}" width="70" height="70">
                                    <label>ESTUDIANTE</label>
                                </div>
                            </div>
                           
                        </div>
                    </div>
                </div>
            </div>

            <!--  -->
        </form>

    
    </div>
    <div class="center-contect row" style="display: table-footer-group;">
        <div class="col-md-8 col-sm-5 col-xs-11 center-margin">
            <h6 class="text-center pad25B pad25T pad25L pad25R font-white text-transform-upr font-size-21">
            SU PRIVACIDAD Y CONFIANZA SON MUY IMPORTANTES PARA NOSOTROS. POR ELLO, QUEREMOS ASEGURARNOS QUE CONOZCA CÓMO SALVAGUARDAMOS LA INTEGRIDAD, CONFIDENCIALIDAD Y DISPONIBILIDAD, DE SUS DATOS PERSONALES. AVATAR TECHNOLOGY S.A.S 
            </h6>
            <div class="row pad25B pad25L pad25R text-center">
                <div class="col-md-3"></div>

                <div class="col-md-2" >
                    <a href="https://web.whatsapp.com/" class="btn btn-round btn-success">
                        <i class="fab fa-whatsapp"></i>
                    </a>
                    <p class="font-white center-margin">3003508928</p>
                   
                </div>
                <div class="col-md-2">
                    <button class="btn btn-round btn-warning">
                        <i class="fas fa-comment-dots"></i>
                    </button>
                    <p class="font-white center-margin">PQR</p>
                </div> 
                <div class="col-md-2">
                    <button class="btn btn-round btn-default">
                        <i class="fas fa-headset"></i>
                    </button>
                    <p class="font-white center-margin">SOPORTE TÉCNICO</p>
                </div> 
                <div class="col-md-3"></div>

            </div>
       
        </div>
    </div>
</div>
@endsection

@section('scripts')
    <script type="text/javascript" src="{{ asset('js/login.js') }}"></script>
@endsection
