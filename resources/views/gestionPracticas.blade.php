@extends('layouts.site')

@section('title')
    Gestion de Prácticas
@endsection

@section('content')
<div id="page-title">
    <h2>Gestion de Prácticas</h2>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="panel">
            <input type="hidden" id="hdEmpresa" value="{{ Auth::user()->tercero_id }}">
            <div class="panel-body">
                <h3 class="title-hero">
                    Total de Cupos
                </h3>
                <div class="row">
                    <div class="col-md-4">
                        <a href="{{ route('cupos-practicas.index') }}" title="Total" class="tile-box btn btn-primary">
                            <div class="tile-header">
                                Total
                            </div>
                            <div class="tile-content-wrapper">
                                <i class="glyph-icon icon-group"></i>
                                <div class="tile-content" id="total">
                                    
                                </div>
                            </div>
                        </a>
                    </div>

                    <div class="col-md-4">
                        <a href="{{ route('estudiantes-seleccionados.index') }}" title="Ocupados" class="tile-box btn btn-primary">
                            <div class="tile-header">
                                Ocupados
                            </div>
                            <div class="tile-content-wrapper">
                                <i class="glyph-icon icon-check"></i>
                                <div class="tile-content" id="ocupados">
                                    
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-md-4">
                        <a href="{{ route('estudiantes-disponibles.index') }}" title="Disponibles" class="tile-box btn btn-primary">
                            <div class="tile-header">
                                Disponibles
                            </div>
                            <div class="tile-content-wrapper">
                                <i class="glyph-icon icon-file-text-o"></i>
                                <div class="tile-content" id="disponibles">
                                    
                                </div>
                            </div>
                        </a>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="panel">
            <div class="panel-body">
                <h3 class="title-hero">
                    Practicantes por Instituciones
                </h3>
                <div class="row" id="practicantesinstituciones">
                   <!--  <div class="col-md-6">
                        <div class="tile-box bg-blue-alt">
                            <div class="tile-header text-center">
                                Inca
                            </div>
                            <div class="tile-content-wrapper">
                                <div class="tile-content font-size-20">
                                    18
                                </div>
                                <small>
                                    Practicantes
                                </small>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="tile-box bg-green">
                            <div class="tile-header text-center">
                                Sena
                            </div>
                            <div class="tile-content-wrapper">
                                <div class="tile-content font-size-20">
                                    7
                                </div>
                                <small>
                                    Practicantes
                                </small>
                            </div>
                        </div>
                    </div> -->
                 </div>  
            </div>
        </div>
    </div>
</div>
<div id="page-title">
    <h3 style="text-transform: uppercase;">Cupos de Prácticas</h3>
    <p>Cupos de prácticas próximos a vencerse</p>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="panel">
            <div class="panel-body">
                <h3 class="title-hero">
                    Prontas a vencer - Menos de 30 días
                </h3>
                <div class="example-box-wrapper">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Dependencia</th>
                            <th>Sucursal</th>
                            <th>Cargo</th>
                        </tr>
                        </thead>
                        <tbody id="tbvencimiento">
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="panel">
            <div class="panel-body">
                <h3 class="title-hero">
                    Cupos Disponibles
                </h3>
                <div class="example-box-wrapper">
                     <table class="table">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Dependencia</th>
                            <th>Sucursal</th>
                            <th>Cargo</th>
                        </tr>
                        </thead>
                        <tbody id="tbdisponibles">
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="panel">
            <div class="panel-body">
                <h3 class="title-hero">
                    Ocupados por Área
                </h3>
                <div class="example-box-wrapper">
                    <table class="table" >
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Dependencia</th>
                             <th>Sucursal</th>
                            <th>Cantidad</th>
                        </tr>
                        </thead>
                        <tbody id="tbareas">
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="row">
            <div class="col-md-6">
                <a href="{{ route('funcionarios.index') }}" title="Empleados" class="tile-box tile-box-shortcut btn-gray-dark">
                    <span class="bs-badge badge-absolute" id="empleados"></span>
                        <div class="tile-header">
                            Empleados
                        </div>
                    <div class="tile-content-wrapper">
                        <i class="glyph-icon icon-group"></i>
                    </div>
                </a>
            </div>
            <div class="col-md-6">
                <a href="{{ route('solicitud-practicantes.index') }}" title="Solicitudes Pendientes" class="tile-box tile-box-shortcut btn-black">
                    <span class="bs-badge badge-absolute" id="solicitudes"></span>
                    <div class="tile-header">
                        Solicitudes Pendientes
                    </div>
                    <div class="tile-content-wrapper">
                        <i class="glyph-icon icon-group"></i>
                    </div>
                </a>
            </div>
        </div>
        <div class="panel mrg20T">
            <div class="panel-body">
                <h3 class="title-hero">
                    Ultimos Cupos Asignados
                </h3>
                <div class="example-box-wrapper">
                    <div class="timeline-box timeline-box-left" id="ultimosCupos">
                        <!-- <div class="tl-row">
                            <div class="tl-item float-right">
                                <div class="tl-icon bg-red">
                                    <i class="glyph-icon icon-info"></i>
                                </div>
                                <div class="popover right">
                                    <div class="arrow"></div>
                                    <div class="popover-content">
                                        <div class="tl-label bs-label label-info">COORDINADOR</div>
                                        <p class="tl-content">Cupo de práctica asigando a <strong>Departamento de sistemas</strong> </p>
                                        <div class="tl-time">
                                            <i class="glyph-icon icon-clock-o"></i>
                                            Lunes 20-jul-2020 
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tl-row">
                            <div class="tl-item float-right">
                                <div class="tl-icon bg-primary">
                                    <i class="glyph-icon icon-info"></i>
                                </div>
                                <div class="popover right">
                                    <div class="arrow"></div>
                                    <div class="popover-content">
                                        <div class="tl-label bs-label bg-yellow">AUX. CONTABLE</div>
                                        <p class="tl-content">Cupo de práctica asigando a <strong>Departamento de Contabilidad</strong> </p>
                                        <div class="tl-time">
                                            <i class="glyph-icon icon-clock-o"></i>
                                            Miercoles 15-jul-2020
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tl-row">
                            <div class="tl-item float-right">
                                <div class="tl-icon bg-black">
                                    <i class="glyph-icon icon-info"></i>
                                </div>
                                <div class="popover right">
                                    <div class="arrow"></div>
                                    <div class="popover-content">
                                        <div class="tl-label bs-label label-danger">DESARROLLADOR</div>
                                        <p class="tl-content">Cupo de práctica asigando a <strong>Departamento de Sistemas</strong> </p>
                                        <div class="tl-time">
                                            <i class="glyph-icon icon-clock-o"></i>
                                            Viernes 10-jul-2020
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script type="text/javascript" src="{{ asset('template/widgets/jgrowl-notifications/jgrowl.js') }}"></script>
<script type="text/javascript" src="{{ asset('template/widgets/datepicker/datepicker.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/gestionPracticas.js') }}"></script>

@endsection