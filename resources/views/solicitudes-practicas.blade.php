@extends('layouts.site')

@section('title')
    Solicitud Practicantes
@endsection

@section('content')
<div id="page-title">
    <h2>Solicitud Practicantes</h2>
</div>
<div class="panel">
    <div class="panel-body">

    
        <form id="frmFiltros">
            <!-- <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Institución</label>
                        <select name="cbInstitucion" class="form-control" id="cbInstitucion">
                            <option value="">Seleccione..</option>
                            @foreach ($instituciones as $institucion)
                                <option value="{{ $institucion->institucion_id }}">{{ $institucion->institucion_nombre }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Area</label>
                        <select class="form-control" name="cbArea" id="cbArea">
                            <option value="">Seleccione..</option>
                            @foreach ($areas as $area)
                                <option value="{{ $area->area_id }}">{{ $area->area_descripcion }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Programa</label>
                        <select class="form-control" name="cbPrograma" id="cbPrograma">
                            <option value="">Seleccione..</option>
                        </select>
                    </div>
                </div>
                
            </div> -->
            <div class="row">
                <div class="col-md-3">
                    <label class="control-label">Fecha Inicio</label>
                    <div class="input-prepend input-group">
                        <span class="add-on input-group-addon">
                            <i class="glyph-icon icon-calendar"></i>
                        </span>
                        <input type="text" class="bootstrap-datepicker form-control creq" name="txtFecInicio" id="txtFecInicio" autocomplete="off">
                    </div>
                </div>
                <div class="col-md-3">
                    <label class="control-label">Fecha Fin</label>
                    <div class="input-prepend input-group">
                        <span class="add-on input-group-addon">
                            <i class="glyph-icon icon-calendar"></i>
                        </span>
                        <input type="text" class="bootstrap-datepicker2 form-control creq" name="txtFecFin" id="txtFecFin" autocomplete="off">
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label>Estados</label>
                        <select class="form-control" name="cbEstados">
                            <option value="">Seleccione..</option>
                            <option value="pendiente">Pendiente</option>
                            <option value="aceptada">Aceptada</option>
                            <option value="rechazada">Rechazada</option>
                            <option value="seleccionada">Seleccionada</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-3">
                    <button class="btn btn-alt btn-primary mrg25T" id="btnFiltro" type="submit">
                        <span>Filtrar</span>
                        <i class="glyph-icon icon-filter"></i>
                    </button>
                </div>
            </div>
        </form>

      
        {{-- tabla --}}
        <div class="example-box-wrapper">
            <input type="hidden" id="hdEmpresa" value="{{ Auth::user()->tercero_id }}">
            <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="datatable-example">
            <thead>
            <tr>
                <th>Código</th>
                <th>Estado de solicitud</th>
                <th>Programa</th>
                <th>Sucursal</th>
                <th>Dependencia</th>
                <th>Maestro</th>
                <th>Fecha emisión</th>
                <th>Fecha respuesta</th>
                <th>Observaciones</th>
            </tr>
            </thead>
            <tbody>
            
            </tbody>
            </table>
        </div>
        {{-- end tabla --}}
    </div>
</div>
@endsection

@section('scripts')
    <script type="text/javascript" src="{{ asset('template/widgets/datatable/datatable.js') }}"></script>
    <script type="text/javascript" src="{{ asset('template/widgets/accordion-ui/accordion.js') }}"></script>
    <script type="text/javascript" src="{{ asset('template/widgets/datatable/datatable-bootstrap.js') }}"></script>
    <script type="text/javascript" src="{{ asset('template/widgets/datatable/datatable-tabletools.js') }}"></script>
    <script type="text/javascript" src="{{ asset('template/widgets/jgrowl-notifications/jgrowl.js') }}"></script>
    <script type="text/javascript" src="{{ asset('template/widgets/datepicker/datepicker.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/solicitudesPracticas.js') }}"></script>
@endsection
