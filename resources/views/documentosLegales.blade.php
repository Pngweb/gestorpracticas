@extends('layouts.site')

@section('title')
    Documentos Legales
@endsection

@section('content')
<div id="page-title">
    <h2>Documentos Legales</h2>
</div>
<div class="panel">
    <div class="panel-body">
    	<div class="row">
            <div class="col-md-12 mrg20B text-right">
                <button class="btn btn-alt btn-hover btn-primary" data-target="#mdlCrear" onclick="abrirModal(this)">
                        <span>Nuevo</span>
                        <i class="glyph-icon icon-plus"></i>
                </button>
            </div>
        </div>
        {{-- Modal Creacion --}}
        <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="mdlCrear">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Nuevo Documento</h4>
                    </div>
                    <div class="modal-body">
                        <form action="{{ route('documentos-legales.store') }}" id="frmCrearDocumentos">
                            @csrf
                            <div class="row">
                                <div class="col-md-4 col-md-offset-2">
                                    <div class="form-group">
                                        <label>Documento</label>
                                        <select name="cbDocumento" class="form-control">
                                            <option value="">Seleccione..</option>
                                            @foreach ($documentos as $doc)
                                                <option value="{{ $doc->documento_id }}">{{ $doc->documento_descripcion }}</option>}
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Archivo</label>
                                        <input type="file" name="file_documento" class="form-control">
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        <button type="button" class="btn btn-primary" onclick="$('#frmCrearDocumentos').submit()">Guardar</button>
                    </div>
                </div>
            </div>
        </div>
        {{-- Modal Creacion --}}

        {{-- Modal editar --}}
        <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="mdlEditar">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Editar Dependencia</h4>
                    </div>
                    <div class="modal-body">
                        <form action="{{ route('dependencias.update','') }}" id="frmEditarDependencia">
                            @csrf
                            {{method_field('PUT')}}
                            <input type="hidden" name="hdDependenciaId" id="hdDependenciaId">
                            <div class="row">
                                <div class="col-md-6 col-md-offset-2">
                                    <div class="form-group">
                                        <label>Nombre</label>
                                        <input type="text" name="txtNombre" id="txtNombreEdit" class="form-control" autocomplete="off">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label class="control-label">Estado</label>
                                        <input type="checkbox" name="chkEstado" id="chkEstado" class="input-switch-alt">
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        <button type="button" class="btn btn-primary" onclick="$('#frmEditarDependencia').submit()">Guardar</button>
                    </div>
                </div>
            </div>
        </div>
        {{-- Modal editar --}}

        {{-- tabla --}}
        <div class="example-box-wrapper">
            <input type="hidden" id="hdEmpresa" value="{{ Auth::user()->tercero_id }}">
            <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="datatable-example">
            <thead>
            <tr>
                <th>Dcouemento</th>
                <th>Archivo</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            
            </tbody>
            </table>
        </div>
        {{-- end tabla --}}
    </div>
</div>
@endsection

@section('scripts')
    <script type="text/javascript" src="{{ asset('template/widgets/datatable/datatable.js') }}"></script>
    <script type="text/javascript" src="{{ asset('template/widgets/datatable/datatable-bootstrap.js') }}"></script>
    <script type="text/javascript" src="{{ asset('template/widgets/datatable/datatable-tabletools.js') }}"></script>
    <script type="text/javascript" src="{{ asset('template/widgets/jgrowl-notifications/jgrowl.js') }}"></script>
    <script type="text/javascript" src="{{ asset('template/widgets/datepicker/datepicker.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/documentosEmpresas.js') }}"></script>
@endsection
