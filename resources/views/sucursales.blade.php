@extends('layouts.site')

@section('title')
    Sucursales
@endsection

@section('content')
<div id="page-title">
    <h2>Sucursales</h2>
</div>
<div class="panel">
    <div class="panel-body">
    	<div class="row">
            <div class="col-md-12 mrg20B text-right">
                <button class="btn btn-alt btn-hover btn-primary" data-target="#mdlCrear" onclick="abrirModal(this)">
                        <span>Nuevo</span>
                        <i class="glyph-icon icon-plus"></i>
                </button>
            </div>
        </div>
        {{-- Modal Creacion --}}
        <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="mdlCrear">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Nueva Sucursal</h4>
                    </div>
                    <div class="modal-body">
                        <form action="{{ route('sucursales.store') }}" id="frmCrearSucursal">
                            @csrf
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Nombre</label>
                                        <input type="text" name="txtNombre" class="form-control" autocomplete="off">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label">Departamento</label>
                                        <select class="form-control" name="cbDepto" id="cbDepto" onchange="consultarCiudades(this)">
                                            <option value="">Seleccione..</option>
                                            @foreach ($deptos as $depto)
                                            <option value="{{ $depto->depto_id }}">{{ $depto->depto_descripcion }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label">Ciudad</label>
                                        <select class="form-control" name="cbCiudad" id="cbCiudad">
                                            <option value="">Seleccione..</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Dirección</label>
                                        <input type="text" name="txtDireccion" class="form-control" autocomplete="off">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Telefono</label>
                                        <input type="text" name="txtTelefono" class="form-control" autocomplete="off">
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        <button type="button" class="btn btn-primary" onclick="$('#frmCrearSucursal').submit()">Guardar</button>
                    </div>
                </div>
            </div>
        </div>
        {{-- Modal Creacion --}}

         {{-- Modal Editar --}}
        <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="mdlEditar">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Editar Sucursal</h4>
                    </div>
                    <div class="modal-body">
                        <form action="{{ route('sucursales.update','') }}" id="frmEditarSucursal">
                            @csrf
                            {{method_field('PUT')}}
                            <input type="hidden" name="hdSucursalId" id="hdSucursalId">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Nombre</label>
                                        <input type="text" name="txtNombre" id="txtNombreEdit" class="form-control" autocomplete="off">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label">Departamento</label>
                                        <select class="form-control" name="cbDepto" id="cbDeptoEdit" onchange="consultarCiudades(this)">
                                            <option value="">Seleccione..</option>
                                            @foreach ($deptos as $depto)
                                            <option value="{{ $depto->depto_id }}">{{ $depto->depto_descripcion }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label">Ciudad</label>
                                        <select class="form-control" name="cbCiudad" id="cbCiudadEdit">
                                            <option value="">Seleccione..</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Dirección</label>
                                        <input type="text" name="txtDireccion" id="txtDireccionEdit" class="form-control" autocomplete="off">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Telefono</label>
                                        <input type="text" name="txtTelefono" id="txtTelefonoEdit" class="form-control" autocomplete="off">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label">Estado</label>
                                        <input type="checkbox" name="chkEstado" id="chkEstado" class="input-switch-alt">
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        <button type="button" class="btn btn-primary" onclick="$('#frmEditarSucursal').submit()">Guardar</button>
                    </div>
                </div>
            </div>
        </div>
        {{-- Modal Editar --}}

        {{-- tabla --}}
        <div class="example-box-wrapper">
            <input type="hidden" id="hdEmpresa" value="{{ Auth::user()->tercero_id }}">
            <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="datatable-example">
            <thead>
            <tr>
                <th>Nombre</th>
                <th>Dirección</th>
                <th>Ciudad</th>
                <th>Telefono</th>
                <th>Estado</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            
            </tbody>
            </table>
        </div>
        {{-- end tabla --}}
    </div>
</div>
@endsection

@section('scripts')
    <script type="text/javascript" src="{{ asset('template/widgets/datatable/datatable.js') }}"></script>
    <script type="text/javascript" src="{{ asset('template/widgets/datatable/datatable-bootstrap.js') }}"></script>
    <script type="text/javascript" src="{{ asset('template/widgets/datatable/datatable-tabletools.js') }}"></script>
    <script type="text/javascript" src="{{ asset('template/widgets/jgrowl-notifications/jgrowl.js') }}"></script>
    <script type="text/javascript" src="{{ asset('template/widgets/datepicker/datepicker.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/sucursales.js') }}"></script>
@endsection
