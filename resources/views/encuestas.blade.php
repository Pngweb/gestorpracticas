@extends('layouts.site')

@section('title')
    Encuentas
@endsection

@section('content')
<div id="page-title">
    <h2>Encuestas</h2>
</div>
<div class="panel">
    <div class="panel-body">
      
        {{-- tabla 1--}}
        <div class="example-box-wrapper">
            <h3 class="text-center font-primary font-bold ">Encuestas por Aplicar</h3><br>

            <input type="hidden" id="hdEmpresa" value="{{ Auth::user()->tercero_id }}">
            <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="tabla1">
            <thead>
            <tr>
                <th>Encuesta</th>
                <th>Fecha</th>
                <th>Opciones</th>
            </tr>
            </thead>
            <tbody>
            
            </tbody>
            </table>
        </div>
        {{-- end tabla --}}

        {{-- tabla 2--}}
        <div class="example-box-wrapper">
            <h3 class="text-center font-primary font-bold ">Encuestas Aplicadas</h3><br>
            <input type="hidden" id="hdEmpresa" value="{{ Auth::user()->tercero_id }}">
            <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="tabla2">
            <thead>
            <tr>
                <th>Encuesta</th>
                <th>Fecha</th>
                <th>Opciones</th>
            </tr>
            </thead>
            <tbody>
            
            </tbody>
            </table>
        </div>
        {{-- end tabla --}}
    </div>
</div>
@endsection

@section('scripts')
    <script type="text/javascript" src="{{ asset('template/widgets/datatable/datatable.js') }}"></script>
    <script type="text/javascript" src="{{ asset('template/widgets/accordion-ui/accordion.js') }}"></script>
    <script type="text/javascript" src="{{ asset('template/widgets/datatable/datatable-bootstrap.js') }}"></script>
    <script type="text/javascript" src="{{ asset('template/widgets/datatable/datatable-tabletools.js') }}"></script>
    <script type="text/javascript" src="{{ asset('template/widgets/jgrowl-notifications/jgrowl.js') }}"></script>
    <script type="text/javascript" src="{{ asset('template/widgets/datepicker/datepicker.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/encuestas.js') }}"></script>
@endsection
