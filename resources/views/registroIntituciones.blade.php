@extends('layouts.app')

@section('title')
    Registro Instituciones
@endsection

@section('content')
    <div class="container">
        <div class="row mrg50T">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel">
                    <div class="panel-body">
                        <h3 class="title-hero text-center">
                            Registro de Instituciones
                        </h3>
                        <hr>
                        <form>
                            <div class="row">
                                <div class="col-md-4">
                                    <label class="control-label">Logo</label>
                                    <div class="form-group text-center">
                                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                                <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 200px; height: 150px;"></div>
                                                <div class="text-center">
                                                    <span class="btn btn-default btn-file">
                                                        <span class="fileinput-new">Seleccionar Imagen</span>
                                                        <span class="fileinput-exists">Change</span>
                                                        <input type="file" name="...">
                                                    </span>
                                                    <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                                                </div>
                                            </div>
                                        </div>
                                </div>
                                <div class="col-md-8">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label">Nit</label>
                                                <input type="text" class="form-control">
                                                <small class="form-text text-muted">*No incluya el Dígito de Verificación.</small>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="control-label">Nombre</label>
                                                <input type="text" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="control-label">Sitio Web</label>
                                                <input type="text" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Dirección</label>
                                        <input type="text" class="form-control">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Departamento</label>
                                        <select class="form-control">
                                            <option value="">Seleccione..</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Ciudad</label>
                                        <select class="form-control">
                                            <option value="">Seleccione..</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Telefono</label>
                                        <input type="text" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <hr>
                            
                        </form>
                        <div class="row">
                            <div class="col-md-12 text-center">
                                <button type="button" class="btn btn-primary" onclick="guardarInstitucion()">Guardar</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script type="text/javascript" src="{{ asset('js/registroInstituciones.js') }}"></script>
@endsection