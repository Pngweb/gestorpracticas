@extends('layouts.site')

@section('title')
    Mi Perfil
@endsection

@section('content')
<div id="page-title">
    <h2>Mi Perfil</h2>
</div>
<div class="panel">
    <div class="panel-body">
        <div class="example-box-wrapper">
                    <ul class="nav-responsive nav nav-tabs">
                        <li class="active"><a href="#datos_generales" data-toggle="tab">Datos Generales de la Empresa</a></li>
                        <li><a href="#caracteristicas" data-toggle="tab">Carácterísticas de la Empresa</a></li>
                    </ul>
                    <form action="{{ route('miPerfil.update') }}" id="frmGuardarMiPerfil">
                    <div class="tab-content">
                        <div class="tab-pane active" id="datos_generales">
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="control-label">Logo</label>
                                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                                <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 200px; height: 150px;">
                                                    <i class="glyph-icon icon-remove {{ ((!$empresa->empresa_logo) ? 'hide' : '' ) }}" id="removeImgEdit" onclick="quitarImagenEdit()" style="right: 40px;"></i>
                                                    <img src="{{ (($empresa->empresa_logo) ? asset('images/images_empresas/'.$empresa->empresa_logo) : '') }}" id="img-logo-edit">
                                                </div>
                                                <div class="text-center">
                                                    <span class="btn btn-default btn-file">
                                                        <span class="fileinput-new">Seleccionar Imagen</span>
                                                        <input type="file" name="logo" id="logoEdit">
                                                        <input type="hidden" name="hdRemImg" id="hdRemImg" value="false">
                                                    </span>
                                                </div>
                                            </div>
                                    </div>
                                </div>
                                <div class="col-md-9">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Nit</label>
                                                <input type="text" name="txtNit" value="{{ $empresa->empresa_nit }}"  class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-8">
                                            <div class="form-group">
                                                <label>Nombre</label>
                                                <input type="text" name="txtNombre" value="{{ $empresa->empresa_nombre }}"  class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Sitio Web</label>
                                                <input type="text" name="txtSitioWeb" value="{{ $empresa->empresa_web }}"  class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="caracteristicas">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Organización Juridica</label>
                                        <select name="cbOrgJuridica" class="form-control">
                                            <option value="">Seleccione..</option>
                                            @foreach ($organizaciones as $organizacion)
                                                <option value="{{ $organizacion->org_jur_id }}" {{ (($empresa->org_jur_id == $organizacion->org_jur_id) ? 'selected' : '' ) }}>{{ $organizacion->org_jur_descripcion }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Sector Productivo</label>
                                        <select name="cbSectorProd" class="form-control">
                                            <option value="">Seleccione..</option>
                                            @foreach ($sectores as $sector)
                                                <option value="{{ $sector->sec_prod_id }}" {{ (($empresa->sec_prod_id == $sector->sec_prod_id) ? 'selected' : '' ) }}>{{ $sector->sec_prod_descripcion }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Jornada de Trabajo</label>
                                        <select name="cbJornada" class="form-control">
                                            <option value="">Seleccione..</option>
                                            @foreach ($jornadas as $jornada)
                                                <option value="{{ $jornada->jorn_trab_id }}" {{ (($empresa->jorn_trab_id == $jornada->jorn_trab_id) ? 'selected' : '' ) }}>{{ $jornada->jorn_trab_descripcion }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>¿Su empresa hace parte de un conglomerado?</label>
                                        <select name="cbConglomerado" class="form-control">
                                            <option value="">Seleccione..</option>
                                            <option value="1" {{ (($empresa->empresa_conglomerado) ? 'selected' : '' ) }}>SI</option>
                                            <option value="0" {{ ((!$empresa->empresa_conglomerado) ? 'selected' : '' ) }}>NO</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>No. de Empleados</label>
                                        <input type="number" name="txtNoEmpleados" class="form-control" value="{{ $empresa->empresa_no_empleados }}">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Objeto Social</label>
                                        <textarea name="txtObjSocial" class="form-control">{{ $empresa->empresa_objeto_social  }}</textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 text-right">
                            <button type="submit" class="btn btn-primary">Guardar</button>
                        </div>
                    </div>
                    </form>
                </div>
    </div>
</div>
@endsection

@section('scripts')
    <script type="text/javascript" src="{{ asset('template/widgets/jgrowl-notifications/jgrowl.js') }}"></script>
    <script type="text/javascript" src="{{ asset('template/widgets/tabs/tabs-responsive.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/miPerfil.js') }}"></script>
@endsection
