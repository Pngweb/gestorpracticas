@extends('layouts.app')

@section('title')
    Registro Empresas
@endsection

@section('content')
<div class="center-vertical" style="background-color: #358fc0;">
        <div class="container">
            <div class="row mrg50T">
                <div class="col-md-8 col-md-offset-2">
                    <div class="panel">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-2">
                                   <a href="{{ route('login') }}"><i class="glyph-icon icon-arrow-left" title="Regresar" style="font-size: 14pt;"></i></a>
                                </div>
                                <div class="col-md-8" style="padding-top: 10px;">
                                    <h3 class="title-hero text-center" style="font-weight: bold; margin: 0;">
                                        Registro de Empresas
                                    </h3>
                                </div>
                            </div>
                            <hr>
                            <form action="{{ route('empresas.store') }}" id="frmCrearEmpresa">
                                @csrf
                                <div class="row">
                                    <div class="col-md-4">
                                        <label class="control-label">Logo</label>
                                        <div class="form-group text-center">
                                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                                    <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 200px; height: 150px;">
                                                        <i class="glyph-icon icon-remove hide" id="removeImg" onclick="quitarImagen()"></i>
                                                        <img src="" id="img-logo">
                                                    </div>
                                                    <div class="text-center">
                                                        <span class="btn btn-default btn-file">
                                                            <span class="fileinput-new">Seleccionar Imagen</span>
                                                            <input type="file" name="logo" id="logo">
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label">Nit</label>
                                                    <input type="text" name="txtNit" class="form-control">
                                                    <small class="form-text text-muted">*No incluya el Dígito de Verificación.</small>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label class="control-label">Nombre</label>
                                                    <input type="text" name="txtNombre" class="form-control">
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label class="control-label">Sitio Web</label>
                                                    <input type="text" name="txtSitioWeb" class="form-control">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">Mensajeria</label>
                                            <select name="cbMensajeria" class="form-control">
                                                <option value="">Seleccione..</option>
                                                <option value="1">SI</option>
                                                <option value="0">NO</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">Organización Jurídica</label>
                                            <select name="cbOrgJuridica" class="form-control">
                                                <option value="">Seleccione..</option>
                                                @foreach ($organizaciones as $org)
                                                    <option value="{{$org->org_jur_id}}">{{$org->org_jur_descripcion}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">Sector Productivo</label>
                                            <select name="cbSectorProd" class="form-control">
                                                <option value="">Seleccione..</option>
                                                @foreach ($sectores as $sector)
                                                    <option value="{{$sector->sec_prod_id}}">{{$sector->sec_prod_descripcion}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">Jornada de Trabajo</label>
                                            <select name="cbJornada" class="form-control">
                                                <option value="">Seleccione..</option>
                                                @foreach ($jornadas as $jornada)
                                                    <option value="{{$jornada->jorn_trab_id}}">{{$jornada->jorn_trab_descripcion}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">No. de Empleados</label>
                                            <input type="text" name="txtNoEmpleados" id="txtNoEmpleados" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">Objeto Social</label>
                                            <textarea class="form-control" name="txtObjSocial"></textarea>
                                        </div>
                                    </div>
                                </div>
                                <hr>
                                
                            </form>
                            <div class="row">
                                <div class="col-md-12 text-center">
                                    <button type="button" class="btn btn-primary" onclick="guardarEmpresas()">Guardar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script type="text/javascript" src="{{ asset('js/registroEmpresas.js') }}"></script>
@endsection