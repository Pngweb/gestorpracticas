@extends('layouts.site')

@section('title')
    Convenios
@endsection

@section('content')
<div id="page-title">
    <h2>Convenios</h2>
</div>
<div class="panel">
    <div class="panel-body">
        <ul class="nav-responsive nav nav-tabs">
            <li class="active"><a href="#tabConvenios" data-toggle="tab">Convenios</a></li>
            <li><a href="#tabEmpDisponibles" data-toggle="tab">Empresas Disponibles</a></li>
        </ul>
        <input type="hidden" id="hdInstitucion" value="{{ Auth::User()->tercero_id }}">
        <div class="tab-content">
            <div class="tab-pane active" id="tabConvenios">
                 {{-- tabla convenios--}}
                 <div class="example-box-wrapper">

                    <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="tbConvenios">
                    <thead>
                    <tr>
                        <th>Logo</th>
                        <th>Nit</th>
                        <th>Nombre</th>
                        <th>Sitio Web</th>
                        <th>Sector Productivo</th>
                        <th>Estado</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    </tbody>
                    </table>
                </div>
                {{-- end tabla convenios --}}
            </div>
            <div class="tab-pane" id="tabEmpDisponibles">
                {{-- tabla convenios--}}
                 <div class="example-box-wrapper">

                    <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="tbDisponibles">
                    <thead>
                    <tr>
                        <th>Logo</th>
                        <th>Nit</th>
                        <th>Nombre</th>
                        <th>Sitio Web</th>
                        <th>Sector Productivo</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    </tbody>
                    </table>
                </div>
                {{-- end tabla convenios --}}
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
    <script type="text/javascript" src="{{ asset('template/widgets/datatable/datatable.js') }}"></script>
    <script type="text/javascript" src="{{ asset('template/widgets/datatable/datatable-bootstrap.js') }}"></script>
    <script type="text/javascript" src="{{ asset('template/widgets/datatable/datatable-tabletools.js') }}"></script>
    <script type="text/javascript" src="{{ asset('template/widgets/jgrowl-notifications/jgrowl.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/convenios.js') }}"></script>
@endsection