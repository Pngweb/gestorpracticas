@extends('layouts.site')

@section('title')
    Home
@endsection

@section('content')
<div id="page-title">
    <h2>Dashboard</h2>
</div>
<div class="panel">
    <div class="panel-body">
        <div class="row">
        	<div class="col-md-4">
                    <div class="tile-box bg-white content-box">
                        <div class="tile-header">
                            Instituciones
                        </div>
                        <div class="tile-content-wrapper">
                            <i class="glyph-icon icon-bank" style="font-size: 50px;"></i>
                            <div class="tile-content">
                                50
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="tile-box bg-white content-box">
                        <div class="tile-header">
                            Empresas
                        </div>
                        <div class="tile-content-wrapper">
                            <i class="glyph-icon icon-building" style="font-size: 50px;"></i>
                            <div class="tile-content">
                                120
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="tile-box bg-white content-box">
                        <div class="tile-header">
                            Estudiantes
                        </div>
                        <div class="tile-content-wrapper">
                            <i class="glyph-icon icon-group" style="font-size: 50px;"></i>
                            <div class="tile-content">
                                400
                            </div>
                        </div>
                    </div>
                </div>
        </div>
    </div>
</div>
@endsection
