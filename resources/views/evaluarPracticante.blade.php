@extends('layouts.site')

@section('title')
    Evaluar Practicante
@endsection

@section('content')
<div id="page-title"  style="display: flex; margin-bottom: 10px;">
    <h2>EVALUACIÓN DEL PRACTICANTE</h2>
     <a id="back" style="margin-bottom: auto;margin-top: auto;margin-left: auto;font-size: 1.2em;text-decoration: none;color: black;cursor: pointer;"> 
            <i class="glyph-icon icon-arrow-left"></i>
        Volver
    </a>
</div>
<div class="panel">
    <div class="panel-body">
       
        <hr>
        {{-- tabla --}}
        <div class="example-box-wrapper">
            <input type="hidden" id="hdEmpresa" value="{{ Auth::user()->tercero_id }}">
            <table class="table">
            <thead>
            <tr>
                <th>Información del Practicante a Evaluar</th>
                <th><input type="text" name="cedula" class="form-control"></th>
                <th><button class="btn btn-small btn-default">Buscar</button></th>
            </tr>
            </thead>
            <tbody>
                <tr>
                    <td>Lorem ipsum dolor sit amet, consectetur adipisicing elit
                    </td>
                    <td>Lorem ipsum dolor sit amet, consectetur adipisicing elit</td>
                    <td></td>
                  
                </tr>
                <tr>
                    <td>Lorem ipsum dolor sit amet, consectetur adipisicing elit</td>
                    <td>Lorem ipsum dolor sit amet, consectetur adipisicing elit</td>
                    <td></td>
                  
                </tr>
                <tr>
                    <td>Lorem ipsum dolor sit amet, consectetur adipisicing elit</td>
                    <td>Lorem ipsum dolor sit amet, consectetur adipisicing elit</td>
                    <td></td>
                  
                </tr>
                <tr>
                    <td>Lorem ipsum dolor sit amet, consectetur adipisicing elit</td>
                    <td>Lorem ipsum dolor sit amet, consectetur adipisicing elit</td>
                    <td></td>
                  
                </tr>
            </tbody>
            </table>
        </div>
        {{-- end tabla --}}

        {{-- tabla --}}
        <h3 class="text-leftfont-primary font-bold">Información Técnica</h3><br>
        <div class="example-box-wrapper">
            <input type="hidden" id="hdEmpresa" value="{{ Auth::user()->tercero_id }}">
            <table class="table">
            <thead>
            <tr>
                <th>Compentencia a evaluar</th>
                <th></th>
                <th></th>
            </tr>
            </thead>
            <tbody>
                <tr>
                    <td>Lorem ipsum dolor sit amet, consectetur adipisicing elit
                    </td>
                    <td>Lorem ipsum dolor sit amet, consectetur adipisicing elit</td>
                    <td></td>
                  
                </tr>
                <tr>
                    <td>Lorem ipsum dolor sit amet, consectetur adipisicing elit</td>
                    <td>Lorem ipsum dolor sit amet, consectetur adipisicing elit</td>
                    <td></td>
                  
                </tr>
                <tr>
                    <td>Lorem ipsum dolor sit amet, consectetur adipisicing elit</td>
                    <td>Lorem ipsum dolor sit amet, consectetur adipisicing elit</td>
                    <td></td>
                  
                </tr>
                <tr>
                    <td>Lorem ipsum dolor sit amet, consectetur adipisicing elit</td>
                    <td>Lorem ipsum dolor sit amet, consectetur adipisicing elit</td>
                    <td></td>
                  
                </tr>
            </tbody>
            </table>
        </div>
        {{-- end tabla --}}

        {{-- tabla --}}
        <h3 class="text-leftfont-primary font-bold">Información Humana</h3><br>
        <div class="example-box-wrapper">
            <input type="hidden" id="hdEmpresa" value="{{ Auth::user()->tercero_id }}">
            <table class="table">
            <thead>
            <tr>
                <th>Compentencia a evaluar</th>
                <th></th>
                <th></th>
            </tr>
            </thead>
            <tbody>
                <tr>
                    <td>Lorem ipsum dolor sit amet, consectetur adipisicing elit
                    </td>
                    <td>Lorem ipsum dolor sit amet, consectetur adipisicing elit</td>
                    <td></td>
                  
                </tr>
                <tr>
                    <td>Lorem ipsum dolor sit amet, consectetur adipisicing elit</td>
                    <td>Lorem ipsum dolor sit amet, consectetur adipisicing elit</td>
                    <td></td>
                  
                </tr>
                <tr>
                    <td>Lorem ipsum dolor sit amet, consectetur adipisicing elit</td>
                    <td>Lorem ipsum dolor sit amet, consectetur adipisicing elit</td>
                    <td></td>
                  
                </tr>
                <tr>
                    <td>Lorem ipsum dolor sit amet, consectetur adipisicing elit</td>
                    <td>Lorem ipsum dolor sit amet, consectetur adipisicing elit</td>
                    <td></td>
                  
                </tr>
            </tbody>
            </table>
        </div>
        {{-- end tabla --}}

        {{-- tabla --}}
        <h3 class="text-leftfont-primary font-bold">Información laboral</h3><br>
        <div class="example-box-wrapper">
            <input type="hidden" id="hdEmpresa" value="{{ Auth::user()->tercero_id }}">
            <table class="table">
            <thead>
            <tr>
                <th>Compentencia a evaluar</th>
                <th></th>
                <th></th>
            </tr>
            </thead>
            <tbody>
                <tr>
                    <td>Lorem ipsum dolor sit amet, consectetur adipisicing elit
                    </td>
                    <td>Lorem ipsum dolor sit amet, consectetur adipisicing elit</td>
                    <td></td>
                  
                </tr>
                <tr>
                    <td>Lorem ipsum dolor sit amet, consectetur adipisicing elit</td>
                    <td>Lorem ipsum dolor sit amet, consectetur adipisicing elit</td>
                    <td></td>
                  
                </tr>
                <tr>
                    <td>Lorem ipsum dolor sit amet, consectetur adipisicing elit</td>
                    <td>Lorem ipsum dolor sit amet, consectetur adipisicing elit</td>
                    <td></td>
                  
                </tr>
                <tr>
                    <td>Lorem ipsum dolor sit amet, consectetur adipisicing elit</td>
                    <td>Lorem ipsum dolor sit amet, consectetur adipisicing elit</td>
                    <td></td>
                  
                </tr>
            </tbody>
            </table>
        </div>
        {{-- end tabla --}}

        <div class="row">
            <div class="col-md-6">
                <div class="form-gropu">
                    <label>Observaciones:</label>
                    <textarea class="form-control"></textarea>
                </div>
            </div>
            <div class="col-md-6 text-center"> 
                <button class="btn btn-small btn-primary">Guardar evaluación</button>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
    {{-- <script type="text/javascript" src="{{ asset('template/widgets/datatable/datatable.js') }}"></script>
    <script type="text/javascript" src="{{ asset('template/widgets/accordion-ui/accordion.js') }}"></script>
    <script type="text/javascript" src="{{ asset('template/widgets/datatable/datatable-bootstrap.js') }}"></script>
    <script type="text/javascript" src="{{ asset('template/widgets/datatable/datatable-tabletools.js') }}"></script>
    <script type="text/javascript" src="{{ asset('template/widgets/jgrowl-notifications/jgrowl.js') }}"></script>
    <script type="text/javascript" src="{{ asset('template/widgets/datepicker/datepicker.js') }}"></script>
    --}}
    <script type="text/javascript">
        $(document).ready(function() {
            $('#back').click(function(){
                 window.history.back();
            });
        })
    </script>
@endsection
