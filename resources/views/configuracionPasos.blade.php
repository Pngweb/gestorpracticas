@extends('layouts.site')

@section('title')
    Configuración de Pasos
@endsection

@section('content')
<div id="page-title">
    <h2>Configuración de Pasos</h2>
</div>
<div class="panel">
    <div class="panel-body">
    	<div class="row">
            <div class="col-md-12 mrg20B text-right">
                <button class="btn btn-alt btn-hover btn-primary" data-target="#mdlCrear" onclick="abrirModal(this)">
                        <span>Nuevo</span>
                        <i class="glyph-icon icon-plus"></i>
                </button>
            </div>
        </div>
        {{-- Modal Creacion --}}
        <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="mdlCrear">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Nueva Configuración</h4>
                    </div>
                    <div class="modal-body">
                        <form action="{{ route('configuracionPasos.store') }}" id="frmCrearPasos">
                            @csrf
                            <div class="row">
                                <div class="col-md-6">
                                    <input type="hidden" name="CRid_dependencias" id="CRid_dependencias">
                                    <div class="form-group">
                                        <label>Nombre del Paso</label>
                                        <input type="text" name="txtNombre" class="form-control" autocomplete="off">
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Tipo</label>
                                        <select name="tipo" class="form-control" id="tipo">
                                            <option value="">Seleccione una Opción</option>
                                            <option value="Presencial">Presencial</option>
                                            <option value="Virtual">Virtual</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Tiempo (días)</label>
                                        <input type="number" name="tiempo" min="1" class="form-control" id="tiempo">
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Orden</label>
                                        <input type="number" name="orden" min="1" class="form-control" id="orden">
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Responsable</label>
                                        <select name="responsable" class="form-control" id="responsable">
                                            <option value="">Seleccione una Opción</option>
                                             @foreach($funcionarios as $funcionario)
                                             <option value="{{ $funcionario->id }}">{{ $funcionario->user_nombre }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Descripción</label>
                                        <textarea class="form-control" name="descripcion" id="descripcion"></textarea>
                                    </div>
                                </div>

                                 <div class="col-md-2">
                                    <div class="form-group">
                                        <label class="control-label">Estado</label>
                                        <input type="checkbox" checked name="chkEstado" id="chkEstado" class="input-switch-alt">
                                    </div>
                                </div>

                            </div>

                            <h5 style="font-weight: bold;">Aplicar a dependencias</h5>
                            <div class="row" id="depe">
                                
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        <button type="button" class="btn btn-primary" onclick="$('#frmCrearPasos').submit()">Guardar</button>
                    </div>
                </div>
            </div>
        </div>
        {{-- Modal Creacion --}}

        {{-- Modal editar --}}
        <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="mdlEditar">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Editar Dependencia</h4>
                    </div>
                    <div class="modal-body">
                        <form action="{{ route('configuracionPasos.update','') }}" id="frmConfiguracionPasos">
                            @csrf
                            {{method_field('PUT')}}
                            <input type="hidden" name="id_dependencias" id="id_dependencias">
                            <input type="hidden" name="idconfiguracion" id="idconfiguracion">
                             <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Nombre del Paso</label>
                                        <input type="text" name="ACtxtNombre" id="ACtxtNombre" class="form-control" autocomplete="off">
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Tipo</label>
                                        <select name="ACtipo" class="form-control" id="ACtipo">
                                            <option value="">Seleccione una Opción</option>
                                            <option value="Presencial">Presencial</option>
                                            <option value="Virtual">Virtual</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Tiempo</label>
                                        <input type="number" name="ACtiempo" min="1" class="form-control" id="ACtiempo">
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Orden</label>
                                        <input type="number" name="ACorden" min="1" class="form-control" id="ACorden">
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Responsable</label>
                                        <select name="ACresponsable" class="form-control" id="ACresponsable">
                                            <option value="">Seleccione una Opción</option>
                                             @foreach($funcionarios as $funcionario)
                                             <option value="{{ $funcionario->id }}">{{ $funcionario->user_nombre }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Descripción</label>
                                        <textarea class="form-control" name="ACdescripcion" id="ACdescripcion"></textarea>
                                    </div>
                                </div>

                                 <div class="col-md-2">
                                    <div class="form-group">
                                        <label class="control-label">Estado</label>
                                        <input type="checkbox" name="ACchkEstado" id="ACchkEstado" class="input-switch-alt">
                                    </div>
                                </div>

                            </div>

                            <h5 style="font-weight: bold;">Aplicar a dependencias</h5>
                            <div class="row" id="dependencias">
                                
                            </div>
                               
                            
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        <button type="button" class="btn btn-primary" onclick="$('#frmConfiguracionPasos').submit()">Guardar</button>
                    </div>
                </div>
            </div>
        </div>
        {{-- Modal editar --}}

        {{-- tabla --}}
        <div class="example-box-wrapper">
            <input type="hidden" id="hdEmpresa" value="{{ Auth::user()->tercero_id }}">
            <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="datatable-example">
            <thead>
            <tr>
                <th>Orden</th>
                <th>Pasos Configurados</th>
                <th>Estado</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            
            </tbody>
            </table>
        </div>
        {{-- end tabla --}}
    </div>
</div>
@endsection

@section('scripts')
    <script type="text/javascript" src="{{ asset('template/widgets/datatable/datatable.js') }}"></script>
    <script type="text/javascript" src="{{ asset('template/widgets/datatable/datatable-bootstrap.js') }}"></script>
    <script type="text/javascript" src="{{ asset('template/widgets/datatable/datatable-tabletools.js') }}"></script>
    <script type="text/javascript" src="{{ asset('template/widgets/jgrowl-notifications/jgrowl.js') }}"></script>
    <script type="text/javascript" src="{{ asset('template/widgets/datepicker/datepicker.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/configuracionPasos.js') }}"></script>
@endsection
