@extends('layouts.site')

@section('title')
    Cupos de Practicas
@endsection

@section('content')
<div id="page-title">
    <h2>Cupos de Practicas</h2>
</div>
<div class="panel">
    <div class="panel-body">
        @include('layouts.partials.lineaProceso')
    	<div class="row">
            <div class="col-md-12 mrg20B text-right">
                <button class="btn btn-alt btn-hover btn-primary" data-target="#mdlImportar" onclick="abrirModal(this)">
                    <span>Excel</span>
                    <i class="glyph-icon icon-file-excel-o"></i>
                </button>
                <button class="btn btn-alt btn-hover btn-primary" data-target="#mdlCrear" onclick="abrirModal(this)">
                        <span>Nuevo</span>
                        <i class="glyph-icon icon-plus"></i>
                </button>
            </div>
        </div>

        {{-- Modal Excel --}}
        <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="mdlImportar">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Importar Cupos</h4>
                    </div>
                    <div class="modal-body">
                        <form action="{{ route('cupos-practicas.importar') }}" id="frmExcel">
                            @csrf
                            <div class="row">
                                <div class="col-md-4 col-md-offset-2">
                                    <div class="form-group">
                                        <label class="control-label">Subir Archivo</label>
                                        <input type="file" name="excelCupos" class="form-control" accept=".xls,.xlsx">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <a href="{{ asset('documentos/plantilla_cupos_practicas.xlsx') }}" target="_blank"><button type="button" class="btn btn-primary mrg20T">Descargar Formato</button></a>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        <button type="button" class="btn btn-primary" onclick="$('#frmExcel').submit()">Guardar</button>
                    </div>
                </div>
            </div>
        </div>
        {{-- Modal Excel --}}

        {{-- Modal Creacion --}}
        <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="mdlCrear">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Nuevo Cupo</h4>
                    </div>
                    <div class="modal-body">
                        <form action="{{ route('cupos-practicas.store') }}" id="frmCrearCupo">
                            @csrf
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Dependencia</label>
                                        <select name="cdDependencia" class="form-control">
                                            <option value="">Seleccione..</option>
                                            @foreach ($dependencias_combo as $dependencia)
                                                <option value="{{ $dependencia->dependencia_id }}">{{ $dependencia->dependencia_descripcion }}</option>}
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Sucursal</label>
                                        <select name="cbSucursal" class="form-control">
                                            <option value="">Seleccione..</option>
                                            @foreach ($sucursales_combo as $sucursal)
                                                <option value="{{ $sucursal->sucursal_id }}">{{ $sucursal->sucursal_descripcion }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Cargo</label>
                                        <select name="cbCargo" class="form-control">
                                            <option value="">Seleccione..</option>
                                            @foreach ($cargos as $cargo)
                                                <option value="{{ $cargo->cargo_id }}">{{ $cargo->cargo_descripcion }}</option>}
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Maestro Guia</label>
                                        <select name="cbMaestro" class="form-control">
                                            <option value="">Seleccione..</option>
                                            @foreach ($maestros as $maestro)
                                                <option value="{{ $maestro->id }}">{{ $maestro->user_nombre }}</option>}
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                 <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Link de monitoreo</label>
                                        <input type="text" class="form-control" name="txtMonitoreo">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Competencias</label>
                                        <textarea class="form-control" name="txtCompetencias"></textarea>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        <button type="button" class="btn btn-primary" onclick="$('#frmCrearCupo').submit()">Guardar</button>
                    </div>
                </div>
            </div>
        </div>
        {{-- Modal Creacion --}}

        {{-- Modal Editar --}}
        <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="mdlEditar">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Editar Cupo</h4>
                    </div>
                    <div class="modal-body">
                        <form action="{{ route('cupos-practicas.update','') }}" id="frmEditarCupo">
                            @csrf
                            {{method_field('PUT')}}
                            <input type="hidden" name="hdCupoId" id="hdCupoId">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Dependencia</label>
                                        <select name="cdDependencia" id="cdDependenciaEdit" class="form-control">
                                            <option value="">Seleccione..</option>
                                            @foreach ($dependencias_combo as $dependencia)
                                                <option value="{{ $dependencia->dependencia_id }}">{{ $dependencia->dependencia_descripcion }}</option>}
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Sucursal</label>
                                        <select name="cbSucursal" id="cbSucursalEdit" class="form-control">
                                            <option value="">Seleccione..</option>
                                            @foreach ($sucursales_combo as $sucursal)
                                                <option value="{{ $sucursal->sucursal_id }}">{{ $sucursal->sucursal_descripcion }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Cargo</label>
                                        <select name="cbCargo" id="cbCargoEdit" class="form-control">
                                            <option value="">Seleccione..</option>
                                            @foreach ($cargos as $cargo)
                                                <option value="{{ $cargo->cargo_id }}">{{ $cargo->cargo_descripcion }}</option>}
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Maestro Guia</label>
                                        <select name="cbMaestro" id="cbMaestroEdit" class="form-control">
                                            <option value="">Seleccione..</option>
                                            @foreach ($maestros as $maestro)
                                                <option value="{{ $maestro->id }}">{{ $maestro->user_nombre }}</option>}
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Link de monitoreo</label>
                                        <input type="text" class="form-control" name="txtMonitoreo" id="txtMonitoreoEdit">
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Competencias</label>
                                        <textarea class="form-control" name="txtCompetencias" id="txtCompetenciasEdit"></textarea>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        <button type="button" class="btn btn-primary" onclick="$('#frmEditarCupo').submit()">Guardar</button>
                    </div>
                </div>
            </div>
        </div>
        {{-- Modal Editar --}}

        {{-- Modal Asignación --}}
        <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="mdlAsignar">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Asignar Estudiante</h4>
                    </div>
                    <div class="modal-body">
                        <form action="{{ route('asignacion.store') }}" id="frmAsignar">
                            @csrf

                            <div class="row">
                                <div class="col-md-12">
                                    <input type="hidden" name="idcupo" id="idcupo">
                                    <div class="form-group">
                                        <label>Nombre completo</label>
                                        <input type="text" class="form-control" name="nombres" id="nombres">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Fecha de ingreso</label>
                                       <input type="text" class="form-control" name="fechaingreso" id="fechaingreso" >
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Fecha de salida</label>
                                        <input type="text" class="form-control" name="fechasalida" id="fechasalida" >
                                    </div>
                                </div>
                            </div>
                            
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        <button type="button" class="btn btn-primary" id="btnguardar" onclick="$('#frmAsignar').submit()">Guardar</button>
                    </div>
                </div>
            </div>
        </div>
        {{-- Modal Asignación --}}
        
        @if ($no_empleados > 15)
            <div class="alert alert-warning" id="notification">
                <div class="bg-yellow alert-icon" id="cerrar" style="cursor: pointer;">
                    <i class="glyph-icon icon-times"></i>
                </div>
                <div class="alert-content" >
                    <h4 class="alert-title">Información a tener en cuenta</h4>
                    <ul style="list-style: none; padding-left: 0;">
                        <li>
                        </i>De acuerdo al número de empleados de su empresa, usted debe tener al menos (1) aprendiez vinculado a su empresa.</strong>
                        </li>
                    </ul>
                </div>
            </div>
        @endif

        <form id="frmFiltrarCupos">
            @csrf
            <div class="row">
                <div class="col-md-3">
                    <div class="form-group">
                        <label>Dependencia</label>
                        <select name="cdDependencia" class="form-control" id="fltDependencia">
                            <option value="">Seleccione..</option>
                            @foreach ($dependencias_combo as $dependencia)
                                <option value="{{ $dependencia->dependencia_id }}">{{ $dependencia->dependencia_descripcion }}</option>}
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label>Sucursal</label>
                        <select name="cbSucursal" class="form-control" id="fltSucursal">
                            <option value="">Seleccione..</option>
                            @foreach ($sucursales_combo as $sucursal)
                                <option value="{{ $sucursal->sucursal_id }}">{{ $sucursal->sucursal_descripcion }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label>Cargo</label>
                        <select name="cbCargo" class="form-control" id="fltCargo">
                            <option value="">Seleccione..</option>
                            @foreach ($cargos as $cargo)
                                <option value="{{ $cargo->cargo_id }}">{{ $cargo->cargo_descripcion }}</option>}
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label>Maestro Guia</label>
                        <select name="cbMaestro" class="form-control" id="fltMaestro">
                            <option value="">Seleccione..</option>
                            @foreach ($maestros as $maestro)
                                <option value="{{ $maestro->id }}">{{ $maestro->user_nombre }}</option>}
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 text-right">
                    <button class="btn btn-alt btn-primary" id="btnFiltro" type="submit">
                        <span>Filtrar</span>
                        <i class="glyph-icon icon-filter icon-spin"></i>
                    </button>
                </div>
            </div>
        </form>

        <hr>

        {{-- tabla --}}
        <div class="example-box-wrapper">
            <input type="hidden" id="hdEmpresa" value="{{ Auth::user()->tercero_id }}">
            <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="datatable-example">
            <thead>
            <tr>
                <th>Dependencia</th>
                <th>Sucursal</th>
                <th>Cargo</th>
                <th>Maestro</th>
                <th>Competencias</th>
                <th>Estado</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            
            </tbody>
            </table>
        </div>
        {{-- end tabla --}}
    </div>
</div>
@endsection

@section('scripts')
 <!-- Importaciones para validar fechas -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.3/js/bootstrapValidator.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/3.1.3/js/bootstrap-datetimepicker.min.js"></script>
    
    <script type="text/javascript" src="{{ asset('template/widgets/datatable/datatable.js') }}"></script>
    <script type="text/javascript" src="{{ asset('template/widgets/datatable/datatable-bootstrap.js') }}"></script>
    <script type="text/javascript" src="{{ asset('template/widgets/datatable/datatable-tabletools.js') }}"></script>
    <script type="text/javascript" src="{{ asset('template/widgets/jgrowl-notifications/jgrowl.js') }}"></script>
    <script type="text/javascript" src="{{ asset('template/widgets/datepicker/datepicker.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/cuposPracticas.js') }}"></script>


@endsection
