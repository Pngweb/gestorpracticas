@extends('layouts.app')

@section('title')
    Registro Estudiantes
@endsection

@section('content')
    <div class="container">
        <div class="row mrg50T">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel">
                    <div class="panel-body">
                        <h3 class="title-hero text-center">
                            Registro de Estudiantes
                        </h3>
                        <hr>
                        <form>
                            <div class="row">
                                <div class="col-md-4">
                                    <label class="control-label">Foto</label>
                                    <div class="form-group text-center">
                                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                                <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 200px; height: 150px;"></div>
                                                <div class="text-center">
                                                    <span class="btn btn-default btn-file">
                                                        <span class="fileinput-new">Seleccionar Imagen</span>
                                                        <span class="fileinput-exists">Change</span>
                                                        <input type="file" name="...">
                                                    </span>
                                                    <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                                                </div>
                                            </div>
                                        </div>
                                </div>
                                <div class="col-md-8">
                                    <div class="row">
                                        <div class="col-md-8">
                                            <div class="form-group">
                                                <label class="control-label">Identificación</label>
                                                <input type="text" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label">Nombres</label>
                                                <input type="text" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label">Apellidos</label>
                                                <input type="text" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label">Sexo</label>
                                                <select class="form-control">
                                                    <option value="">Seleccione..</option>
                                                    <option value="M">M</option>
                                                    <option value="F">F</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label">Grupo Sanguineo</label>
                                                <select class="form-control">
                                                    <option value="">Seleccione..</option>
                                                    <option value="">A+</option>
                                                    <option value="">A-</option>
                                                    <option value="">B+</option>
                                                    <option value="">B-</option>
                                                    <option value="">AB+</option>
                                                    <option value="">AB-</option>
                                                    <option value="">O+</option>
                                                    <option value="">O-</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Dirección</label>
                                        <input type="text" class="form-control">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Departamento</label>
                                        <select class="form-control">
                                            <option value="">Seleccione..</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Ciudad</label>
                                        <select class="form-control">
                                            <option value="">Seleccione..</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Telefono</label>
                                        <input type="text" class="form-control">
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <label class="control-label">Correo Electrónico</label>
                                        <input type="text" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <hr>
                            
                        </form>
                        <div class="row">
                            <div class="col-md-12 text-center">
                                <button type="button" class="btn btn-primary" onclick="guardarEstudiante()">Guardar</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script type="text/javascript" src="{{ asset('js/registroEstudiantes.js') }}"></script>
@endsection