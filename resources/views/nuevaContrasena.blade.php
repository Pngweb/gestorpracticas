@extends('layouts.app')

@section('title')
    Practicas Inca
@endsection

@section('content')
<div class="center-vertical" style="background-color: #358fc0;">
    <div class="center-content row">
        <form id="reset-password" class="col-md-4 col-sm-5 col-xs-11 col-lg-3 center-margin" method="POST" action="{{ route('login') }}">
            @csrf
            <div id="login-form" class="content-box bg-default password">
                
                <span class="input-group-addon addon-inside bg-gray" title="Volver" style="cursor: pointer;" id="volver">
                    <i class="glyph-icon icon-arrow-left" style="font-size: 20px;"></i>
                </span>
                <input type="hidden" name="hdUsuario" value="{{ $id }}">
                <div class="content-box-wrapper pad20A">
                <img class="mrg25B center-margin display-block" src="{{ asset('images/Logo-práctica-gestor-1.png') }}" alt="">
                    <div class="form-group">
                        <div class="input-group">
                            <span class="input-group-addon addon-inside bg-gray">
                                <i class="glyph-icon icon-user"></i>
                            </span>
                            <input type="password" class="form-control" name="password" id="password" value="{{ old('identificacion') }}" placeholder="Digite la nueva Contraseña" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-group">
                            <span class="input-group-addon addon-inside bg-gray">
                                <i class="glyph-icon icon-unlock-alt"></i>
                            </span>
                            <input type="password" class="form-control" name="cPassword" id="cPassword" placeholder="Confirme la nueva Contraseña" required>
                        </div>
                    </div>
                    <div class="form-group" style="margin-top: 15px;">
                        <button type="submit" class="btn btn-block btn-primary">Reestablecer Contraseña</button>
                    </div>
                </div>
            </div>

            <!--  -->
        </form>

    
    </div>
    <div class="center-contect row" style="display: table-footer-group;">
        <div class="col-md-8 col-sm-5 col-xs-11 center-margin">
            <h6 class="text-center pad25B pad25T pad25L pad25R font-white text-transform-upr font-size-21">
            SU PRIVACIDAD Y CONFIANZA SON MUY IMPORTANTES PARA NOSOTROS. POR ELLO, QUEREMOS ASEGURARNOS QUE CONOZCA CÓMO SALVAGUARDAMOS LA INTEGRIDAD, CONFIDENCIALIDAD Y DISPONIBILIDAD, DE SUS DATOS PERSONALES. AVATAR TECHNOLOGY S.A.S 
            </h6>
            <div class="row pad25B pad25L pad25R text-center">
                <div class="col-md-3"></div>

                <div class="col-md-2" >
                    <a href="https://web.whatsapp.com/" class="btn btn-round btn-success">
                        <i class="fab fa-whatsapp"></i>
                    </a>
                    <p class="font-white center-margin">3003508928</p>
                   
                </div>
                <div class="col-md-2">
                    <button class="btn btn-round btn-warning">
                        <i class="fas fa-comment-dots"></i>
                    </button>
                    <p class="font-white center-margin">PQR</p>
                </div> 
                <div class="col-md-2">
                    <button class="btn btn-round btn-default">
                        <i class="fas fa-headset"></i>
                    </button>
                    <p class="font-white center-margin">SOPORTE TÉCNICO</p>
                </div> 
                <div class="col-md-3"></div>

            </div>
       
        </div>
    </div>
</div>
@endsection

@section('scripts')
    <script type="text/javascript" src="{{ asset('js/login.js') }}"></script>
@endsection
