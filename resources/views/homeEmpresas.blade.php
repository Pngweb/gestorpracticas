@extends('layouts.site')

@section('title')
    Home
@endsection

@section('content')
<?php 
    $user = Auth::user();
    if($user->rol_id == 1){
        $session = array('name'=>$user->tercero->adm_nombre,'imagen'=> null, 'nit' => $user->tercero->adm_identificacion);
    }elseif($user->rol_id == 2){
        $session = array('name'=>$user->tercero->institucion_nombre,'imagen' => $user->tercero->institucion_logo,'folder' => 'images_institucion', 'nit' => $user->tercero->institucion_identificacion);
    }elseif($user->rol_id == 3){
        $session = array('name'=>$user->tercero->empresa_nombre,'imagen' => $user->tercero->empresa_logo, 'folder' => 'images_empresas','nit' => $user->tercero->empresa_nit);
    }else{
        $session = array('name'=>$user->user_nombre,'imagen' => $user->tercero->estudiante_foto, 'folder' => 'images_estudiantes', 'nit' => $user->tercero->estudiante_identificacion);
    }
?>
<div id="page-title">
    <h2>Bienvenido(a), {{ $user->user_nombre }}</h2>
    <input type="hidden" id="hdEmpresa" value="{{ Auth::user()->tercero_id }}">
</div>
<div class="panel">
    {{-- {{ $val_mensajeria }} - {{ $mensajeria }} - {{ $dependencias }} - {{ $sucursales }} --}}
    <div class="panel-body">

        @include('layouts.partials.lineaProceso')

        @if ($mensajeria == 0 || $dependencias == 0 || $sucursales == 0 || $cupos == 0)
        {{-- @if(false) --}}
        <div class="row">

            <div class="col-md-12">
                <div class="alert alert-danger">
                        <div class="bg-red alert-icon">
                            <i class="glyph-icon icon-times"></i>
                        </div>
                        <div class="alert-content">
                            <h4 class="alert-title">Información Pendiente</h4>
                            <ul style="list-style: none; padding-left: 0;">
                                @if ($val_mensajeria && $mensajeria == 0)
                                    <li><i class="glyph-icon icon-times font-size-16 font-red"></i> Debe registrar por lo menos un <a href="{{ route('mensajeria.index') }}">usuario de mensajeria</a>.</li>
                                @endif
                                @if ($dependencias == 0)
                                    <li><i class="glyph-icon icon-times font-size-16 font-red"></i> Debe registrar por lo menos una <a href="{{ route('dependencias.index') }}">dependencia</a>.</li>
                                @endif
                                @if ($sucursales == 0)
                                    <li><i class="glyph-icon icon-times font-size-16 font-red"></i> Debe registrar por lo menos una <a href="{{ route('sucursales.index') }}">sucursal</a>.</li>
                                @endif
                                @if ($cupos == 0)
                                    <li><i class="glyph-icon icon-times font-size-16 font-red"></i> Debe configurar por lo menos un <a href="{{ route('cupos-practicas.index') }}">cupo de práctica</a>.</li>
                                @endif
                            </ul>
                        </div>
                    </div>
{{--                 <div class="content-box">
                    <h3 class="content-box-header bg-danger">
                        <span class="icon-separator">
                            <i class="glyph-icon icon-ban"></i>
                        </span>
                        Información Pendiente
                    </h3>
                    <div class="content-box-wrapper pad0A">
                        <table class="table mrg0B">
                            <tbody>
                                @if ($val_mensajeria && $mensajeria == 0)
                                <tr>
                                        <td><i class="glyph-icon icon-times font-size-16 font-red"></i> Debe registrar por lo menos un usuario de mensajeria.</td>
                                </tr>
                                @endif
                                @if ($dependencias == 0)
                                <tr>
                                        <td><i class="glyph-icon icon-times font-size-16 font-red"></i> Debe registrar por lo menos una dependecia.</td>
                                </tr>
                                @endif
                                @if ($sucursales == 0)
                                <tr>
                                        <td><i class="glyph-icon icon-times font-size-16 font-red"></i> Debe registrar por lo menos una sucursal.</td>
                                </tr>
                                @endif
                            </tbody>
                        </table>
                        
                    </div>
                </div> --}}
            </div>
        </div>
        @else
            <div class="alert alert-notice">
                    <div class="bg-blue alert-icon">
                        <i class="glyph-icon icon-info"></i>
                    </div>
                    <div class="alert-content">
                        <h4 class="alert-title">Información Pendiente</h4>
                        <p>Has completado el registro de información pendiente en tu perfil.</p>
                    </div>
                </div>
        @endif
        <div class="row">
            <div id="calendar-example-1" class="col-md-8"></div>
            
            <div class="col-md-4">
                <div class="col-md-12">
                    <div class="col-md-12">
                        <div class="tile-box bg-primary content-box">
                            <div class="tile-header">
                                Convenios
                            </div>
                            <div class="tile-content-wrapper">
                                <i class="glyph-icon icon-bank" style="font-size: 50px;"></i>
                                <div class="tile-content" id="convenios">
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="tile-box bg-primary content-box">
                            <div class="tile-header">
                                Solicitudes Pendientes
                                
                            </div>
                            <div class="tile-content-wrapper">
                                <i class="glyph-icon  icon-bell-o" style="font-size: 50px;"></i>
                                <div class="tile-content" id="solicitudes">
                                    0
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="tile-box bg-primary content-box">
                            <div class="tile-header" >
                                Practicantes
                            </div>
                            <div class="tile-content-wrapper">
                                <i class="glyph-icon icon-group" style="font-size: 50px;"></i>
                                <div class="tile-content" id="practicantes">
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="tile-box bg-primary content-box">
                            <div class="tile-header">
                                Cupos de Practicas
                                <div class="float-right" id="disponibles">
                                    
                                </div>
                            </div>
                            <div class="tile-content-wrapper">
                                <i class="glyph-icon icon-dashboard" style="font-size: 50px;"></i>
                                <div class="tile-content" id="total">
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
        <div class="row" style="margin-top: 20px;">
            <div class="col-md-12">
                <h3 class="title-hero">
                    Practicantes por Instituciones
                </h3>
                <div class="row" id="practicantesinstituciones">
                   <!--  <div class="col-md-6">
                        <div class="tile-box bg-blue-alt">
                            <div class="tile-header text-center">
                                Inca
                            </div>
                            <div class="tile-content-wrapper">
                                <div class="tile-content font-size-20">
                                    18
                                </div>
                                <small>
                                    Practicantes
                                </small>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="tile-box bg-green">
                            <div class="tile-header text-center">
                                Sena
                            </div>
                            <div class="tile-content-wrapper">
                                <div class="tile-content font-size-20">
                                    7
                                </div>
                                <small>
                                    Practicantes
                                </small>
                            </div>
                        </div>
                    </div> -->
                 </div>  
            </div>
        </div>
    </div>
</div>
<hr>
<div id="page-title">
    <h3 style="text-transform: uppercase;">Cupos de Prácticas</h3>
    <p>Cupos de prácticas próximos a vencerse</p>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="panel">
            <div class="panel-body">
                <h3 class="title-hero">
                    Prontas a vencer - Menos de 30 días
                </h3>
                <div class="example-box-wrapper">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Dependencia</th>
                            <th>Sucursal</th>
                            <th>Cargo</th>
                        </tr>
                        </thead>
                        <tbody id="tbvencimiento">
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="panel">
            <div class="panel-body">
                <h3 class="title-hero">
                    Cupos Disponibles
                </h3>
                <div class="example-box-wrapper">
                     <table class="table">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Dependencia</th>
                            <th>Sucursal</th>
                            <th>Cargo</th>
                        </tr>
                        </thead>
                        <tbody id="tbdisponibles">
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="panel">
            <div class="panel-body">
                <h3 class="title-hero">
                    Ocupados por Área
                </h3>
                <div class="example-box-wrapper">
                    <table class="table" >
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Dependencia</th>
                             <th>Sucursal</th>
                            <th>Cantidad</th>
                        </tr>
                        </thead>
                        <tbody id="tbareas">
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="row">
            <div class="col-md-6">
                <a href="{{ route('funcionarios.index') }}" title="Empleados" class="tile-box tile-box-shortcut btn-gray-dark">
                    <span class="bs-badge badge-absolute" id="empleados"></span>
                        <div class="tile-header">
                            Empleados
                        </div>
                    <div class="tile-content-wrapper">
                        <i class="glyph-icon icon-group"></i>
                    </div>
                </a>
            </div>
            <div class="col-md-6">
                <a href="{{ route('solicitud-practicantes.index') }}" title="Solicitudes Pendientes" class="tile-box tile-box-shortcut btn-black">
                    <span class="bs-badge badge-absolute" id="c_solicitudes"></span>
                    <div class="tile-header">
                        Solicitudes Pendientes
                    </div>
                    <div class="tile-content-wrapper">
                        <i class="glyph-icon icon-group"></i>
                    </div>
                </a>
            </div>
        </div>
        <div class="panel mrg20T">
            <div class="panel-body">
                <h3 class="title-hero">
                    Ultimos Cupos Asignados
                </h3>
                <div class="example-box-wrapper">
                    <div class="timeline-box timeline-box-left" id="ultimosCupos">
                        <!-- <div class="tl-row">
                            <div class="tl-item float-right">
                                <div class="tl-icon bg-red">
                                    <i class="glyph-icon icon-info"></i>
                                </div>
                                <div class="popover right">
                                    <div class="arrow"></div>
                                    <div class="popover-content">
                                        <div class="tl-label bs-label label-info">COORDINADOR</div>
                                        <p class="tl-content">Cupo de práctica asigando a <strong>Departamento de sistemas</strong> </p>
                                        <div class="tl-time">
                                            <i class="glyph-icon icon-clock-o"></i>
                                            Lunes 20-jul-2020 
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tl-row">
                            <div class="tl-item float-right">
                                <div class="tl-icon bg-primary">
                                    <i class="glyph-icon icon-info"></i>
                                </div>
                                <div class="popover right">
                                    <div class="arrow"></div>
                                    <div class="popover-content">
                                        <div class="tl-label bs-label bg-yellow">AUX. CONTABLE</div>
                                        <p class="tl-content">Cupo de práctica asigando a <strong>Departamento de Contabilidad</strong> </p>
                                        <div class="tl-time">
                                            <i class="glyph-icon icon-clock-o"></i>
                                            Miercoles 15-jul-2020
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tl-row">
                            <div class="tl-item float-right">
                                <div class="tl-icon bg-black">
                                    <i class="glyph-icon icon-info"></i>
                                </div>
                                <div class="popover right">
                                    <div class="arrow"></div>
                                    <div class="popover-content">
                                        <div class="tl-label bs-label label-danger">DESARROLLADOR</div>
                                        <p class="tl-content">Cupo de práctica asigando a <strong>Departamento de Sistemas</strong> </p>
                                        <div class="tl-time">
                                            <i class="glyph-icon icon-clock-o"></i>
                                            Viernes 10-jul-2020
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
    <script type="text/javascript" src="{{ asset('js/homeEmpresas.js') }}"></script>
    <script type="text/javascript" src="{{ asset ('template/widgets/daterangepicker/moment.js') }}"></script>
    <script type="text/javascript" src="{{ asset('template/widgets/calendar/calendar.js') }}"></script>
    <!-- <script type="text/javascript" src="{{ asset('template/widgets/calendar/calendar-demo.js') }}"></script> -->
@endsection
