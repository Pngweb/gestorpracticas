@extends('layouts.site')

@section('title')
    Empresas
@endsection

@section('content')
<div id="page-title">
    <h2>Empresas</h2>
</div>
<div class="panel">
    <div class="panel-body">
        <div class="row">
            <div class="col-md-12 mrg20B text-right">
                <button class="btn btn-alt btn-hover btn-primary" data-target="#mdlEmail" onclick="abrirModal(this)">
                    <span>Email</span>
                    <i class="glyph-icon icon-envelope"></i>
                </button>
                <button class="btn btn-alt btn-hover btn-primary" data-target="#mdlImportar" onclick="abrirModal(this)">
                    <span>Excel</span>
                    <i class="glyph-icon icon-file-excel-o"></i>
                </button>
                <button class="btn btn-alt btn-hover btn-primary" data-target="#mdlCrear" onclick="abrirModal(this)">
                    <span>Nueva</span>
                    <i class="glyph-icon icon-plus"></i>
                </button>
            </div>
        </div>

        {{-- Modal Email --}}
        <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="mdlEmail">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Invitar Empresas</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <form id="frmInvitacionEmpresas" action="{{ route('InvitacionEmpresas') }}">
                                <div class="col-md-6 col-md-offset-3">
                                    <div class="form-group">
                                        <label class="control-label">Correo Electrónico</label>
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="glyph-icon icon-envelope"></i></span>
                                            <input type="text" id="txtEmailInvitacion" class="form-control" name="txtEmail">
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        <button type="button" class="btn btn-primary" onclick="enviarInvitacion()">Enviar</button>
                    </div>
                </div>
            </div>
        </div>
        {{-- Modal Email --}}

        {{-- Modal Excel --}}
        <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="mdlImportar">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Importar Empresas</h4>
                    </div>
                    <div class="modal-body">
                        <form action="{{ route('importarEmpresas') }}" id="frmExcel">
                            @csrf
                            <div class="row">
                                <div class="col-md-4 col-md-offset-2">
                                    <div class="form-group">
                                        <label class="control-label">Subir Archivo</label>
                                        <input type="file" name="excelEmpresas" class="form-control">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <button type="button" class="btn btn-primary mrg20T">Descargar Formato</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="importarExcel()">Guardar</button>
                    </div>
                </div>
            </div>
        </div>
        {{-- Modal Excel --}}

        {{-- Modal Creacion --}}
        <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="mdlCrear">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Nueva Empresa</h4>
                    </div>
                    <div class="modal-body">
                        <form action="{{ route('empresas.store') }}" id="frmCrearEmpresa">
                            @csrf
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="control-label">Logo</label>
                                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                                <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 200px; height: 150px;">
                                                    <i class="glyph-icon icon-remove hide" id="removeImg" onclick="quitarImagen()"></i>
                                                    <img src="" id="img-logo">
                                                </div>
                                                <div class="text-center">
                                                    <span class="btn btn-default btn-file">
                                                        <span class="fileinput-new">Seleccionar Imagen</span>
                                                        <input type="file" name="logo" id="logo">
                                                    </span>
                                                </div>
                                            </div>
                                    </div>
                                </div>
                                <div class="col-md-9">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="control-label">Nit</label>
                                                <input type="text" name="txtNit" id="txtNit" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-8">
                                            <div class="form-group">
                                                <label class="control-label">Nombre</label>
                                                <input type="text" name="txtNombre" id="txtNombre" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="control-label">Sitio Web</label>
                                                <input type="text" name="txtSitioWeb" id="txtSitioWeb" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="control-label">Mensajería</label>
                                                <select class="form-control" name="cbMensajeria" id="cbMensajeria">
                                                    <option value="">Seleccione..</option>
                                                    <option value="1">SI</option>
                                                    <option value="0">NO</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="control-label">Organización Jurídica</label>
                                                <select class="form-control" name="cbOrgJuridica" id="cbOrgJuridica">
                                                    <option value="">Seleccione..</option>
                                                    @foreach ($organizaciones as $org)
                                                        <option value="{{$org->org_jur_id}}">{{$org->org_jur_descripcion}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="control-label">Sector Productivo</label>
                                                <select class="form-control" name="cbSectorProd" id="cbSectorProd">
                                                    <option value="">Seleccione..</option>
                                                    @foreach ($sectores as $sector)
                                                        <option value="{{$sector->sec_prod_id}}">{{$sector->sec_prod_descripcion}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="control-label">Jornada de Trabajo</label>
                                                <select class="form-control" name="cbJornada" id="cbJornada">
                                                    <option value="">Seleccione..</option>
                                                    @foreach ($jornadas as $jornada)
                                                        <option value="{{$jornada->jorn_trab_id}}">{{$jornada->jorn_trab_descripcion}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="control-label">No. de Empleados</label>
                                                <input type="text" name="txtNoEmpleados" id="txtNoEmpleados" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="control-label">Objeto Social</label>
                                                <textarea class="form-control" name="txtObjSocial" id="txtObjSocial"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        <button type="button" class="btn btn-primary" onclick="guardarEmpresas()">Guardar</button>
                    </div>
                </div>
            </div>
        </div>
        {{-- Modal Creacion --}}

        {{-- Modal Editar --}}
        <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="mdlEditar">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Actualizar Empresa</h4>
                    </div>
                    <div class="modal-body">
                        <form action="{{ route('empresas.update','') }}" id="frmEditarEmpresa">
                            @csrf
                            {{method_field('PUT')}}
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="control-label">Logo</label>
                                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                                <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 200px; height: 150px;">
                                                    <i class="glyph-icon icon-remove hide" id="removeImgEdit" onclick="quitarImagenEdit()"></i>
                                                    <img src="" id="img-logo-edit">
                                                </div>
                                                <div class="text-center">
                                                    <span class="btn btn-default btn-file">
                                                        <span class="fileinput-new">Seleccionar Imagen</span>
                                                        <input type="file" name="logo" id="logoEdit">
                                                        <input type="hidden" name="hdRemImg" id="hdRemImg" value="false">
                                                    </span>
                                                </div>
                                            </div>
                                    </div>
                                </div>
                                <div class="col-md-9">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="control-label">Nit</label>
                                                <input type="text" name="txtNit" id="txtNitEdit" class="form-control">
                                                <input type="hidden" name="hdEmpresa" id="hdEmpresaEdit">
                                            </div>
                                        </div>
                                        <div class="col-md-8">
                                            <div class="form-group">
                                                <label class="control-label">Nombre</label>
                                                <input type="text" name="txtNombre" id="txtNombreEdit" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="control-label">Sitio Web</label>
                                                <input type="text" name="txtSitioWeb" id="txtSitioWebEdit" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="control-label">Mensajería</label>
                                                <select class="form-control" name="cbMensajeria" id="cbMensajeriaEdit">
                                                    <option value="">Seleccione..</option>
                                                    <option value="1">SI</option>
                                                    <option value="0">NO</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="control-label">Organización Jurídica</label>
                                                <select class="form-control" name="cbOrgJuridica" id="cbOrgJuridicaEdit">
                                                    <option value="">Seleccione..</option>
                                                    @foreach ($organizaciones as $org)
                                                        <option value="{{$org->org_jur_id}}">{{$org->org_jur_descripcion}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="control-label">Sector Productivo</label>
                                                <select class="form-control" name="cbSectorProd" id="cbSectorProdEdit">
                                                    <option value="">Seleccione..</option>
                                                    @foreach ($sectores as $sector)
                                                        <option value="{{$sector->sec_prod_id}}">{{$sector->sec_prod_descripcion}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="control-label">Jornada de Trabajo</label>
                                                <select class="form-control" name="cbJornada" id="cbJornadaEdit">
                                                    <option value="">Seleccione..</option>
                                                    @foreach ($jornadas as $jornada)
                                                        <option value="{{$jornada->jorn_trab_id}}">{{$jornada->jorn_trab_descripcion}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="control-label">No. de Empleados</label>
                                                <input type="text" name="txtNoEmpleados" id="txtNoEmpleadosEdit" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="control-label">Objeto Social</label>
                                                <textarea class="form-control" name="txtObjSocial" id="txtObjSocialEdit"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        <button type="button" class="btn btn-primary" onclick="EditarEmpresas()">Guardar</button>
                    </div>
                </div>
            </div>
        </div>
        {{-- Modal Editar --}}

        {{-- Modal Usuarios --}}
        <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="mdlUsuarios">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Usuarios</h4>
                    </div>
                    <div class="modal-body">
                        <form action="{{ route('users.store') }}" id="frmUsuario">
                            @csrf
                            <div class="row">
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <label class="control-label">Nombre</label>
                                        <input type="hidden" name="hdTercero" id="hdTercero" value="0">
                                        <input type="hidden" name="hdRol" id="hdRol" value="3">
                                        <input type="text" name="txtNombreUser" id="txtNombreUser" class="form-control">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label">Cargo</label>
                                        <select class="form-control" name="cbCargo" id="cbCargo">
                                            <option value="">Seleccione..</option>
                                            @foreach ($cargos as $cargo)
                                                <option value="{{$cargo->cargo_id}}">{{$cargo->cargo_descripcion}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label">Identificacion</label>
                                        <input type="text" name="txtIdentificacion" id="txtIdentificacion" class="form-control">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label">Departamento de expedición</label>
                                        <select class="form-control" name="cbDepto" id="cbDepto" onchange="consultarCiudades(this)">
                                            <option value="">Seleccione..</option>
                                            @foreach ($deptos as $depto)
                                                <option value="{{ $depto->depto_id }}">{{ $depto->depto_descripcion }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label">Ciudad de Expedicion</label>
                                        <select class="form-control" name="cbCiudad" id="cbCiudad">
                                            <option value="">Seleccione..</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label">Correo Electronico</label>
                                        <input type="text" name="txtEmailUser" id="txtEmailUser" class="form-control">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label">Telefono</label>
                                        <input type="text" name="txtTelefonoUser" id="txtTelefonoUser" class="form-control">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label">Cumpleaños</label>
                                        <div class="input-prepend input-group">
                                            <span class="add-on input-group-addon">
                                                <i class="glyph-icon icon-calendar"></i>
                                            </span>
                                            <input type="text" class="bootstrap-datepicker form-control" name="txtFechaNac" id="txtFechaNac">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label">Contraseña</label>
                                        <input type="password" name="txtPassword" id="txtPassword" class="form-control">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label">Confirmar Contraseña</label>
                                        <input type="password" name="txtPasswordConfirm" id="txtPasswordConfirm" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 text-right">
                                    <button type="button" class="btn btn-primary" onclick="guardarUsuario()">Guardar</button>
                                </div>
                            </div>
                        </form>
                        <form action="{{ route('users.update','') }}" id="frmEditUsuario" class="hide">
                            @csrf
                            {{method_field('PUT')}}
                            <div class="row">
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <label class="control-label">Nombre</label>
                                        <input type="hidden" name="hdTercero" id="hdTercero" value="0">
                                        <input type="hidden" name="hdRol" id="hdRol" value="3">
                                        <input type="hidden" name="hdUser" id="hdUser">
                                        <input type="text" name="txtNombreUser" id="txtNombreUserEdit" class="form-control">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label">Cargo</label>
                                        <select class="form-control" name="cbCargo" id="cbCargoEdit">
                                            <option value="">Seleccione..</option>
                                            @foreach ($cargos as $cargo)
                                                <option value="{{$cargo->cargo_id}}">{{$cargo->cargo_descripcion}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label">Identificacion</label>
                                        <input type="text" name="txtIdentificacion" id="txtIdentificacionEdit" class="form-control">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label">Departamento de expedición</label>
                                        <select class="form-control" name="cbDepto" id="cbDeptoEdit" onchange="consultarCiudades(this)">
                                            <option value="">Seleccione..</option>
                                            @foreach ($deptos as $depto)
                                                <option value="{{ $depto->depto_id }}">{{ $depto->depto_descripcion }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label">Ciudad de Expedicion</label>
                                        <select class="form-control" name="cbCiudad" id="cbCiudadEdit">
                                            <option value="">Seleccione..</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label">Correo Electronico</label>
                                        <input type="text" name="txtEmailUser" id="txtEmailUserEdit" class="form-control">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label">Telefono</label>
                                        <input type="text" name="txtTelefonoUser" id="txtTelefonoUserEdit" class="form-control">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label">Cumpleaños</label>
                                        <div class="input-prepend input-group">
                                            <span class="add-on input-group-addon">
                                                <i class="glyph-icon icon-calendar"></i>
                                            </span>
                                            <input type="text" class="bootstrap-datepicker form-control" name="txtFechaNac" id="txtFechaNacEdit">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label">Contraseña</label>
                                        <input type="password" name="txtPassword" id="txtPasswordEdit" class="form-control">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label">Confirmar Contraseña</label>
                                        <input type="password" name="txtPasswordConfirm" id="txtPasswordConfirmEdit" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 text-right">
                                    <button type="button" class="btn btn-primary" id="btnGuardarUser" onclick="editarUsuario()">Guardar</button>
                                </div>
                            </div>
                        </form>
                        <hr>
                        <div class="example-box-wrapper">
                            <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="datatable-users">
                            <thead>
                            <tr>
                                <th>Nombre</th>
                                <th>Cargo</th>
                                <th>Identificación</th>
                                <th>Correo Electronico</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    </div>
                </div>
            </div>
        </div>
        {{-- Modal Usuarios --}}

        

        {{-- tabla --}}
        <div class="example-box-wrapper">

            <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="datatable-example">
            <thead>
            <tr>
                <th>Logo</th>
                <th>Nit</th>
                <th>Nombre</th>
                <th>Organizacion Juridica</th>
                <th>Sitio Web</th>
                <th>Sector Productivo</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            </tbody>
            </table>
        </div>
        {{-- end tabla --}}

    </div>
</div>
@endsection

@section('scripts')
    <script type="text/javascript" src="{{ asset('template/widgets/datatable/datatable.js') }}"></script>
    <script type="text/javascript" src="{{ asset('template/widgets/datatable/datatable-bootstrap.js') }}"></script>
    <script type="text/javascript" src="{{ asset('template/widgets/datatable/datatable-tabletools.js') }}"></script>
    <script type="text/javascript" src="{{ asset('template/widgets/datepicker/datepicker.js') }}"></script>
    <script type="text/javascript" src="{{ asset('template/widgets/jgrowl-notifications/jgrowl.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/empresas.js') }}"></script>
@endsection