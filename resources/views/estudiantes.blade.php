@extends('layouts.site')

@section('title')
    Estudiantes
@endsection

@section('content')
<div id="page-title">
    <h2>Estudiantes</h2>
</div>
<div class="panel">
    <div class="panel-body">
        <div class="row">
            <div class="col-md-12 mrg20B text-right">
                <button class="btn btn-alt btn-hover btn-primary" data-target="#mdlEmail" onclick="abrirModal(this)">
                    <span>Email</span>
                    <i class="glyph-icon icon-envelope"></i>
                </button>
                <button class="btn btn-alt btn-hover btn-primary" data-target="#mdlImportar" onclick="abrirModal(this)">
                    <span>Excel</span>
                    <i class="glyph-icon icon-file-excel-o"></i>
                </button>
                <button class="btn btn-alt btn-hover btn-primary" data-target="#mdlCrear" onclick="abrirModal(this)">
                    <span>Nuevo</span>
                    <i class="glyph-icon icon-plus"></i>
                </button>
            </div>
        </div>

        {{-- Modal Email --}}
        <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="mdlEmail">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Invitar Estudiante</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-6 col-md-offset-3">
                                <div class="form-group">
                                    <label class="control-label">Correo Electrónico</label>
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="glyph-icon icon-envelope"></i></span>
                                        <input type="text" id="txtEmailInvitacion" class="form-control">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="enviarInvitacion()">Enviar</button>
                    </div>
                </div>
            </div>
        </div>
        {{-- Modal Email --}}

        {{-- Modal Excel --}}
        <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="mdlImportar">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Importar Estudiantes</h4>
                    </div>
                    <div class="modal-body">
                        <form action="{{ route('importarEstudiantes') }}" id="frmImportarEstudiantes">
                            <div class="row">
                                <div class="col-md-4 col-md-offset-2">
                                            <div class="form-group">
                                                <label class="control-label">Subir Archivo</label>
                                                <input type="file" name="file" class="form-control">
                                            </div>
                                </div>
                                <div class="col-md-3">
                                    <a href="{{ asset('documentos/plantilla_estudiantes.xlsx') }}" target="_blank"><button type="button" class="btn btn-primary mrg20T">Descargar Formato</button></a>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        <button type="button" class="btn btn-primary" onclick="importarExcel()">Guardar</button>
                    </div>
                </div>
            </div>
        </div>
        {{-- Modal Excel --}}

        {{-- Modal Creacion --}}
        <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="mdlCrear">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Nuevo Estudiante</h4>
                    </div>
                    <div class="modal-body">
                        <form action="{{ route('estudiantes.store') }}" id="frmCrearEstudiante">
                            @csrf
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="control-label">Foto</label>
                                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                                <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 200px; height: 150px;">
                                                    <i class="glyph-icon icon-remove hide" id="removeImg" onclick="quitarImagen()"></i>
                                                    <img src="" id="img-logo">
                                                </div>
                                                <div class="text-center">
                                                    <span class="btn btn-default btn-file">
                                                        <span class="fileinput-new">Seleccionar Imagen</span>
                                                        <span class="fileinput-exists">Change</span>
                                                        <input type="file" name="logo" id="logo">
                                                    </span>
                                                    <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                                                </div>
                                            </div>
                                    </div>
                                </div>
                                <div class="col-md-9">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="control-label">Identificación</label>
                                                <input type="text" name="txtIdentificacion" id="txtIdentificacion" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="control-label">Nombres</label>
                                                <input type="text" name="txtNombres" id="txtNombres" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="control-label">Apellidos</label>
                                                <input type="text" name="txtApellidos" id="txtApellidos" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="control-label">Dirección</label>
                                                <input type="text" name="txtDireccion" id="txtDireccion" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="control-label">Departamento</label>
                                                <select class="form-control" name="cbDepto" id="cbDepto" onchange="consultarCiudades(this)">
                                                    <option value="">Seleccione..</option>
                                                    @foreach ($deptos as $depto)
                                                        <option value="{{ $depto->depto_id }}">{{ $depto->depto_descripcion }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="control-label">Ciudad</label>
                                                <select class="form-control" name="cbCiudad" id="cbCiudad">
                                                    <option value="">Seleccione..</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="control-label">Teléfono</label>
                                                <input type="text" name="txtTelefono" id="txtTelefono" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="control-label">Sexo</label>
                                                <select  class="form-control" name="cbSexo" id="cbSexo">
                                                    <option value="">Seleccione..</option>
                                                    <option value="M">M</option>
                                                    <option value="F">F</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="control-label">Fecha de Nacimiento</label>
                                                <div class="input-prepend input-group">
                                                    <span class="add-on input-group-addon">
                                                        <i class="glyph-icon icon-calendar"></i>
                                                    </span>
                                                    <input type="text" class="bootstrap-datepicker form-control" name="txtFechaNac" id="txtFechaNac">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="control-label">Correo Electrónico</label>
                                                <input type="text" name="txtEmail" id="txtEmail" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="control-label">Grupo Sanguineo</label>
                                                <select  class="form-control" name="cbGrupoSanguineo" id="cbGrupoSanguineo">
                                                    <option value="">Seleccione..</option>
                                                    @foreach ($grupos as $grupo)
                                                        <option value="{{ $grupo->grupo_sanguineo_id }}">{{ $grupo->grupo_sanguineo_descripcion }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        <button type="button" class="btn btn-primary" onclick="guardarEstudiante()">Guardar</button>
                    </div>
                </div>
            </div>
        </div>
        {{-- Modal Creacion --}}

        {{-- Modal Editar --}}
        <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="mdlEditar">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Nuevo Estudiante</h4>
                    </div>
                    <div class="modal-body">
                        <form action="{{ route('estudiantes.update','') }}" id="frmEditarEstudiante">
                            @csrf
                            {{method_field('PUT')}}
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="control-label">Foto</label>
                                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                                <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 200px; height: 150px;">
                                                    <i class="glyph-icon icon-remove hide" id="removeImgEdit" onclick="quitarImagenEdit()"></i>
                                                    <img src="" id="img-logo-edit">
                                                </div>
                                                <div class="text-center">
                                                    <span class="btn btn-default btn-file">
                                                        <span class="fileinput-new">Seleccionar Imagen</span>
                                                        <span class="fileinput-exists">Change</span>
                                                        <input type="file" name="logo" id="logoEdit">
                                                        <input type="hidden" name="hdRemImg" id="hdRemImg" value="false">
                                                    </span>
                                                    <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                                                </div>
                                            </div>
                                    </div>
                                </div>
                                <div class="col-md-9">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="control-label">Identificación</label>
                                                <input type="text" name="txtIdentificacion" id="txtIdentificacionEdit" class="form-control">
                                                <input type="hidden" name="hdEstudiante" id="hdEstudianteEdit">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="control-label">Nombres</label>
                                                <input type="text" name="txtNombres" id="txtNombresEdit" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="control-label">Apellidos</label>
                                                <input type="text" name="txtApellidos" id="txtApellidosEdit" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="control-label">Dirección</label>
                                                <input type="text" name="txtDireccion" id="txtDireccionEdit" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="control-label">Departamento</label>
                                                <select class="form-control" name="cbDepto" id="cbDeptoEdit" onchange="consultarCiudades(this)">
                                                    <option value="">Seleccione..</option>
                                                    @foreach ($deptos as $depto)
                                                        <option value="{{ $depto->depto_id }}">{{ $depto->depto_descripcion }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="control-label">Ciudad</label>
                                                <select class="form-control" name="cbCiudad" id="cbCiudadEdit">
                                                    <option value="">Seleccione..</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="control-label">Teléfono</label>
                                                <input type="text" name="txtTelefono" id="txtTelefonoEdit" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="control-label">Sexo</label>
                                                <select  class="form-control" name="cbSexo" id="cbSexoEdit">
                                                    <option value="">Seleccione..</option>
                                                    <option value="M">M</option>
                                                    <option value="F">F</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="control-label">Fecha de Nacimiento</label>
                                                <div class="input-prepend input-group">
                                                    <span class="add-on input-group-addon">
                                                        <i class="glyph-icon icon-calendar"></i>
                                                    </span>
                                                    <input type="text" class="bootstrap-datepicker form-control" name="txtFechaNac" id="txtFechaNacEdit">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="control-label">Correo Electrónico</label>
                                                <input type="text" name="txtEmail" id="txtEmailEdit" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="control-label">Grupo Sanguineo</label>
                                                <select  class="form-control" name="cbGrupoSanguineo" id="cbGrupoSanguineoEdit">
                                                    <option value="">Seleccione..</option>
                                                    @foreach ($grupos as $grupo)
                                                        <option value="{{ $grupo->grupo_sanguineo_id }}">{{ $grupo->grupo_sanguineo_descripcion }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        <button type="button" class="btn btn-primary" onclick="EditarEstudiante()">Guardar</button>
                    </div>
                </div>
            </div>
        </div>
        {{-- Modal Editar --}}

        {{-- tabla --}}
        <div class="example-box-wrapper">

            <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="datatable-example">
            <thead>
            <tr>
                <th>Foto</th>
                <th>Identificación</th>
                <th>Nombre</th>
                <th>Email</th>
                <th>Telefono</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            
            </tbody>
            </table>
        </div>
        {{-- end tabla --}}

    </div>
</div>
@endsection

@section('scripts')
    <script type="text/javascript" src="{{ asset('template/widgets/datatable/datatable.js') }}"></script>
    <script type="text/javascript" src="{{ asset('template/widgets/datatable/datatable-bootstrap.js') }}"></script>
    <script type="text/javascript" src="{{ asset('template/widgets/datatable/datatable-tabletools.js') }}"></script>
    <script type="text/javascript" src="{{ asset('template/widgets/jgrowl-notifications/jgrowl.js') }}"></script>
    <script type="text/javascript" src="{{ asset('template/widgets/datepicker/datepicker.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/estudiantes.js') }}"></script>
@endsection