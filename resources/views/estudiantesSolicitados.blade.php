@extends('layouts.site')

@section('title')
    Estudiantes Solicitados
@endsection

@section('content')
<div id="page-title">
    <h2>Estudiantes Solicitados</h2>
</div>
<div class="panel">
    <div class="panel-body">
        {{-- Modal Solicitar --}}
        <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="mdlEstado">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Responder Solicitud</h4>
                    </div>
                    <div class="modal-body">
                        <input type="hidden" id="hdEstado">
                        <input type="hidden" id="hdSolicitud">
                        <div class="row">
                            <div class="col-md-6 col-md-offset-3">
                                <div class="form-group">
                                    <label>Observaciones</label>
                                    <textarea class="form-control" rows="6" id="txtObservaciones"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        <button type="button" class="btn btn-primary" id="btnResponder">Enviar</button>
                    </div>
                </div>
            </div>
        </div>
        {{-- Modal Solicitar --}}

        <form id="frmFiltros">
            <div class="row">
                <div class="col-md-3">
                    <div class="form-group">
                        <label>Area</label>
                        <select class="form-control" name="cbArea" id="cbArea">
                            <option value="">Seleccione..</option>
                            @foreach ($areas as $area)
                                <option value="{{ $area->area_id }}">{{ $area->area_descripcion }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label>Programa</label>
                        <select class="form-control" name="cbPrograma" id="cbPrograma">
                            <option value="">Seleccione..</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-3">
                    <label class="control-label">Fecha Inicio</label>
                    <div class="input-prepend input-group">
                        <span class="add-on input-group-addon">
                            <i class="glyph-icon icon-calendar"></i>
                        </span>
                        <input type="text" class="bootstrap-datepicker form-control creq" name="txtFecInicio" id="txtFecInicio" autocomplete="off">
                    </div>
                </div>
                <div class="col-md-3">
                    <label class="control-label">Fecha Fin</label>
                    <div class="input-prepend input-group">
                        <span class="add-on input-group-addon">
                            <i class="glyph-icon icon-calendar"></i>
                        </span>
                        <input type="text" class="bootstrap-datepicker2 form-control creq" name="txtFecFin" id="txtFecFin" autocomplete="off">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3">
                    <div class="form-group">
                        <label>Estados</label>
                        <select class="form-control" name="cbEstados">
                            <option value="">Seleccione..</option>
                            <option value="pendiente">Pendiente</option>
                            <option value="aceptada">Aceptada</option>
                            <option value="rechazada">Rechazada</option>
                            <option value="seleccionada">Seleccionada</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-3">
                    <button class="btn btn-alt btn-primary mrg25T" id="btnFiltro" type="submit">
                        <span>Filtrar</span>
                        <i class="glyph-icon icon-filter"></i>
                    </button>
                </div>
            </div>
        </form>

        <div class="example-box-wrapper">
            <input type="hidden" id="hdInstitucion" value="{{ Auth::user()->tercero_id }}">
            <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="datatable-example">
            <thead>
            <tr>
                <th></th>
                <th>Datos Personales</th>
                <th>Datos Institucion y Empresa</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            </tbody>
            </table>
        </div>
    </div>
</div>
@endsection

@section('scripts')
    <script type="text/javascript" src="{{ asset('template/widgets/datatable/datatable.js') }}"></script>
    <script type="text/javascript" src="{{ asset('template/widgets/accordion-ui/accordion.js') }}"></script>
    <script type="text/javascript" src="{{ asset('template/widgets/datatable/datatable-bootstrap.js') }}"></script>
    <script type="text/javascript" src="{{ asset('template/widgets/datatable/datatable-tabletools.js') }}"></script>
    <script type="text/javascript" src="{{ asset('template/widgets/jgrowl-notifications/jgrowl.js') }}"></script>
    <script type="text/javascript" src="{{ asset('template/widgets/datepicker/datepicker.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/estudiantesSolicitados.js') }}"></script>
@endsection
