@extends('layouts.site')

@section('title')
    Estudiante Seleccionado
@endsection

@section('content')

<div id="page-title" style="display: flex; margin-bottom: 10px;">
    <h2>Estudiante Seleccionado</h2>
    <a href="{{ route('estudiantes-seleccionados.index') }}" style="margin-bottom: auto;margin-top: auto;margin-left: auto;font-size: 1.2em;text-decoration: none;color: black;cursor: pointer;"> 
            <i class="glyph-icon icon-arrow-left"></i>
        Volver
    </a>
</div>

<div class="panel">
    <div class="panel-body">
        <input type="hidden" id="hdEmpresa" value="{{ Auth::user()->tercero_id }}">
        <input type="hidden" id="idEstudiante" value="{{ $idestudiante }}">
        <input type="hidden" id="idseleccionado" name="{{ $idseleccionado }}">
       <div class="row">
           <div class="col-md-2">
               <img class="img-thumbnail" id="foto" src="" width="100"><br>
           </div>
           <div class="col-md-10">
               <strong>Matricula-Codigo:</strong> 3572 <br>
               <strong>Identificación:</strong> <span id="cedula"></span><br>
               <strong>Nombre:</strong> <span id="nombres"></span><br>
               <strong>Programa:</strong><span id="programa"></span>
           </div>
       </div>
       <div class="example-box-wrapper mrg20T">
            <ul class="nav-responsive nav nav-tabs">
                <li class="active"><a href="#tab1" data-toggle="tab">Datos Personales</a></li>
                <li><a href="#tab2" data-toggle="tab">Referencias Personales</a></li>
                <li><a href="#tab3" data-toggle="tab">Modulos</a></li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="tab1">
                    <!-- <p>Datos Personales</p> -->
                    <div class="example-box-wrapper" style="display: flex;">
                        
                        <table class="table" style="width: 60% !important;">
                        <tbody>
                            <tr>
                                <td>Grupo Sanguineo:</td>
                                <td id="sangre"></td>
                            </tr>
                            <tr>
                                <td>Telefono/celular:</td>
                                <td id="telefono"></td>
                            </tr>
                            <tr>
                                <td>Dirección:</td>
                                <td id="direccion"></td>

                            </tr>
                            <tr>
                                <td>Correo:</td>
                                <td id="correo"></td>
                                
                            </tr>
                            <tr>
                                <td>Fecha de Inicio:</td>
                                <td>Ciclo B - 8 de Agosto de 2020</td>
                                
                            </tr>
                            <tr>
                                <td>Horario de clases:</td>
                                <td>10:00 Am a 12:15 Pm</td>
                                
                            </tr>
                            <tr>
                                <td>Modulo Actual:</td>
                                <td>5</td>
                                
                            </tr>
                            <tr>
                                <td>Modulos Pendientes:</td>
                                <td>2</td>
                                
                            </tr>
                            <tr>
                                <td>Hoja de Vida:</td>
                                <td> 
                                    <i class="textos-info glyph-icon icon-file-text-o" style="margin-right:2px;"></i>
                                    <span>Hoja de Vida</span>
                                </td>
                                
                            </tr>
                            <tr>
                                <td>Ciclo Aprobación:</td>
                                <td>Paz y Salvo Administrativo<br>Prueba Psicotécnicas</td>
                                
                            </tr>
                            <tr>
                                <td>Lugar:</td>
                                <td>Centro Inca - Sede Principal</td>
                                
                            </tr>
                            <tr>
                                <td>Supervisora:</td>
                                <td>Juana Acosta</td>
                                
                            </tr>

                        </tbody>
                        </table>
                        <div class="row" style="width: 40% !important;">
                            <div class="col-md-12">
                                <div class="col-6 col-md-12">
                                    <label class="text-left" style="margin-left: 10px;">Finaliza Practica en: 5 Meses</label>
                                </div>
                                
                                <div class="col-2 col-md-2">
                                    <a href="{{ route('evaluar-practicante.index') }}">
                                        <i class="glyph-icon tooltip-button demo-icon icon-bar-chart-o" data-original-title="EVALUAR PRACTICANTE"></i>
                                    </a>
                                </div>

                                 <div class="col-2 col-md-2">
                                    <!-- {{ route('asistencia-practicante.index') }} -->
                                    <a href="{{ url('/asistencia-practicante/'.$idseleccionado) }}">
                                        <i class="glyph-icon tooltip-button demo-icon icon-file-text-o" data-original-title="ASISTENCIA"></i>
                                    </a>
                                </div>

                                 <div class="col-2 col-md-2">
                                    <a href="{{ url('/retiro-practicante/'.$idseleccionado) }}">
                                        <i class="glyph-icon tooltip-button demo-icon icon-close" data-original-title="RETIRAR PRACTICANTE"></i>
                                    </a>
                                </div>

                                 <div class="col-2 col-md-2">
                                    <a href="{{ url('/observacion-practicante/'.$idseleccionado) }}">
                                        <i class="glyph-icon tooltip-button demo-icon icon-edit" data-original-title="OBSERVACIONES"></i>
                                    </a>
                                </div>

                                 <div class="col-2 col-md-2">
                                    <a href="#">
                                        <i class="glyph-icon tooltip-button demo-icon icon-calendar" data-original-title="HISTORIAL"></i>
                                    </a>
                                </div>

                                 <div class="col-2 col-md-2">
                                    <a href="#">
                                        <i class="glyph-icon tooltip-button demo-icon icon-clipboard" data-original-title="LEGALIZACIÓN"></i>
                                    </a>
                                </div>
                                 <div class="col-2 col-md-2">
                                    <a href="{{ url('/anexar-doc-practicantes/'.$idseleccionado) }}">
                                        <i class="glyph-icon tooltip-button demo-icon icon-upload" data-original-title="ANEXAR ARL"></i>
                                    </a>
                                </div>
                                
                            </div>
                        </div>
                            
                    </div><!-- cierre div example-box -->
                </div><!-- fin div datos personales -->
                <div class="tab-pane" id="tab2">
                    <div class="example-box-wrapper">
                        <table class="table">
                        <thead>
                        <tr>
                            <th>Nombres</th>
                            <th>Apellidos</th>
                            <th>Parentesco</th>
                            <th>Telefono Fijo</th>
                            <th>Telefono Movil</th>
                            <th>Actividad Actual</th>
                        </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>LISETH MARIA</td>
                                <td>JIMENEZ PEREZ</td>
                                <td>MAMÁ</td>
                                <td>3794554</td>
                                <td>3102589631 <a href="https://web.whatsapp.com/"><button class="btn btn-round btn-success"><i class="fab fa-whatsapp"></i></button></a></td>
                                <td>SECRETARIA</td>
                            </tr>
                            <tr>
                                <td>JAIRO ANDRES</td>
                                <td>PEREZ GARCIA</td>
                                <td>PAPÁ</td>
                                <td>3789562</td>
                                <td>3116547892 <a href="https://web.whatsapp.com/"><button class="btn btn-round btn-success"><i class="fab fa-whatsapp"></i></button></a></td>
                                <td>ABOGADO</td>
                            </tr>
                        </tbody>
                        </table>
                    </div>
                </div>
                <div class="tab-pane" id="tab3">
                    <div class="example-box-wrapper">
                        <table class="table">
                        <thead>
                        <tr>
                            <th>Nombre</th>
                            <th>Promedio de Notas</th>
                            <th colspan="2">Estado</th>
                            <th>Fecha Inicio</th>
                            <th>Aprobado</th>
                            <th>Reprobado</th>
                        </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>DISEÑO WEB</td>
                                <td>4.0</td>
                                <td>X</td>
                                <td></td>
                                <td>FINALIZADO</td>
                                <td><button class="btn btn-round btn-success"><i class="glyph-icon icon-check"></i></button></td>
                                <td>01/06/2020</td>
                            </tr>
                            <tr>
                                <td>DESARROLLO WEB</td>
                                <td>4.2</td>
                                <td>X</td>
                                <td></td>
                                <td>FINALIZADO</td>
                                <td><button class="btn btn-round btn-success"><i class="glyph-icon icon-check"></i></button></td>
                                <td>01/07/2020</td>
                            </tr>
                        </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
    <script type="text/javascript" src="{{ asset('js/estudiantesSeleccionadosInfo.js') }}"></script>
@endsection
