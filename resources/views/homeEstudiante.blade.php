@extends('layouts.site')

@section('title')
    Home
@endsection

@section('content')
<div id="page-title">
    <h2>Dashboard</h2>
</div>
<div class="row mailbox-wrapper">
    <div class="col-md-4">
        <div class="panel-layout">
        <div class="panel-box">

            <div class="panel-content image-box">
                {{-- <div class="ribbon">
                    <div class="bg-primary">Ribbon</div>
                </div> --}}
                <div class="image-content font-white">

                    <div class="meta-box meta-box-bottom">
                        <img src="{{ asset((Auth::user()->tercero->estudiante_foto == null) ? 'images/no-image.png' : 'images/images_estudiantes/'.Auth::user()->tercero->estudiante_foto ) }}" alt="" class="meta-image img-bordered img-circle">
                        <h3 class="meta-heading">{{ Auth::user()->user_nombre }}</h3>
                        <h4 class="meta-subheading">{{ Auth::user()->user_email }}</h4>
                    </div>

                </div>
                <img src="{{asset('images/fondo_profile.jpg')}}" alt="">

            </div>
            {{-- <div class="panel-content pad15A bg-white radius-bottom-all-4">

                <div class="clear profile-box">
                    <ul class="nav nav-pills nav-justified">
                        <li>
                            <a href="#" class="btn btn-sm bg-google">
                                <span class="glyph-icon icon-separator">
                                    <i class="glyph-icon icon-google-plus"></i>
                                </span>
                                <span class="button-content">
                                    Google+
                                </span>
                            </a>
                        </li>
                        <li>
                            <a href="#" class="btn btn-sm bg-facebook">
                                <span class="glyph-icon icon-separator">
                                    <i class="glyph-icon icon-facebook"></i>
                                </span>
                                <span class="button-content">
                                    Facebook
                                </span>
                            </a>
                        </li>
                        <li>
                            <a href="#" class="btn btn-sm bg-twitter">
                                <span class="glyph-icon icon-separator">
                                    <i class="glyph-icon icon-twitter"></i>
                                </span>
                                <span class="button-content">
                                    Twitter
                                </span>
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="mrg15T mrg15B"></div>
                <blockquote class="font-gray">
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.</p>
                    <small>
                        Programmer at
                        <cite title="Monarch">Monarch</cite>
                    </small>
                </blockquote>
            </div> --}}
        </div>
    </div>
    </div>
</div>
@endsection
