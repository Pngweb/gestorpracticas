@extends('layouts.site')

@section('title')
    Programas
@endsection

@section('content')
<div id="page-title">
    <h2>Programas</h2>
</div>
<div class="panel">
    <div class="panel-body">
        <div class="row">
            <div class="col-md-12 text-right mrg20B">
                <button class="btn btn-alt btn-hover btn-primary" data-target="#mdlImportar" onclick="abrirModal(this)">
                    <span>Excel</span>
                    <i class="glyph-icon icon-file-excel-o"></i>
                </button>
                <button class="btn btn-alt btn-hover btn-primary" data-target="#mdlCrear" onclick="abrirModal(this)">
                    <span>Nuevo</span>
                    <i class="glyph-icon icon-plus"></i>
                </button>
            </div>
        </div>

         {{-- Modal Excel --}}
        <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="mdlImportar">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Importar Programas</h4>
                    </div>
                    <div class="modal-body">
                        <form action="{{ route('importarProgramas') }}" id="frmImportarProgramas">
                            <div class="row">
                                <div class="col-md-4 col-md-offset-2">
                                            <div class="form-group">
                                                <label class="control-label">Subir Archivo</label>
                                                <input type="file" name="file" class="form-control">
                                            </div>
                                </div>
                                <div class="col-md-3">
                                    <a href="{{ asset('documentos/plantilla_programas.xlsx') }}" target="_blank"><button type="button" class="btn btn-primary mrg20T">Descargar Formato</button></a>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        <button type="button" class="btn btn-primary" onclick="importarExcel()">Guardar</button>
                    </div>
                </div>
            </div>
        </div>
        {{-- Modal Excel --}}

        {{-- Modal Creacion --}}
        <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="mdlCrear">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Nuevo Programa</h4>
                    </div>
                    <div class="modal-body">
                        <form action="{{ route('programas.store') }}" id="frmCrearPrograma">
                            @csrf
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Area</label>
                                        <select class="form-control" name="cbArea" id="cbArea">
                                            <option value="">Seleccione..</option>
                                            @foreach ($areas as $area)
                                                <option value="{{ $area->area_id }}">{{ $area->area_descripcion }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <label>Nombre</label>
                                        <input type="text" name="txtNombre" class="form-control" autocomplete="off">
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        <button type="button" class="btn btn-primary" onclick="$('#frmCrearPrograma').submit()">Guardar</button>
                    </div>
                </div>
            </div>
        </div>
        {{-- Modal Creacion --}}

        {{-- Modal editar --}}
        <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="mdlEditar">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Editar Programa</h4>
                    </div>
                    <div class="modal-body">
                        <form action="{{ route('programas.update','') }}" id="frmEditarPrograma">
                            @csrf
                            {{method_field('PUT')}}
                            <input type="hidden" name="hdProgramaId" id="hdProgramaId">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Area</label>
                                        <select class="form-control" name="cbArea" id="cbAreaEdit">
                                            <option value="">Seleccione..</option>
                                            @foreach ($areas as $area)
                                                <option value="{{ $area->area_id }}">{{ $area->area_descripcion }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Nombre</label>
                                        <input type="text" name="txtNombre" id="txtNombreEdit" class="form-control" autocomplete="off">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label class="control-label">Estado</label>
                                        <input type="checkbox" name="chkEstado" id="chkEstado" class="input-switch-alt">
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        <button type="button" class="btn btn-primary" onclick="$('#frmEditarPrograma').submit()">Guardar</button>
                    </div>
                </div>
            </div>
        </div>
        {{-- Modal editar --}}

        {{-- tabla --}}
        <div class="example-box-wrapper">
            <input type="hidden" id="hdInstitucion" value="{{ Auth::user()->tercero_id }}">
            <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="datatable-example">
            <thead>
            <tr>
                <th>Programa</th>
                <th>Area</th>
                <th>Estado</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            
            </tbody>
            </table>
        </div>
        {{-- end tabla --}}
    </div>
</div>
@endsection

@section('scripts')
    <script type="text/javascript" src="{{ asset('template/widgets/datatable/datatable.js') }}"></script>
    <script type="text/javascript" src="{{ asset('template/widgets/datatable/datatable-bootstrap.js') }}"></script>
    <script type="text/javascript" src="{{ asset('template/widgets/datatable/datatable-tabletools.js') }}"></script>
    <script type="text/javascript" src="{{ asset('template/widgets/jgrowl-notifications/jgrowl.js') }}"></script>
    <script type="text/javascript" src="{{ asset('template/widgets/datepicker/datepicker.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/programas.js') }}"></script>
@endsection
