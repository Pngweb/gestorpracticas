@extends('layouts.site')

@section('title')
    Retirar Practicante
@endsection

@section('content')
<div id="page-title"  style="display: flex; margin-bottom: 10px;">
    <h2>RETIRAR PRÁCTICANTE</h2>
     <a id="back" style="margin-bottom: auto;margin-top: auto;margin-left: auto;font-size: 1.2em;text-decoration: none;color: black;cursor: pointer;"> 
            <i class="glyph-icon icon-arrow-left"></i>
        Volver
    </a>
</div>
<div class="panel">
    <div class="panel-body">
      
        {{-- tabla --}}
        <div class="example-box-wrapper">
            <input type="hidden" id="hdEmpresa" value="{{ Auth::user()->tercero_id }}">
            <table class="table">
            <tbody>
                <tr>
                    <td>Documento</td>
                    <td id="cedula"></td>
                </tr>
                <tr>
                    <td>Estudiante</td>
                    <td id="nombres"></td>
                  
                </tr>
                <tr>
                    <td>Programa</td>
                    <td id="programa"></td>
                  
                </tr>
                <tr>
                    <td>Tipo de Práctica</td>
                    <td id="tipo"></td>
                  
                </tr>
                <tr>
                    <td>Empresa</td>
                    <td id="empresa"></td>
                </tr>
                <tr>
                    <td>Dependencia</td>
                    <td id="dependencia"></td>
                  
                </tr>
                <tr>
                    <td>Fecha de Inicio</td>
                    <td id="fechaI"></td>
                  
                </tr>
                <tr>
                    <td>Fecha de Finalización</td>
                    <td id="fechaF"></td>
                  
                </tr>
                <!--  <tr>
                    <td>Estado</td>
                    <td>Legalizado</td>
                  
                </tr> -->
            </tbody>
            </table>
        </div>
        {{-- end tabla --}}


         {{-- tabla --}}
        <div class="example-box-wrapper">

             <form action="{{ route('retiro-practicante.store') }}" id="frmCrearRetiro">
                @csrf
                <input type="hidden" name="hdTercero" id="hdTercero" value="{{ Auth::user()->tercero_id }}">
                <input type="hidden" name="seleccionado_id" id="seleccionado_id" value="{{ $idseleccionado }}">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Fecha de Retiro</label>
                            <input type="text" name="fecha" class="bootstrap-datepicker form-control" autocomplete="off">
                        </div>
                    </div>
                    
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label">Motivo</label>
                            <select class="form-control" name="motivo">
                                <option value="">Seleccione</option>
                                <option value="Falta de compromiso">Falta de compromiso</option>
                                <option value="Salir sin permiso">Salir sin permiso</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="control-label">Describa porque se retira el practicante</label>
                            <textarea class="form-control" name="observacion"></textarea>
                        </div>
                    </div>
                    
                </div>
            </form>

            <button type="button" class="btn btn-primary" onclick="$('#frmCrearRetiro').submit()">Guardar</button>
           
        </div>
        {{-- end tabla --}}
    </div>
</div>
@endsection

@section('scripts')
    <script type="text/javascript" src="{{ asset('template/widgets/datatable/datatable.js') }}"></script>
    <script type="text/javascript" src="{{ asset('template/widgets/datatable/datatable-bootstrap.js') }}"></script>
    <script type="text/javascript" src="{{ asset('template/widgets/datatable/datatable-tabletools.js') }}"></script>
    <script type="text/javascript" src="{{ asset('template/widgets/jgrowl-notifications/jgrowl.js') }}"></script>
    <script type="text/javascript" src="{{ asset('template/widgets/datepicker/datepicker.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/retirarPracticante.js') }}"></script>
@endsection
