@extends('layouts.app')

@section('title')
    Practicas Inca
@endsection

@section('content')
<div class="center-vertical container">
    <div class="center-content row">
        <div class="col-md-4 optPract">
            <a href="{{ route('loginEmpresas') }}"><h2>EMPRESAS</h2></a>
        </div>
        <div class="col-md-4 optPract">
            <a href="{{ route('loginEstudinates') }}"><h2>ESTUDIANTES</h2></a>
        </div>
        <div class="col-md-4 optPract">
            <a href="{{ route('loginCoordinacion') }}"><h2>PRACTICAS EDUCATIVAS</h2></a>
        </div>
    </div>
</div>
@endsection