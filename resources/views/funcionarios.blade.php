@extends('layouts.site')

@section('title')
    Funcionarios
@endsection

@section('content')
<div id="page-title">
    <h2>Funcionarios</h2>
</div>
<div class="panel">
    <div class="panel-body">
    	<div class="row">
            <div class="col-md-12 mrg20B text-right">
                <button class="btn btn-alt btn-hover btn-primary" data-target="#mdlCrear" onclick="abrirModal(this)">
                        <span>Nuevo</span>
                        <i class="glyph-icon icon-plus"></i>
                </button>
            </div>
        </div>
        {{-- Modal Creacion --}}
        <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="mdlCrear">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Nuevo Funcionario</h4>
                    </div>
                    <div class="modal-body">
                        <form action="{{ route('users.store') }}" id="frmCrearFuncionario">
                            @csrf
                            <input type="hidden" name="hdTercero" id="hdTercero" value="{{ Auth::user()->tercero_id }}">
                            <input type="hidden" name="hdRol" id="hdRol" value="3">
                            <div class="row">
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <label>Nombre</label>
                                        <input type="text" name="txtNombreUser" class="form-control" autocomplete="off">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label">Cargo</label>
                                        <select class="form-control" name="cbCargo">
                                            <option value="">Seleccione..</option>
                                            @foreach ($cargos as $cargo)
                                                <option value="{{ $cargo->cargo_id }}">{{ $cargo->cargo_descripcion }}</option>}
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Doc. de Identidad</label>
                                        <input type="text" name="txtIdentificacion" class="form-control" autocomplete="off">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label">Departamento de expedición</label>
                                        <select class="form-control" name="cbDepto" id="cbDepto" onchange="consultarCiudades(this)">
                                            <option value="">Seleccione..</option>
                                            @foreach ($deptos as $depto)
                                                <option value="{{ $depto->depto_id }}">{{ $depto->depto_descripcion }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label">Ciudad de Expedicion</label>
                                        <select class="form-control" name="cbCiudad" id="cbCiudad">
                                            <option value="">Seleccione..</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Telefono</label>
                                        <input type="text" name="txtTelefonoUser" class="form-control" autocomplete="off">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Correo electrónico</label>
                                        <input type="text" name="txtEmailUser" class="form-control" autocomplete="off">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label">Cumpleaños</label>
                                        <div class="input-prepend input-group">
                                            <span class="add-on input-group-addon">
                                                <i class="glyph-icon icon-calendar"></i>
                                            </span>
                                            <input type="text" class="bootstrap-datepicker form-control" name="txtFechaNac" id="txtFechaNac">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label">Contraseña</label>
                                        <input type="password" name="txtPassword" id="txtPassword" class="form-control">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label">Confirmar Contraseña</label>
                                        <input type="password" name="txtPasswordConfirm" id="txtPasswordConfirm" class="form-control">
                                    </div>
                                </div>

                                 <div class="col-md-4">
                                    <div class="form-group" style="margin-top: 25px;">
                                        <label>Notificaciones por Correo</label> 
                                        <label class="control-label">
                                        <input type="checkbox"  name="notificacion" id="notificacion">
                                            
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        <button type="button" class="btn btn-primary" onclick="$('#frmCrearFuncionario').submit()">Guardar</button>
                    </div>
                </div>
            </div>
        </div>
        {{-- Modal Creacion --}}

        {{-- Modal Editar --}}
        <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="mdlEditar">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Editar Funcionario</h4>
                    </div>
                    <div class="modal-body">
                        <form action="{{ route('users.update','') }}" id="frmEditarFuncionario">
                            @csrf
                             {{method_field('PUT')}}
                            <input type="hidden" name="hdTercero" id="hdTercero" value="{{ Auth::user()->tercero_id }}">
                            <input type="hidden" name="hdRol" id="hdRol" value="3">
                            <input type="hidden" name="hdUser" id="hdUser" value="">
                            <div class="row">
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <label>Nombre</label>
                                        <input type="text" name="txtNombreUser" id="txtNombreUserEdit" class="form-control" autocomplete="off">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label">Cargo</label>
                                        <select class="form-control" name="cbCargo" id="cbCargoEdit">
                                            <option value="">Seleccione..</option>
                                            @foreach ($cargos as $cargo)
                                                <option value="{{ $cargo->cargo_id }}">{{ $cargo->cargo_descripcion }}</option>}
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Doc. de Identidad</label>
                                        <input type="text" name="txtIdentificacion" id="txtIdentificacionEdit" class="form-control" autocomplete="off">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label">Departamento de expedición</label>
                                        <select class="form-control" name="cbDepto" id="cbDeptoEdit" onchange="consultarCiudades(this)">
                                            <option value="">Seleccione..</option>
                                            @foreach ($deptos as $depto)
                                                <option value="{{ $depto->depto_id }}">{{ $depto->depto_descripcion }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label">Ciudad de Expedicion</label>
                                        <select class="form-control" name="cbCiudad" id="cbCiudadEdit">
                                            <option value="">Seleccione..</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Telefono</label>
                                        <input type="text" name="txtTelefonoUser" id="txtTelefonoUserEdit" class="form-control" autocomplete="off">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Correo electrónico</label>
                                        <input type="text" name="txtEmailUser" id="txtEmailUserEdit" class="form-control" autocomplete="off">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label">Cumpleaños</label>
                                        <div class="input-prepend input-group">
                                            <span class="add-on input-group-addon">
                                                <i class="glyph-icon icon-calendar"></i>
                                            </span>
                                            <input type="text" class="bootstrap-datepicker form-control" name="txtFechaNac" id="txtFechaNacEdit">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label">Contraseña</label>
                                        <input type="password" name="txtPassword" id="txtPasswordEdit" class="form-control">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label">Confirmar Contraseña</label>
                                        <input type="password" name="txtPasswordConfirm" id="txtPasswordConfirmEdit" class="form-control">
                                    </div>
                                </div>
                                 <div class="col-md-4">
                                    <div class="form-group" style="margin-top: 25px;">
                                        <label>Notificaciones por Correo</label> 
                                        <label class="control-label">
                                        <input type="checkbox"  name="notificacionEdit" id="notificacionEdit">
                                            
                                        </label>
                                    </div>
                                </div>

                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        <button type="button" class="btn btn-primary" onclick="$('#frmEditarFuncionario').submit()">Guardar</button>
                    </div>
                </div>
            </div>
        </div>
        {{-- Modal Editar --}}

        {{-- tabla --}}
        <div class="example-box-wrapper">

            <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="datatable-example">
            <thead>
            <tr>
                <th>Nombre</th>
                <th>Doc. Identidad</th>
                <th>Expedido en</th>
                <th>Telefono</th>
                <th>Correo Electronico</th>
                <th>Cargo</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            
            </tbody>
            </table>
        </div>
        {{-- end tabla --}}
    </div>
</div>
@endsection

@section('scripts')
    <script type="text/javascript" src="{{ asset('template/widgets/datatable/datatable.js') }}"></script>
    <script type="text/javascript" src="{{ asset('template/widgets/datatable/datatable-bootstrap.js') }}"></script>
    <script type="text/javascript" src="{{ asset('template/widgets/datatable/datatable-tabletools.js') }}"></script>
    <script type="text/javascript" src="{{ asset('template/widgets/jgrowl-notifications/jgrowl.js') }}"></script>
    <script type="text/javascript" src="{{ asset('template/widgets/datepicker/datepicker.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/funcionarios.js') }}"></script>
@endsection
