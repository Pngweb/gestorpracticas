@extends('layouts.site')

@section('title')
    Convenios
@endsection

@section('content')
<div id="page-title">
    <h2>Convenios</h2>
</div>
<div class="panel">
    <div class="panel-body">
        <ul class="nav-responsive nav nav-tabs">
            <li class="active"><a href="#tabConvenios" data-toggle="tab">Convenios</a></li>
            <li><a href="#tabEmpDisponibles" data-toggle="tab">Instituciones Disponibles</a></li>
        </ul>
        <input type="hidden" id="hdEmpresa" value="{{ Auth::User()->tercero_id }}">
        <div class="tab-content">
            <div class="tab-pane active" id="tabConvenios">
                 {{-- tabla convenios--}}
                 <div class="example-box-wrapper">

                    <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="tbConvenios">
                    <thead>
                    <tr>
                        <th>Logo</th>
                        <th>Nombre</th>
                        <th>Ciudad</th>
                        <th>Correo Electrónico</th>
                        <th>Documento</th>
                        <th>Estado</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    </tbody>
                    </table>
                </div>
                {{-- end tabla convenios --}}
            </div>
            <div class="tab-pane" id="tabEmpDisponibles">
                {{-- tabla convenios--}}
                 <div class="example-box-wrapper">

                    <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="tbDisponibles">
                    <thead>
                    <tr>
                        <th>Logo</th>
                        <th>Nombre</th>
                        <th>Ciudad</th>
                        <th>Correo Electrónico</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    </tbody>
                    </table>
                </div>
                {{-- end tabla convenios --}}

            </div>
        </div>
        {{-- Modal Documento --}}
            <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="mdlDocumento">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title">Selecionar Documento</h4>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <form id="frmConvenioDoc" action="{{ route('documento-convenio','') }}">
                                    @csrf
                                    <input type="hidden" id="hdConvenio">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">Documento</label>
                                            <select name="cbDocumento" id="cbDocumento" class="form-control" required>
                                                <option value="">Seleccione..</option>
                                                @foreach ($documentos as $doc)
                                                    <option value="{{ $doc->documento_id }}">{{ $doc->documento_descripcion }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Archivo</label>
                                            <select name="cbArchivo" id="cbArchivo" class="form-control" required>
                                                <option value="">Seleccione..</option>
                                            </select>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                            <button type="button" class="btn btn-primary" onclick="$('#frmConvenioDoc').submit();">Enviar</button>
                        </div>
                    </div>
                </div>
            </div>
            {{-- Modal Documento --}}
    </div>
</div>
@endsection

@section('scripts')
    <script type="text/javascript" src="{{ asset('template/widgets/datatable/datatable.js') }}"></script>
    <script type="text/javascript" src="{{ asset('template/widgets/datatable/datatable-bootstrap.js') }}"></script>
    <script type="text/javascript" src="{{ asset('template/widgets/datatable/datatable-tabletools.js') }}"></script>
    <script type="text/javascript" src="{{ asset('template/widgets/jgrowl-notifications/jgrowl.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/conveniosEmpresas.js') }}"></script>
@endsection