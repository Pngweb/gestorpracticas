<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Solicitud de Convenio</title>
</head>
<body>
    <h2>Solicitud de Convenio</h2>
    <p>La Institucion {{ $institucion->institucion_nombre }} ha solicitado realizar un covenio con ustedes.</p>
    <p>Para responder a la solicitud, ingrese <a href="{{ route('convenios.index') }}">Aqui</a></p>
</body>
</html>