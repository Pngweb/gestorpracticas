<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Solicitud de Practicas</title>
</head>
<body>
    <h2>Solicitud de Practicas</h2>
    <p>Hola <strong>{{ $estudiante->estudiante_nombres }} {{ $estudiante->estudiante_apellidos }}</strong></p>
    <p>La empresa <strong>{{ $empresa }}</strong>, le ha realizado una solicitud para realizar su proceso de prácticas.</p>
</body>
</html>