<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Recuperar Contraseña</title>
</head>
<body>
    <h2>Recuperar Contraseña</h2>
    <p>Para reestablecer su contraseña ingrese <a href="{{ route('nueva-contrasena',$id_user) }}">Aquí</a>.</p>
</body>
</html>